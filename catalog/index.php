<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
?>
<div class="catalog-b-list">
	<?$APPLICATION->IncludeComponent(
		"it-invest:catalog.custom", 
		"main_siberia", 
		array(
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => IBLOCK_CATALOG_ID,
			"TEMPLATE_THEME" => "",
			"ACTION_VARIABLE" => "action",
			"PRODUCT_ID_VARIABLE" => "id",
			"SECTION_ID_VARIABLE" => "SECTION_ID",
			"USE_PRODUCT_QUANTITY" => "Y",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"SEF_MODE" => "Y",
			"SEF_FOLDER" => "/catalog/",
			"AJAX_MODE" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "Y",
			"CACHE_GROUPS" => "Y",
			"ADD_SECTION_CHAIN" => "Y",
			"ADD_ELEMENT_CHAIN" => "Y",
			"SET_STATUS_404" => "Y",
			"USE_ELEMENT_COUNTER" => "Y",
			"PRICE_CODE" => array(
				0 => "BASE",
			),
			"SHOW_PRICES" => INCLUDE_PRICES,
			"PAGE_ELEMENT_COUNT" => "8",
			"LINE_ELEMENT_COUNT" => "8",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Products",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
			"PAGER_SHOW_ALL" => "N",
			"SHOW_DISCOUNT_PERCENT" => "Y",
			"SHOW_OLD_PRICE" => "Y",
			"SEF_URL_TEMPLATES" => array(
				"sections" => "/catalog/",
				"section" => "#SECTION_CODE#/",
				"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
				"compare" => "compare/",
				"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
			),
			"CONVERT_CURRENCY" => "Y",
			'CURRENCY_ID' => CURRENCY_ID,
			"ADD_SECTIONS_CHAIN" => 'Y'
		),
		false
	);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>