<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Контакты");
$APPLICATION->SetTitle("Контакты");
?>
<div class="static page" id="contacts">
	<h1>НАША КОМАНДА</h1>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Михаил Казачков</p>
					<p class="contact_post">Генеральный Директор</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/1.png"/></div><!--
					--><div class="info">
						<div class="number data">+7 3842 650 230</div>
						<div class="mail data">info@siberia-products.com</div>
						<div class="www data">siberia-products.com</div>
						<div class="address data">г.Кемерово, 650025 ул.Рукавишникова 12, оф.311</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Наталья Колесникова</p>
					<p class="contact_post">Коммерческий директор</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/3.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">kni@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">г.Кемерово, 650025 ул.Рукавишникова 12, оф.311</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Михаил Гостевских</p>
					<p class="contact_post">Исполнительный директор</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/8.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">marketing@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">г.Кемерово, 650025 ул.Рукавишникова 12, оф.311</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Вадим Савельев</p>
					<p class="contact_post">Руководитель проектов</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/5.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">project@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">г.Кемерово, 650025 ул.Рукавишникова 12, оф.311</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Александр Медведев</p>
					<p class="contact_post">Программист</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/6.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">info@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">г.Кемерово, 650025 ул.Рукавишникова 12, оф.311</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Марина Махова</p>
					<p class="contact_post">Менеджер по работе с клиентами</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/7.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">mmn@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">г.Кемерово, 650025 ул.Рукавишникова 12, оф.311</p>
					</div>
				</div>
			</div>
		</div>
	</div>
 
	<div class="map"><?$APPLICATION->IncludeComponent(
	"it-invest:map.google.view", 
	"siberia", 
	array(
		"API_KEY" => "AIzaSyB1PHcTxc2rTrJbRQd8q5z53XUVPJayvKY",
		"COMPONENT_TEMPLATE" => "siberia",
		"CONTROLS" => array(
			0 => "SMALL_ZOOM_CONTROL",
			1 => "TYPECONTROL",
		),
		"INIT_MAP_TYPE" => "ROADMAP",
		"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.349715191453;s:10:\"google_lon\";d:86.067663917247;s:12:\"google_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:99:\"РОССИЯ###RN###г.Кемерово, 650025###RN###ул.Рукавишникова 12, оф.311\";s:3:\"LON\";d:86.066800737754;s:3:\"LAT\";d:55.349411674048;}}}",
		"MAP_HEIGHT" => "460px",
		"MAP_ID" => "yam_1",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(
		)
	),
	false
);?></div>
</div>
<style>
	body{
		background:#fff;
	}
	section.main{
		background:#fff;
	}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>