<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Contacts");
$APPLICATION->SetTitle("Contacts");
?>
<div class="static page" id="contacts">
	<h1>TRADE ASSOCIATION SIBERIA PRODUCTS TM</h1>
	<div class="contact">
		<div class="top_data">
			<p class="contact_name">Mikhail Kazachkov</p>
			<p class="contact_post">General Director</p>
		</div>
		<div class="body_data">
			<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/1.jpg"/></div><!--
			--><div class="info">
				<div class="number data">+7 3842 650 230</div>
				<div class="mail data">info@siberia-products.com</div>
				<div class="www data">siberia-products.com</div>
				<div class="address data">650025, 克麦罗沃市，卢卡卫士尼括夫大街12号，311号办公室</div>
			</div>
		</div>
	</div>
	<div class="contact">
		<div class="top_data">
			<p class="contact_name">Natalia Kolesnikova</p>
			<p class="contact_post">Commercial Director</p>
		</div>
		<div class="body_data">
			<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/3.jpg"/></div><!--
			--><div class="info">
				<p class="number data">+7 3842 650 230</p>
				<p class="mail data">kni@siberia-products.com</p>
				<p class="www data">siberia-products.com</p>
				<p class="address data">650025, 克麦罗沃市，卢卡卫士尼括夫大街12号，311号办公室</p>
			</div>
		</div>
	</div>
	<div class="contact">
		<div class="top_data">
			<p class="contact_name">Vadim Saveliev</p>
			<p class="contact_post">Project manager</p>
		</div>
		<div class="body_data">
			<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/5.jpg"/></div><!--
			--><div class="info">
				<p class="number data">+7 3842 650 230</p>
				<p class="mail data">info@siberia-products.com</p>
				<p class="www data">siberia-products.com</p>
				<p class="address data">650025, 克麦罗沃市，卢卡卫士尼括夫大街12号，311号办公室</p>
			</div>
		</div>
	</div>
	<div class="contact">
		<div class="top_data">
			<p class="contact_name">Alexander Medvedev</p>
			<p class="contact_post">Programmer</p>
		</div>
		<div class="body_data">
			<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/6.jpg"/></div><!--
			--><div class="info">
				<p class="number data">+7 3842 650 230</p>
				<p class="mail data">info@siberia-products.com</p>
				<p class="www data">siberia-products.com</p>
				<p class="address data">650025, 克麦罗沃市，卢卡卫士尼括夫大街12号，311号办公室</p>
			</div>
		</div>
	</div>
	<div class="contact">
		<div class="top_data">
			<p class="contact_name">Marina Makhova</p>
			<p class="contact_post">Interpreter</p>
		</div>
		<div class="body_data">
			<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/7.jpg"/></div><!--
			--><div class="info">
				<p class="number data">+7 3842 650 230</p>
				<p class="mail data">info@siberia-products.com</p>
				<p class="www data">siberia-products.com</p>
				<p class="address data">650025, 克麦罗沃市，卢卡卫士尼括夫大街12号，311号办公室</p>
			</div>
		</div>
	</div>

	<div class="map"><?$APPLICATION->IncludeComponent(
		"it-invest:map.google.view", 
		"siberia", 
		array(
			"API_KEY" => "AIzaSyB1PHcTxc2rTrJbRQd8q5z53XUVPJayvKY",
			"COMPONENT_TEMPLATE" => ".default",
			"CONTROLS" => array(
				0 => "TYPECONTROL",
				1 => "SCALELINE",
				1 => "SMALL_ZOOM_CONTROL"
			),
			"INIT_MAP_TYPE" => "ROADMAP",
			"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.349715191452944;s:10:\"google_lon\";d:86.0676639172469;s:12:\"google_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:75:\"RUSSIA###RN###Rukavishnikova street 12, of.302###RN###Kemerovo city, 650025\";s:3:\"LON\";d:86.066800737754;s:3:\"LAT\";d:55.349411674048;}}}",
			"MAP_HEIGHT" => "900",
			"MAP_ID" => "yam_1",
			"MAP_WIDTH" => "100%",
			"MAP_HEIGHT" => "460px",
			"OPTIONS" => array(
				1 => "ENABLE_DBLCLICK_ZOOM",
				2 => "ENABLE_DRAGGING",
			)
		),
		false
	);?></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>