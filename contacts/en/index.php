<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Contacts");
$APPLICATION->SetTitle("Contacts");
?>
<div class="static page" id="contacts">
	<h1>OUR TEAM</h1>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Mikhail Kazachkov</p>
					<p class="contact_post">General Director</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/1.png"/></div><!--
					--><div class="info">
						<div class="number data">+7 3842 650 230</div>
						<div class="mail data">info@siberia-products.com</div>
						<div class="www data">siberia-products.com</div>
						<div class="address data">Rukavishnikova street 12, of.311 Kemerovo city, 650025</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Natalia Kolesnikova</p>
					<p class="contact_post">Commercial Director</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/3.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">kni@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">Rukavishnikova street 12, of.311 Kemerovo city, 650025</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Mikhail Gostevsky</p>
					<p class="contact_post">Executive Director</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/8.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">marketing@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">Rukavishnikova street 12, of.311 Kemerovo city, 650025</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Vadim Saveliev</p>
					<p class="contact_post">Project manager</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/5.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">project@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">Rukavishnikova street 12, of.311 Kemerovo city, 650025</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Alexander Medvedev</p>
					<p class="contact_post">Programmer</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/6.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">info@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">Rukavishnikova street 12, of.311 Kemerovo city, 650025</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="contact">
				<div class="top_data">
					<p class="contact_name">Marina Makhova</p>
					<p class="contact_post">Interpreter</p>
				</div>
				<div class="body_data">
					<div class="photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/contacts/7.png"/></div><!--
					--><div class="info">
						<p class="number data">+7 3842 650 230</p>
						<p class="mail data">mmn@siberia-products.com</p>
						<p class="www data">siberia-products.com</p>
						<p class="address data">Rukavishnikova street 12, of.311 Kemerovo city, 650025</p>
					</div>
				</div>
			</div>
		</div>
	</div>
 
	<div class="map"><?$APPLICATION->IncludeComponent(
		"it-invest:map.google.view", 
		"siberia", 
		array(
			"API_KEY" => "AIzaSyB1PHcTxc2rTrJbRQd8q5z53XUVPJayvKY",
			"COMPONENT_TEMPLATE" => ".default",
			"CONTROLS" => array(
				0 => "TYPECONTROL",
				1 => "SCALELINE",
				1 => "SMALL_ZOOM_CONTROL"
			),
			"INIT_MAP_TYPE" => "ROADMAP",
			"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.349715191452944;s:10:\"google_lon\";d:86.0676639172469;s:12:\"google_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:75:\"RUSSIA###RN###Rukavishnikova street 12, of.302###RN###Kemerovo city, 650025\";s:3:\"LON\";d:86.066800737754;s:3:\"LAT\";d:55.349411674048;}}}",
			"MAP_HEIGHT" => "900",
			"MAP_ID" => "yam_1",
			"MAP_WIDTH" => "100%",
			"MAP_HEIGHT" => "460px",
			"OPTIONS" => array(
				1 => "ENABLE_DBLCLICK_ZOOM",
				2 => "ENABLE_DRAGGING",
			)
		),
		false
	);?></div>
</div>
<style>
	body{
		background:#fff;
	}
	section.main{
		background:#fff;
	}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>