<?
//define("NEED_AUTH", true);

require($_SERVER["DOCUMENT_ROOT"]."/eshop_app/headers.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if (!$USER->IsAuthorized()){
	LocalRedirect('/eshop_app/auth');
}
?>
<script>
	//Показываем навигационную панель
	BXMobileApp.UI.Page.TopBar.show(); 
	//Разблокируем открытие левой части слайдера
	BXMobileApp.UI.Slider.setStateEnabled(BXMobileApp.UI.Slider.state.LEFT, true);
	
	//возьмем токен устройства
	BX.ready(function(){
		DEV.getToken();
	});
	DEV = {
		getToken : function ()
		{
			var _this = this,
				dt = "APPLE";

			if (platform != "ios")
				dt = "GOOGLE";

			var params = {
				callback: function (token)
				{
					var postData = {
						action: "save_device_token",
						device_name: device.name,
						uuid: device.uuid,
						device_token: token,
						device_type: dt,
						sessid: BX.bitrix_sessid()
					};

					/*BX.ajax({
						timeout:   30,
						method:   'POST',
						dataType: 'json',
						url:       '/eshop_app/myurl.php',
						data:      postData,
					});*/
				}
			};

			return app.exec("getToken", params);
		}
	};
</script>
<?
$APPLICATION->SetPageProperty("BodyClass", "main");
?>
<div class="maincontent_component">
	<!--<div class="main_button_component">
		<a href="javascript:void(0)" class="item_title" id="saleleader_block_title" class = "current" onclick="BX('saleleaders_block').style.display='block'; BX('newproduct_block').style.display='none';BX('item_listcategory_block').style.display='none'; BX.removeClass(BX('newproduct_block_title'), 'current'); BX.removeClass(BX('item_listcategory_block_title'), 'current'); BX.addClass(BX(this), 'current');"><?=GetMessage("TOP_SELLER")?></a><!--
		--><!--<a href="javascript:void(0)" class="item_title" id="newproduct_block_title" onclick="BX('saleleaders_block').style.display='none'; BX('item_listcategory_block').style.display='none'; BX('newproduct_block').style.display='block';  BX.removeClass(BX('saleleader_block_title'), 'current'); BX.removeClass(BX('item_listcategory_block_title'), 'current'); BX.addClass(BX(this), 'current');"><?=GetMessage("NEW_SELLER")?></a><!--
		--><!--<a href="javascript:void(0)" class="item_title" id="item_listcategory_block_title" onclick="BX('saleleaders_block').style.display='none'; BX('newproduct_block').style.display='none'; BX('item_listcategory_block').style.display='block';  BX.removeClass(BX('newproduct_block_title'), 'current'); BX.removeClass(BX('saleleader_block_title'), 'current'); BX.addClass(BX(this), 'current');"><?=GetMessage("CATALOG_SELLER")?></a><!--
		--><!--<div class="clb"></div>
	</div>-->
	<div class="bx_component" id="saleleaders_block">
	<?$APPLICATION->IncludeComponent(
		"app:eshopapp.top", 
		"siberia", 
		array(
			"IBLOCK_TYPE_ID" => "catalog",
			"IBLOCK_ID" => IBLOCK_CATALOG_ID,
			"ELEMENT_SORT_FIELD" => "sort",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_COUNT" => "100",
			"FLAG_PROPERTY_CODE" => "SPECIALOFFER",
			"OFFERS_LIMIT" => "100",
			"ACTION_VARIABLE" => "action",
			"PRODUCT_ID_VARIABLE" => "id_top1",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"PRODUCT_PROPS_VARIABLE" => "prop1",
			"CATALOG_FOLDER" => SITE_DIR."catalog/",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "86400",
			"CACHE_GROUPS" => "Y",
			"DISPLAY_COMPARE" => "N",
			"PRICE_CODE" => array(
				0 => "BASE",
			),
			"SHOW_PRICES" => INCLUDE_PRICES,
			"USE_PRICE_COUNT" => "N",
			"SHOW_PRICE_COUNT" => "1",
			"PRICE_VAT_INCLUDE" => "Y",
			"PRODUCT_PROPERTIES" => array(
			),
			"CONVERT_CURRENCY" => "Y",
			"DISPLAY_IMG_WIDTH" => "147",
			"DISPLAY_IMG_HEIGHT" => "147",
			"BASKET_URL" => SITE_DIR."personal/cart/",
			"SHARPEN" => "30",
			"COMPONENT_TEMPLATE" => "mobile",
			"OFFERS_FIELD_CODE" => array(
				0 => "",
				1 => "",
			),
			"OFFERS_PROPERTY_CODE" => array(
				0 => "",
				1 => "",
			),
			"OFFERS_SORT_FIELD" => "sort",
			"OFFERS_SORT_ORDER" => "asc",
			"CURRENCY_ID" => CURRENCY_ID,
			"OFFERS_CART_PROPERTIES" => "",
			"VARIABLE_ALIASES" => array(
				"SECTION_ID" => "SECTION_ID",
				"ELEMENT_ID" => "ELEMENT_ID",
			)
		),
		false
	);?>
	</div>

	<div class="bx_component" id="newproduct_block" style="display: none;">
	<?/*$APPLICATION->IncludeComponent(
	"it-invest:eshopapp.top", 
	"mobile", 
	array(
		"IBLOCK_TYPE_ID" => "catalog",
		"IBLOCK_ID" => IBLOCK_CATALOG_ID,
		"ELEMENT_SORT_FIELD" => "RAND",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_COUNT" => "4",
		"FLAG_PROPERTY_CODE" => "NEWPRODUCT",
		"OFFERS_LIMIT" => "5",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id_top2",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"CATALOG_FOLDER" => SITE_DIR."eshop_app/catalog/",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "86400",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_COMPARE" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => array(
		),
		"CONVERT_CURRENCY" => "Y",
		"DISPLAY_IMG_WIDTH" => "147",
		"DISPLAY_IMG_HEIGHT" => "147",
		"BASKET_URL" => SITE_DIR."eshop_app/personal/cart/",
		"SHARPEN" => "30",
		"COMPONENT_TEMPLATE" => "mobile",
		"CURRENCY_ID" => CURRENCY_ID,
		"VARIABLE_ALIASES" => array(
			"SECTION_ID" => "SECTION_ID",
			"ELEMENT_ID" => "ELEMENT_ID",
		)
	),
	false
);*/?>
	</div>
	<div class="bx_component" id="item_listcategory_block" style="display:none">
	<?$APPLICATION->IncludeComponent(
		"it-invest:catalog.section.list", 
		"mobile", 
		array(
			"VIEW_MODE" => "TEXT",
			"SHOW_PARENT_NAME" => "Y",
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => IBLOCK_CATALOG_ID,
			"SECTION_ID" => (int)$_REQUEST["SECTION_ID"],
			"SECTION_CODE" => "",
			"SECTION_URL" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "2",
			"SECTION_FIELDS" => array(
				0 => "",
				1 => "",
			),
			"SECTION_USER_FIELDS" => array(
				0 => "",
				1 => "",
			),
			"ADD_SECTIONS_CHAIN" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "86400",
			"CACHE_NOTES" => "",
			"CACHE_GROUPS" => "Y",
			"COMPONENT_TEMPLATE" => "mobile"
		),
		false
	);?>
	</div>
</div>
<?
if($context->getRequest()->get('active_component')){?>
	<script>
		$('.bx_component').css('display','none'); 
		$('#<?=$context->getRequest()->get('active_component')?>').css('display','block');  
		
		$('.item_title').removeClass('current');
		$('#<?=$context->getRequest()->get('active_component')?>_title').addClass('current');
	</script>
<?}?>
<script type="text/javascript">
	app.setPageTitle({"title" : "<?=htmlspecialcharsbx(COption::GetOptionString("main", "site_name", ""))?>"});
	//обновим экран слайдера
	app.onCustomEvent('initSlider', {});
	function openSectionList()
	{
		app.openBXTable({
			url: '<?=SITE_DIR?>catalog/sections.php',
			TABLE_SETTINGS : {
				cache : true,
				use_sections : true,
				searchField : false,
				showtitle : true,
				name : "Catalog",
				button:
				{
					type:    'basket',
					style:   'custom',
					callback: function()
					{
						app.openNewPage("<?=SITE_DIR?>personal/cart/");
					}
				}
			}
		});
	}
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>