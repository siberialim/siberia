<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arMobileMenuItems = array(
	array(
		"type" => "section",
		"text" =>"Меню",
		"sort" => "100",
		"items" =>	array(
			array(
				"text" => "Главная",
				"data-url" => SITE_DIR,
				"class" => "menu-item",
				"data-pageid" => "main",
				"id" => "main"
			),
			array(
				"text" => "Каталог",
				"class" => "menu-item",
				"onclick" => "activeComponent('item_listcategory_block');",
				"data-pageid" => "catalog",
				"id" => "catalog"
			),
			array(
				"text" => "Корзина",
				"class" => "menu-item",
				"data-url" => SITE_DIR."personal/cart/",
				"id" => "cart",
				"data-pageid" => "cart",
			),
			/*array(
				"text" => "Заказы",
				"data-url" => SITE_DIR."personal/order/order_list.php",
				"class" => "menu-item",
				"id" => "orders",
			),*/
			array(
				"text" => "Настройки",
				"class" => "menu-item",
				"data-url" => SITE_DIR."personal/profile",
				"id" => "settings",
				"data-pageid" => "settings",
			),
		)
	)
);
	
?>
<script>
	/*принимает id элемента dom, 
	далее перенаправляет по ссылке 
	на которой скрипт показывает нужный нам компонент*/
	function activeComponent(id)
	{
		/*app.closeMenu();
		app.openBXTable({
			url: '<?=SITE_DIR?>eshop_app/',
			isroot: true,
			TABLE_SETTINGS : {
				cache : true,
				use_sections : true,
				searchField : false,
				showtitle : true,
				name : "Каталог",
				button:
				{
					type:    'basket',
					style:   'custom',
					callback: function()
					{
						app.openNewPage("<?=SITE_DIR?>eshop_app/personal/cart/");
					}
				}
			}
		});*/
		
		
		var res = BXMobileApp.PageManager.loadPageStart({
			url: "<?=SITE_DIR?>?active_component="+id,
			title: "",
		});		
		
	}
	
</script>