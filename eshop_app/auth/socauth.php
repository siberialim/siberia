<?
//define("NEED_AUTH", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$client_id = '650433851813619'; // Client ID
$client_secret = 'b1b32211317477e2bf95360b089117ca'; // Client secret
$redirect_uri = 'https://siberia-products.com/eshop_app/auth/socauth.php'; // Redirect URIs

$url = 'https://www.facebook.com/dialog/oauth';

if($_GET['code']){
	$result = false;
	
    $params = array(
        'client_id'     => $client_id,
        'redirect_uri'  => $redirect_uri,
        'client_secret' => $client_secret,
        'code'          => $_GET['code']
    );
	
	$url = 'https://graph.facebook.com/oauth/access_token';
	
    $tokenInfo = null;
    $tokenInfo = json_decode(file_get_contents($url . '?' . urldecode(http_build_query($params))), true);

	if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
        $params = array('access_token' => $tokenInfo['access_token'], 'fields' => 'id,first_name,last_name,email,gender');

        $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params))), true);

        if (isset($userInfo['id'])) {
            $userInfo = $userInfo;
            $result = true;
        }
    }

    if ($result) {
		global $USER;
		$rsUser = $USER->GetByLogin("FB_".$userInfo['id']);
		if($arUser = $rsUser->Fetch())
		{
			$USER->Authorize($arUser['ID']);
		} else {
			$arFields = Array(
				"NAME"              => $userInfo['first_name'],
				"LAST_NAME"         => $userInfo['last_name'],
				"EMAIL"             => $userInfo['email'],
				"LOGIN"             => "FB_".$userInfo['id'],
				"XML_ID"			=> $userInfo['id'],
				"LID"               => SITE_ID,
				"ACTIVE"            => "Y",
				"GROUP_ID"          => array(10,11),
				"EXTERNAL_AUTH_ID"	=> "Facebook",
				"PERSONAL_GENDER"	=> ($userInfo['gender'] == 'male') ? 'M' : 'F'
				//"PERSONAL_PHOTO"    => $arIMAGE
			);

			$ID = $USER->Add($arFields);
			$USER->Authorize($ID);
			//ShowMessage($arResult); // выводим результат в виде сообщения
		}		
    }

	LocalRedirect('/eshop_app/auth/index.php?auth=success');
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>