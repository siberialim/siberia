<?
require($_SERVER["DOCUMENT_ROOT"]."/eshop_app/headers.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetPageProperty("BodyClass", "detail");
$arParams = array(
	"ORDER_DETAIL_PATH" => SITE_DIR.'personal/order/order_detail.php'
	);
if ($USER->IsAuthorized())
	$arParams["FILTER"] = array("USER_ID" => $USER->GetID());
	
	$APPLICATION->IncludeComponent(
		'app:sale.mobile.orders.list',
		'siberia',
		$arParams,
		false
	);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>