<?
require($_SERVER["DOCUMENT_ROOT"]."/eshop_app/headers.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->IncludeComponent(
	"app:sale.mobile.order.detail2", 
	"siberia", 
	array(
		"PATH_TO_LIST" => "order_list.php",
		"PATH_TO_CANCEL" => "order_cancel.php",
		"PATH_TO_PAYMENT" => "payment.php",
		"PATH_TO_COPY" => "",
		"ID" => (int)$_REQUEST["id"],
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"PICTURE_WIDTH" => "110",
		"PICTURE_HEIGHT" => "110",
		"PICTURE_RESAMPLE_TYPE" => "1",
		"CUSTOM_SELECT_PROPS" => array(
		),
		"PROP_1" => array(
		),
		"PROP_2" => array(
		),
		"COMPONENT_TEMPLATE" => "siberia"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>