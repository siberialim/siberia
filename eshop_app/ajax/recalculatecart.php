<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
if (!CModule::IncludeModule("sale")) die;
$itemsInBasket = CSaleBasket::GetList(
array(
		"NAME" => "ASC",
		"ID" => "ASC"
	),
array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"ORDER_ID" => "NULL"
	),
false,
false,
array("ID")
);
$countItemsInBasket = 0;
while($arItemInBasket = $itemsInBasket->Fetch()){
	$countItemsInBasket+=1;
}
echo $countItemsInBasket;
exit;
?>