<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arMobileMenuItems = array(
	array(
		"type" => "section",
		"text" =>"제품 카탈로그",
		"sort" => "100",
		"items" =>	array(
			array(
				"text" => "메인 페이즈",
				"data-url" => SITE_DIR,
				"class" => "menu-item",
				"data-pageid" => "main",
				"id" => "main"
			),
			array(
				"text" => "카탈로그",
				"class" => "menu-item",
				"onclick" => "activeComponent('item_listcategory_block');",
				"data-pageid" => "catalog",
				"id" => "catalog"
			),
			array(
				"text" => "바구니",
				"class" => "menu-item",
				"data-url" => SITE_DIR."personal/cart/",
				"id" => "cart",
				"data-pageid" => "cart",
			),
		)
	)
);

	$arMobileMenuItems[] = array(
		"type" => "section",
		"text" =>"내 주문",
		"sort" => "300",
		"items" =>	array(
			array(
				"text" => "주문",
				"data-url" => SITE_DIR."personal/order/order_list.php",
				"class" => "menu-item",
				"id" => "orders",
			)
		)
	);
	
?>
<script>
	/*принимает id элемента dom, 
	далее перенаправляет по ссылке 
	на которой скрипт показывает нужный нам компонент*/
	function activeComponent(id)
	{
		/*app.closeMenu();
		app.openBXTable({
			url: '<?=SITE_DIR?>eshop_app/',
			isroot: true,
			TABLE_SETTINGS : {
				cache : true,
				use_sections : true,
				searchField : false,
				showtitle : true,
				name : "Каталог",
				button:
				{
					type:    'basket',
					style:   'custom',
					callback: function()
					{
						app.openNewPage("<?=SITE_DIR?>eshop_app/personal/cart/");
					}
				}
			}
		});*/
		
		
		var res = BXMobileApp.PageManager.loadPageStart({
			url: "<?=SITE_DIR?>?active_component="+id,
			title: "",
		});		
		
	}
</script>