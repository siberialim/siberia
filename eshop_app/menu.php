<?
require($_SERVER["DOCUMENT_ROOT"]."/eshop_app/headers.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CModule::IncludeModule('pull');
CJSCore::Init(array('pull'));
$APPLICATION->AddHeadString('
	<script type="text/javascript">
		app.enableSliderMenu(true);
	</script>
');?>
<?
CMobile::getInstance()->setLargeScreenSupport(false);
CMobile::getInstance()->setScreenCategory("NORMAL");

$arParams = array(
	"MENU_FILE_PATH" => SITE_DIR.".mobile_menu".SLANGCODE.".php",
);

$APPLICATION->IncludeComponent(
	'app:mobileapp.menu',
	'siberia',
	$arParams,
	false,
	Array('HIDE_ICONS' => 'Y'));
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>