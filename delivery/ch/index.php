<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "产品运送方式");
$APPLICATION->SetTitle("产品运送方式");
?><img src="<?=SITE_DIR?>images/top_delivery.jpg" style="width:100%;">
<div id="wrap_disc_siberia">
	<h1 id="title_about" class="grinch">
		 国际产品运送还从来没有这么简单！
	</h1>
</div>
<div id="wrap_steps">
	<div class="container no-indent">
		<div class="row">
			<div class="step step1 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step1.png"/>
				</div>
				<p>
			 我们与俄罗斯和中国领先的物流公司合作。<br>
		</p>
			</div>
			<div class="step step2 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step2.png"/>
				</div>
				<p>
			 您不用担心产品运送问题，因为运送货物的全部费用和风险是我们承担的。
		</p>
			</div>
			<div class="step step3 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step3.png"/>
				</div>
				<p>
			 您只管选择产品后，在我们的网站（siberia-products.com）填下订单格式，其他的工作内容是我们的专家来负责。
			</p>
			</div>
			<div class="step step4 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step4.png"/>
				</div>
				<p align="left">
			 产品交给运送部门后，您只管等待送信员给您将产品送到门上。<br>
		</p>
			</div>
		</div>
	</div>
</div>
<style>
	#title_about{
		text-align:center;
		font-size:2.4em;
	}
	.grinch{
		color:#71a53b;
	}
	p{
		font-size:1.8em;
	}
	#siberia{
		display:block;
		margin:0 auto;
	}
	#wrap_disc_siberia{
		margin:5rem 0;
	}
	#wrap_steps{
		margin-bottom:4rem;
	}
	#wrap_steps .step{
		display:inline-block;
		vertical-align:top;
		padding:0 3rem;
	}
	#wrap_steps .step .pic{
		border-radius:50%;
		border:3px solid #71a53b;
		width:23rem;
		height:23rem;
		margin:0 auto;
		margin-bottom:3.5rem;
		background-color:#fff;
		position:relative;
	}
	#wrap_steps .step .pic img{
		position:absolute;
		left:0;
		right:0;
		top:0;
		bottom:0;
		margin:auto;
	}
	@media (max-width: 767px){
		#wrap_steps .step p{
			margin-bottom:3.5rem;
			text-align:center;
		}
	}
	@media (min-width: 768px) and (max-width: 991px) {
		#wrap_steps .step p{
			margin-bottom:3.5rem;
			text-align:center;
		}
	}
</style><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>