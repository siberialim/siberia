<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "배달");
$APPLICATION->SetTitle("배달");
?><img src="<?=SITE_DIR?>images/top_delivery.jpg" style="width:100%;">
<div id="wrap_disc_siberia">
	<h1 id="title_about" class="grinch">
		 국제 운송이라면 쉬운 업무가 아니지만 우리한테 맡기시면 아무 걱정 없이 주문하시는 대로 받으실 수 있겠습니다!
 </h1>
</div>
<div id="wrap_steps">
	<div class="container no-indent">
		<div class="row">
			<div class="step step1 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step1.png"/>
				</div>
				<p>
			 우리는 러시아와 한국의 선두 물류 업체와 협력하고 있습니다.
		</p>
			</div>
			<div class="step step2 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step2.png"/>
				</div>
				<p>
			 주문된 제품 운송에 대하여 걱정을 안 하셔도 됩니다. 우리는 해당된 비용과 리스크를 부담하겠습니다.
		</p>
			</div>
			<div class="step step3 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step3.png"/>
				</div>
				<p>
			 주문을 하려면siberia-products.com 웹사이트에서 제품을 선택해서 신청을 하시면 됩니다. 나머지는 일은 우리 직원들이 알아서 해드리겠습니다.
		</p>
			</div>
			<div class="step step4 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step4.png"/>
				</div>
				<p align="left">
			 주문이 운송부서에 넘기게 되고 나면 배달원을 기다려서 제품을 받기만 하시면 됩니다.
		</p>
			</div>
		</div>
	</div>
</div>
<style>
	#title_about{
		text-align:center;
		font-size:2.4em;
	}
	.grinch{
		color:#71a53b;
	}
	p{
		font-size:1.8em;
	}
	#siberia{
		display:block;
		margin:0 auto;
	}
	#wrap_disc_siberia{
		margin:5rem 0;
	}
	#wrap_steps{
		margin-bottom:4rem;
	}
	#wrap_steps .step{
		display:inline-block;
		vertical-align:top;
		padding:0 3rem;
	}
	#wrap_steps .step .pic{
		border-radius:50%;
		border:3px solid #71a53b;
		width:23rem;
		height:23rem;
		margin:0 auto;
		margin-bottom:3.5rem;
		background-color:#fff;
		position:relative;
	}
	#wrap_steps .step .pic img{
		position:absolute;
		left:0;
		right:0;
		top:0;
		bottom:0;
		margin:auto;
	}
	@media (max-width: 767px){
		#wrap_steps .step p{
			margin-bottom:3.5rem;
			text-align:center;
		}
	}
	@media (min-width: 768px) and (max-width: 991px) {
		#wrap_steps .step p{
			margin-bottom:3.5rem;
			text-align:center;
		}
	}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>