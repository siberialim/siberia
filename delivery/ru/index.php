<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Доставка и оплата");
$APPLICATION->SetTitle("Доставка и оплата");
?>
<div class="delivery">
	<div class="container" style="background:#fff;padding:3rem 1.5rem;">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
				<h1 align="center">Доставка и оплата</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-5 col-md-push-7">
				<br/><br/><br/><br/>
				<img src="/images/d1.png" class="img1" alt=""/>
				<br/><br/><br/><br/>
			</div>
			<div class="col-xs-12 col-md-7 col-md-pull-5">
				<br/><br/><br/><br/>
				<p>
					Оплата производится по безналичному расчету на банковский счет ООО «Сибирские продукты»	
				</p><br/><br/><br/><br/>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-3">
				<img src="/images/d2.png" class="img2" alt=""/>
				<br/><br/><br/><br/>
			</div>
			<div class="col-xs-12 col-md-9">
				<p>
					Доставка груза осуществляется транспортной компанией за счет покупателя.
				</p><br/><br/><br/><br/>
			</div>
		</div>
	</div>
</div>
<style>
.delivery .grinch{
	color:#71a53b;
}.delivery h1{
	font-size:1.8em;
	padding-bottom:3rem;
	text-transform:uppercase;
}.delivery p{
	font-size:1.8em;
}.delivery ul li{
	font-size: 1.8em;
	list-style: initial;
	margin-left: 15px;
}.delivery img.img1{
	width:100%;
	margin:0 auto;
	max-width:353px;
	display:block;
}.delivery img.img2{
	width:100%;
	margin:0 auto;
	max-width:255px;
	display:block;
}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>