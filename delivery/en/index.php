<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Delivery");
$APPLICATION->SetTitle("Delivery");
?><img src="<?=SITE_DIR?>images/top_delivery.jpg" style="width:100%;">
<div id="wrap_disc_siberia">
	<h1 id="title_about" class="grinch">
		 International delivery has never been so easy!
	</h1>
</div>
<div id="wrap_steps">
	<div class="container no-indent">
		<div class="row">
			<div class="step step1 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step1.png"/>
				</div>
				<p>
					 We are in cooperation with leading logistics companies of Russia and South Korea.
				</p>
			</div>
			<div class="step step2 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step2.png"/>
				</div>
				<p>
					 Do not you be afraid of your cargo's delivery as we take all the costs and risks on ourselves.
				</p>
			</div>
			<div class="step step3 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step3.png"/>
				</div>
				<p>
					 You just have to choose a product and submit your request on siberia-limited.com, the rest will be completed by our specialists.
				</p>
			</div>
			<div class="step step4 col-sm-6 col-md-3 col-lg-3 col-xs-12">
				<div class="pic">
					<img src="/images/step4.png"/>
				</div>
				<p align="left">
					 After transferring your order for delivery you just need to wait for the courier and get your order.
				</p>
			</div>
		</div>
	</div>
</div>
<style>
	#title_about{
		text-align:center;
		font-size:2.4em;
	}
	.grinch{
		color:#71a53b;
	}
	p{
		font-size:1.8em;
	}
	#siberia{
		display:block;
		margin:0 auto;
	}
	#wrap_disc_siberia{
		margin:5rem 0;
	}
	#wrap_steps{
		margin-bottom:4rem;
	}
	#wrap_steps .step{
		display:inline-block;
		vertical-align:top;
		padding:0 3rem;
	}
	#wrap_steps .step .pic{
		border-radius:50%;
		border:3px solid #71a53b;
		width:23rem;
		height:23rem;
		margin:0 auto;
		margin-bottom:3.5rem;
		background-color:#fff;
		position:relative;
	}
	#wrap_steps .step .pic img{
		position:absolute;
		left:0;
		right:0;
		top:0;
		bottom:0;
		margin:auto;
	}
	@media (max-width: 767px){
		#wrap_steps .step p{
			margin-bottom:3.5rem;
			text-align:center;
		}
	}
	@media (min-width: 768px) and (max-width: 991px) {
		#wrap_steps .step p{
			margin-bottom:3.5rem;
			text-align:center;
		}
	}
</style><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>