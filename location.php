<?
//select currency
session_start();

$code_location = $_REQUEST['code_location'];

//содержит коды языков(дефолт)
$exceptions = array('en');
//содержит коды языков
//$sites = array('kn', 'ch', 'ru');
$sites = array('ru');

//Проверяет "входной параметр" кода валюты
function checkValidCodeLocation($code){
	global $sites;
	foreach($sites as $site){
		if($code == $site){
			$_SESSION['code_location'] = $code;
			break;
		}
	}
}

checkValidCodeLocation($code_location);

//выбор валюты
if($code_location == 'en'){
	$_SESSION['CURRENCY_ID'] = 'USD';
}
/*else if($code_location == 'kn'){
	$_SESSION['CURRENCY_ID'] = 'KRW';
}
else if($code_location == 'ch'){
	$_SESSION['CURRENCY_ID'] = 'CNY';
}*/
else if($code_location == 'ru'){
	$_SESSION['CURRENCY_ID'] = 'RUB';
}
else{
	$_SESSION['CURRENCY_ID'] = 'USD';
}

//...
foreach($exceptions as $ex){
	if($code_location == $ex){
		$code_location = '';
		break;
	}
}

$_SESSION['code_location'] = $code_location;
header('Location:/');