<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<div id="wrap_personal">
<?
if ($USER->IsAuthorized()){
	$APPLICATION->IncludeComponent("it-invest:menu", "siberia_personal_menu", array(
		"ROOT_MENU_TYPE" => "personal".SLANGCODE,
		"MAX_LEVEL" => "1",
		"MENU_CACHE_TYPE" => "A",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		),
		false
	);
	$APPLICATION->IncludeComponent(
		"it-invest:sale.personal.section", 
		"siberia", 
		array(
			"ACCOUNT_PAYMENT_ELIMINATED_PAY_SYSTEMS" => array(
				0 => "0",
			),
			"ACCOUNT_PAYMENT_PERSON_TYPE" => "1",
			"ACCOUNT_PAYMENT_SELL_SHOW_FIXED_VALUES" => "Y",
			"ACCOUNT_PAYMENT_SELL_TOTAL" => array(
				0 => "100",
				1 => "200",
				2 => "500",
				3 => "1000",
				4 => "5000",
				5 => "",
			),
			"ACCOUNT_PAYMENT_SELL_USER_INPUT" => "Y",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "3600",
			"CACHE_TYPE" => "N",
			"CHECK_RIGHTS_PRIVATE" => "N",
			"COMPATIBLE_LOCATION_MODE_PROFILE" => "N",
			"CUSTOM_PAGES" => "",
			"CUSTOM_SELECT_PROPS" => array(
			),
			"NAV_TEMPLATE" => "",
			"ORDER_HISTORIC_STATUSES" => array(
				0 => "F",
			),
			"PATH_TO_BASKET" => "/personal/cart",
			"PATH_TO_CATALOG" => "/catalog/",
			"PATH_TO_CONTACT" => "/about/contacts",
			"PATH_TO_PAYMENT" => "/personal/order/payment",
			"PER_PAGE" => "20",
			"PROP_1" => "",
			"PROP_2" => "",
			"SAVE_IN_SESSION" => "Y",
			"SEF_FOLDER" => "/personal/",
			"SEF_MODE" => "Y",
			"SEND_INFO_PRIVATE" => "N",
			"SET_TITLE" => "Y",
			"SHOW_ACCOUNT_COMPONENT" => "Y",
			"SHOW_ACCOUNT_PAGE" => "Y",
			"SHOW_ACCOUNT_PAY_COMPONENT" => "Y",
			"SHOW_BASKET_PAGE" => "Y",
			"SHOW_CONTACT_PAGE" => "Y",
			"SHOW_ORDER_PAGE" => "Y",
			"SHOW_PRIVATE_PAGE" => "Y",
			"SHOW_PROFILE_PAGE" => "Y",
			"SHOW_SUBSCRIBE_PAGE" => "Y",
			"USER_PROPERTY_PRIVATE" => "",
			"USE_AJAX_LOCATIONS_PROFILE" => "N",
			"ORDER_ID" => $_POST["ORDER_ID"],
			"PRODUCT_NAME" => $_POST["PRODUCT_NAME"],
			"COMPONENT_TEMPLATE" => ".default",
			"SEF_URL_TEMPLATES" => array(
				"index" => "index.php",
				"orders" => "orders/",
				"account" => "account/",
				"subscribe" => "subscribe/",
				"profile" => "profiles/",
				"profile_detail" => "profiles/#ID#",
				"settings" => "profile/settings/",
				"order_detail" => "orders/#ID#",
				"order_cancel" => "cancel/#ID#",
			)
		),
		false
	);
?></div>
	<?
}
else{
	$APPLICATION->IncludeComponent("it-invest:system.auth.form","flat",Array(
		"REGISTER_URL" => "/login?register=yes",
		"FORGOT_PASSWORD_URL" => "/login?forgot_password=yes",
		"PROFILE_URL" => "/personal/profile",
		"SHOW_ERRORS" => "Y" 
		)
	);
}
?>
	<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>