<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Siberia Products");
$APPLICATION->SetTitle("Siberia Products");
$APPLICATION->SetPageProperty("description", "Siberia Products");
$APPLICATION->SetPageProperty("keywords", "Siberia Products");
?>
<? if ($APPLICATION->GetCurPage(false) === '/'): ?> 
<div class="col-sm-3 col-md-3 wrapper-primary-menu">
	<div class="primary-menu<?if($curPage != '/index.php'){echo ' contracted';}?>">
		<?$APPLICATION->IncludeComponent("it-invest:custom.section.list", "siberia", array(
			),
			false
		);?>
		<div class="catalog-b-list new">
			<?$APPLICATION->IncludeComponent(
				"it-invest:catalog.custom.special.elements.new", 
				"siberia", 
				array(
					"IBLOCK_TYPE" => "catalog",
					"IBLOCK_ID" => IBLOCK_CATALOG_ID,
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"USE_PRODUCT_QUANTITY" => "Y",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"SHOW_PRICES" => INCLUDE_PRICES,
					"PRICE_CODE" => array(
						0 => "BASE",
					),
					"CONVERT_CURRENCY" => "Y",
					"CURRENCY_ID" => CURRENCY_ID,
				),
				false
			);?>         			
		</div>
	</div>
</div>
<? endif; ?> 
<?if ($APPLICATION->GetCurPage(false) === '/'): ?> 
	<div id="wrapper_main_carousel">
		<div class="col-xs-12 col-sm-9 col-md-9">
			<div id="carousel-example-generic" class="carousel-main slide" data-ride="carousel">
				<?$APPLICATION->IncludeComponent(
					"it-invest:banners.custom", 
					"banners_r", 
					array(
					"IBLOCK_TYPE" => "news",
					"IBLOCK_ID" => IBLOCK_BANNERS_ID,
					"NEWS_COUNT" => "20",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "86400",
					"CACHE_NOTES" => "",
					"CACHE_FILTER" => "N",
					),
					false
				);?>
			</div>
		</div>
	</div>
<? endif; ?> 
<?if ($APPLICATION->GetCurPage(false) === '/'): ?> 
	<div id="carousel-example-generic" class="carousel bn_m carousel-main slide" data-ride="carousel">
		<?$APPLICATION->IncludeComponent(
			"it-invest:banners.custom", 
			"banners_m", 
			array(
			"IBLOCK_TYPE" => "news",
			"IBLOCK_ID" => IBLOCK_BANNERS_ID,
			"NEWS_COUNT" => "20",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "86400",
			"CACHE_NOTES" => "",
			"CACHE_FILTER" => "N",
			),
			false
		);?>
	</div>
<? endif; ?> 
<?if(SLANGCODE == 'ru' || SLANGCODE == 'en')
{
?>
	<div class="col-xs-12 col-sm-9 col-md-9 out-bx-newslist">
		<?$APPLICATION->IncludeComponent(
		   "it-invest:news", 
		   "main", 
		   array(
			  "ADD_ELEMENT_CHAIN" => "Y",
			  "ADD_SECTIONS_CHAIN" => "Y",
			  "AJAX_MODE" => "N",
			  "AJAX_OPTION_ADDITIONAL" => "",
			  "AJAX_OPTION_HISTORY" => "N",
			  "AJAX_OPTION_JUMP" => "N",
			  "AJAX_OPTION_STYLE" => "Y",
			  "BROWSER_TITLE" => "NAME",
			  "CACHE_FILTER" => "N",
			  "CACHE_GROUPS" => "Y",
			  "CACHE_TIME" => "36000000",
			  "CACHE_TYPE" => "A",
			  "CHECK_DATES" => "Y",
			  "COMPONENT_TEMPLATE" => "siberia",
			  "DETAIL_ACTIVE_DATE_FORMAT" => "d/m/Y",
			  "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
			  "DETAIL_DISPLAY_TOP_PAGER" => "N",
			  "DETAIL_FIELD_CODE" => array(
				 0 => "",
				 1 => "",
			  ),
			  "DETAIL_PAGER_SHOW_ALL" => "N",
			  "DETAIL_PAGER_TEMPLATE" => "siberia",
			  "DETAIL_PAGER_TITLE" => "Страница",
			  "DETAIL_PROPERTY_CODE" => array(
				 0 => "",
				 1 => "",
			  ),
			  "DETAIL_SET_CANONICAL_URL" => "Y",
			  "DISPLAY_BOTTOM_PAGER" => "N",
			  "DISPLAY_DATE" => "Y",
			  "DISPLAY_NAME" => "Y",
			  "DISPLAY_PICTURE" => "Y",
			  "DISPLAY_PREVIEW_TEXT" => "Y",
			  "DISPLAY_TOP_PAGER" => "N",
			  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
			  "IBLOCK_ID" => IBLOCK_NEWS_ID,
			  "IBLOCK_TYPE" => "news_2",
			  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			  "LIST_ACTIVE_DATE_FORMAT" => "d/m/Y",
			  "LIST_FIELD_CODE" => array(
				 0 => "",
				 1 => "",
			  ),
			  "LIST_PROPERTY_CODE" => array(
				 0 => "",
				 1 => "",
			  ),
			  "MESSAGE_404" => "",
			  "META_DESCRIPTION" => "-",
			  "META_KEYWORDS" => "-",
			  "NEWS_COUNT" => "4",
			  "PAGER_BASE_LINK_ENABLE" => "N",
			  "PAGER_DESC_NUMBERING" => "N",
			  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			  "PAGER_SHOW_ALL" => "N",
			  "PAGER_SHOW_ALWAYS" => "N",
			  "PAGER_TEMPLATE" => "",
			  "PAGER_TITLE" => "Новости",
			  "PREVIEW_TRUNCATE_LEN" => "",
			  "SEF_FOLDER" => "/about/".SLANGCODE."/",
			  "SEF_MODE" => "Y",
			  "SET_LAST_MODIFIED" => "N",
			  "SET_STATUS_404" => "N",
			  "SET_TITLE" => "N",
			  "SHOW_404" => "N",
			  "SORT_BY1" => "ACTIVE_FROM",
			  "SORT_BY2" => "SORT",
			  "SORT_ORDER1" => "DESC",
			  "SORT_ORDER2" => "ASC",
			  "USE_CATEGORIES" => "N",
			  "USE_FILTER" => "N",
			  "USE_PERMISSIONS" => "N",
			  "USE_RATING" => "N",
			  "USE_REVIEW" => "N",
			  "USE_RSS" => "N",
			  "USE_SEARCH" => "N",
			  "USE_SHARE" => "N",
			  "SEF_URL_TEMPLATES" => array(
				 "news" => "/about/".SLANGCODE."/",
				 "section" => "#SECTION_CODE#/",
				 "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			  )
		   ),
		   false
		);?>
	</div>
<?}?>
<div class="col-xs-12 col-sm-9 col-md-9 catalog-b-list">
	<?$APPLICATION->IncludeComponent(
		"it-invest:catalog.custom.special.elements", 
		"siberia", 
		array(
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => IBLOCK_CATALOG_ID,
			"ACTION_VARIABLE" => "action",
			"PRODUCT_ID_VARIABLE" => "id",
			"SECTION_ID_VARIABLE" => "SECTION_ID",
			"USE_PRODUCT_QUANTITY" => "Y",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "Y",
			"CACHE_GROUPS" => "Y",
			"SHOW_PRICES" => INCLUDE_PRICES,
			"PRICE_CODE" => array(
				0 => "BASE",
			),
			"CONVERT_CURRENCY" => "Y",
			"CURRENCY_ID" => CURRENCY_ID,
		),
		false
	);?>         			
</div>
<script>
	$( document ).ready(function() {
		// Навигация в категориях
		$(".catalog-b-list .catalog-nav a").click(function(e) {
			e.preventDefault();
			$('html, body').animate({
				scrollTop: $(".catalog-b[data-cat="+$(this).attr('data-cat-button')+"]").offset().top - 49
			}, 800);
		});
			
		$('.primary-menu.contracted').click(function(e) {
			$('.primary-menu__cont').show();
		});
		
		$('.primary-menu__cont').click(function(event) {
			if ($(event.target).closest("#message").length) return;
			//$('.primary-menu__cont').hide();
			event.stopPropagation();
		});
		$(document).mouseup(function (e){ // событие клика по веб-документу
			var el = $(".primary-menu.contracted .primary-menu__cont"); // 
			if (!el.is(e.target) // если клик был не по нашему блоку
				&& el.has(e.target).length === 0) { // и не по его дочерним элементам
				el.hide(); // скрываем его
			}
		});
		
		// Подсветка категорий при  прокрутке
		wrapper = $(".catalog-b-list");
		function highlightCat() {
			var top = wrapper.offset().top - 50;
			var bottom = wrapper.height() + top;
			var scroll_top = $(this).scrollTop();
			
			if((scroll_top > top) && (scroll_top < bottom))
			{
				if(!wrapper.hasClass('active'))
				{
					wrapper.addClass('active');
				}
				
				
				wrapper.find('.catalog-b').each(function() {
					topEl = $(this).offset().top - 50;
					bottomEl = $(this).height() + topEl;
					scroll_topEl = $(window).scrollTop();
					
					if((scroll_topEl > topEl) && (scroll_topEl < bottomEl)) {
						if(!$(this).hasClass('active'))
						{
							$(this).addClass('active');
							$('.catalog-b-list .catalog-nav a[data-cat-button='+$(this).attr('data-cat')+']').addClass('active');
						}
						
					} else {
						$(this).removeClass('active');
						$('.catalog-b-list .catalog-nav a[data-cat-button='+$(this).attr('data-cat')+']').removeClass('active');
					}
				});
				
			}
			else {
				wrapper.removeClass('active');
				$('.catalog-b').removeClass('active');
				$('.catalog-b-list .catalog-nav a').removeClass('active');
			}
			setTimeout(highlightCat, 100);
		}
		highlightCat();
		
		//js/jquery-contained-sticky-scroll.js
		$('.catalog-nav').containedStickyScroll({duration: 0});
	});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>