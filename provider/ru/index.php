<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Поставщикам");
$APPLICATION->SetTitle("Поставщикам");
?>
<div class="provider">
	<div class="container" style="background:#fff;padding:3rem 1.5rem;">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
				<h1 align="center">Поставщикам</h1>			
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
				<p align="center">Страница находится в разработке.</p>
			</div>
		</div>
		<!--<div class="row">
			<div class="col-xs-12 col-md-2 col-md-push-10">
				<img src="/images/p1.png" class="img1" alt=""/>
				<br/><br/><br/><br/>
			</div>
			<div class="col-xs-12 col-md-10 col-md-pull-2">
				<p>
					Электронная торговая платформа Siberia – это удобный и современный инструмент, 
					позволяющий с минимальными затратами организовать процесс электронной торговли.			
				</p><br/><br/><br/><br/>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-3">
				<img src="/images/p2.png" class="img2" alt=""/>
				<br/><br/><br/><br/>
			</div>
			<div class="col-xs-12 col-md-9">
				<p>
					Уважаемые поставщики и производители, 
					если Вас заинтересовало сотрудничество с нашей компанией по размещению продукции на электронной торговой площадке Siberia, 
					предлагаем ознакомиться с условиями сотрудничества и типовым договором поставки.
				</p><br/><br/><br/><br/>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-5 col-md-push-7">
				<img src="/images/p3.png" class="img3" alt=""/>
				<br/><br/><br/><br/>
			</div>
			<div class="col-xs-12 col-md-7 col-md-pull-5">
				<p>
					<b>Мы предлагаем решение, которое включает в себя:</b><br/>
					- единую площадку для размещения товаров,<br/>
					- единую систему доставки товаров,<br/>
					- единую систему продвижения товаров
				</p><br/><br/><br/><br/>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<img src="/images/p4.png" class="img4" alt=""/>
				<br/><br/><br/><br/>
			</div>
			<div class="col-xs-12 col-md-7">
				<p>
					<b>Простота начала работы на сайте:</b><br/>
					- необходимо зарегистрироваться на сайте,<br/>
					- дождаться звонка от нашего менеджера, который поможет Вам выполнить дальнейшие действия,<br/>
				</p><br/><br/><br/><br/>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-5 col-md-push-7">
				<img src="/images/p5.png" class="img5" alt=""/>
				<br/><br/><br/><br/>
			</div>
			<div class="col-xs-12 col-md-7 col-md-pull-5">
				<p>
					<b>Возможности:</b><br/>
					<ul>
						<li>Размещение товаров на электронной торговой платформе</li>
						<li>Размещение акций, специальных предложений, рекламных баннеров на электронной торговой платформе</li>
						<li>Продвижение товаров для оптовых покупателей в России (территория продвижения зависит от выбранного пакета услуг)</li>
						<li>Доставка товаров до покупателей</li>
						<li>Разработка качественного дизайна упаковки по международным стандартам</li>
						<li>Продвижение товаров для оптовых покупателей в Корее, Китае, Вьетнаме</li>
					</ul>
				</p><br/><br/><br/><br/>
			</div>
		</div>
		<div class="row">
			<p>
				<b>Кто пользуется нашей платформой:</b><br/>
				<ul>
					<li style="list-style:none;margin-bottom: 30px;">
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<br/><br/>
								<img src="/images/p6.png" class="img6" alt=""/>
								<br/><br/>
							</div>
							<div class="col-xs-12 col-md-9">
								<br/><br/>
								• 	закупщики: розничные продуктовые магазины, супермаркеты, кафе, рестораны и др. предприятия питания
								<br/><br/>
							</div>
						</div>
					</li>
					<li style="list-style:none;margin-bottom: 30px;">
						<div class="row">
							<div class="col-xs-12 col-md-4 col-md-push-8">
								<img src="/images/p7.png" class="img7" alt=""/>
								<br/><br/>
							</div>
							<div class="col-xs-12 col-md-8 col-md-pull-4">
								•	поставщики: оптовые продуктовые распределительные и логистические центры
								<br/><br/>
							</div>
						</div>
					</li>
					<li style="list-style:none">
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<img src="/images/p8.png" class="img8" alt=""/>
								<br/><br/>
							</div>
							<div class="col-xs-12 col-md-9">
								•	производители: производители продуктов питания, дистрибьюторы и представительства
								<br/><br/>
							</div>
						</div>
					</li>
				</ul>
			</p><br/><br/><br/><br/>
		</div>
		<div class="row">
			<p>
				<b>Преимущества работы с нами:</b><br/>
				<ul>
					<li style="list-style:none;margin-bottom: 30px;">
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<br/><br/>
								<img src="/images/p9.png" class="img10" alt=""/>
								<br/><br/>
							</div>
							<div class="col-xs-12 col-md-9">
								<br/><br/>
								•	упрощается покупка-продажа продуктов питания оптом
								<br/><br/>
							</div>
						</div>
					</li>
					<li style="list-style:none;margin-bottom: 30px;">
						<div class="row">
							<div class="col-xs-12 col-md-4 col-md-push-8">
								<img src="/images/p11.png" class="img11" alt=""/>
								<br/><br/>
							</div>
							<div class="col-xs-12 col-md-8 col-md-pull-4">
								•	минимальные затраты на внедрение канала онлайн-продаж, достаточно компьютера с выходом в Интернет
								<br/><br/>
							</div>
						</div>
					</li>
					<li style="list-style:none">
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<img src="/images/p12.png" class="img12" alt=""/>
								<br/><br/>
							</div>
							<div class="col-xs-12 col-md-9">
								•	описания и характеристики товаров от производителей, копии сертификатов, заключений и т.д.
								<br/><br/>
							</div>
						</div>
					</li>
				</ul>
			</p><br/><br/><br/><br/>
		</div>-->
	</div>
</div>
<style>
.provider .grinch{
	color:#71a53b;
}.provider h1{
	font-size:1.8em;
	padding-bottom:3rem;
	text-transform:uppercase;
}.provider p{
	font-size:1.8em;
}.provider ul li{
	font-size: 1.8em;
	list-style: initial;
	margin-left: 15px;
}.provider img.img1{
	width:100%;
	margin:0 auto;
	max-width:219px;
	display:block;
}.provider img.img2{
	width:100%;
	margin:0 auto;
	max-width:262px;
	display:block;
}.provider img.img3{
	width:100%;
	margin:0 auto;
	max-width:433px;
	display:block;
}.provider img.img4{
	width:100%;
	margin:0 auto;
	max-width:288px;
	display:block;
}.provider img.img5{
	width:100%;
	margin:0 auto;
	max-width:304px;
	display:block;
}.provider img.img6{
	width:100%;
	margin:0 auto;
	max-width:215px;
	display:block;
}.provider img.img7{
	width:100%;
	margin:0 auto;
	max-width:321px;
	display:block;
}.provider img.img8{
	width:100%;
	margin:0 auto;
	max-width:165px;
	display:block;
}
.provider img.img10{
	width:100%;
	margin:0 auto;
	max-width:128px;
	display:block;
}
.provider img.img11{
	width:100%;
	margin:0 auto;
	max-width:182px;
	display:block;
}
.provider img.img12{
	width:100%;
	margin:0 auto;
	max-width:192px;
	display:block;
}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>