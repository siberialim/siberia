<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

Bitrix\Main\Loader::includeModule('sale');
Bitrix\Main\Loader::includeModule('catalog');

$arParams = $_REQUEST;

if ($USER->IsAuthorized())
{
	$rsUser = CUser::GetByID($USER->GetID())->Fetch();
	$order = Bitrix\Sale\Order::create(SITE_ID, $USER->GetID());
	$user_name = $USER->GetFullName()?:$USER->GetLogin();
	$email_to = $rsUser['EMAIL'];
}
else
{
	//special_user - специальный пользователь для не авторизованных посетителей
	$rsUser = CUser::GetByLogin('special_user')->Fetch();
	$order = Bitrix\Sale\Order::create(SITE_ID, $rsUser['ID']);
	$user_name = $arParams['user']['name'];
	$email_to = $arParams['user']['email'];
}

$email_from = COption::GetOptionString('main', 'email_from');
$sale_email = COption::GetOptionString('sale', 'order_email');

$user_id = $rsUser['ID'];

//список сайтов
$res_sites = CSite::GetList();
$sites = array();

//id покупателя
$fuserId = \Bitrix\Sale\Fuser::getId(true);

while ($site = $res_sites->Fetch())
{
	array_push($sites, $site);
}

$basket = Bitrix\Sale\Basket::create(SITE_ID);

$products = array(array('PRODUCT_ID' => $arParams['product']['id'], 'NAME' => $arParams['product']['name'], 'PRICE' => 0, 'CURRENCY' => CURRENCY_ID, 'QUANTITY' => $arParams['product']['Quantity']));

/*foreach ($sites as $site)
{
	$res_basket = CSaleBasket::GetList(false, array("FUSER_ID" => $fuserId,"LID" => $site['LID'],"ORDER_ID" => "NULL"), false, false, array('PRODUCT_ID', 'NAME', 'PRICE', 'CURRENCY', 'QUANTITY'));
	
	while ($arItems = $res_basket->Fetch())
	{
		 array_push($products,$arItems);
	}
	
}*/

$html_order_list = '';

foreach ($products as $product)
{
	$item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
	unset($product["PRODUCT_ID"]);
	$item->setFields($product);
	$html_order_list .= $product['NAME'].' - '.(int)$product['QUANTITY'];//.' : '.CurrencyFormat($product['PRICE'], $product['CURRENCY']).' <br>';
}

\Bitrix\Sale\Notify::setNotifyDisable(true);

$order = Bitrix\Sale\Order::create(SITE_ID, $rsUser['ID']);
$order->setPersonTypeId(1);
$order->setBasket($basket);

$result = $order->save();

$order_id = $order->getId();
$order_data = CSaleOrder::GetByID($order_id);

if ($USER->IsAuthorized())
{
	$order_disc = $order_data['USER_DESCRIPTION'];
	
}
else
{
	$order_disc = $arParams['user']['comment'];
};

Bitrix\Main\Mail\Event::send(array(
	"EVENT_NAME" => (SLANGCODE == 'en') ? 'SALE_NEW_ORDER' : 'SALE_NEW_ORDER_'.SLANGCODE,
	"LID" => SITE_ID,
	"C_FIELDS" => array(
		"EMAIL" => $email_to,
		"EMAIL_TO" => $email_to,
		"SALE_EMAIL" => $sale_email,
		"USER_ID" => $user_id,
		"ORDER_ID" => $order_id,
		"ORDER_DATE" => $order_data['DATE_INSERT'],
		"ORDER_LIST" => $html_order_list,
		"ORDER_USER" => $user_name
	),
));

Bitrix\Main\Mail\Event::send(array(
	"EVENT_NAME" => (SLANGCODE == 'en') ? 'SALE_NEW_ORDER' : 'SALE_NEW_ORDER_'.SLANGCODE,
	"LID" => SITE_ID,
	"C_FIELDS" => array(
		"EMAIL" => $email_from,
		"EMAIL_TO" => $email_from,
		"SALE_EMAIL" => $sale_email,
		"USER_ID" => $user_id,
		"ORDER_ID" => $order_id,
		"ORDER_DATE" => $order_data['DATE_INSERT'],
		"ORDER_LIST" => $html_order_list,
		"ORDER_USER" => $user_name
	),
));

$site_data = CSite::GetByID(SITE_ID)->fetch();
//$arr_status = array();
$res_status = CSaleStatus::GetList(array(), array("LID" => SLANGCODE));
$status_name = ' ';

while ($ob = $res_status->fetch())
{
	//array_push($arr_status, $ob);
	if($order_data['STATUS_ID'] == $ob['ID']){
		$status_name = $ob['NAME'];
		break;
	}
}

Bitrix\Main\Mail\Event::send(array(
	"EVENT_NAME" => (SLANGCODE == 'en') ? 'SALE_STATUS_CHANGED_N' : 'SALE_STATUS_CHANGED_N_'.SLANGCODE,
	"LID" => SITE_ID,
	"C_FIELDS" => array(
		"EMAIL" => $email_to,
		"EMAIL_TO" => $email_to,
		"SALE_EMAIL" => $sale_email,
		"SITE_NAME" => $site_data['SITE_NAME'],
		"ORDER_ID" => $order_id,
		"ORDER_DATE" => $order_data['DATE_INSERT'],
		"ORDER_STATUS" => $status_name,
		"ORDER_DESCRIPTION" => $order_disc,
		"TEXT" => ' '
	),
));

Bitrix\Main\Mail\Event::send(array(
	"EVENT_NAME" => (SLANGCODE == 'en') ? 'SALE_STATUS_CHANGED_N' : 'SALE_STATUS_CHANGED_N_'.SLANGCODE,
	"LID" => SITE_ID,
	"C_FIELDS" => array(
		"EMAIL" => $email_from,
		"EMAIL_TO" => $email_from,
		"SALE_EMAIL" => $sale_email,
		"SITE_NAME" => $site_data['SITE_NAME'],
		"ORDER_ID" => $order_id,
		"ORDER_DATE" => $order_data['DATE_INSERT'],
		"ORDER_STATUS" => $status_name,
		"ORDER_DESCRIPTION" => $order_disc.'<br/>'.$arParams['user']['phone'].'<br/>'.$arParams['user']['email'],
		"TEXT" => ' '
	),
));

$shipmentCollection = $order->getShipmentCollection();
$shipment = $shipmentCollection->createItem(
	Bitrix\Sale\Delivery\Services\Manager::getObjectById(1)
);
	
$shipmentItemCollection = $shipment->getShipmentItemCollection();

foreach ($basket as $basketItem)
{
	$item = $shipmentItemCollection->createItem($basketItem);
	$item->setQuantity($basketItem->getQuantity());
}

$paymentCollection = $order->getPaymentCollection();
$payment = $paymentCollection->createItem(
	Bitrix\Sale\PaySystem\Manager::getObjectById(1)
);

$payment->setField("SUM", $order->getPrice());
$payment->setField("CURRENCY", $order->getCurrency());

if (!$result->isSuccess())
{
	//$result->getErrors();
	echo false;
}
else
{
	//очистим корзину
	CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
	echo $order_id;
}