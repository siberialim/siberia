<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "우리 회사");
$APPLICATION->SetTitle("우리 회사");
?><div id="main_pic"><img src="<?=SITE_DIR?>images/top_about.jpg"></div>
<div id="wrap_disc_siberia" style="text-align: center;">
	<h1 id="title_about" class="grinch">
	<p>
		 러시아 시베리아 지방에서 경험이 많고 신뢰할 만한 제작사들은 협력해서 시베리아의 친환경과 엄격한 품질 준수를 상징하는 통일한 브랜드를 창조했습니다.
	</p>
 </h1>
</div>
<p style="text-align: center;">
</p>
<div id="wrap_siberia_logo">
 <img src="<?=SITE_DIR?>images/siberia.png" id="siberia">
</div>
<div id="wrap_siberia_map">
	<p style="text-align: center;">
		 이렇게 해서 아시아 시장을 겨냥하는 'Siberia Products'라는 공동업체가 설립된 것입니다.
	</p>
 <img src="<?=SITE_DIR?>images/map.png" id="siberia_map">
</div>
<div id="partners">
	<div class="container"> 
		<div class="row">
			<div class="item col-sm-6 col-md-6 col-lg-6 col-xs-12">
				<img src="<?=SITE_DIR?>images/item1.jpg">
				<p style="text-align: center;">
					 우리는 쿠스바쓰 상공회의소의 의원이며 케메로보 주 행정부에서 지원을 받고 있습니다.
				</p>
			</div>
			<div class="item col-sm-6 col-md-6 col-lg-6 col-xs-12">
				<img src="<?=SITE_DIR?>images/item2.png">
				<p>
					 공동업체 중에 큰 하나인 OOO «Imperia MOKS»사는 빵공장 6개와 체인 전문점 50개로 구성되어 있습니다. 이 회사는 쿠스바쓰 지역에서 좋은 품질의 빵식품, 제과 식품과 건빵식품을 제작해서 유통하는 업체 중에 선두 기업입니다. &nbsp;
				</p>
			</div>
		</div>
	</div>
</div>
<p style="background: #e2e2e2; padding: 30px 15px; margin: 50px 0px;">
</p>
<p>
	 'Siberia Products'사가 제공하는 제품들은 전통적인 요리법에 따라 만들어져 있어서 경쟁사의 것보다 뛰어납니다
</p>
<p>
	 우리의 냉동된 빵, 케이크, 제과 식품과 유통기한이 긴 테트라 팩 우유는 농업 박람회나 대회에서 항상 우승 상을 받게 됩니다.&nbsp;
</p>
<p>
</p>
<div id="achivki">
	<div class="container"> 
		<div class="row">
			<div class="item col-sm-4 col-md-4 col-lg-4 col-xs-12">
				<p>
					 예를 들자면, 2014년 «Imperia MOKS»의 제품들은 노보시비르스크에서 열린 국제 박람회 «ITE Siberia»에서 작은 금메달을 탔습니다.
				</p>
			</div>
			<div class="item col-sm-4 col-md-4 col-lg-4 col-xs-12">
				<p>
				</p>
				<p>
					 2015년 모스크바에서 열린 박람회 'Gold Fall'에서 1등부터 3등까지 모든 상을 받게 되었습니다. (1등은 '미니 빵링'이 받았습니다).
				</p>
			</div>
			<div class="item col-sm-4 col-md-4 col-lg-4 col-xs-12">
				<p>
					 2016년 노보쿠즈네츠크에서 열린 전문박람회 'Prodmarkert'에서 '미니 빵링'과 '전통' 빵링 제품은 상을 탔습니다.
				</p>
			</div>
		</div>
	</div>
</div>
<div id="honor">
 <img src="<?=SITE_DIR?>images/honors.png">
</div>
<style>
	#title_about{
		text-align:center;
		font-size:2.4em;
	}.grinch{
		color:#71a53b;
	}p{
		font-size:1.8em;
	}#main_pic img{
		width:100%;
	}#siberia{
		display:block;
		margin:0 auto;
		width:100%;
		max-width:45.4rem;
	}#wrap_disc_siberia{
		margin:5rem 0;
		padding:0 1.5rem;
	}#wrap_siberia_logo{
		margin:5rem 0;
		padding:0 1.5rem;
	}#wrap_siberia_map{
		background:#e2e2e2;
		padding:3rem;
		margin:5rem 0;
	}#siberia_map{
		display:block;
		margin:0 auto;
		width:100%;
		max-width:40.9rem;
	}#partners{
		margin: 5rem 0;
	}#partners .item{
		vertical-align:top;
		padding:0 1.5rem;
		text-align:center;
	}#partners .item img{
		display:block;
		margin:0 auto;
		height:12rem;
		margin-bottom:1.5rem;
	}#achivki{
		margin:5rem 0;
	}#achivki .item{
		vertical-align:top;
		padding:0 3.8rem;
		position:relative;
	}#achivki .item:before{
		content:" ";
		background:url(images/ok.png) no-repeat;
		background-size:2.1rem;
		width:2.1rem;
		height:1.8rem;
		position:absolute;
		left:0;
		top:0;
	}#honor{
		background:#e2e2e2;
		padding:4rem 10rem;
		margin:5rem 0;
	}#honor img{
		width:100%;
		max-width:101.9rem;
	}@media (max-width: 767px){
		#main_pic{
			display:none;
		}#achivki .item{
		    padding: 0 1.5rem 3.8rem 2.5rem;
		}#partners .item{
			padding: 1.5rem;
		}#honor{
			padding:1.5rem;
		}#honor img{
			width:100%;
		}
	}
</style><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>