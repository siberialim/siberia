<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "公司简介");
$APPLICATION->SetTitle("公司简介");
?><div id="main_pic"><img src="<?=SITE_DIR?>images/top_about.jpg"></div>
<div id="wrap_disc_siberia" style="text-align: center;">
	<h1 id="title_about" class="grinch">
		 Siberia Products™贸易协会给消费者提供最优质的食品。
 </h1>
</div>
<div id="wrap_siberia_logo">
 <img src="<?=SITE_DIR?>images/siberia.png" id="siberia">
</div>
<div id="wrap_siberia_map">
	<p>
		 Products™贸易协会是在久经考验且具有丰富经验的全西伯利亚区域制造商并在统一品牌联手下而成立的。我们品牌的含义包括西伯利亚区域的净生态和产品质量严格管理。
	</p>
	<p>
		 Siberia Products™以亚洲市场为导向<br>
	</p>
 <img src="<?=SITE_DIR?>images/map.png" id="siberia_map">
</div>
<div id="partners">
	<div class="container">
		<div class="row">
			<div class="item col-sm-6 col-md-6 col-lg-6 col-xs-12">
				<img src="<?=SITE_DIR?>images/item1.jpg">
				<p>
					 我们是库兹涅茨煤矿区工商会的成员。在发展业务过程中得到了克麦罗沃州政府全面的支持。
				</p>
			</div>
			<div class="item col-sm-6 col-md-6 col-lg-6 col-xs-12">
				<img src="<?=SITE_DIR?>images/item2.png">
				<p>
					 当地规模最大的食品加工企业 – “MOKS王国”有限公司同时也是Siberia Products™贸易协会成员之一。该企业拥有直属贸易网络，旗下包括六家面包制造厂和五十家商店。在库兹涅茨煤矿区该企业是生产和销售优质的面包类、糖果点心类、面包干类和面包圈类的龙头企业。
				</p>
			</div>
		</div>
	</div>
</div>
<p style="background: #e2e2e2; padding: 30px 15px; margin: 50px 0px;">
</p>
<p>
	 Siberia Products™贸易协会成员的企业其产品均按照传统配方制造而成，因此可以把我们的竞争者远远的甩在后面。贸易协会成员的企业其生产的食品，包括冷冻蛋糕、冷冻面包、糖果点心、利乐包装等可以长期贮存的牛奶经常赢得各种农业展览会的奖牌和证书。
</p>
<p>
</p>
<div id="achivki">
	<div class="container"> 
		<div class="row">
			<div class="item col-sm-4 col-md-4 col-lg-4 col-xs-12">
				<p>
					 2014 年我们参加了新西伯利亚市“ITE西伯利亚”专业展览会，“MOKS王国”有限公司其产品得到了小金牌。
				</p>
			</div>
			<div class="item col-sm-4 col-md-4 col-lg-4 col-xs-12">
				<p>
				</p>
				<p>
					 在2015 年Siberia Products™贸易协会参加莫斯科“金色秋天”展览会的时候，并赢得了所有的荣誉奖项。（ “微型小面包圈”得到了第一奖）
				</p>
			</div>
			<div class="item col-sm-4 col-md-4 col-lg-4 col-xs-12">
				<p>
					 在2016 年我们参加新库兹涅茨克市“食品市场”专业展览会的时候，同时得到了两个奖项（“微型小面包圈”、“传统小面包圈”）
				</p>
			</div>
		</div>
	</div>
</div>
<div id="honor">
 <img src="<?=SITE_DIR?>images/honors.png">
</div>
<style>
	#title_about{
		text-align:center;
		font-size:2.4em;
	}.grinch{
		color:#71a53b;
	}p{
		font-size:1.8em;
	}#main_pic img{
		width:100%;
	}#siberia{
		display:block;
		margin:0 auto;
		width:100%;
		max-width:45.4rem;
	}#wrap_disc_siberia{
		margin:5rem 0;
		padding:0 1.5rem;
	}#wrap_siberia_logo{
		margin:5rem 0;
		padding:0 1.5rem;
	}#wrap_siberia_map{
		background:#e2e2e2;
		padding:3rem;
		margin:5rem 0;
	}#siberia_map{
		display:block;
		margin:0 auto;
		width:100%;
		max-width:40.9rem;
	}#partners{
		margin: 5rem 0;
	}#partners .item{
		vertical-align:top;
		padding:0 1.5rem;
		text-align:center;
	}#partners .item img{
		display:block;
		margin:0 auto;
		height:12rem;
		margin-bottom:1.5rem;
	}#achivki{
		margin:5rem 0;
	}#achivki .item{
		vertical-align:top;
		padding:0 3.8rem;
		position:relative;
	}#achivki .item:before{
		content:" ";
		background:url(images/ok.png) no-repeat;
		background-size:2.1rem;
		width:2.1rem;
		height:1.8rem;
		position:absolute;
		left:0;
		top:0;
	}#honor{
		background:#e2e2e2;
		padding:4rem 10rem;
		margin:5rem 0;
	}#honor img{
		width:100%;
		max-width:101.9rem;
	}@media (max-width: 767px){
		#main_pic{
			display:none;
		}#achivki .item{
		    padding: 0 1.5rem 3.8rem 2.5rem;
		}#partners .item{
			padding: 1.5rem;
		}#honor{
			padding:1.5rem;
		}#honor img{
			width:100%;
		}
	}
</style><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>