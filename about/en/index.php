<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "About us");
$APPLICATION->SetTitle("About us");
?>
<div class="achievements">
	<div class="row">
		<div class="col-xs-12">
			<h1>Our awards</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-4">
			<div class="responsive">
				<a href="<?=SITE_TEMPLATE_PATH?>/images/about/achievement1.jpg" class="gallery item" rel="group"><span class="cnt"><img src="<?=SITE_TEMPLATE_PATH?>/images/about/achievement1_small.jpg"/></span></a>
				<a href="" class="gallery item" rel="group"><span class="cnt"><img src=""/></span></a>
				<a href="" class="gallery item" rel="group"><span class="cnt"><img src=""/></span></a>
				<a href="" class="gallery item" rel="group"><span class="cnt"><img src=""/></span></a>
			</div>
		</div>
	</div>
</div>
<?
$APPLICATION->IncludeComponent(
   "it-invest:news", 
   "siberia", 
   array(
      "ADD_ELEMENT_CHAIN" => "Y",
      "ADD_SECTIONS_CHAIN" => "Y",
      "AJAX_MODE" => "N",
      "AJAX_OPTION_ADDITIONAL" => "",
      "AJAX_OPTION_HISTORY" => "N",
      "AJAX_OPTION_JUMP" => "N",
      "AJAX_OPTION_STYLE" => "Y",
      "BROWSER_TITLE" => "NAME",
      "CACHE_FILTER" => "N",
      "CACHE_GROUPS" => "Y",
      "CACHE_TIME" => "36000000",
      "CACHE_TYPE" => "A",
      "CHECK_DATES" => "Y",
      "COMPONENT_TEMPLATE" => "siberia",
      "DETAIL_ACTIVE_DATE_FORMAT" => "d/m/Y",
      "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
      "DETAIL_DISPLAY_TOP_PAGER" => "N",
      "DETAIL_FIELD_CODE" => array(
         0 => "",
         1 => "",
      ),
      "DETAIL_PAGER_SHOW_ALL" => "N",
      "DETAIL_PAGER_TEMPLATE" => "siberia",
      "DETAIL_PAGER_TITLE" => "Страница",
      "DETAIL_PROPERTY_CODE" => array(
         0 => "",
         1 => "",
      ),
      "DETAIL_SET_CANONICAL_URL" => "Y",
      "DISPLAY_BOTTOM_PAGER" => "Y",
      "DISPLAY_DATE" => "Y",
      "DISPLAY_NAME" => "Y",
      "DISPLAY_PICTURE" => "Y",
      "DISPLAY_PREVIEW_TEXT" => "Y",
      "DISPLAY_TOP_PAGER" => "N",
      "HIDE_LINK_WHEN_NO_DETAIL" => "N",
      "IBLOCK_ID" => "14",
      "IBLOCK_TYPE" => "news_2",
      "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
      "LIST_ACTIVE_DATE_FORMAT" => "d/m/Y",
      "LIST_FIELD_CODE" => array(
         0 => "",
         1 => "",
      ),
      "LIST_PROPERTY_CODE" => array(
         0 => "",
         1 => "",
      ),
      "MESSAGE_404" => "",
      "META_DESCRIPTION" => "-",
      "META_KEYWORDS" => "-",
      "NEWS_COUNT" => "6",
      "PAGER_BASE_LINK_ENABLE" => "N",
      "PAGER_DESC_NUMBERING" => "N",
      "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
      "PAGER_SHOW_ALL" => "N",
      "PAGER_SHOW_ALWAYS" => "N",
      "PAGER_TEMPLATE" => "",
      "PAGER_TITLE" => "Новости",
      "PREVIEW_TRUNCATE_LEN" => "",
      "SEF_FOLDER" => "/about/".SLANGCODE."/",
      "SEF_MODE" => "Y",
      "SET_LAST_MODIFIED" => "N",
      "SET_STATUS_404" => "N",
      "SET_TITLE" => "N",
      "SHOW_404" => "N",
      "SORT_BY1" => "ACTIVE_FROM",
      "SORT_BY2" => "SORT",
      "SORT_ORDER1" => "DESC",
      "SORT_ORDER2" => "ASC",
      "USE_CATEGORIES" => "N",
      "USE_FILTER" => "N",
      "USE_PERMISSIONS" => "N",
      "USE_RATING" => "N",
      "USE_REVIEW" => "N",
      "USE_RSS" => "N",
      "USE_SEARCH" => "N",
      "USE_SHARE" => "N",
      "SEF_URL_TEMPLATES" => array(
         "news" => "/about/".SLANGCODE."/",
         "section" => "#SECTION_CODE#/",
         "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
      )
   ),
   false
);
?>
<style>
.responsive {
    padding: 0 1.5rem;
}.responsive .item{
	height: 136px;
	box-sizing: border-box;
	cursor: pointer;
	display: block;
	float: left;
	moz-box-sizing: border-box;
	position: relative;
	webkit-box-sizing: border-box;
}.responsive .item .cnt{
	position: absolute;
	left: .5rem;
	right: .5rem;
	top: 0;
}.responsive .item .cnt img{
	margin: 0 auto;
}.responsive .slick-arrow.slick-prev {
    left: 0;
}.responsive .slick-arrow.slick-next {
    right: 0;
}.responsive .slick-arrow {
    font-size: 0;
    color: #646464;
    border: none;
    background: none;
    outline: none;
    position: absolute;
    top: 0;
    bottom: 0;
    margin: auto;
    z-index: 1;
}.responsive .slick-arrow:before {
    content: "\f104";
    font-family: FontAwesome;
    font-size: 30px;
    cursor: pointer;
    z-index: 1;
    margin: -1rem;
}.responsive .slick-next:before {
    content: "\f105";
}
</style>
<script type="text/javascript">
$('.responsive').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 4,
  responsive: [
	{
	  breakpoint: 1024,
	  settings: {
		slidesToShow: 1,
		slidesToScroll: 4,
		infinite: true,
		dots: false
	  }
	},
	{
	  breakpoint: 600,
	  settings: {
		slidesToShow: 1,
		slidesToScroll: 4
	  }
	},
	{
	  breakpoint: 480,
	  settings: {
		slidesToShow: 1,
		slidesToScroll: 4
	  }
	}
	// You can unslick at a given breakpoint now by adding:
	// settings: "unslick"
	// instead of a settings object
  ]
});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>