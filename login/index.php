<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->IncludeComponent("it-invest:system.auth.form","flat",Array(
	 "REGISTER_URL" => "/login?register=yes",
	 "FORGOT_PASSWORD_URL" => "/login?forgot_password=yes",
	 "PROFILE_URL" => "/personal/profile",
	 "SHOW_ERRORS" => "Y" 
	 )
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>