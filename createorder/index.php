<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Оформление заказа");
$APPLICATION->SetTitle("Оформление заказа");
?>












<?$APPLICATION->IncludeComponent(
"bitrix:sale.order.ajax",
    "",
    Array(
        "ADDITIONAL_PICT_PROP_8" => "-",
        "ALLOW_AUTO_REGISTER" => "Y",
        "ALLOW_NEW_PROFILE" => "Y",
        "ALLOW_USER_PROFILES" => "Y",
        "BASKET_IMAGES_SCALING" => "standard",
        "BASKET_POSITION" => "after",
        "COMPATIBLE_MODE" => "Y",
        "DELIVERIES_PER_PAGE" => "8",
        "DELIVERY_FADE_EXTRA_SERVICES" => "Y",
        "DELIVERY_NO_AJAX" => "Y",
        "DELIVERY_NO_SESSION" => "Y",
        "DELIVERY_TO_PAYSYSTEM" => "d2p",
        "DISABLE_BASKET_REDIRECT" => "N",
        "MESS_DELIVERY_CALC_ERROR_TEXT" => "Вы можете продолжить оформление заказа, а чуть позже менеджер магазина свяжется с вами и уточнит информацию по доставке.",
        "MESS_DELIVERY_CALC_ERROR_TITLE" => "Не удалось рассчитать стоимость доставки.",
        "MESS_FAIL_PRELOAD_TEXT" => "Вы заказывали в нашем интернет-магазине, поэтому мы заполнили все данные автоматически. Обратите внимание на развернутый блок с информацией о заказе. Здесь вы можете внести необходимые изменения или оставить как есть и нажать кнопку \"#ORDER_BUTTON#\".",
        "MESS_SUCCESS_PRELOAD_TEXT" => "Вы заказывали в нашем интернет-магазине, поэтому мы заполнили все данные автоматически. Если все заполнено верно, нажмите кнопку \"#ORDER_BUTTON#\".",
        "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
        "PATH_TO_AUTH" => "/auth/",
        "PATH_TO_BASKET" => "basket.php",
        "PATH_TO_PAYMENT" => "payment.php",
        "PATH_TO_PERSONAL" => "index.php",
        "PAY_FROM_ACCOUNT" => "Y",
        "PAY_SYSTEMS_PER_PAGE" => "8",
        "PICKUPS_PER_PAGE" => "5",
        "PRODUCT_COLUMNS_HIDDEN" => array("PROPERTY_MATERIAL"),
        "PRODUCT_COLUMNS_VISIBLE" => array("PREVIEW_PICTURE","PROPS"),
        "PROPS_FADE_LIST_1" => array("17","19"),
        "SEND_NEW_USER_NOTIFY" => "Y",
        "SERVICES_IMAGES_SCALING" => "standard",
        "SET_TITLE" => "Y",
        "SHOW_BASKET_HEADERS" => "N",
        "SHOW_COUPONS_BASKET" => "Y",
        "SHOW_COUPONS_DELIVERY" => "Y",
        "SHOW_COUPONS_PAY_SYSTEM" => "Y",
        "SHOW_DELIVERY_INFO_NAME" => "Y",
        "SHOW_DELIVERY_LIST_NAMES" => "Y",
        "SHOW_DELIVERY_PARENT_NAMES" => "Y",
        "SHOW_MAP_IN_PROPS" => "N",
        "SHOW_NEAREST_PICKUP" => "N",
        "SHOW_ORDER_BUTTON" => "final_step",
        "SHOW_PAY_SYSTEM_INFO_NAME" => "Y",
        "SHOW_PAY_SYSTEM_LIST_NAMES" => "Y",
        "SHOW_STORES_IMAGES" => "Y",
        "SHOW_TOTAL_ORDER_BUTTON" => "Y",
        "SHOW_VAT_PRICE" => "Y",
        "SKIP_USELESS_BLOCK" => "Y",
        "TEMPLATE_LOCATION" => "popup",
        "TEMPLATE_THEME" => "site",
        "USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
        "USE_CUSTOM_ERROR_MESSAGES" => "Y",
        "USE_CUSTOM_MAIN_MESSAGES" => "N",
        "USE_PREPAYMENT" => "N",
        "USE_YM_GOALS" => "N",
        "USER_CONSENT" => "Y",
        "USER_CONSENT_ID" => "1",
        "USER_CONSENT_IS_CHECKED" => "Y",
        "USER_CONSENT_IS_LOADED" => "N"
    )
);?>

<div id="wrap_order">
	<div class="bx-authform">
		<h1 class="b-cart-empty__title">Оформление заказа</h1>
		<form action="/handlers/fast_buy.php" method="post" id="bue_form" class="popupform">
			<input type="hidden" name="antibot" value="">
			<div class="row">
				<label for="name">Name</label>
				<input type="text" name="name" data-required="true" class="input_text" maxlength="128" value="">
			</div>
			<div class="row">
				<label for="name">Second name</label>
				<input type="text" name="second_name" data-required="true" class="input_text" maxlength="128" value="">
			</div>
			<div class="row">
				<label for="email">E-mail</label>
				<input type="email" name="email" data-required="true" class="input_text" maxlength="128" value="">
			</div>
			<div class="row">
				<label for="phone">Phone</label>
				<input type="text" name="phone" data-required="true" class="input_text" maxlength="128" value="">
			</div>
			<div class="row">
				<label for="phone">Company</label>
				<input type="text" name="company" data-required="true" class="input_text" maxlength="128" value="">
			</div>
			<div class="row">
				<label for="comment">Message</label>
				<textarea name="comment" class="text_area" rows="5" maxlength="256"></textarea>
			</div>
			<div class="row">
				<input type="submit" class="siberia_btn" value="Create order">
			</div>
		</form>
	</div>
</div>
<style>
#wrap_order {
    background: #fff;
    box-shadow: 2px 3px 9px #d2d2d2;
    margin-bottom: 2.5rem;
    margin-top: 1.5rem;
    moz-box-shadow: 2px 3px 9px #d2d2d2;
    progid: DXImageTransform.Microsoft.Shadow(color=#000000, direction=0, strength=80);
    webkit-box-shadow: 2px 3px 9px #d2d2d2;
    overflow: hidden;
}h1 {
    line-height: 1;
    text-transform: uppercase;
    font-size: 2.4em;
    margin: 1.5rem;
}.bx-authform-formgroup-container {
    margin-bottom: 2.5rem;
}.bx-authform-content-container, .bx-authform-label-container {
    font-size: 1.8em;
    color: #5a6c77;
    padding-bottom: 0.5rem;
}.bx-authform-input-container input[type="text"], .bx-authform-input-container input[type="password"] {
    display: block;
    width: 100%;
    font-size: 1.8em;
    height: 5rem;
    margin: 0;
    padding: 0 1rem;
    border: 1px solid #000;
    border-radius: 2px;
    background: #fff;
    outline: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}.bx-authform .btn.btn-primary {
    height: 5rem;
    min-width: 15rem;
    text-transform: uppercase;
    font-size: 1.4em;
    line-height: 3rem;
}.bx-authform {
    margin: 2.5rem 0 2.5rem;
    max-width: 50rem;
	padding: 0 1.5rem;
}
</style>
<script>
	function validateFormFastBuy(event)
	{
		event.preventDefault();
		
		var data = {};
		data.fields = [];
		var hasError = false;

		$('#bue_form input, #bue_form textarea').each(function( index ) {
			var self = this;

			if ($( this ).is('[data-required]') && $( this ).data('required') == true) {
				if ($( this ).hasClass('error')) {
					$( this ).removeClass('error');
				}
				if ($( this ).attr('name') == 'name' && $( this ).val().length <= 0) {
					hasError = true;
					$( this ).addClass('error');
				}
				else if ($( this ).attr('name') == 'second_name' && $( this ).val().length <= 0) {
					hasError = true;
					$( this ).addClass('error');
				}
				else if ($( this ).attr('name') == 'email' && $( this ).val().length <= 0) {
					hasError = true;
					$( this ).addClass('error');
				}
				else if ($( this ).attr('name') == 'company' && $( this ).val().length <= 0) {
					hasError = true;
					$( this ).addClass('error');
				}
				else if ($( this ).attr('name') == 'phone' && $( this ).val().length <= 0) {
					hasError = true;
					$( this ).addClass('error');
				}
			}
		});

		if (hasError == false)
		{
			data.fields = $('#bue_form').serializeArray();
			data.product = this.product;
			
			//if ($('[data-add2="buy"]').attr('data-click') == true)
			//{
				$.ajax({
					method: "POST",
					url: '/handlers/fast_buy.php',
					data: data,
					success: function(result){
						window.location = "/catalog/";
					}
				})
			//}
		}
		else
		{
			$('[data-add2="buy"]').attr('data-click', true);
		}
		return false;
	}

	$("#bue_form").on("submit", function (event) {
		event.preventDefault();
		$(this).attr('data-send', false);
		validateFormFastBuy(event);
		return false;
	});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>