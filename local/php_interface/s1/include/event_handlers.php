<?
	AddEventHandler("main", "OnBeforeEventAdd", array("CustomEventHandlers", "onBeforeEventAddHandler"));
	
	class CustomEventHandlers
	{
		function onBeforeEventAddHandler(&$event, &$lid, &$arFields)
		{
			if($event == "USER_PASS_REQUEST"){
				Bitrix\Main\Mail\Event::send(array(
					"EVENT_NAME" => ($USER_LANGUAGE == 'en') ? 'CUSTOM_USER_PASS_REQUEST' : 'CUSTOM_USER_PASS_REQUEST_'.strtoupper(SLANGCODE),
					"LID" => $lid,
					"C_FIELDS" => array(
						"USER_ID" 	=> $arFields["USER_ID"],
						"STATUS" 	=> $arFields["STATUS"],
						"MESSAGE" 	=> $arFields["MESSAGE"],
						"LOGIN" 	=> $arFields["LOGIN"],
						"URL_LOGIN" => $arFields["URL_LOGIN"],
						"CHECKWORD" => $arFields["CHECKWORD"],
						"NAME" 		=> $arFields["NAME"],
						"LAST_NAME" => $arFields["LAST_NAME"],
						"EMAIL" 	=> $arFields["EMAIL"]
					),
				));
			}
			if($event == "USER_PASS_CHANGED"){
				Bitrix\Main\Mail\Event::send(array(
					"EVENT_NAME" => ($USER_LANGUAGE == 'en') ? 'CUSTOM_USER_PASS_CHANGED' : 'CUSTOM_USER_PASS_CHANGED_'.strtoupper(SLANGCODE),
					"LID" => $lid,
					"C_FIELDS" => array(
						"USER_ID" 	=> $arFields["USER_ID"],
						"STATUS" 	=> $arFields["STATUS"],
						"MESSAGE" 	=> $arFields["MESSAGE"],
						"LOGIN" 	=> $arFields["LOGIN"],
						"URL_LOGIN" => $arFields["URL_LOGIN"],
						"CHECKWORD" => $arFields["CHECKWORD"],
						"NAME" 		=> $arFields["NAME"],
						"LAST_NAME" => $arFields["LAST_NAME"],
						"EMAIL" 	=> $arFields["EMAIL"]
					),
				));
			}
		}
	}
?>