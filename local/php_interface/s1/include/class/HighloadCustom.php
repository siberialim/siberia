<?Class HighloadCustom{
	function __construct() {
		\Bitrix\Main\Loader::includeModule('highloadblock');
		use Bitrix\Highloadblock as HL; 
		use Bitrix\Main\Entity; 
	}
	public static function GetList($id, $arrSelect = array(), $arrFilter = array(), $arrSort = array(), $limit = 100){
		$hl = Bitrix\Highloadblock\HighloadBlockTable::getById($id)->fetch();
		$obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hl);
		$strEntityDataClass = $obEntity->getDataClass();		
		$resData = $strEntityDataClass::getList(array(
			'select' => $arrSelect,
			'filter' => $arrFilter,
			'order' => $arrSort,
			'limit' => $limit,
		));
		$arrResult = array();
		while ($arItem = $resData->Fetch()) {
			array_push($arrResult, $arItem);
		};
		return $arrResult;
	}

}