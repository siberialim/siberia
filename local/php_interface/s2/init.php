<?
	session_start();
	
	include('include/event_handlers.php');
	
	define("INCLUDE_PRICES", "N"); // Y || N
	
	CModule::AddAutoloadClasses(
		'', // не указываем имя модуля
		array(
			// ключ - имя класса, значение - путь относительно корня сайта к файлу с классом
			'GarbageStorage' => '/local/php_interface/'.SITE_ID.'/include/class/GarbageStorage.php',
		)
	);	
	/*===========================================================*/
	//Вычислим его по ip(не шутка)
	require_once($_SERVER['DOCUMENT_ROOT']."/geo/geoipcity.inc");
	$gi = geoip_open($_SERVER['DOCUMENT_ROOT']."/geo/GeoLiteCity.dat",GEOIP_STANDARD);
	$record = geoip_record_by_addr($gi,$_SERVER['REMOTE_ADDR']);
	geoip_close($gi);	
	/*===========================================================*/
	//Взависимости от страны определяем ID инфоблоков и языковые файлы
	if($record->country_code && !isset($_SESSION['code_location'])){		
		/*if($record->country_code == 'KR'):
		
			define("SLANGCODE", 'kn');
			define("IBLOCK_CATALOG_ID", 5);
			define("IBLOCK_BANNERS_ID", 8);
			
		elseif($record->country_code == 'CN'):
		
			define("SLANGCODE", 'ch');
			define("IBLOCK_CATALOG_ID", 7);
			define("IBLOCK_BANNERS_ID", 9);
		*/
		if($record->country_code == 'RU'):

			define("SLANGCODE", 'ru');
			define("IBLOCK_CATALOG_ID", 11);
			define("IBLOCK_BANNERS_ID", 1);
			define("ID_WIDGET_BX24", 'loader_2_xwjya9');
			
		else:
		
			define("SLANGCODE", 'en');
			define("IBLOCK_CATALOG_ID", 2);
			define("IBLOCK_BANNERS_ID", 1);
			define("ID_WIDGET_BX24", 'loader_8_oe8wok');
			
		endif;
	}
	else if(isset($_SESSION['code_location'])){
		if($_SESSION['code_location'] == 'kn'):
		
			define("SLANGCODE", 'kn');
			define("IBLOCK_CATALOG_ID", 5);
			define("IBLOCK_BANNERS_ID", 8);
			
		elseif($_SESSION['code_location'] == 'ch'):
		
			define("SLANGCODE", 'ch');
			define("IBLOCK_CATALOG_ID", 7);
			define("IBLOCK_BANNERS_ID", 9);
			
		elseif($_SESSION['code_location'] == 'ru'):
		
			define("SLANGCODE", 'ru');
			define("IBLOCK_CATALOG_ID", 11);	
			define("IBLOCK_BANNERS_ID", 1);
			define("ID_WIDGET_BX24", 'loader_2_xwjya9');
			
		else:
		
			define("SLANGCODE", 'en');
			define("IBLOCK_CATALOG_ID", 2);	
			define("IBLOCK_BANNERS_ID", 1);
			define("ID_WIDGET_BX24", 'loader_8_oe8wok');
			
		endif;
		IncludeTemplateLangFile(__FILE__,SLANGCODE);
		\Bitrix\Main\Localization\Loc::setCurrentLang(SLANGCODE);
	}
	else
	{		
		define("IBLOCK_CATALOG_ID", 2);
		define("IBLOCK_BANNERS_ID", 1);
		define("SLANGCODE", 'en');
		define("ID_WIDGET_BX24", 'loader_8_oe8wok');
	}
	/*===========================================================*/
	//Определяем в какой валюте будут показываться цены
	if(!$_COOKIE["CURRENCY_ID"])
	{
		//if(SLANGCODE == 'kn') define("CURRENCY_ID", 'KRW');
		if(SLANGCODE == 'en') define("CURRENCY_ID", 'USD');
		else if(SLANGCODE == 'ru') define("CURRENCY_ID", 'RUB');
		//else if(SLANGCODE == 'ch') define("CURRENCY_ID", 'CNY');
		else define("CURRENCY_ID", 'USD');
	}
	else
	{
		define("CURRENCY_ID", $_COOKIE["CURRENCY_ID"]);
	}
	
	IncludeTemplateLangFile(__FILE__,SLANGCODE);
	\Bitrix\Main\Localization\Loc::setCurrentLang(SLANGCODE);
?>