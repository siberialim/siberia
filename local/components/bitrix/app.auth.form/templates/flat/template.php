<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */
?>
<div id="app" class="bx-authform">
<?if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']){?>
	<div class="alert alert-danger"><?=ShowMessage($arResult['ERROR_MESSAGE']);?></div>
<?}
	if($_REQUEST['register'] == 'yes'){
		$APPLICATION->IncludeComponent(
			"bitrix:app.auth.registration",
			"flat",
			Array(
				"REGISTER_URL" => "/eshop_app/auth/index.php?register=yes",
				"PROFILE_URL" => "/eshop_app",
				"SHOW_ERRORS" => "Y"
			),
			false
		);
	}
	else if($_REQUEST['forgot_password'] == 'yes'){
		$APPLICATION->IncludeComponent(
			"bitrix:app.auth.forgotpasswd",
			"flat",
			Array(
				"REGISTER_URL" => "/eshop_app/auth/index.php?register=yes",
				"PROFILE_URL" => "/eshop_app",
				"SHOW_ERRORS" => "Y"
			),
			false
		);
	}
else{
	?>
	<script type="text/javascript">
		app.setPageTitle({"title" : "<?=GetMessage("AUTH_PLEASE_AUTH")?>"});
	</script>
	<form name="form_auth" id="auth" method="post" target="_top" action="<?=$arParams["PROFILE_URL"]?>">
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<?if (strlen($arResult["BACKURL"]) > 0):?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		<?foreach ($arResult["POST"] as $key => $value):?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
		<?endforeach?>

		<div class="bx-authform-formgroup-container">
			<div class="bx-authform-label-container"></div>
			<div class="bx-authform-input-container">
				<input type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" placeholder="<?=GetMessage("AUTH_LOGIN")?>" />
			</div>
		</div>
		<div class="bx-authform-formgroup-container">
			<div class="bx-authform-label-container"></div>
				<div class="bx-authform-input-container">
					<?if($arResult["SECURE_AUTH"]):?>
						<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>
						<script type="text/javascript">
						document.getElementById('bx_auth_secure').style.display = '';
						</script>
					<?endif?>
					<input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" placeholder="<?=GetMessage("AUTH_PASSWORD")?>"/>
				</div>
		</div>
		<?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
			<noindex>
				<div class="bx-authform-link-container">
					<a href="<?=$arParams["FORGOT_PASSWORD_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></b></a>
				</div>
			</noindex>
		<?endif?>
		<div class="bx-authform-formgroup-container">
			<input type="submit" class="btn btn-primary" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />
		</div>
		<?if($arResult["CAPTCHA_CODE"]):?>
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<div class="bx-authform-formgroup-container dbg_captha">
				<div class="bx-authform-label-container">
					<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>
				</div>
				<div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
				<div class="bx-authform-input-container">
					<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />
				</div>
			</div>
		<?endif;?>

	<?i/*f ($arResult["STORE_PASSWORD"] == "Y"):?>
	<div class="bx-authform-formgroup-container">
	<div class="checkbox">
	<label class="bx-filter-param-label">
	<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" />
	<span class="bx-filter-param-text"><?=GetMessage("AUTH_REMEMBER_ME")?></span>
	</label>
	</div>
	</div>
	<?endif*/?>
	</form>

	<noindex>
		<div class="bx-authform-link-container">
			<?=GetMessage("AUTH_FIRST_ONE")?><br />
			<a href="<?=$arParams["REGISTER_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_REGISTER")?></b></a>
		</div>
	</noindex>

<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form",
	"flat",
	array(
		"AUTH_SERVICES" => $arResult["~AUTH_SERVICES"],
		"AUTH_URL" => $arParams["PROFILE_URL"],
		"POST" => $arResult["POST"],
	),
	$component,
	array("HIDE_ICONS"=>"Y")
);
?>

<?
}
?>
</div>
<style>
	header{display:none;}
</style>
<script type="text/javascript">
	<?if (strlen($arResult["LAST_LOGIN"])>0):?>
		try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
	<?else:?>
		try{document.form_auth.USER_LOGIN.focus();}catch(e){}
	<?endif?>
</script>



