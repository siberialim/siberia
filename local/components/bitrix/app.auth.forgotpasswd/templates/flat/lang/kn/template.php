<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "비밀번호를 잊어버리면 아이디나 이메일을 입력하세요. 고객님의 계정정보와 비밀번호 재설정 링크를 이메일로 보내드리겠습니다.";
$MESS ['AUTH_GET_CHECK_STRING'] = "비밀번호 재설정 링크를 보내기";
$MESS ['AUTH_SEND'] = "보내기";
$MESS ['AUTH_AUTH'] = "로그인";
$MESS["AUTH_LOGIN_EMAIL"] = "아이디나 이메일";
$MESS["REQUEST_NEW_PASS"] = "비밀번호 변경";
?>