<?
$MESS["CATALOG_COMPARE"] = "Compare";
$MESS["CATALOG_IN_COMPARE"] = "Compare";
$MESS["CATALOG_ADD"] = "바구니에 넣기";
$MESS["CATALOG_ADD_TITLE"] = "바구니에 넣기";
$MESS["CATALOG_MORE"] = "Details";
$MESS["CR_TITLE_SALELEADER"] = "Most Popular";
$MESS["CR_TITLE_NEWPRODUCT"] = "Just In";
$MESS["CR_TITLE_SPECIALOFFER"] = "Special Offers";
$MESS["CR_TITLE_NULL"] = "You don't have any top seller.<br>To make a product show as a top seller, open it for editing and mark the \"Top Seller\" checkbox.";
$MESS["CR_PRICE_OT"] = "from&nbsp;";
$MESS["CATALOG_SUBSCRIBE"] = "Notify when available";
$MESS["CATALOG_IN_SUBSCRIBE"] = "Subscribed";
$MESS["CATALOG_IN_CART"] = "상품이 장바구니에 담겨있습니다";
$MESS["CATALOG_ADD_TO_CART"] = "바구니에 넣기";
$MESS["CATALOG_IN_CART_DELAY"] = "Saved for later";
$MESS["CATALOG_OFFER_NAME"] = "Name";
$MESS["EMPTY_VALUE_SKU"] = "Undefined";
$MESS["CATALOG_NOT_AVAILABLE"] = "Unavailable";
$MESS["CATALOG_NOT_AVAILABLE2"] = "Out of stock";
$MESS["CATALOG_CHOOSE"] = "Select";
$MESS["CATALOG_COUNT"] = "Number";
$MESS["MB_PULLDOWN_PULL"] = "Pull down to refresh";
$MESS["MB_PULLDOWN_DOWN"] = "Release to refresh";
$MESS["MB_PULLDOWN_LOADING"] = "Updating data...";
$MESS["MB_PRICE"] = "가격";
$MESS["PROP_ING"] = "성분";
$MESS["PROP_PACK"] = "포장";
$MESS["PROP_HP"] = "유통 기한";
$MESS["PROP_WEIGHT"] = "무게";
$MESS["PROP_NUTR"] = "제품 100그램당";
?>