<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>

<?if(count($arResult["ITEMS"]) > 0): ?>
	<?
	//$notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
	//$arNotify = unserialize($notifyOption);
	?>

<div class="main_catalog">
<?
$i = 0;
foreach($arResult["ITEMS"] as $key => $arItem):
	if(is_array($arItem))
	{
		$bPicture = is_array($arItem["PREVIEW_IMG"]);
		?>
	<div class="main_catalog_item<?echo ($i >= count($arResult["ITEMS"]) - 1) ? ' last_item' : '';?>">
		<?/*if($arParams["DISPLAY_COMPARE"]):?>
		<noindex>
			<?if(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"])):?>
				<span class="checkbox">
					<a href="javascript:void(0)" onclick="return showOfferPopup(this, 'list', '<?=Loc::getMessage("CATALOG_IN_CART")?>', <?=CUtil::PhpToJsObject($arItem["SKU_ELEMENTS"])?>, <?=CUtil::PhpToJsObject($arItem["SKU_PROPERTIES"])?>, <?=CUtil::PhpToJsObject($arResult["POPUP_MESS"])?>, 'compare');">
						<input type="checkbox" class="addtoCompareCheckbox"/><span class="checkbox_text"><?=Loc::getMessage("CATALOG_COMPARE")?></span>
					</a>
				</span>
			<?else:?>
				<span class="checkbox">
					<a href="<?echo $arItem["COMPARE_URL"]?>" rel="nofollow" onclick="return addToCompare(this, 'list', '<?=Loc::getMessage("CATALOG_IN_COMPARE")?>', '<?=$arItem["DELETE_COMPARE_URL"]?>');" id="catalog_add2compare_link_<?=$arItem['ID']?>">
						<input type="checkbox" class="addtoCompareCheckbox"/><span class="checkbox_text"><?=Loc::getMessage("CATALOG_COMPARE")?></span>
					</a>
				</span>
			<?endif?>
		</noindex>
		<?endif*/?>
		<?if ($bPicture):?>
			<div class="detail_item_img_container">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="main_catalog_item_img"><img src="<?=$arItem["PREVIEW_IMG"]["SRC"]?>"  alt="<?=$arElement["NAME"]?>" /></a>
			</div>
		<?endif?>
		<div class="data_item">
			<h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a></h2>
			<?if($arParams['SHOW_PRICES'] == 'Y'){?>
				<div class="main_catalog_item_price">
				<?if(!is_array($arItem["OFFERS"]) || empty($arItem["OFFERS"])):?>
					<?
						$numPrices = count($arParams["PRICE_CODE"]);
						foreach($arItem["PRICES"] as $code=>$arPrice):
							if($arPrice["CAN_ACCESS"]):?>
								<?if ($numPrices>1):?><p style="padding: 0; margin-bottom: 5px;"><?=$arResult["PRICES"][$code]["TITLE"];?>:</p><?endif?>
								<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
									<div class="price">
										<div class="main_price_container oldprice"><?=Loc::getMessage("MB_PRICE")?>:
											<span class="item_price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
											<span class="item_price_old"><?=$arPrice["PRINT_VALUE"]?></span>
										</div>
									</div>
								<?else:?>
									<div class="main_price_container">
										<span class="item_price"><?=$arPrice["PRINT_VALUE"]?></span>
									</div>
								<?endif;
							endif;
						endforeach;
					?>
				<?endif?>
				</div>
			<?}?>
			<!--<div class="detail_item_count"><?//=Loc::getMessage("CATALOG_COUNT")?>:
				<a href="javascript:void(0)" class="count_minus" id="count_minus" ontouchstart="if (BX('item_quantity').value > 1) BX('item_quantity').value--;"><span>-</span></a><!--
				--><!--<input type="number" id="item_quantity" name="<?//echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1"><!--
				--><!--<a href="javascript:void(0)" class="count_plus" id="count_plus" ontouchstart="BX('item_quantity').value++;"><span>+</span></a>
			</div>-->
		</div>
		<div class="clear"></div>
		<div class="item_info_section">
			<ul class="props">
				<?if(isset($arItem['PROPERTIES']['SHELF_LIFE']['VALUE']) && !empty($arItem['PROPERTIES']['SHELF_LIFE']['VALUE'])){?>
				<li class="prop"><span><?=Loc::getMessage("PROP_HP")?>:</span>
				<span><?=$arItem['PROPERTIES']['SHELF_LIFE']['VALUE']?></span>
				</li>
				<?}?>
				<?if(isset($arItem['PROPERTIES']['WEIGHT']['VALUE']) && !empty($arItem['PROPERTIES']['WEIGHT']['VALUE'])){?>
				<li class="prop"><span><?=Loc::getMessage("PROP_WEIGHT")?>:</span>
				<span><?=$arItem['PROPERTIES']['WEIGHT']['VALUE']?></span>
				</li>
				<?}?>
			</ul>
		</div>
		<?if ($arItem["CAN_BUY"]):?>
			<div class="wrap_control_btn">
				<noindex>
				<a href="<?=$arItem["ADD_URL"]?>"
					class="main_item_buy"
					rel="nofollow"
					onclick="
						BX.addClass(BX.findParent(this, {class : 'main_catalog_item'}, false), 'add2cart');//	setTimeout('BX.removeClass(obj, \'add2cart\')', 3000);
						return addItemToCart(this);"
					id="catalog_add2cart_link_<?=$arItem['ID']?>">
					<?=Loc::getMessage("CATALOG_ADD_TO_CART")?>
				</a>
				<a href="<?=$arParams["BASKET_URL"]?>" class="main_catalog_item_cartlink" ontouchstart="BX.toggleClass(this, 'active');" ontouchend="BX.toggleClass(this, 'active');"><?=Loc::getMessage("CATALOG_IN_CART")?></a>
				</noindex>
			</div>
			<?/*elseif ($arNotify[SITE_ID]['use'] == 'Y'):?>
				<?if ($USER->IsAuthorized()):?>
					<noindex><a href="<?echo $arItem["SUBSCRIBE_URL"]?>" rel="nofollow" class="subscribe_link" onclick="return addToSubscribe(this, '<?=Loc::getMessage("CATALOG_IN_SUBSCRIBE")?>');" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?echo Loc::getMessage("CATALOG_SUBSCRIBE")?></a></noindex>
				<?else:?>
					<noindex><a href="javascript:void(0)" rel="nofollow" class="subscribe_link" onclick="showAuthForSubscribe(this, <?=$arItem['ID']?>, '<?echo $arItem["SUBSCRIBE_URL"]?>')" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?echo Loc::getMessage("CATALOG_SUBSCRIBE")?></a></noindex>
				<?endif;?>
			<?*/?>
		<?endif?>
	</div>
<?
	}
	$i+=1;
endforeach;
?>
</div>
<?elseif($USER->IsAdmin()):?>
	<div class="empty_list_text">
		<span class="hitsale tac"><span></span><?//=Loc::getMessage("CR_TITLE_".$arParams["FLAG_PROPERTY_CODE"])?></span>
		<div class="listitem-carousel tac">
			<?//=Loc::getMessage("CR_TITLE_NULL")?>
		</div>
	</div>
<?endif;?>
