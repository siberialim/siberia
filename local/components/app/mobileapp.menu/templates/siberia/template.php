<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->SetPageProperty("BodyClass","menu-page");
$APPLICATION->AddHeadString("
<style type=\"text/css\">
html { -webkit-text-size-adjust:none;}

.menu-page {
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	margin:0;
	padding:0;
	background:#fff;
}

.menu-items {-webkit-tap-highlight-color: transparent;}

.menu-separator {
	background-repeat: repeat-x;
	height: 44px;
	line-height: 44px;
	border-bottom: 1px solid #c1c4bd;
	color: #fff;
	font-size: 15px;
	padding-left: 15px;
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#343434), to(#3f4040));
	background-image: -webkit-linear-gradient(#343434 0%, #3f4040 100%);
	background-image: linear-gradient(#343434 0%, #3f4040 100%);
	text-transform: uppercase;
	background:#71a53b;
	font-weight:bold;
}

.menu-item-title {
	text-align: center;
	color: #fff !important;
	font-weight: bold;
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(114,114,114,1)), color-stop(51%,rgba(68,68,68,1)), color-stop(100%,rgba(68,68,68,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgba(114,114,114,1) 0%,rgba(68,68,68,1) 51%,rgba(68,68,68,1) 100%); /* Chrome10+,Safari5.1+ */
	background: linear-gradient(to bottom, rgba(114,114,114,1) 0%,rgba(68,68,68,1) 51%,rgba(68,68,68,1) 100%); /* W3C */
}
.menu-item, .menu-item a{
	height: 44px;
	line-height: 42px;
	font-size: 14px;
	color: #000;
	padding: 0;
	-webkit-tap-highlight-color: transparent;
	border-bottom: 1px solid #c1c4bd;
	text-shadow:0 1px 0 #2e2f30;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	position: relative;
	background:#fff;
	text-shadow:none;
	text-decoration:none;
}
.menu-section-groups .menu-item { padding-left: 15px; padding-right: 15px;}

.menu-item:after {
	display: block;
	height: 2px;
	/*background-image: url(images/menu/menu-item-border.png);*/
	background-repeat: repeat-x;
	background-size: 1px 2px;
	width: 100%;
	position: absolute;
	content: '';
	left: 0;
	z-index: -1;
}

.menu-item:last-child:after { background: none; }
.menu-item:before { content: ''; position: absolute; left: 0; height: 44px; width: 60px; }

.menu-item-avatar {
	background-color: #63839c;
	/*background-image: url(images/menu/menu-avatar.png);*/
	background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAIAAACR5s1WAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoxQzAzNTk0Mjk4MjM2ODExOTIzQ0UzMzY5M0ZCNjAwQSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDowRUEzRkQ1MkNEM0UxMUUxQTZCOURCRkUzNzFFQURBMyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5Q0FGN0U5Q0NEMjkxMUUxQTZCOURCRkUzNzFFQURBMyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IE1hY2ludG9zaCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkFFMUExODRBNzQyMDY4MTE4OEM2OEExMTQ2OTFCMDNEIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjFDMDM1OTQyOTgyMzY4MTE5MjNDRTMzNjkzRkI2MDBBIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+dqo5jQAABQRJREFUeNrsWF1PHFUYPl8zszM7sNgPKBawlCpuV0ArYBqVWJNe6ZXx3gt/in/DG+OF8UpNaqKmJFxoiDVBLQUF+mER+mEplGW3uztz5hzfM2dZkd0dZmCJXvBm9iOzM3Oe87zved7nLP7wo4/Rfx0E/Q/iCMR2sH3cg6sf+F9npax+HjYIHI5de9ffqhAkhhcgwCEaeUggqmNjTDAiuBZVFgCAUCikEEhDkS0HUUNAcfgpBRJqODVvDQSwKXhQZkRsUyIPg4nqtLn3Ql/X2+ND2XPPMUrh1NrG5tzS3clrs2v5MiYUql0kyUhsEDBNoggQ3H/39ZF33npNz1PRgdCxTNvE2ND48OCnX03+srSCqKHIik0GScIBkoK7Jr78xqsBDB4WQe2AM6bBPnjvsmMRJDmkCLdcJ3QdBL5/aTRHQ0bqDwj4aeLlwcDzAC/GuKXpCFcBDBP4lRMdbUEQRFx7pqcLLqOmBflTNRsjISzu0pAhCO5BYQRhHTQLk1EAAawRwnC88oy/OlRNnuk+mRs8J4IoEH2nu/t7ulY2ypjKltVETREF56eOd0AuRGTA753PZOBiTQJuIRMKRBB4vg8gotedLmG4Gl6gYK1hQm7nVWL8842Few8eAhURx1axODO3WBMr2cIlGqoVK/HgyndTivHmxzdXp/LFMiVU61XrdEL1SMwMw7DSM7/dfLyxHjSJJ5v5q9Mzhp0mhgVNBcXrHglMDTBh2g427U8+/1JL5K4DTn72xRViOJadpowdgmIqEDA923Yzd+6vf/3tZD0NU99Pz99Zddo7jFQK2phSqniqyWK6GNXEQacoJXYatWfWnuTDmcudiwLWg5VyUrZNDQsWBqqJ2l42h+3p4UIXo1po6BckvGzLHH9lWKvCThATF8csJ/3T78ue4CCX4HyIREIduGq8EoHA/wAICVBnJAq4xfDoyMDw88pG+D7f9VjANDaUHR16cXbx7g+zt0pcEsqU2VEFFGW3WITmEKIzKmTA2x1jLHs2N9AHRcQDXqlwUddBggADMshYbqD3/EDv7NLytfnbBU/oiUCC5A4/HAOEJkHpVJC2yJsXBrP9PTC2V6loxYwQzfBOj1KaO/vsSwM909eXflz4ExEGLVU0kS8WYWGE5Ibk71+66FhmoVjcc/j6WUDWxnP9m1uF+ZV1wiz13GRMAAm+dyHbaxDytPg0un03Cx9zn/Ox8/2/3ly10owQGjRyGE0LEzoQr5RPdjilcinaxUQHJDFlWdwrGSlbLRkUk4nQR6mezCuQRt/3QREOsstTXYeoJkyYTKYToY8CMj3OWZKNTIOA0cFeECSSiJUqPhx6aFQqe04qdWAQQnuMZrLJmnkIvaOaW7w9PpLVO5z9IghuLN4q+SIN+imTKKaiAkrZSs3/8eD6/MLpE5nuU50d7e2uY4PhdV037TgNbyyVyvmtPMw7v1XIF4r3Hz6699djZLnusS5oac0YZQ1pCHeYlJmOnTleMYzVzcLyoyUoESgTlSW1XKV+313PwB8JWw1l0M2pYRqZTst2TdsFvZJNdgBRsk2ZkXLbDFhgaTCusEZArIJw1612OnjXE7HqDEroiQqYAwAhjDHDBAdAoYnAyUS9Q24bCEosQk1mKeOK9L4fh49q3JzV3i8Eh/TOByuTR2quIlkX3XmDbqVIm5T6Xt/olvr/buQBLb+se+i+/xY6+vfuCETc+FuAAQCMJERFG/TKEQAAAABJRU5ErkJggg==');
	background-repeat: no-repeat;
	background-size: 22px 22px;
	border: 1px solid #fff;
	width: 22px;
	height: 22px;
	position: absolute;
	top: 9px;
	left: 21px;
}
.menu-item-selected {
	background: #cdcdcd;
	height: 46px;
	margin-top: -2px;
	line-height: 46px;
	border-bottom: 1px solid #c1c4bd;
}
.menu-item-selected:after { background: none;}
.menu-item-selected:before { height: 48px;}

.notis{
	background:#eaeaea;
	color:#000;
	padding: 5px;
	float:right;
	margin:10px 0 0 0;
	border-radius:3px;
	vertical-align:middle;
	line-height: 1;
}
</style>");

/*			array(
				"text" => Loc::getMessage("MOBILEAPP_ADMIN"),
				"type" => "item",
				"sort" => "3"$arResult['MENU_TITLE']
			),
*/
if ($USER->IsAuthorized())
{
	$USER_ID = $USER->GetID();

	$arResult["USER_FULL_NAME"] = CUser::FormatName("#NAME# #LAST_NAME#", array(
		"NAME"	 => $USER->GetFirstName(),
		"LAST_NAME" 	=> $USER->GetLastName(),
		"SECOND_NAME" => 	$USER->GetSecondName(),
		"LOGIN" => $USER->GetLogin()
	));

	$arResult["USER"] = $USER->GetByID($USER_ID)->GetNext();
}
?>
<div class="menu-items" id="menu-items">
	<div class="menu-section  menu-section-groups" id="auth_block">
		<?if (is_array($arResult["USER"])):?>
		<div id="user_name" class="menu-item"><?=$arResult['USER']['NAME']?> <?=$arResult['USER']['LAST_NAME']?>
		<?if ($USER->IsAuthorized()){?>
		<!--<div class="menu-section  menu-section-groups" id="notice_block">-->
		<a href="/eshop_app/?logout=yes" style="float:right"><?=Loc::getMessage("MB_EXIT")?></a>
		<!--</div>-->
		<?}?>
		</div>

		<?else:?>
		<div class="menu-item" id="auth" data-url="<?=SITE_DIR?>auth/"><?=Loc::getMessage("MB_AUTH");?></div>
		<?endif?>
	</div>
	<!--<div class="menu-section  menu-section-groups" id="notice_block">
		<div class="menu-item" id="notice" data-url="<?=SITE_DIR?>eshop_app/chat/list.php"><?=Loc::getMessage("MB_MESS");?> <!--<span class="notis">01</span>--><!--</div>
	</div>-->
<?

$htmlMenu = "";

foreach ($arResult["MENU"] as $arMenuSection)
{
	if(!isset($arMenuSection['type']) && $arMenuSection['type'] != "section")
		continue;

	$htmlMenu .= '<div class="menu-separator">'.(isset($arMenuSection['text']) ? $arMenuSection['text'] : '').'</div>';

	if(!isset($arMenuSection['items']) || !is_array($arMenuSection['items']))
		continue;

	$htmlMenu .= '<div class="menu-section menu-section-groups">';

	foreach ($arMenuSection['items'] as $arMenuItem)
	{
		$htmlMenu .= '<div class="menu-item';

		if(isset($arMenuItem["class"]))
			$htmlMenu .= ' '.$arMenuItem["class"];

		$htmlMenu .= '"';

		foreach ($arMenuItem as $attrName => $attrVal)
		{
			if($attrName == 'text' || $attrName == 'type' || $attrName == 'class')
				continue;

			$htmlMenu .= ' '.$attrName.'="'.$attrVal.'"';
		}

		$htmlMenu .= '>';

		if(isset($arMenuItem['text']))
			$htmlMenu .= $arMenuItem['text'];

		$htmlMenu .= '</div>';
	}

	$htmlMenu .= '</div>';
}

echo $htmlMenu;
?>
</div>

<script type="text/javascript">

	//кастомное событие обновляет экран слайдера
	BX.addCustomEvent("initSlider", function(data) {
		//перегружаем текущий экран
		BXMobileApp.UI.Page.reload();
	});

	BX.addCustomEvent("onAuthSuccess", function(data) {
		BX.remove(BX("auth"));
		if (BX("user_name"))
			BX.remove(BX("user_name"));
		var user_div = BX.create('DIV', {
			props: {
				id :  "user_name"
			},
			html : data.user_name ,
			attrs : {
				class : "menu-item"
			}
		});
		BX('auth_block').appendChild(user_div);
		BX('logout_block').style.display = "block";
		//app.removeAllCache();
		if (data.open_left)
			app.openLeft();
		
	});

	function logout()
	{
		
		BX.ajax({
		//	timeout:   30,
			method:   'POST',
			url: '<?=SITE_DIR?>eshop_app/?logout=yes',
			processData: false,
			onsuccess: function(reply){
				BX.remove(BX("user_name"));
				var auth_div = BX.create('DIV', {
					props: {
						id :  "auth"
					},
					html : '<?=Loc::getMessage("MB_AUTH");?>',
					attrs : {
						class : "menu-item",
						'data-url' : "/eshop_app/auth/"
					}
				});
				BX('auth_block').appendChild(auth_div);
				BX('logout_block').style.display = "none";
				app.removeAllCache();
				app.loadPage("<?=SITE_DIR."eshop_app/"?>");
			}
		});
		return false;
	}

	function catalogSections()
	{
		app.openBXTable({
			url: '/eshop_app/catalog/sections.php',
			isroot: true,
			TABLE_SETTINGS : {
				cache : true,
				use_sections : true,
				searchField : false,
				showtitle : true,
				name : '<?=Loc::getMessage("MB_SECTIONS")?>',
				button:
				{
					type:    'basket',
					style:   'custom',
					callback: function()
					{
						app.openNewPage('<?=SITE_DIR?>eshop_app/personal/cart/');
					}
				}
			}
		});
		app.closeMenu();
	}


	document.addEventListener("DOMContentLoaded", function() {
		Menu.init(null);
	}, false);

	Menu = {
		currentItem : null,

		init : function(currentItem)
		{
			this.currentItem = currentItem;
			var items = document.getElementById("menu-items");
			var that = this;
			items.addEventListener("click", function(event) {that.onItemClick(event); }, false);
		},

		onItemClick : function(event)
		{
			var target = event.target;
			if (target && target.nodeType && target.nodeType == 1 && BX.hasClass(target, "menu-item"))
			{
				if (this.currentItem != null)
					this.unselectItem(this.currentItem);
				this.selectItem(target);

				var url = target.getAttribute("data-url");
				var pageId = target.getAttribute("data-pageid");

				if(BX.type.isNotEmptyString(url) && BX.type.isNotEmptyString(pageId))
					app.loadPage(url, pageId);
				else if(BX.type.isNotEmptyString(url))
					app.loadPage(url);

				this.currentItem = target;
			}

		},

		selectItem : function(item)
		{
			if (!BX.hasClass(item, "menu-item-selected"))
				BX.addClass(item, "menu-item-selected");
		},

		unselectItem : function(item)
		{
			BX.removeClass(item,"menu-item-selected");
		},

		logOut :  function()
		{
			if(app.enableInVersion(2))
			{
				app.asyncRequest({ url:"<?=$arResult['LOGOUT_REQUEST_URL']?>"+"&uuid="+device.uuid});
				return app.exec("showAuthForm");
			}
		}
	}
</script>
