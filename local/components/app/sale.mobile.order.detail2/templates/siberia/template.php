<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);
	
CJSCore::Init(array('clipboard'));
$screenTitle = '#'.$arResult['ID'];

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach ($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}

	$component = $this->__component;

	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}
}
else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach ($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	?>

	<div class="container-fluid sale-order-detail">
		<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-general">

			<div class="row sale-order-detail-about-order">

				<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-title">
							<h3 class="sale-order-detail-about-order-title-element">
								<?= Loc::getMessage('SPOD_LIST_ORDER_INFO') ?>
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-inner-container">
							<div class="row">
								<div class="col-md-4 col-sm-6 sale-order-detail-about-order-inner-container-name">
									<div class="sale-order-detail-about-order-inner-container-name-title">
										<?
										$userName = $arResult["USER"]["NAME"] ." ". $arResult["USER"]["SECOND_NAME"] ." ". $arResult["USER"]["LAST_NAME"];
										if (strlen($userName) || strlen($arResult['FIO']))
										{
											echo Loc::getMessage('SPOD_LIST_FIO').':';
										}
										else
										{
											echo Loc::getMessage('SPOD_LOGIN').':';
										}
										?>
									</div>
									<div class="sale-order-detail-about-order-inner-container-name-detail">
										<?
										if (strlen($userName))
										{
											echo htmlspecialcharsbx($userName);
										}
										elseif (strlen($arResult['FIO']))
										{
											echo htmlspecialcharsbx($arResult['FIO']);
										}
										else
										{
											echo htmlspecialcharsbx($arResult["USER"]['LOGIN']);
										}
										?>
									</div>
				
								</div><!--
								--><div class="col-md-4 col-sm-6 sale-order-detail-about-order-inner-container-status">
									<div class="sale-order-detail-about-order-inner-container-status-title">
										<?= Loc::getMessage('SPOD_LIST_CURRENT_STATUS', array(
											'#DATE_ORDER_CREATE#' => $arResult["DATE_INSERT_FORMATED"]
										)) ?>
									</div>
									<div class="sale-order-detail-about-order-inner-container-status-detail">
										<?
										if ($arResult['CANCELED'] !== 'Y')
										{
											echo Loc::getMessage('STATUS_'.$arResult["STATUS"]["ID"]);
										}
										else
										{
											echo Loc::getMessage('SPOD_ORDER_CANCELED');
										}
										?>
									</div>
								</div><!--
								--><!--<div class="col-md-2 col-sm-6 sale-order-detail-about-order-inner-container-price">
									<div class="sale-order-detail-about-order-inner-container-price-title">
										<?//= Loc::Loc::getMessage('SPOD_ORDER_PRICE')?>:
									</div>
									<div class="sale-order-detail-about-order-inner-container-price-detail">
										<?//= $arResult["PRICE_FORMATED"]?>
									</div>
								</div>-->
								<div class="control-detail-order">
									<a class="sale-order-detail-about-order-inner-container-name-read-less">
										<?= Loc::getMessage('SPOD_LIST_LESS') ?>
									</a>
									<a class="sale-order-detail-about-order-inner-container-name-read-more">
										<?= Loc::getMessage('SPOD_LIST_MORE') ?>
									</a>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-inner-container-details">
								<h4 class="sale-order-detail-about-order-inner-container-details-title">
									<?= Loc::getMessage('SPOD_USER_INFORMATION') ?>
								</h4>
								<ul class="sale-order-detail-about-order-inner-container-details-list">
									<?
									if (strlen($arResult["USER"]["EMAIL"]))
									{
										?>
										<li class="sale-order-detail-about-order-inner-container-list-item">
											<?= Loc::getMessage('SPOD_LOGIN')?>:<?= htmlspecialcharsbx($arResult["USER"]["LOGIN"]) ?>
										</li>
										<?
									}
									if (strlen($arResult["USER"]["EMAIL"]))
									{
										?>
										<li class="sale-order-detail-about-order-inner-container-list-item">
											<?= Loc::getMessage('SPOD_EMAIL')?>:
											<a class="sale-order-detail-about-order-inner-container-list-item-link"
											   href="mailto:<?= htmlspecialcharsbx($arResult["USER"]["EMAIL"]) ?>"><?= htmlspecialcharsbx($arResult["USER"]["EMAIL"]) ?></a>
										</li>
										<?
									}
									/*if (strlen($arResult["USER"]["EMAIL"]))
									{
										?>
										<li class="sale-order-detail-about-order-inner-container-list-item">
											<?= Loc::Loc::getMessage('SPOD_ORDER_PERS_TYPE') ?>:<?= htmlspecialcharsbx($arResult["PERSON_TYPE"]["NAME"]) ?>
										</li>
										<?
									}*/
									if (isset($arResult["ORDER_PROPS"]))
									{
										foreach ($arResult["ORDER_PROPS"] as $property)
										{
											?>
											<li class="sale-order-detail-about-order-inner-container-list-item">
												<?= htmlspecialcharsbx($property['NAME']) ?>:
												<div class="sale-order-detail-about-order-inner-container-list-item-element">
													<?
													if ($property["TYPE"] == "Y/N")
													{
														echo Loc::getMessage('SPOD_' . ($property["VALUE"] == "Y" ? 'YES' : 'NO'));
													}
													else
													{
														if ($property['MULTIPLE'] == 'Y' && $property['TYPE'] !== 'FILE')
														{
															$propertyList = unserialize($property["VALUE"]);
															foreach ($propertyList as $propertyElement)
															{
																echo $propertyElement . '</br>';
															}
														}
														else
														{
															echo $property["VALUE"];
														}
													}
													?>
												</div>
											</li>
											<?
										}
									}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row sale-order-detail-payment-options">
				<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-payment-options-container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-payment-options-title">
							<h3 class="sale-order-detail-payment-options-title-element"><?//= Loc::Loc::getMessage('SPOD_ORDER_PAYMENT') ?></h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-payment-options-inner-container">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-payment-options-info">
									<div class="row">
										<div class="col-md-11 col-sm-10 col-xs-10 sale-order-detail-payment-options-info-container">
											<div class="sale-order-detail-payment-options-info-order-number"><b>
												<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
													"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
													"#DATE_ORDER_CREATE#"=> $arResult["DATE_INSERT_FORMATED"]
												))?>
												<?
												if ($arResult['CANCELED'] !== 'Y')
												{
													echo Loc::getMessage('STATUS_'.$arResult["STATUS"]["ID"]);
												}
												else
												{
													echo Loc::getMessage('SPOD_ORDER_CANCELED');
												}
												?>
											</b></div>
											<!--<div class="sale-order-detail-payment-options-info-total-price">
												<?//=Loc::Loc::getMessage('SPOD_ORDER_PRICE_FULL')?>:
												<span><?//=$arResult["PRICE_FORMATED"]?></span>
											</div>-->
										</div>
									</div>
								</div><!--sale-order-detail-payment-options-info-->
							</div>
						</div>
					</div>
				</div>
				<div class="row sale-order-detail-payment-options-order-content">
					<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-payment-options-order-content-container">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-payment-options-order-content-title">
								<h3 class="sale-order-detail-payment-options-order-content-title-element"><?= Loc::getMessage('SPOD_ORDER_BASKET')?></h3>
							</div>
							<div class="sale-order-detail-order-section bx-active">
								<div class="sale-order-detail-order-section-content container-fluid">
									<div class="sale-order-detail-order-table-fade sale-order-detail-order-table-fade-right">
										<div style="width: 100%; overflow-x: auto; overflow-y: hidden;">
											<div class="sale-order-detail-order-item-table">
												<?
												foreach ($arResult['BASKET'] as $basketItem)
												{
													?>
													<div class="sale-order-detail-order-item-tr sale-order-detail-order-basket-info sale-order-detail-order-item-tr-first">
														<div class="sale-order-detail-order-item-td" style="min-width: 300px;">
															<div class="sale-order-detail-order-item-block">
																<div class="sale-order-detail-order-item-img-block">
																	<a href="<?=$basketItem['DETAIL_PAGE_URL']?>">
																		<?
																		if (strlen($basketItem['PICTURE']['SRC']))
																		{
																			$imageSrc = $basketItem['PICTURE']['SRC'];
																		}
																		else
																		{
																			$imageSrc = $this->GetFolder().'/images/no_photo.png';
																		}
																		?>
																		<div class="sale-order-detail-order-item-imgcontainer"
																			 style="background-image: url(<?=$imageSrc?>);
																				 background-image: -webkit-image-set(url(<?=$imageSrc?>) 1x,
																				 url(<?=$imageSrc?>) 2x)">
																		</div>
																	</a>
																</div>
																<div class="sale-order-detail-order-item-content">
																	<div class="sale-order-detail-order-item-title">
																		<a href="<?=$basketItem['DETAIL_PAGE_URL']?>">
																			<?=htmlspecialcharsbx($basketItem['NAME'])?>
																		</a>
																	</div>
																	<?
																	if (isset($basketItem['PROPS']) && is_array($basketItem['PROPS']))
																	{
																		foreach ($basketItem['PROPS'] as $itemProps)
																		{
																			?>
																			<div class="sale-order-detail-order-item-color">
																			<span class="sale-order-detail-order-item-color-name">
																				<?=htmlspecialcharsbx($itemProps['NAME'])?>:</span>
																				<span class="sale-order-detail-order-item-color-type">
																				<?=htmlspecialcharsbx($itemProps['VALUE'])?></span>
																			</div>
																			<?
																		}
																	}
																	?>
																</div>
															</div>
														</div>

														<div class="sale-order-detail-order-item-nth-4p1"></div>
														<div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right">
															<div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm">
																<?= Loc::getMessage('SPOD_QUANTITY')?>
															</div>
															<div class="sale-order-detail-order-item-td-text">
															<span><?=$basketItem['QUANTITY']?>&nbsp;
																<?
																if (strlen($basketItem['MEASURE_NAME']))
																{
																	echo htmlspecialcharsbx($basketItem['MEASURE_NAME']);
																}
																else
																{
																	echo Loc::getMessage('SPOD_DEFAULT_MEASURE');
																}
																?></span>
															</div>
														</div>
														<!--<div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right">
															<div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm"><?=Loc::getMessage('SPOD_ORDER_PRICE')?></div>
															<div class="sale-order-detail-order-item-td-text">
																<strong class="bx-price all"><?//=$basketItem['FORMATED_SUM']?></strong>
															</div>
														</div>-->
													</div>
													<?
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row sale-order-detail-total-payment">
					<div class="col-md-7 col-md-offset-5 col-sm-12 col-xs-12 sale-order-detail-total-payment-container">
						<div class="row">
							<ul class="sale-order-detail-total-payment-list-left">
								<?
								if (floatval($arResult["ORDER_WEIGHT"]))
								{
									?>
									<li class="sale-order-detail-total-payment-list-left-item">
										<?= Loc::getMessage('SPOD_TOTAL_WEIGHT')?>:
									</li>
									<?
								}

								/*if ($arResult['PRODUCT_SUM_FORMATED'] != $arResult['PRICE_FORMATED'] && !empty($arResult['PRODUCT_SUM_FORMATED']))
								{
									?>
									<li class="sale-order-detail-total-payment-list-left-item">
										<?= Loc::Loc::getMessage('SPOD_COMMON_SUM')?>:
									</li>
									<?
								}*/

								/*if (strlen($arResult["PRICE_DELIVERY_FORMATED"]))
								{
									?>
									<li class="sale-order-detail-total-payment-list-left-item">
										<?= Loc::Loc::getMessage('SPOD_DELIVERY')?>:
									</li>
									<?
								}*/

								foreach ($arResult["TAX_LIST"] as $tax)
								{
									?>
									<li class="sale-order-detail-total-payment-list-left-item">
										<?= Loc::getMessage('SPOD_TAX') ?>:
									</li>
									<?
								}
								?>
								<!--<li class="sale-order-detail-total-payment-list-left-item"><?//= Loc::Loc::getMessage('SPOD_SUMMARY')?>:</li>-->
							</ul>
							<ul class="sale-order-detail-total-payment-list-right">
								<?
								if (floatval($arResult["ORDER_WEIGHT"]))
								{
									?>
									<li class="sale-order-detail-total-payment-list-right-item"><?= $arResult['ORDER_WEIGHT_FORMATED'] ?></li>
									<?
								}

								/*if ($arResult['PRODUCT_SUM_FORMATED'] != $arResult['PRICE_FORMATED'] && !empty($arResult['PRODUCT_SUM_FORMATED']))
								{
									?>
									<li class="sale-order-detail-total-payment-list-right-item"><?=$arResult['PRODUCT_SUM_FORMATED']?></li>
									<?
								}*/

								/*if (strlen($arResult["PRICE_DELIVERY_FORMATED"]))
								{
									?>
									<li class="sale-order-detail-total-payment-list-right-item"><?= $arResult["PRICE_DELIVERY_FORMATED"] ?></li>
									<?
								}*/

								foreach ($arResult["TAX_LIST"] as $tax)
								{
									?>
									<li class="sale-order-detail-total-payment-list-right-item"><?= $tax["VALUE_MONEY_FORMATED"] ?></li>
									<?
								}
								?>
								<!--<li class="sale-order-detail-total-payment-list-right-item"><?//=$arResult['PRICE_FORMATED']?></li>-->
							</ul>
						</div>
					</div>
				</div>
		</div><!--sale-order-detail-general-->
	</div>
	<?
	$javascriptParams = array(
	"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
	"templateFolder" => CUtil::JSEscape($templateFolder),
	"paymentList" => $paymentData
	);
	$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
	?>
	<script>
		app.setPageTitle({title: "<?=$screenTitle?>"});
		BX.Sale.PersonalOrderComponent.PersonalOrderDetail.init(<?=$javascriptParams?>);
	</script>
<?
}
?>

