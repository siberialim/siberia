<?
$MESS["AUTH_REGISTER"] = "회원가입";
$MESS["AUTH_NAME"] = "이름";
$MESS["AUTH_LAST_NAME"] = "성";
$MESS["AUTH_LOGIN_MIN"] = "로그인";
$MESS["AUTH_CONFIRM"] = "비밀번호 확인";
$MESS["CAPTCHA_REGF_PROMT"] = "Type text from image";
$MESS["AUTH_REQ"] = "Required fields.";
$MESS["AUTH_AUTH"] = "로그인";
$MESS["AUTH_PASSWORD_REQ"] = "비밀번호";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "A registration confirmation request will be sent to the specified e-mail address.";
$MESS["AUTH_EMAIL_SENT"] = "A registration confirmation request has been sent to the specified e-mail address.";
$MESS["AUTH_EMAIL"] = "E-Mail";
$MESS["AUTH_SECURE_NOTE"] = "The password will be encrypted before it is sent. This will prevent the password from appearing in open form over data transmission channels.";
?>