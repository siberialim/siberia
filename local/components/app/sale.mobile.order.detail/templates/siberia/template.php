<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
	<h2 class="order_itemlist_item_title">Order information</h2>
	<div class="top_data_order order_infoblock_content" id="detail_info_body_<?=$arResult['ORDER']['ID']?>">
			<p><span>Full name <span class="val"><?=$arResult['ORDER']['USER_NAME'].' '.$arResult['ORDER']['USER_LAST_NAME']?></span></span></p>
			<p><span>Current status as of <?=$arResult['ORDER']['DATE_STATUS_SHORT']?> <span class="val"><?=$arResult['ORDER']['STATUS_NAME']?></span></span></p>
			<p><span>Total <span class="val"><?=$arResult['ORDER']['PRICE_IN_ALL']?></span></span></p>
		<?//=CSaleMobileOrderUtils::makeDetailClassFromOrder($arResult["ORDER"]);?>
		<div class="order_step_payment step">
			<h3>Delivery and payment</h3>
			<p><b>Order №40 created on 01.12.2016, sending is pending</b></p><p>Order total:$13.l1</p>
			<p><b>Invoice №40/1 of 01.12.2016</b></p><p>Оплата по счету</p>
			<div class="status_order"><span>Unpaid</span></div>
			<p style="color:#8a8a8a"><b>Invoice amount:</b> $12.09</p>
		</div>
		<div class="order_step_payment step">
			<h3>Shipment parameters</h3>
			<p><b>Delivery service:</b>Самовывоз</p>
			<p><b>Shipment status:</b>Ожидает обработки</p>
		</div>
	</div>
<script type="text/javascript">

	app.setPageTitle({title: "<?=(Loc::getMessage('SMOD_ORDER_N').$arResult['ORDER']['ID'])?>"});

	var orderDetail = new __MASaleOrderDetail({id: "<?=$arResult['ORDER']['ID']?>",
					dialogUrl: "<?=$arResult['CURRENT_PAGE']?>",
					ajaxUrl: "<?=$arResult['AJAX_URL']?>",
					showUpperButtons: <?=($arResult['SHOW_UPPER_BUTTONS'] ? "true" : "false")?>
				});

	orderDetail.messages = {
		cancel: "<?=Loc::getMessage('SMOD_CANCEL')?>",
		cancelCancel: "<?=Loc::getMessage('SMOD_CANCEL_CANCEL')?>"
	}

	<?if(!empty($arResult['MENU_ITEMS'])):?>
		orderDetail.detailMenuItems = {items: []};

		<?if(in_array("STATUS_CHANGE", $arResult['MENU_ITEMS'])):?>
			orderDetail.detailMenuItems.items.push({
				name: "<?=Loc::getMessage('SMOD_CHANGE_STATUS');?>",
				action: function() {orderDetail.dialogShow("status"); },
				icon: 'edit'
			});
		<?endif;?>

		<?if(in_array("DELIVERY", $arResult['MENU_ITEMS'])):?>
			orderDetail.detailMenuItems.items.push({
				name: "<?=Loc::getMessage('SMOD_ALLOW_DELIVERY');?>",
				action: function() {orderDetail.dialogShow("delivery"); },
				icon: 'edit'
			});
		<?endif;?>

		<?if(in_array("PAYMENT", $arResult['MENU_ITEMS'])):?>
			orderDetail.detailMenuItems.items.push({
				name: "<?=Loc::getMessage('SMOD_PAY_FOR_ORDER');?>",
				action: function() {orderDetail.dialogShow("payment"); },
				icon: 'edit'
			});
		<?endif;?>

		<?if(in_array("DEDUCTION", $arResult['MENU_ITEMS'])):?>
			orderDetail.detailMenuItems.items.push({
				name: "<?=($arResult["ORDER"]["DEDUCTED"] == 'N' ? Loc::getMessage('SMOD_DEDUCT') : Loc::getMessage('SMOD_DEDUCT_UNDO'))?>",
				action: function() {
					app.loadPageBlank({
						url: "<?=$arResult['CURRENT_PAGE']?>?action=get_deduct_dialog&id=<?=$arResult['ORDER']['ID']?>"
					});
				},
				icon: 'edit'
			});
		<?endif;?>

		<?if(in_array("ORDER_CANCEL", $arResult['MENU_ITEMS'])):?>
			orderDetail.detailMenuItems.items.push({
				name: "<?=($arResult['ORDER']['CANCELED'] == 'N' ? Loc::getMessage('SMOD_CANCEL') : Loc::getMessage('SMOD_CANCEL_CANCEL'))?>",
				action: function() { orderDetail.dialogShow("cancel"); },
				icon: 'cancel'
			});

			BX.addCustomEvent('onAfterOrderCancel', function (params){
														if(params.id == <?=$arResult["ORDER"]["ID"]?>)
															orderDetail.onItemCancelChange(params);
													});

		<?endif;?>

		orderDetail.menuShow();

	<?endif;?>

	BX.addCustomEvent('onAfterOrderChange', function (params){
												if(params.id == <?=$arResult["ORDER"]["ID"]?>)
													orderDetail.updateOrder(params);
											});

</script>