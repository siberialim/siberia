<?
$MESS["CATALOG_BUY"] = "Купить";
$MESS["CATALOG_ADD"] = "В корзину";
$MESS["CATALOG_COMPARE"] = "Сравнить";
$MESS["CATALOG_NOT_AVAILABLE"] = "(нет на складе)";
$MESS["CATALOG_QUANTITY"] = "Количество";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "От #FROM# до #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "От #FROM#";
$MESS["CATALOG_QUANTITY_TO"] = "До #TO#";
$MESS["CT_BCS_QUANTITY"] = "Количество";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["MB_PRICE"] = "Цена";
$MESS["CATALOG_IN_CART"] = "В корзине";
$MESS["PROP_ING"] = "Ингредиенты";
$MESS["PROP_PACK"] = "Упаковка";
$MESS["PROP_HP"] = "Срок годности";
$MESS["PROP_WEIGHT"] = "Вес";
$MESS["PROP_NUTR"] = "Питательные вещества";
?>