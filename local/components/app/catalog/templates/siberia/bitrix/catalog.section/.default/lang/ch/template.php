<?
$MESS["CATALOG_BUY"] = "Buy";
$MESS["CATALOG_ADD"] = "加入购物车";
$MESS["CATALOG_COMPARE"] = "Compare";
$MESS["CATALOG_NOT_AVAILABLE"] = "(not available from stock)";
$MESS["CATALOG_QUANTITY"] = "数量";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "from #FROM# to #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# and more";
$MESS["CATALOG_QUANTITY_TO"] = "up to #TO#";
$MESS["CT_BCS_QUANTITY"] = "数量";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["MB_PRICE"] = "价格";
$MESS["CATALOG_IN_CART"] = "已在购物车";
$MESS["PROP_ING"] = "成分";
$MESS["PROP_PACK"] = "商品包装类型";
$MESS["PROP_HP"] = "有效期";
$MESS["PROP_WEIGHT"] = "货物总重量";
$MESS["PROP_NUTR"] = "营养价值";
?>