<?
$MESS["CATALOG_BUY"] = "바로 구매하기";
$MESS["CATALOG_ADD"] = "바구니에 넣기";
$MESS["CATALOG_COMPARE"] = "Compare";
$MESS["CATALOG_NOT_AVAILABLE"] = "(not available from stock)";
$MESS["CATALOG_QUANTITY"] = "수량";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "from #FROM# to #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# and more";
$MESS["CATALOG_QUANTITY_TO"] = "up to #TO#";
$MESS["CT_BCS_QUANTITY"] = "수량";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["MB_PRICE"] = "가격";
$MESS["CATALOG_IN_CART"] = "상품이 장바구니에 담겨있습니다";
$MESS["PROP_ING"] = "성분";
$MESS["PROP_PACK"] = "포장";
$MESS["PROP_HP"] = "유통 기한";
$MESS["PROP_WEIGHT"] = "무게";
$MESS["PROP_NUTR"] = "제품 100그램당";
?>