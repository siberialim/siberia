<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>

<div class="detail_item">
	<?if(is_array($arResult["PREVIEW_PICTURE"]) || is_array($arResult["DETAIL_PICTURE"])):?>
		<div class="detail_item_img_container" <?if (!empty($arResult["PHOTO_GALLERY"])):?>onclick="showPhoto(<?=CUtil::PhpToJsObject($arResult["PHOTO_GALLERY"])?>, '<?=$arResult["NAME"]?>')"<?endif?>>
			<a class="detail_item_img" href="javascript:void(0)">
				<?if(is_array($arResult["DETAIL_PICTURE_SMALL"])):?>
					<img id="catalog_detail_image" src="<?=$arResult["DETAIL_PICTURE_SMALL"]["SRC"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" />
				<?elseif(is_array($arResult["PREVIEW_PICTURE"])):?>
					<img id="catalog_detail_image" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" />
				<?endif?>
			</a>
		</div>
	<?endif;?>
	<h2 class="detail_item_title">
		<span><?=$arResult["NAME"]?></span>
	</h2>
	<?if($arParams['SHOW_PRICES'] == 'Y'):?>
		<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
			<div class="detail_price_container oldprice"><?=Loc::getMessage("CATALOG_PRICE")?>:
				<span class="item_price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
				<span class="item_price_old"><?=$arPrice["PRINT_VALUE"]?></span>
			</div>
		<?else:?>
			<div class="detail_price_container">
				<span class="item_price"><?=$arPrice["PRINT_VALUE"]?></span>
			</div>
		<?endif;?>
	<?endif;?>
	<div class="clear"></div>
	<div class="item_info_section">
		<ul class="props">
			<?if(isset($arResult['PROPERTIES']['COMPOSITION']['VALUE']) && !empty($arResult['PROPERTIES']['COMPOSITION']['VALUE'])){?>
			<li class="prop"><span><?=Loc::getMessage("PROP_ING")?>:</span>
				<span><?=$arResult['PROPERTIES']['COMPOSITION']['VALUE']?></span>
			</li>
			<?}?>
			<?if(isset($arResult['PROPERTIES']['PACK']['VALUE']) && !empty($arResult['PROPERTIES']['PACK']['VALUE'])){?>
			<li class="prop"><span><?=Loc::getMessage("PROP_PACK")?>:</span>
				<span><?=$arResult['PROPERTIES']['PACK']['VALUE']?></span>
			</li>
			<?}?>
			<?if(isset($arResult['PROPERTIES']['SHELF_LIFE']['VALUE']) && !empty($arResult['PROPERTIES']['SHELF_LIFE']['VALUE'])){?>
			<li class="prop"><span><?=Loc::getMessage("PROP_HP")?>:</span>
				<span><?=$arResult['PROPERTIES']['SHELF_LIFE']['VALUE']?></span>
			</li>
			<?}?>
			<?if(isset($arResult['PROPERTIES']['WEIGHT']['VALUE']) && !empty($arResult['PROPERTIES']['WEIGHT']['VALUE'])){?>
			<li class="prop"><span><?=Loc::getMessage("PROP_WEIGHT")?>:</span>
				<span><?=$arResult['PROPERTIES']['WEIGHT']['VALUE']?></span>
			</li>
			<?}?>
			<?if(isset($arResult['PROPERTIES']['NUTRIENTS']['VALUE']) && !empty($arResult['PROPERTIES']['NUTRIENTS']['VALUE'])){?>
			<li class="prop"><span><?=Loc::getMessage("PROP_NUTR")?>:</span>
				<span><?=$arResult['PROPERTIES']['NUTRIENTS']['VALUE']?></span>
			</li>
			<?}?>
		</ul>
	</div>
	<?if ($arResult["DETAIL_TEXT"] || $arResult["PREVIEW_TEXT"]):?>
	<div class="detail_item_description">
		<div class="detail_item_description_text">
			<?if($arResult["DETAIL_TEXT"]):?>
				<?=$arResult["DETAIL_TEXT"]?><br />
			<?elseif($arResult["PREVIEW_TEXT"]):?>
				<?=$arResult["PREVIEW_TEXT"]?><br />
			<?endif;?>
		</div>
	</div>
	<?endif?>	
	<?if($arResult["CAN_BUY"]):?>
		<?if($arParams["USE_PRODUCT_QUANTITY"]):?>
		<div class="clb"></div>
		<div class="detail_item_buy_container">
			<form action="<?=POST_FORM_ACTION_URI?>" id="quantity_form" method="post" enctype="multipart/form-data"  >
				<div class="detail_item_count"><?=Loc::getMessage("CATALOG_QUANTITY")?>:
					<a href="javascript:void(0)" class="count_minus" id="count_minus" ontouchstart="if (BX('item_quantity').value > 1) BX('item_quantity').value--;">-</a><!--
						--><input type="number" id="item_quantity" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1"><!--
					--><a href="javascript:void(0)" class="count_plus" id="count_plus" ontouchstart="BX('item_quantity').value++;">+</a>
				</div>
				<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="ADD2BASKET">
				<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arResult["ID"]?>">
				<a class="detail_item_buykey" ontouchstart="BX.toggleClass(this, 'active');" ontouchend="BX.toggleClass(this, 'active');" href="javascript:void(0)" onclick="
						BX.addClass(BX.findParent(this, {class : 'detail_item'}, false), 'add2cart');
						app.onCustomEvent('onItemBuy', {});
						BX.ajax({
							timeout:   30,
							method:   'POST',
							url:       '<?=CUtil::JSEscape(POST_FORM_ACTION_URI)?>',
							processData: false,
							data: {
								<?echo $arParams["ACTION_VARIABLE"]?>: 'ADD2BASKET',
								<?echo $arParams["PRODUCT_ID_VARIABLE"]?>: '<?echo $arResult["ID"]?>',
								<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>: BX('quantity_form').elements['<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>'].value
							},
							onsuccess: function(reply){			
								getCountItemsInCart();
							},
							onfailure: function(){
							}
						});
						return BX.PreventDefault(event);
				"><?echo Loc::getMessage("CT_BCE_CATALOG_ADD")?></a>
				<a class="detail_item_buykey_cartlink" href="<?echo $arParams["BASKET_URL"]?>" rel="nofollow"><?echo Loc::getMessage("CATALOG_IN_CART")?></a>
			</form>
		</div>
		<?else:?>
		<div class="detail_item_buy_container">
			<noindex>
				<a class="detail_item_buykey button_red_medium" ontouchstart="BX.toggleClass(this, 'active');" ontouchend="BX.toggleClass(this, 'active');" href="<?echo $arResult["ADD_URL"]?>" onclick="
					BX.addClass(BX.findParent(this, {class : 'detail_item'}, false), 'add2cart');
					return addItemToCart(this);" rel="nofollow"><?echo Loc::getMessage("CT_BCE_CATALOG_ADD")?></a>
				<a class="detail_item_buykey_cartlink" href="<?echo $arParams["BASKET_URL"]?>" rel="nofollow"><?echo Loc::getMessage("CATALOG_IN_CART")?></a>
			</noindex> 
		</div>
		<?endif;
	endif?>
</div>

<script type="text/javascript">
	app.setPageTitle({"title" : "<?=CUtil::JSEscape(htmlspecialcharsback($arResult["NAME"]))?>"});
	function showPhoto(arPhotos, descr)
	{
		var photos = [];
		for (var i=0; i<arPhotos.length; i++)
		{
			photos[i] = {url : arPhotos[i], description : descr};
		}
		app.openPhotos({
			"photos": photos
		});
	}
</script>


