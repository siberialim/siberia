<?
$MESS["ACCOUNT_UPDATE"] = "The administrator has updated your personal information.";
$MESS["PROFILE_DEFAULT_TITLE"] = "User profile";
$MESS["USER_DONT_KNOW"] = "(unknown)";
$MESS["main_profile_sess_expired"] = "Your session has expired. Please try again.";
$MESS["main_profile_decode_err"] = "Password decryption error (#ERRCODE#).";
$MESS["TITLE_PAGE"] = "My account";
$MESS["EMPTY"] = "Empty";
$MESS["P_NAME"] = "Name";
$MESS["DATE_OF_BIRTH"] = "Date of birth";
$MESS["PERSONAL_INFORMATION"] = "Personal information";
$MESS["SEX"] = "Sex";
$MESS["CHANGE_PERSONAL_INFORMATION"] = "Change personal information";
$MESS["MOBILE_PHONE_NO"] = "Mobile phone no";
$MESS["CHANGE_PHONE_NO"] = "Change phone no";
$MESS["HERE_YOU_CAN_CHANGE_YOUR_PASSWORD"] = "Here you can change your password";
$MESS["PASSWORD"] = "Password";
$MESS["CHANGE_PASSWORD"] = "Change password";
$MESS["DELETE_YOUR_ACCOUNT"] = "Delete your account";
$MESS["DELETE_YOUR_ACCOUNT_MESS"] = "Once your account is deleted you will automatically be logged out and no longer be able to log in to it";
?>