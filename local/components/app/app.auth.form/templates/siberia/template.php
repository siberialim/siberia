<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */
?>
<div id="app" class="bx-authform">
<?if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']){?>
	<div class="alert alert-danger"><?=ShowMessage($arResult['ERROR_MESSAGE']);?></div>
<?}
	if($_REQUEST['register'] == 'yes'){
		$APPLICATION->IncludeComponent(
			"app:app.auth.registration",
			"flat",
			Array(
				"REGISTER_URL" => "/eshop_app/auth/index.php?register=yes",
				"PROFILE_URL" => "/eshop_app",
				"SHOW_ERRORS" => "Y"
			),
			false
		);
	}
	else if($_REQUEST['forgot_password'] == 'yes'){
		$APPLICATION->IncludeComponent(
			"app:app.auth.forgotpasswd",
			"flat",
			Array(
				"REGISTER_URL" => "/eshop_app/auth/index.php?register=yes",
				"PROFILE_URL" => "/eshop_app",
				"SHOW_ERRORS" => "Y"
			),
			false
		);
	}
else{
	if($_REQUEST['login'] == 'yes' && !$arResult['ERROR']){
		LocalRedirect('/eshop_app/');
	}
	else{		
		$APPLICATION->IncludeComponent(
				"app:app.auth.authorize",
				"flat",
				Array(
					"REGISTER_URL" => "/eshop_app/auth/index.php?register=yes",
					"AUTH_URL" => "/eshop_app/auth/index.php?login=yes",
					"PROFILE_URL" => "/eshop_app",
					"SHOW_ERRORS" => "Y",
				),
				false
			);
	}
}
?>
</div>
<style>
	header{display:none;}
</style>
<script type="text/javascript">
	<?if (strlen($arResult["LAST_LOGIN"])>0):?>
		try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
	<?else:?>
		try{document.form_auth.USER_LOGIN.focus();}catch(e){}
	<?endif?>
</script>



