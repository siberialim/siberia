<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Security\Mfa;

if(CModule::IncludeModule("socialservices"))
{
	$oAuthManager = new CSocServAuthManager();
	$arResult['SERVICES'] = $oAuthManager->GetActiveAuthServices();
}	

$this->IncludeComponentTemplate();
