<?
$MESS["AUTH_PLEASE_AUTH"] = "请登录";
$MESS["AUTH_LOGIN"] = "登录名";
$MESS["AUTH_PASSWORD"] = "密码";
$MESS["AUTH_REMEMBER_ME"] = "Remember me on this computer";
$MESS["AUTH_AUTHORIZE"] = "登录";
$MESS["AUTH_REGISTER"] = "注册";
$MESS["AUTH_FIRST_ONE"] = "如果您头一次访问本站，请填写注册表格";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "忘记密码？";
$MESS["AUTH_CAPTCHA_PROMT"] = "Type text from image";
$MESS["AUTH_TITLE"] = "Log In";
$MESS["AUTH_SECURE_NOTE"] = "The password will be encrypted before it is sent. This will prevent the password from appearing in open form over data transmission channels.";
$MESS["AUTH_NONSECURE_NOTE"] = "The password will be sent in open form. Enable JavaScript in your web browser to enable password encryption.";
?>