<?
$MESS["AUTH_PLEASE_AUTH"] = "로그인해 주십시오";
$MESS["AUTH_LOGIN"] = "로그인";
$MESS["AUTH_PASSWORD"] = "비밀번호";
$MESS["AUTH_REMEMBER_ME"] = "Remember me on this computer";
$MESS["AUTH_AUTHORIZE"] = "총 금액";
$MESS["AUTH_REGISTER"] = "회원가입";
$MESS["AUTH_FIRST_ONE"] = "이 사이트를 처음에 사용하시는 경우, 등록양식을 기입해주시기 바랍니다.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "비밀번호 찾기";
$MESS["AUTH_CAPTCHA_PROMT"] = "Type text from image";
$MESS["AUTH_TITLE"] = "Log In";
$MESS["AUTH_SECURE_NOTE"] = "The password will be encrypted before it is sent. This will prevent the password from appearing in open form over data transmission channels.";
$MESS["AUTH_NONSECURE_NOTE"] = "The password will be sent in open form. Enable JavaScript in your web browser to enable password encryption.";
?>