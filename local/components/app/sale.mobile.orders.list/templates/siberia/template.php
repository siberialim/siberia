<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;
	
Loc::loadMessages(__FILE__);
	
$order_payment_is_pending = array();
$order_sending_is_pending = array();
$order_order_is_sent = array();
$order_cancel_order = array();

foreach ($arResult["ORDERS"] as $order)
{
	if($order['CANCELED'] == 'Yes'){
		array_push($order_cancel_order, $order);
	}
	else{
		if($order['STATUS_ID'] == 'P'){
			array_push($order_sending_is_pending, $order);
		}
		else if($order['STATUS_ID'] == 'S'){
			array_push($order_order_is_sent, $order);
		}
		else if($order['STATUS_ID'] == 'N'){
			array_push($order_payment_is_pending, $order);
		}
	}
}

CJSCore::Init('ajax');

?>
<div class="main_button_component">
	<a href="javascript:void(0)" class="item_title current" id="payment_is_pending_title" onclick="BX('payment_is_pending').style.display='block'; BX('sending_is_pending').style.display='none';BX('order_is_sent').style.display='none';BX('cancel_order').style.display='none'; BX.removeClass(BX('sending_is_pending_title'), 'current'); BX.removeClass(BX('order_is_sent_title'), 'current'); BX.removeClass(BX('cancel_order_title'), 'current');BX.addClass(BX(this), 'current');" style="font-size:7px;font-weight:bold;"><?=Loc::getMessage("STATUS_N")?></a><!--
	--><a href="javascript:void(0)" class="item_title" id="sending_is_pending_title" onclick="BX('payment_is_pending').style.display='none'; BX('sending_is_pending').style.display='block';BX('order_is_sent').style.display='none';BX('cancel_order').style.display='none';BX.removeClass(BX('payment_is_pending_title'), 'current'); BX.removeClass(BX('order_is_sent_title'), 'current'); BX.removeClass(BX('cancel_order_title'), 'current');BX.addClass(BX(this), 'current');" style="font-size:7px;font-weight:bold;"><?=Loc::getMessage("STATUS_P")?></a><!--
	--><a href="javascript:void(0)" class="item_title" id="order_is_sent_title" onclick="BX('payment_is_pending').style.display='none'; BX('sending_is_pending').style.display='none';BX('order_is_sent').style.display='block';BX('cancel_order').style.display='none';BX.removeClass(BX('payment_is_pending_title'), 'current'); BX.removeClass(BX('cancel_order_title'), 'current'); BX.removeClass(BX('sending_is_pending_title'), 'current');BX.addClass(BX(this), 'current');" style="font-size:7px;font-weight:bold;"><?=Loc::getMessage("STATUS_S")?></a><!--
	--><a href="javascript:void(0)" class="item_title" id="cancel_order_title" onclick="BX('payment_is_pending').style.display='none'; BX('sending_is_pending').style.display='none';BX('order_is_sent').style.display='none';BX('cancel_order').style.display='block';BX.removeClass(BX('payment_is_pending_title'), 'current'); BX.removeClass(BX('order_is_sent_title'), 'current'); BX.removeClass(BX('sending_is_pending_title'), 'current');BX.addClass(BX(this), 'current');"><?=Loc::getMessage("STATUS_CANCELE")?></a><!--
	--><div class="clb"></div>
</div>
<div id="payment_is_pending" class="order_itemlist_item_container">
	<!--<h2 class="order_itemlist_item_title"><?//=Loc::getMessage("ORDER_STATUS")?> "payment is pending"</h2>-->
	<?
foreach ($order_payment_is_pending as $order)
{
	?>
		<div id="order_item_<?=$order['ID']?>" class="order_item">
			<div class="order_data">
				<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
					"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($order['ID']),
					"#DATE_ORDER_CREATE#"=> $order['DATE_INSERT']
				))?>
				<?
				if ($arResult['CANCELED'] !== 'Y')
				{
					echo Loc::getMessage('STATUS_'.$order["STATUS_ID"]);
				}
				else
				{
					echo Loc::getMessage('SPOD_ORDER_CANCELED');
				}
				?>
			</div>
			<div class="order_step_payment step">
				<h3><?=Loc::getMessage("PAYMENT")?></h3>
				<div class="status_payment <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>"><span><?=$order['ADD_ALLOW_PAYED_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Invoice amount:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="order_step_delivery step">
				<h3><?=Loc::getMessage("DELIVERY")?></h3>
				<div class="status_delivery <?=strtolower($order['ADD_ALLOW_DELIVERY_PHRASE'])?>"><span><?=$order['ADD_ALLOW_DELIVERY_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Shipment status:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="control_order">
				<div class="button_wrap">
					<a href="<?=$order['ORDER_DETAIL_LINK']?>"><?=Loc::getMessage("ORDER_DETAILS")?></a>
				</div><!--
				--><!--
				--><div class="button_wrap" style="text-align:right;">
					<a href="javascript:void(0)" onclick="cancelOrder(<?=$order['ID']?>)"><?=Loc::getMessage("CANCEL_ORDER")?></a>
				</div>
			</div>
		</div>
	<?
}
?>
</div>

<div id="sending_is_pending" class="order_itemlist_item_container" style="display:none">
	<!--<h2 class="order_itemlist_item_title"><?//=Loc::getMessage("ORDER_STATUS")?> "payment is pending"</h2>-->
	<?
foreach ($order_sending_is_pending as $order)
{
	?>
		<div id="order_item_<?=$order['ID']?>" class="order_item <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>">
			<div class="order_data">
				<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
					"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($order['ID']),
					"#DATE_ORDER_CREATE#"=> $order['DATE_INSERT']
				))?>
				<?
				if ($arResult['CANCELED'] !== 'Y')
				{
					echo Loc::getMessage('STATUS_'.$order["STATUS_ID"]);
				}
				else
				{
					echo Loc::getMessage('SPOD_ORDER_CANCELED');
				}
				?>
			</div>
			<div class="order_step_payment step">
				<h3><?=Loc::getMessage("PAYMENT")?></h3>
				<div class="status_payment <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>"><span><?=$order['ADD_ALLOW_PAYED_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Invoice amount:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="order_step_delivery step">
				<h3><?=Loc::getMessage("DELIVERY")?></h3>
				<div class="status_delivery <?=strtolower($order['ADD_ALLOW_DELIVERY_PHRASE'])?>"><span><?=$order['ADD_ALLOW_DELIVERY_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Shipment status:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="control_order">
				<div class="button_wrap">
					<a href="<?=$order['ORDER_DETAIL_LINK']?>"><?=Loc::getMessage("ORDER_DETAILS")?></a>
				</div><!--
				--><!--
				--><div class="button_wrap" style="text-align:right;">
					<a href="javascript:void(0)" onclick="cancelOrder(<?=$order['ID']?>)"><?=Loc::getMessage("CANCEL_ORDER")?></a>
				</div>
			</div>
		</div>
	<?
}
?>
</div>

<div id="order_is_sent" class="order_itemlist_item_container" style="display:none">
	<!--<h2 class="order_itemlist_item_title"><?//=Loc::getMessage("ORDER_STATUS")?> "payment is pending"</h2>-->
	<?
foreach ($order_order_is_sent as $order)
{
	?>
		<div id="order_item_<?=$order['ID']?>" class="order_item <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>">
			<div class="order_data">
				<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
					"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($order['ID']),
					"#DATE_ORDER_CREATE#"=> $order['DATE_INSERT']
				))?>
				<?
				if ($arResult['CANCELED'] !== 'Y')
				{
					echo Loc::getMessage('STATUS_'.$order["STATUS_ID"]);
				}
				else
				{
					echo Loc::getMessage('SPOD_ORDER_CANCELED');
				}
				?>
			</div>
			<div class="order_step_payment step">
				<h3><?=Loc::getMessage("PAYMENT")?></h3>
				<div class="status_payment <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>"><span><?=$order['ADD_ALLOW_PAYED_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Invoice amount:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="order_step_delivery step">
				<h3><?=Loc::getMessage("DELIVERY")?></h3>
				<div class="status_delivery <?=strtolower($order['ADD_ALLOW_DELIVERY_PHRASE'])?>"><span><?=$order['ADD_ALLOW_DELIVERY_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Shipment status:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="control_order">
				<div class="button_wrap">
					<a href="<?=$order['ORDER_DETAIL_LINK']?>"><?=Loc::getMessage("ORDER_DETAILS")?></a>
				</div><!--
				--><!--
				--><div class="button_wrap" style="text-align:right;">
					<a href="javascript:void(0)" onclick="cancelOrder(<?=$order['ID']?>)"><?=Loc::getMessage("CANCEL_ORDER")?></a>
				</div>
			</div>
		</div>
	<?
}
?>
</div>

<div id="payment_is_pending" class="order_itemlist_item_container" style="display:none">
	<!--<h2 class="order_itemlist_item_title"><?//=Loc::getMessage("ORDER_STATUS")?> "payment is pending"</h2>-->
	<?
foreach ($order_payment_is_pending as $order)
{
	?>
		<div id="order_item_<?=$order['ID']?>" class="order_item <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>">
			<div class="order_data">
				<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
					"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($order['ID']),
					"#DATE_ORDER_CREATE#"=> $order['DATE_INSERT']
				))?>
				<?
				if ($arResult['CANCELED'] !== 'Y')
				{
					echo Loc::getMessage('STATUS_'.$order["STATUS_ID"]);
				}
				else
				{
					echo Loc::getMessage('SPOD_ORDER_CANCELED');
				}
				?>
			</div>
			<div class="order_step_payment step">
				<h3><?=Loc::getMessage("PAYMENT")?></h3>
				<div class="status_payment <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>"><span><?=$order['ADD_ALLOW_PAYED_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Invoice amount:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="order_step_delivery step">
				<h3><?=Loc::getMessage("DELIVERY")?></h3>
				<div class="status_delivery <?=strtolower($order['ADD_ALLOW_DELIVERY_PHRASE'])?>"><span><?=$order['ADD_ALLOW_DELIVERY_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Shipment status:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="control_order">
				<div class="button_wrap">
					<a href="<?=$order['ORDER_DETAIL_LINK']?>"><?=Loc::getMessage("ORDER_DETAILS")?></a>
				</div><!--
				--><!--
				--><div class="button_wrap" style="text-align:right;">
					<a href="javascript:void(0)" onclick="cancelOrder(<?=$order['ID']?>)"><?=Loc::getMessage("CANCEL_ORDER")?></a>
				</div>
			</div>
		</div>
	<?
}
?>
</div>

<div id="cancel_order" class="order_itemlist_item_container" style="display:none">
	<!--<h2 class="order_itemlist_item_title"><?//=Loc::getMessage("ORDER_STATUS")?> "payment is pending"</h2>-->
	<?
foreach ($order_cancel_order as $order)
{
	?>
		<div id="order_item_<?=$order['ID']?>" class="order_item <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>">
			<div class="order_data">
				<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
					"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($order['ID']),
					"#DATE_ORDER_CREATE#"=> $order['DATE_INSERT']
				))?>
				<?
				if ($arResult['CANCELED'] !== 'Y')
				{
					echo Loc::getMessage('STATUS_'.$order["STATUS_ID"]);
				}
				else
				{
					echo Loc::getMessage('SPOD_ORDER_CANCELED');
				}
				?>
			</div>
			<div class="order_step_payment step">
				<h3><?=Loc::getMessage("PAYMENT")?></h3>
				<div class="status_payment <?=strtolower($order['ADD_ALLOW_PAYED_PHRASE'])?>"><span><?=$order['ADD_ALLOW_PAYED_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Invoice amount:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="order_step_delivery step">
				<h3><?=Loc::getMessage("DELIVERY")?></h3>
				<div class="status_delivery <?=strtolower($order['ADD_ALLOW_DELIVERY_PHRASE'])?>"><span><?=$order['ADD_ALLOW_DELIVERY_PHRASE']?></span></div>
				<!--<p style="color:#8a8a8a"><b>Shipment status:</b> <?//=$order['ADD_PRICE']?></p>-->
			</div>
			<div class="control_order">
				<div class="button_wrap">
					<a href="<?=$order['ORDER_DETAIL_LINK']?>"><?=Loc::getMessage("ORDER_DETAILS")?></a>
				</div><!--
				--><!--
				--><div class="button_wrap" style="text-align:right;">
					<a href="javascript:void(0)" onclick="cancelOrder(<?=$order['ID']?>)"><?=Loc::getMessage("CANCEL_ORDER")?></a>
				</div>
			</div>
		</div>
	<?
}
?>
</div>

<?
$pageTitle = Loc::getMessage("SMOL_ALL_ORDERS");
?>
<script type="text/javascript">

	app.setPageTitle({title: "<?=$pageTitle?>"});

	var ordersListParams = {
							lastOrder: '<?=$lastOrderId?>',
							filter: <?=CUtil::PhpToJsObject($arResult["FILTER"])?>,
							ajaxUrl: '<?=$arResult['AJAX_URL']?>',
							dialogUrl: "<?=$arResult['CURRENT_PAGE']?>",
							orderDetailPath: "<?=$arResult['ORDER_DETAIL_PATH']?>"
						};

	var ordersList = new __MASaleOrdersList(ordersListParams);
	var checkTimeout = <?=SALE_ORDERS_LIST_CHECK_TIMEOUT?>;

	function cancelOrder(id){
		var postData = {};
		postData.action = 'cancel_order';
		postData.id = id;
		
		$.ajax({
		   // url: '/<?=urlencode($templateFolder.'/ajax.php');?>',
		   url:'<?=$templateFolder.'/ajax.php';?>',
           data: postData,
           method: 'POST',
           dataType: 'json',
           success: function(data){
				$('#order_item_'+id).appendTo("#cancel_order");
           },

       });
	}
	
	function returnOrder(id, status){
		var postData = {};
		postData.action = 'return_order';
		postData.id = id;
		$.ajax({
		   // url: '/<?=urlencode($templateFolder.'/ajax.php');?>',
		   url:'/local/components/app/sale.mobile.orders.list/templates/siberia/ajax.php',
           data: postData,
           method: 'POST',
           dataType: 'json',
           success: function(data){
				if(status == 'P'){
					$('#order_item_'+id).appendTo("#sending_is_pending");
				}
				else if(status == 'S'){
					$('#order_item_'+id).appendTo("#order_is_sent");
				}
				else if(status == 'N'){
					$('#order_item_'+id).appendTo("#payment_is_pending");
				}
           },

       }); 
	}
</script>