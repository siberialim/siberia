<?
$MESS["SPOL_SALE_MODULE_NOT_INSTALL"] = "The e-Store module is not installed.";
$MESS["SPOL_CATALOG_MODULE_NOT_INSTALL"] = "The Commercial Catalog module is not installed.";
$MESS['SPOL_DEFAULT_TITLE'] = "내 주문";
$MESS["SPOL_ACCESS_DENIED"] = "Please log in to view orders.";
$MESS['SPOL_PAGES'] = "주문";
$MESS["SPOL_CANNOT_COPY_ORDER"] = "Cannot copy order.";
?>