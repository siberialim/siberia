<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// D7
//use Bitrix\Main\Page\Asset;
//Asset::getInstance()->addJs("/bitrix/components/it-invest/sale.personal.order.list/templates/siberia/script.js");
?>
<div class="profile my-data">
	<div class="profile-b-cont">
		<div class="wrap-profile option">
			<h1 class="pageTitle"><?=GetMessage("TITLE_PAGE")?></h1>
			<div class="block-out-background" style="padding:12px">

				<?foreach($arResult['INFO']['STATUS'] as $key_status=>$status){
					$count_status = 0;
						foreach($arResult["ORDER_BY_STATUS"] as $key => $val){
							foreach($val as $vval){
							   if($vval['ORDER']['STATUS_ID'] == $key_status){
								  $count_status+=1;
							   }
							}
						}
					if($status['ID'] != 'F'){?><span class="status_order"><?=GetMessage("STATUS_".$status['ID'])?>(<?=$count_status?>)</span><?}
				}
				unset($count_status);?>
				<!--<span class="status_order">Заказ отправлен(1)</span>
				<span class="status_order">Непрочитанные сообщения(1)</span>-->
			</div>
			<div class="block-out-background" style="padding:12px">
				<form class="profile-form-search-prod" method="post" action="/personal/orders/">
					<fieldset>
						<div class="row_input">	
							<div class="wrap_input">
								<label><?=GetMessage("ORDER_NO")?>: </label>&nbsp;&nbsp;&nbsp;<input type="text" name="ORDER_ID" value="<?=$arParams["ORDER_ID"]?>" placeholder=""/><br/>
							</div>
							<div class="wrap_input">
								<label><?=GetMessage("NAME_OF_GOODS")?>: </label>&nbsp;&nbsp;&nbsp;<input type="text" name="PRODUCT_NAME" value="<?=$arParams["PRODUCT_NAME"]?>" placeholder=""/><br/>
							</div>
							<div class="wrap-btn wrap_input">
								<input type="submit" name="" value="<?=GetMessage("SEARCH")?>"/>
							</div>
						</div>
					</fieldset>					
				</form>
			</div>
			<div class="block-out-background">
				
				<table class="delivery">
					<thead>
						<tr><th><?=GetMessage("NAME_OF_GOODS")?></th><th><?=GetMessage("ORDER_STATUS")?></th><th><?=GetMessage("DATE_AND_TIME")?></th><!--<th><?//=GetMessage("SUM")?></th>--></tr>
					</thead>
					<tbody>
						<?foreach($arResult["ORDER_BY_STATUS"] as $key => $val){?>
							<?$bShowStatus = true;
							foreach($val as $vval)
							{
								foreach($vval['BASKET_ITEMS'] as $vvval){
							?>
								<tr>
									<td>
                                                                            
										<div class="prod">
                                                                                    <?if($vvval['PREVIEW_PICTURE']['src']){?>
											<a class="tuningLink" href="<?=$vvval['DETAIL_PAGE_URL']?>">
												<img src="<?=$vvval['PREVIEW_PICTURE']['src']?>" alt=""/>
											</a>
                                                                                    <?}?>
										</div><!--                                               
                                                                            -->
										<a class="tuningLink" href="<?=$vvval['DETAIL_PAGE_URL']?>"><b><?=$vvval['NAME']?></b></a>
                                                                            
                                                                        </td>
									<td><?=GetMessage('STATUS_'.$arResult["INFO"]["STATUS"][$key]["ID"])?></td>
									<td>
                                                                            <?
                                                                                $stmp = MakeTimeStamp($vvval['DATE_INSERT'], "DD.MM.YYYY HH:MI:SS");
                                                                                $stmp = AddToTimeStamp(array("DD" => -1, "MM" => 1), $stmp); // 1115454720
                                                                                echo date("d.m.Y H:i:s", $stmp); // 06.05.2005 11:32:00
                                                                            ?>
                                                                        </td>
									<!--<td class="summ"><?
										/*if(isset($_SESSION['CURRENCY_ID'])&&(CCurrency::GetByID($_SESSION['CURRENCY_ID'])))  
										$vvval["FORMATED_PRICE"] = SaleFormatCurrency(CCurrencyRates::ConvertCurrency($vvval["PRICE"],
										$vvval["CURRENCY"],$_SESSION['CURRENCY_ID']), $_SESSION['CURRENCY_ID']);  
										else   		
										$vvval["FORMATED_PRICE"] = SaleFormatCurrency($vvval["BASE_PRICE"], $vvval["CURRENCY"]);
									
										echo $vvval["FORMATED_PRICE"];*/
									?></td>-->
								</tr>
						<?		}
							}
						}?>
					</tbody>
				</table>
			</div>
			<div style="margin-bottom:15px">
				<?echo $arResult["NAV_STRING"];?>
			</div>
		</div>
	</div>
</div>
