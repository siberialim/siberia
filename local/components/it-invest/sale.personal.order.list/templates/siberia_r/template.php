<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
CJSCore::Init(array('clipboard'));
if(!CUser::IsAuthorized()){die();}
Loc::loadMessages(__FILE__);

$order_payment_is_pending = array();
$order_sending_is_pending = array();
$order_order_is_sent = array();
$order_cancel_order = array();

foreach ($arResult["ORDERS"] as $order)
{
	if($order['ORDER']['CANCELED'] == 'Y'){
		array_push($order_cancel_order, $order);
	}
	else{
		if($order['ORDER']['STATUS_ID'] == 'P'){
			array_push($order_sending_is_pending, $order);
		}
		else if($order['ORDER']['STATUS_ID'] == 'S'){
			array_push($order_order_is_sent, $order);
		}
		else if($order['ORDER']['STATUS_ID'] == 'N'){
			array_push($order_payment_is_pending, $order);
		}
	}
}
?>
<div id="wrap_section_list">
	<div class="main_button_component">
		<a href="?filter_status=N" class="item_title <?if($_REQUEST['filter_status'] == 'N' || !$_REQUEST['filter_status'] && $_REQUEST['filter_canceled'] != 'Y'){echo 'current';}?>" id="payment_is_pending_title" onclick="BX('payment_is_pending').style.display='block'; BX('sending_is_pending').style.display='none';BX('order_is_sent').style.display='none';BX('cancel_order').style.display='none'; BX.removeClass(BX('sending_is_pending_title'), 'current'); BX.removeClass(BX('order_is_sent_title'), 'current'); BX.removeClass(BX('cancel_order_title'), 'current');BX.addClass(BX(this), 'current');"><?=Loc::getMessage("STATUS_N")?></a><!--
		--><a href="?filter_status=P" class="item_title <?if($_REQUEST['filter_status'] == 'P'){echo 'current';}?>" id="sending_is_pending_title" onclick="BX('payment_is_pending').style.display='none'; BX('sending_is_pending').style.display='block';BX('order_is_sent').style.display='none';BX('cancel_order').style.display='none';BX.removeClass(BX('payment_is_pending_title'), 'current'); BX.removeClass(BX('order_is_sent_title'), 'current'); BX.removeClass(BX('cancel_order_title'), 'current');BX.addClass(BX(this), 'current');"><?=Loc::getMessage("STATUS_P")?></a><!--
		--><a href="?filter_status=S" class="item_title <?if($_REQUEST['filter_status'] == 'S'){echo 'current';}?>" id="order_is_sent_title" onclick="BX('payment_is_pending').style.display='none'; BX('sending_is_pending').style.display='none';BX('order_is_sent').style.display='block';BX('cancel_order').style.display='none';BX.removeClass(BX('payment_is_pending_title'), 'current'); BX.removeClass(BX('cancel_order_title'), 'current'); BX.removeClass(BX('sending_is_pending_title'), 'current');BX.addClass(BX(this), 'current');"><?=Loc::getMessage("STATUS_S")?></a><!--
		--><a href="?filter_canceled=Y" class="item_title <?if($_REQUEST['filter_canceled'] == 'Y'){echo 'current';}?>" id="cancel_order_title" onclick="BX('payment_is_pending').style.display='none'; BX('sending_is_pending').style.display='none';BX('order_is_sent').style.display='none';BX('cancel_order').style.display='block';BX.removeClass(BX('payment_is_pending_title'), 'current'); BX.removeClass(BX('order_is_sent_title'), 'current'); BX.removeClass(BX('sending_is_pending_title'), 'current');BX.addClass(BX(this), 'current');"><?=Loc::getMessage("STATUS_CANCELE")?></a><!--
		--><div class="clb"></div>
	</div>
	<div class="order_itemlist_item_container">
	<?
	foreach ($arResult["ORDERS"] as $key => $order)
	{
		?>
		<div id="order_item_<?=$order['ORDER']['ID']?>" class="col-md-12 col-sm-12 sale-order-list-container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 sale-order-list-title-container">
					<h2 class="sale-order-list-title">
						<?=Loc::getMessage('SPOL_TPL_ORDER')?>
						<?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
						<?=Loc::getMessage('SPOL_TPL_FROM_DATE')?>
						<?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?>,
						<?
						if ($order['ORDER']['CANCELED'] !== 'Y')
						{
							echo Loc::getMessage('STATUS_'.$order['ORDER']['STATUS_ID']);
						}
						else
						{
							echo Loc::getMessage('STATUS_CANCELE');
						}
						?>
						<?//=Loc::getMessage('SPOL_TPL_SUMOF')?>
						<?//=$order['ORDER']['FORMATED_PRICE']?>
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sale-order-list-inner-container">
					<span class="sale-order-list-inner-title-line">
						<span class="sale-order-list-inner-title-line-item"><?=Loc::getMessage('SPOL_TPL_PAYMENT')?></span>
						<div class="status_payment <?=$order["ORDER"]["PAYED"]== 'Y' ? 'paid' : 'unpaid';?>"><span><?=$order['ADD_ALLOW_PAYED_PHRASE']?></span></div>
					</span>
					<span class="sale-order-list-inner-title-line">
						<span class="sale-order-list-inner-title-line-item"><?=Loc::getMessage('SPOL_TPL_DELIVERY')?></span>
						<div class="status_delivery <?=$order["ORDER"]["ALLOW_DELIVERY"]== 'Y' ? 'shipped' : 'not shipped';?>"><span><?=$order['ADD_ALLOW_DELIVERY_PHRASE']?></span></div>
					</span>
					<div class="row sale-order-list-inner-row">
						<div class="sale-order-list-top-border"></div>
						<div class="col-xs-4 sale-order-list-about-container">
							<a class="sale-order-list-about-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>"><?=Loc::getMessage('SPOL_TPL_MORE_ON_ORDER')?></a>
						</div>
						<div class="col-xs-4 sale-order-list-repeat-container">
							<a class="sale-order-list-repeat-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>"><?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?></a>
						</div>
						<div class="col-xs-4 sale-order-list-cancel-container">
							<a class="sale-order-list-cancel-link" href="javascript:void(0)" onclick="cancelOrder(<?=$order['ORDER']['ID']?>)"><?=Loc::getMessage('SPOL_TPL_CANCEL_ORDER')?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?
	}
	
	?>
	</div>
		<div class="clearfix"></div>
		<div><?=$arResult["NAV_STRING"];?></div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<script>
	function cancelOrder(id){
		BX.showWait();
		
		var postData = {};
		postData.action = 'cancel_order';
		postData.id = id;
		$.ajax({
		   // url: '/<?=urlencode($templateFolder.'/ajax.php');?>',
		   url:'<?=$templateFolder.'/ajax.php';?>',
           data: postData,
           method: 'POST',
           dataType: 'json',
           success: function(data){
				BX.closeWait();
				$('#order_item_'+id+' .sale-order-list-cancel-container').remove();
				$('#order_item_'+id).appendTo("#cancel_order");
           },

       });
	}
</script>

