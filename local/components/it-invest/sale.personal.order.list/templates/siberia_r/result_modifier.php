<?
foreach($arResult['ORDERS'] as &$arOrderItem)
{
	$arOrderItem["ADD_ALLOW_DELIVERY_PHRASE"] = $arOrderItem["ORDER"]["ALLOW_DELIVERY"] == 'Y' ? GetMessage("SPOL_TPL_LOADED") : GetMessage("SPOL_TPL_NOTLOADED");
	$arOrderItem["ADD_ALLOW_PAYED_PHRASE"] = $arOrderItem["ORDER"]["PAYED"] == 'Y' ? GetMessage("SPOL_TPL_PAID") : GetMessage("SPOL_TPL_NOTPAID");
}
?>