<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Currency,
	Bitrix\Catalog,
	Bitrix\Iblock;
/*=========================================================================
	Cache
=========================================================================*/
$cache = new CPHPCache();

$e = (isset($arParams['~ELEMENT_CODE']) && !empty($arParams['~ELEMENT_CODE'])) ? $arParams['~ELEMENT_CODE'] : $arParams['~ELEMENT_ID'];
$s = (isset($arParams['~SECTION_CODE']) && !empty($arParams['~SECTION_CODE'])) ? $arParams['~SECTION_CODE'] : $arParams['~SECTION_ID'];

if($e){//для рекомендаций в элементе
	$cache_id = 'catalog.custom.recommended.products.'.IBLOCK_CATALOG_ID.'.'.$e;
	$cache_path = 'catalog.custom.recommended.products/'.IBLOCK_CATALOG_ID.'/'.$e;
}
else if($s){//для рекомендаций в разделе
	$cache_id = 'catalog.custom.recommended.products.'.IBLOCK_CATALOG_ID.'.'.$s;
	$cache_path = 'catalog.custom.recommended.products/'.IBLOCK_CATALOG_ID.'/'.$s;
}
else{
	die();
}

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$cache_time = $arParams["CACHE_TIME"];
if (CModule::includeModule('currency')){
	$currency = Currency\CurrencyTable::getList(array(
		'select' => array('CURRENCY'),
		'filter' => array('=CURRENCY' => $arParams['CURRENCY_ID'])
	))->fetch();
}
else{die();}
$arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];
//isset cache?
if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
{
   $res = $cache->GetVars();
   
   if (is_array($res[$cache_id]) && (count($res[$cache_id]) > 0))
	  $arResult = $res[$cache_id];
  
}
//!cache
if (!is_array($res[$cache_id]))
{
	$arFilter = array('ID' => $arParams['IDS'], 'IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ACTIVE' => 'Y'); 
	$arSort = array('SORT' => 'ASC');
	$arSelect = array('ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'CATALOG_GROUP_1', 'CATALOG_QUANTITY');
	$res = CIBlockElement::GetList($arSort, $arFilter, false,  array('nTopCount' => $arParams['LINE_ELEMENT_COUNT']), $arSelect);
	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
	/*===============================================================================================================
			ajax уходит в component.php компонента catalog.custom.element
	===================================================================================================================*/
	$ajaxPath = '/catalog/section/element/index.php?';
	/*======================================*/
	$arUrlTemplates['~BUY_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~BUY_URL_TEMPLATE']);
	$arUrlTemplates['~ADD_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~ADD_URL_TEMPLATE']);
	$arUrlTemplates['~FAVOURITE_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2FAVOURITE&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['FAVOURITE_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~FAVOURITE_URL_TEMPLATE']);

	$arResult = array_merge($arResult, $arUrlTemplates);

	$arResult['RECOMMEND'] = array();
	while($ob = $res->GetNextElement()) 
	{
		$ob->fields["PRICES"] = array();
		if ($ob->fields['PREVIEW_PICTURE'])
		{
			$file = CFile::ResizeImageGet($ob->fields['PREVIEW_PICTURE'], array('width'=>$arParams['RESIZE_WIDTH'], 'height'=>$arParams['RESIZE_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
			$ob->fields['RESIZE_PREVIEW_PICTURE'] = $file;
		}
		if (!isset($ob->fields["CATALOG_MEASURE_RATIO"])){
			$ob->fields["CATALOG_MEASURE_RATIO"] = 1;
		}

		$arResult['RECOMMEND'][] = $ob->fields;
	}
	//////////// end cache /////////
	if ($cache_time > 0)
	{	
		// начинаем буферизирование вывода
		$cache->StartDataCache($cache_time, $cache_id, $cache_path);
		// записываем предварительно буферизированный вывод в файл кеша
		$cache->EndDataCache(array($cache_id=>$arResult));
	}
}

	foreach($arResult['RECOMMEND'] as &$item){
		$item["PRICES"] = array();
		$item['MIN_PRICE'] = false;
		$item["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $item, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
		if (!empty($item['PRICES']))
			$item['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($item['PRICES']);

		$item["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $item);		
	}

$this->includeComponentTemplate();
