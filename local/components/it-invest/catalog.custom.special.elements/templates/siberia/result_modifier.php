<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if($arResult['SECTION'])
{
	$arResult['SECTION']["PATH"] = array();
	$rsPath = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"],$arResult['SECTION']["ID"]);
	$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
	while($arPath = $rsPath->GetNext())
	{
		$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
		$arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
		$arResult['SECTION']["PATH"][] = $arPath;
	}
	
}
if ($arParams["ADD_SECTIONS_CHAIN"])
{
	foreach($arResult["SECTION"]["PATH"] as $arPath)
	{
		if ($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
			$APPLICATION->AddChainItem($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arPath["~SECTION_PAGE_URL"]);
		else
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
	}
	//$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
	
}
