<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<dl class="basic-ex-breadcrumbs clearfix">';
$atr_target = '';
if(SLANGCODE == 'ch'){$atr_target = 'target="_blank"';};
$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	
	
	
	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '
			<dd class="bx-breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"'.$child.$nextRef.'>
				<a '.$atr_target.' class="beb-nav" href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url"><span>'.$title.'</span></a>
			</dd>';
	}
	else
	{
		$strReturn .= '
			<dd class="bx-breadcrumb-item">
				<span>'.$title.'</span>
			</dd>';
	}
}

$strReturn .= '</dl>';

return $strReturn;
