<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<?if (!empty($arResult)):?>
<ul>
<!--<li><button class="cross js-click-openMenu">&#215;</buton></li>-->
<li><?if ($USER->IsAuthorized()):
		$name = trim($USER->GetFullName());
		if (! $name)
			$name = trim($USER->GetLogin());
		if (strlen($name) > 15)
			$name = substr($name, 0, 12).'...';
		?>
		<a style="float:left" href="<?=SITE_DIR?>personal/"><?=$USER->GetLogin()?></a>
		&nbsp;
		<a style="float:right" href="?logout=yes"><?=GetMessage('TSB1_LOGOUT')?></a>
	<?else:?>
		<a href="<?=SITE_DIR?>login/"><?=GetMessage('TSB1_LOGIN')?></a>
		&nbsp;
		<!--<a href="<?//=$arParams['PATH_TO_REGISTER']?>?register=yes"><?//=GetMessage('TSB1_REGISTER')?></a>-->
<?endif?></li>
<li><a href="javascript:void(0)" class="js-click-openSelectLanguage"><?=Loc::getMessage("LANGUAGE")?></a></li>
<?
$c = 0;
foreach($arResult as $arItem):
	if($c == 1)
	{
?>
		<li><a href="<?=SITE_DIR?>catalog/"><?=Loc::getMessage("CATALOG")?></a></li>
<?
	}
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?
$c+=1;
endforeach?>

</ul>
<?endif?>