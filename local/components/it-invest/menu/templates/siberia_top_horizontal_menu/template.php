<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
$menuId = "bx_menu_".SLANGCODE;
$arParams['menuId'] = $menuId;
?>
<div id="<?=$menuId?>">
<?if (!empty($arResult)):?>
<?
$frame = $this->createFrame($menuId, false)->begin();
?>
<ul class="secondary-menu">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>
<?
$frame->beginStub();
$arResult['COMPOSITE_STUB'] = 'Y';
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
if (!$compositeStub):
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>
<?endif?>
<!--Здесь можно добавить статику-->
<!--код-->
<?
unset($arResult['COMPOSITE_STUB']);
$frame->end();
?>

</ul>
<?endif?>
</div>