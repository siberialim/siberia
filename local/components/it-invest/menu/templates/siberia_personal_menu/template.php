<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="personal_menu">
	<div class="sticky-anchor"></div>
	<div class="wrap-sidebar-menu">
		<div class="block-out-background">
			<!--<span class="head"><?//=GetMessage('MENU')?></span>-->
			<?if (!empty($arResult)):?>
				<ul class="sidebar-menu">
					<?
					foreach($arResult as $arItem):
						if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
						continue;
						?>
						<?if($arItem["SELECTED"]):?>
							<li><a href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
						<?else:?>
							<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
						<?endif?>
					<?endforeach?>
				</ul>
			<?endif?>
		</div>
	</div>
</div>