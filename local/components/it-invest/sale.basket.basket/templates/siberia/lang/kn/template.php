<?
$MESS["CART"] = "바구니";
$MESS["CART_IS_EMPTY"] = "바구니는 비어 있다";
$MESS["RETURN_TO_CATALOG"] = "#CATALOG# 가서 상품을 골라서 추가하세요";
$MESS["CATALOG"] = "카탈로그에";
$MESS["SALE_ORDER"] = "주문하기";
$MESS["SALE_OR"] = "or";
$MESS["SALE_NAME"] = "상품을";
$$MESS["SALE_PROPS"] = "Properties";
$MESS["SALE_PRICE"] = "가격";
$MESS["SALE_TYPE"] = "Price type";
$MESS["SALE_QUANTITY"] = "수량";
$MESS["SALE_SUM"] = "Total";
$MESS["SALE_DELETE"] = "Delete";
$MESS["SALE_DELAY"] = "즐겨찾기";
$MESS["SALE_ADD_TO_BASKET"] = "Add to cart";
$MESS["SALE_WEIGHT"] = "무게";
$MESS["SALE_TOTAL_WEIGHT"] = "전체 무게:";
$MESS["SALE_WEIGHT_G"] = "g";
$MESS["SALE_DELAYED_TITLE"] = "즐겨찾기";
$MESS["SALE_UNAVAIL_TITLE"] = "Currently unavailable";
$MESS["STB_ORDER_PROMT"] = "Click \"Check out\" to complete your order";
$MESS["STB_COUPON_PROMT"] = "If you have special coupon code for discount, please enter it here:";
$MESS["SALE_VAT"] = "Tax:";
$MESS["SALE_VAT_EXCLUDED"] = "어치 추문";
$MESS["SALE_VAT_INCLUDED"] = "Tax included:";
$MESS["SALE_TOTAL"] = "총 금액:";
$MESS["SALE_CONTENT_DISCOUNT"] = "할인";
$MESS["SALE_DISCOUNT"] = "할인";
$MESS["SALE_NOTIFY_TITLE"] = "Backordered";
$MESS["SALE_REFRESH_NOTIFY_DESCR"] = "Click this button to remove products.";
$MESS["SALE_ITEMS"] = "상품이 장바구니에 담겨있습니다:";
$MESS["SALE_BASKET_ITEMS"] = "Available";
$MESS["SALE_BASKET_ITEMS_DELAYED"] = "Saved for later";
$MESS["SALE_BASKET_ITEMS_SUBSCRIBED"] = "Back in stock soon";
$MESS["SALE_BASKET_ITEMS_NOT_AVAILABLE"] = "Unavailable";
$MESS["SALE_NO_ITEMS"] = "바구니는 비어 있다";
$MESS["SALE_REFRESH"] = "Refresh";
$MESS["SALE_COUPON_APPLY"] = "Ок";
$MESS["SALE_COUPON_APPLY_TITLE"] = "Click to apply the new coupon";
$MESS["BTN_LOG_IN"] = "로그인";
$MESS["NOT_AUTORIZ"] = "오더 목록을 보려면 #BTN_LOG_IN# 필요합니다";
$MESS["ORDER_SENT"] = "주문된 상품은 배송되었다";
$MESS["ERR"] = "오류";
?>