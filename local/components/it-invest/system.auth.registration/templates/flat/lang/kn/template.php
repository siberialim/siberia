<?
$MESS["AUTH_REGISTER"] = "회원가입";
$MESS["AUTH_NAME"] = "이름";
$MESS["AUTH_LAST_NAME"] = "성";
$MESS["AUTH_LOGIN_MIN"] = "로그인";
$MESS["AUTH_CONFIRM"] = "비밀번호 확인";
$MESS["CAPTCHA_REGF_PROMT"] = "";
$MESS["AUTH_REQ"] = "";
$MESS["AUTH_AUTH"] = "로그인";
$MESS["AUTH_PASSWORD_REQ"] = "비밀번호";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "";
$MESS["AUTH_EMAIL_SENT"] = "";
$MESS["AUTH_EMAIL"] = "고객님의 E-mail";
$MESS["AUTH_SECURE_NOTE"] = "";
?>