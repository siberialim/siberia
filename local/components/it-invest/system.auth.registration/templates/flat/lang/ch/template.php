<?
$MESS["AUTH_REGISTER"] = "本网站注册";
$MESS["AUTH_NAME"] = "收货人的名字";
$MESS["AUTH_LAST_NAME"] = "收货人的姓";
$MESS["AUTH_LOGIN_MIN"] = "登录名";
$MESS["AUTH_CONFIRM"] = "Password confirmation";
$MESS["CAPTCHA_REGF_PROMT"] = "Type text from image";
$MESS["AUTH_REQ"] = "Required fields.";
$MESS["AUTH_AUTH"] = "登录";
$MESS["AUTH_PASSWORD_REQ"] = "密码";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "A registration confirmation request will be sent to the specified e-mail address.";
$MESS["AUTH_EMAIL_SENT"] = "A registration confirmation request has been sent to the specified e-mail address.";
$MESS["AUTH_EMAIL"] = "您的邮箱地址";
$MESS["AUTH_SECURE_NOTE"] = "The password will be encrypted before it is sent. This will prevent the password from appearing in open form over data transmission channels.";
?>