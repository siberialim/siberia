<?
$MESS["AUTH_PLEASE_AUTH"] = "로그인해 주십시오";
$MESS["AUTH_LOGIN"] = "로그인";
$MESS["AUTH_PASSWORD"] = "비밀번호";
$MESS["AUTH_REMEMBER_ME"] = "이 컴퓨터에서 내 정보 저장";
$MESS["AUTH_AUTHORIZE"] = "들어가기";
$MESS["AUTH_REGISTER"] = "회원가입";
$MESS["AUTH_FIRST_ONE"] = "이 사이트를 처음에 사용하시는 경우, 등록양식을 기입해주시기 바랍니다.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "비밀번호 찾기";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_TITLE"] = "회원가입";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
?>