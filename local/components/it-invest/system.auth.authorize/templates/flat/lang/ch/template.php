<?
$MESS["AUTH_PLEASE_AUTH"] = "请登录";
$MESS["AUTH_LOGIN"] = "登录名";
$MESS["AUTH_PASSWORD"] = "密码";
$MESS["AUTH_REMEMBER_ME"] = "在此电脑记住注册信息";
$MESS["AUTH_AUTHORIZE"] = "进入本站";
$MESS["AUTH_REGISTER"] = "注册";
$MESS["AUTH_FIRST_ONE"] = "如果您头一次访问本站，请填写注册表格";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "忘记密码？";
$MESS["AUTH_CAPTCHA_PROMT"] = "";
$MESS["AUTH_TITLE"] = "进入本站";
$MESS["AUTH_SECURE_NOTE"] = "";
$MESS["AUTH_NONSECURE_NOTE"] = "";
?>