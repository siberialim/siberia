<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$this->setFrameMode(true);
$menuId = "bx_catalog_nav_".SLANGCODE;
$arParams['catalogNavId'] = $menuId;

$map_sections = array_chunk($arResult['ROOT']['CHILD'], 100, true);

function outTree($category_arr,$parent_id,$parent=true) 
{
	foreach ($category_arr as $value) //Обходим 
	{ 
		?>
		<li class="dropdown-submenu isset_child">
			<?
				if($value['SECTION_PAGE_URL'] == '/catalog/dry_plant_extracts/') 
				{
					$value['SECTION_PAGE_URL'] = '/catalog/dry_plant_extracts/dry_plant_extracts/';
				}
			?>
			<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> tabindex="-1" href="<?=$value['SECTION_PAGE_URL']?>">
				<?
					if($value['UF_ICON']['src'])
					{
				?>
						<img src="<?=$value['UF_ICON']['src']?>"/>
				<?}?>
				<?=$value['NAME']?>
			</a>
			<?
				if (count($value['CHILD'])>0)
				{ 
			?>
					<div class="primary-menu__cont secondary" style="left: 28rem;">
						<ul>
							<?outTree($value['CHILD']);?>
						</ul>
					</div>
			<?
				}
			?>
		</li>
		<?
		if (!$parent_id)
		{
			$c_section+=1;
		}
	} 
}
?>
<div class="primary-menu__trigger">
	<i class="fa fa-list-ul" aria-hidden="true"></i>
	<span><?=Loc::getMessage("HEADER_MENU")?></span>
</div>
<div id="<?=$menuId?>">
	<?if (!empty($arResult)):?>
	<?
	$frame = $this->createFrame($menuId, false)->begin();
	?>
	<div style="position:relative;">
		<?	
			$left_style = 0;
			$first_step = true;
			foreach ($map_sections as $key=>$sections)
			{
		?>
				<div class="primary-menu__cont parent <?if(!$first_step){echo ' more';}?>" style="left:<?=$left_style?>rem;">
					<ul><?
						outTree($sections);
						if ($first_step)
						{
					?>
							<!--<li class="dropdown-submenu css-show-hidden-section isset_child"><a tabindex="-1" href="javascript:void(0)"><?//=Loc::getMessage("MORE")?></a></li>-->
					<?
						}
					?>
					</ul>
				</div>
		<?
				$first_step = false;
				$left_style += 28;
				break;
			}
		?>
	</div>
	<?
	$frame->beginStub();
	$arResult['COMPOSITE_STUB'] = 'Y';
	$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
	if (!$compositeStub):
	?>
	<div style="position:relative;">
		<?	
			$left_style = 0;
			$first_step = true;
			foreach ($map_sections as $key=>$sections)
			{
		?>
				<div class="primary-menu__cont parent <?if(!$first_step){echo ' more';}?>" style="left:<?=$left_style?>rem;">
					<ul><?
						outTree($sections);
						if ($first_step)
						{
					?>
							<!--<li class="dropdown-submenu css-show-hidden-section isset_child"><a tabindex="-1" href="javascript:void(0)"><?//=Loc::getMessage("MORE")?></a></li>-->
					<?
						}
					?>
					</ul>
				</div>
		<?
				$first_step = false;
				$left_style += 28;
			}
		?>
	</div>
	<?endif?>
	<!--Здесь можно добавить статику-->
	<!--код-->
	<?
	unset($arResult['COMPOSITE_STUB']);
	$frame->end();
	?>
	<?endif?>
</div>