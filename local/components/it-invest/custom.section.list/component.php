<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$cache = new CPHPCache();

if (!isset($arParams["CACHE_TIME"])){
	$arParams["CACHE_TIME"] = 36000000;
}

$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'catalog.nav.custom.section.list.'.IBLOCK_CATALOG_ID;
$cache_path = 'catalog.nav.custom.section.list/'.IBLOCK_CATALOG_ID;

//isset cache?
if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)) {
   $res = $cache->GetVars();
   if (is_array($res[$cache_id]) && (count($res[$cache_id]) > 0))
	  $arResult = $res[$cache_id];
}
//!cache
if (!is_array($res[$cache_id])) {
	$arFilter = array(		
		'ACTIVE' => 'Y',
		'IBLOCK_ID' => IBLOCK_CATALOG_ID,
		'GLOBAL_ACTIVE'=>'Y',
	);
	$arSelect = array('IBLOCK_ID','ID','NAME','DESCRIPTION','PICTURE','IBLOCK_SECTION_ID', 'SECTION_PAGE_URL', "UF_COLOR","UF_ICON", "UF_BACKGROUND_IMAGE");
	$arOrder = array('DEPTH_LEVEL'=>'ASC','SORT'=>'ASC');
	$rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
	$sectionLinc = array();
	$arResult['ROOT'] = array();

	while ($arSection = $rsSections->GetNext()) 
	{
		if (!is_array($arSection['UF_ICON']))
		{
			$file = CFile::ResizeImageGet($arSection['UF_ICON'], array('width'=>17, 'height'=>22), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
			$arSection['UF_ICON'] = $file;
		}
		else
		{
			$file = CFile::ResizeImageGet($arSection['UF_ICON'][0], array('width'=>17, 'height'=>22), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
			$arSection['UF_ICON'] = $file;
		}
		
		$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']] = $arSection;
		$sectionLinc[$arSection['ID']] = &$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']];
		
		$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']] = $arSection;
		$sectionLinc[$arSection['ID']] = &$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']];
	}

	$arResult['ROOT']['CHILD'] = $sectionLinc[0]['CHILD'];
	/////////// end cache /////////
	if ($cache_time > 0) {
		$cache->StartDataCache($cache_time, $cache_id, $cache_path);
		$cache->EndDataCache(array($cache_id=>$arResult));
	}
}

$this->includeComponentTemplate();
