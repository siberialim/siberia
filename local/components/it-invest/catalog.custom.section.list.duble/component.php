<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Iblock;

$cache = new CPHPCache();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'catalog.custom.section.list.catalog.'.IBLOCK_CATALOG_ID;
$cache_path = 'catalog.custom.section.list.catalog/'.IBLOCK_CATALOG_ID;
	//isset cache?
	if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
	{
	   $res = $cache->GetVars();
	   if (is_array($res[$cache_id]) && (count($res[$cache_id]) > 0))
		  $arResult = $res[$cache_id];
	}
	//!cache
	if (!is_array($res[$cache_id]))
	{
		$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"],'SECTION_ID' =>  SECTION_ID, 'ACTIVE' => 'Y'); 
		$arSort = array('SORT' => 'ASC');
		$arSelect = array('ID', 'NAME', 'CODE', 'SECTION_PAGE_URL', 'UF_BACKGROUND_IMAGE', 'UF_ICON', 'UF_COLOR');
		$rsSections = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
		
		while ($arSection = $rsSections->GetNext())
		{
			/*if ($arSection['UF_ICON'][0])
			{
				$file = CFile::ResizeImageGet($arSection['UF_ICON'][0], array('width'=>42, 'height'=>42), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$arSection['UF_ICON'] = $file;
			}*/
			
			if ($arSection['UF_BACKGROUND_IMAGE'])
			{
				$file = CFile::ResizeImageGet($arSection['UF_BACKGROUND_IMAGE'], array('width'=>248, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$arSection['PICTURE_RESIZE'] = $file;
			}
			
			
			
			//найдем элементы раздела
			$arFilter = array();
			$arItems = array();

		
			$arSort = $arParams['AR_SORT'];

			if($_REQUEST['sort'] && $_REQUEST['type']){
				 $arSort[$_REQUEST['sort']] = $_REQUEST['type'];
			}
			$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => $arSection['ID'], 'ACTIVE' => 'Y'); 
			$res = CIBlockElement::GetList($arSort, $arFilter, false,  array("nPageSize" => $arParams['PAGE_ELEMENT_COUNT'], "bShowAll" => false), $arParams['AR_SELECT']);
			$arItems["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);

			$currentPath = CHTTP::urlDeleteParams(
				$arParams['CUSTOM_CURRENT_PAGE']?: $APPLICATION->GetCurPageParam(),
				array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
				array('delete_system_params' => true)
			);

			$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');
			if ($arParams['COMPARE_PATH'] == '')
			{
				$comparePath = $currentPath;
			}
			else
			{
				$comparePath = CHTTP::urlDeleteParams(
				$arParams['COMPARE_PATH'],
				array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
				array('delete_system_params' => true)
				);
				$comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');
			}
			/*===============================================================================================================
				ajax уходит в component.php компонента catalog.custom.element
			===================================================================================================================*/
			$ajaxPath = '/catalog/section/element/index.php?';
			/*======================================*/
			$arUrlTemplates['~BUY_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
			$arUrlTemplates['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~BUY_URL_TEMPLATE']);
			$arUrlTemplates['~ADD_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
			$arUrlTemplates['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~ADD_URL_TEMPLATE']);
			$arUrlTemplates['~FAVOURITE_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2FAVOURITE&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
			$arUrlTemplates['FAVOURITE_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~FAVOURITE_URL_TEMPLATE']);

			$arItems = array_merge($arItems, $arUrlTemplates);
			while($ob = $res->GetNextElement()) 
			{
				if ($ob->fields['PREVIEW_PICTURE'])
				{
					$file = CFile::ResizeImageGet($ob->fields['PREVIEW_PICTURE'], array('width'=>200, 'height'=>163), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);                
					$ob->fields['PREVIEW_PHOTO'] = $file;
				}
				if (!isset($ob->fields["CATALOG_MEASURE_RATIO"])){
					$ob->fields["CATALOG_MEASURE_RATIO"] = 1;
				}
				$arItems['ITEMS'][] = $ob->fields;
			} 
			$arSection['RESULT'] = $arItems;
			$arResult['SECTIONS'][] = $arSection;
			$arIBlockListID[] = $arSection['ID'];
				
		}
	
		//////////// end cache /////////
		if ($cache_time > 0)
		{
			// начинаем буферизирование вывода
			$cache->StartDataCache($cache_time, $cache_id, $cache_path);
			// записываем предварительно буферизированный вывод в файл кеша
			$cache->EndDataCache(array($cache_id=>$arResult));
		}
	}
	//cформируем массив id разделов для компонента section
	$id_sections = array();
	for($i = 0; $i < count($arResult['SECTIONS']); $i++){
		array_push($id_sections, $arResult['SECTIONS'][$i]);
	}
	//сохраним ids разделов
	GarbageStorage::set('id_sections', $id_sections); #установить значение

	$this->includeComponentTemplate();
