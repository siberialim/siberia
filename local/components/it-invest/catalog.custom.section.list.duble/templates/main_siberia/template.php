<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID(IB_PRODUCTS, "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID(IB_PRODUCTS, "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<?
foreach ($arResult['SECTIONS'] as $arSection)
{	
	//if(CIBlockSection::GetSectionElementsCount($arSection['ID'], Array("CNT_ACTIVE"=>"Y")) > 0){
		?><div class="catalog-b" data-cat="<?=$arSection["ID"]?>" style="border-color:<?=$arSection["UF_COLOR"]?>">	
			<div class="catalog-b-header" style="border-bottom-color:<?=$arSection["UF_COLOR"]?>">
				<!--<strong style="color: <?//=$arSection["UF_COLOR"]?>"></strong>-->
				<!--<a href="<?//=$arSection['SECTION_PAGE_URL']?>"><?//=$arSection['NAME']?></a>-->
			</div>
			<div class="catalog-preview">
				<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arSection['SECTION_PAGE_URL']?>"><img height="500px" width="248px" src="<?=$arSection['PICTURE_RESIZE']['src']?>" alt="<?=$arSection["NAME"]?>"></a>
			</div>
		</div><?
//	}
}
?>
