<?
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hlBrand = HL\HighloadBlockTable::getById(2)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlBrand);

$main_query = new Entity\Query($entity);
$main_query->setSelect(array('*'));
$main_query->setOrder(array());

$result = $main_query->exec();
$result = new CDBResult($result);

// build results
$brand_rows = array();

$tableColumns = array();

while ($row = $result->Fetch())
{
	foreach ($row as $k => $v)
	{
		if ($k == 'ID')
		{
			$tableColumns['ID'] = true;
			continue;
		}

		$arUserField = $fields[$k];

		if ($arUserField["SHOW_IN_LIST"]!="Y")
		{
			continue;
		}

		$html = call_user_func_array(
			array($arUserField["USER_TYPE"]["CLASS_NAME"], "getadminlistviewhtml"),
			array(
				$arUserField,
				array(
					"NAME" => "FIELDS[".$row['ID']."][".$arUserField["FIELD_NAME"]."]",
					"VALUE" => htmlspecialcharsbx($v)
				)
			)
		);

		if($html == '')
		{
			$html = '&nbsp;';
		}

		$tableColumns[$k] = true;

		$row[$k] = $html;
	}


	$brand_rows[] = $row;
}

?>
<div id="filter_container" class="filter-container">
	<form name="arrFilter" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
		<input type="text" name="BRAND" value="<?=htmlspecialcharsEx($arParams['BRAND'])?>"/>
		<input type="hidden" name="WEIGHT" value="<?=htmlspecialcharsEx($arParams['WEIGHT'])?>"/>
		<div class="filter-title clearfix">
			<span>商品筛选</span>
			(共<b class="op-search-result">22</b>件商品)
			<div class="filter-clean"><a href="javascript:location.reload();Memory.clean('gallery.filter');">Clear filter</a></div>
		</div>
		<div id="gallery_filter" class="gallery-filter">
			<div id="filter_selected" class="filter-selected clearfix">
				<dl class="filter-selected-list clearfix">
					<dt class="filter-selected-title"></dt>
					<dd class="filter-selected-values"></dd>
				</dl>
			</div>
			<div id="filter_lists" class="filter-lists-container">
				<dl class="filter-entries clearfix">
					<dt class="filter-entries-label">Brand:</dt>
					<?foreach($brand_rows as $brand){?>
						<dd class="filter-entries-values <?=($brand['ID'] == $arParams['BRAND']) ? ' active' : '';?>">
							<span class="filter-item"><a href="javascript:void(0)" data-value="<?=$brand['ID']?>" class="handle action-cat-filter"><?=$brand['UF_NAME']?></a></span>
						</dd>
					<?}?>
				</dl>
				<dl class="filter-entries clearfix" data-label="s_4">
					<dt class="filter-entries-label">Weight：</dt>
					<dd class="filter-entries-values">
						<span class="filter-item "><a href="javascript:void(0);" class="handle action-select-unlimit">1kg</a></span>
						<span class="filter-item " data-fid="s_4-39"><a href="javascript:void(0);" class="handle action-select-filter">2kg</a><a href="javascript:void(0);" class="icon action-delete-filter">×</a></span>
						<span class="filter-item " data-fid="s_4-40"><a href="javascript:void(0);" class="handle action-select-filter">3kg</a><a href="javascript:void(0);" class="icon action-delete-filter">×</a></span>
					</dd>
				</dl>
			</div>
		</div>
		<input type="submit" name="set_filter" value="Show" />
	</form>
</div>