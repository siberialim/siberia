<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);
	
CJSCore::Init(array('clipboard'));
$title = Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
	"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
	"#DATE_ORDER_CREATE#"=> $arResult["DATE_INSERT_FORMATED"]
));

if ($arResult['CANCELED'] !== 'Y')
{
	$title .= GetMessage('STATUS_'.$arResult["STATUS"]["ID"]);
}
else
{
	$title .= Loc::getMessage('SPOD_ORDER_CANCELED');
}
	
$APPLICATION->SetTitle($title);

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach ($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}

	$component = $this->__component;

	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}
}
else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach ($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	?>

	<div class="sale-order-detail no-indent">
		<div class="sale-order-detail-general no-indent">
			<div class="row">
				<div class="sale-order-detail-general-head">
					<span class="sale-order-detail-general-item">
						<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
							"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
							"#DATE_ORDER_CREATE#"=> $arResult["DATE_INSERT_FORMATED"]
						))?>
						<?
						if ($arResult['CANCELED'] !== 'Y')
						{
							echo GetMessage('STATUS_'.$arResult["STATUS"]["ID"]);
						}
						else
						{
							echo Loc::getMessage('SPOD_ORDER_CANCELED');
						}
						?>
						<?//= count($arResult['BASKET']);?>
						<?
						/*$count = count($arResult['BASKET']) % 10;
						if ($count == '1')
						{
							echo Loc::getMessage('SPOD_TPL_GOOD');
						}
						elseif ($count >= '2' && $count <= '4')
						{
							echo Loc::getMessage('SPOD_TPL_TWO_GOODS');
						}
						else
						{
							echo Loc::getMessage('SPOD_TPL_GOODS');
						}*/
						?>
						<?//=Loc::getMessage('SPOD_TPL_SUMOF')?>
						<?//=$arResult["PRICE_FORMATED"]?>
					</span>
				</div>
			</div>

			<div class="row sale-order-detail-about-order">

				<div class="sale-order-detail-about-order-container">
					<div class="row">
						<div class="sale-order-detail-about-order-title no-indent">
							<h3 class="sale-order-detail-about-order-title-element">
								<?= Loc::getMessage('SPOD_LIST_ORDER_INFO') ?>
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="sale-order-detail-about-order-inner-container no-indent">
							<div class="row">
								<div class="sale-order-detail-about-order-inner-container-name">
									<div class="sale-order-detail-about-order-inner-container-name-title">
										<?
										$userName = $arResult["USER"]["NAME"] ." ". $arResult["USER"]["SECOND_NAME"] ." ". $arResult["USER"]["LAST_NAME"];
										if (strlen($userName) || strlen($arResult['FIO']))
										{
											echo Loc::getMessage('SPOD_LIST_FIO').':';
										}
										else
										{
											echo Loc::getMessage('SPOD_LOGIN').':';
										}
										?>
									</div>
									<div class="sale-order-detail-about-order-inner-container-name-detail">
										<?
										if (strlen($userName))
										{
											echo htmlspecialcharsbx($userName);
										}
										elseif (strlen($arResult['FIO']))
										{
											echo htmlspecialcharsbx($arResult['FIO']);
										}
										else
										{
											echo htmlspecialcharsbx($arResult["USER"]['LOGIN']);
										}
										?>
									</div>
								</div>

								<div class="sale-order-detail-about-order-inner-container-status">
									<div class="sale-order-detail-about-order-inner-container-status-title">
										<?= Loc::getMessage('SPOD_LIST_CURRENT_STATUS', array(
											'#DATE_ORDER_CREATE#' => $arResult["DATE_INSERT_FORMATED"]
										)) ?>
									</div>
									<div class="sale-order-detail-about-order-inner-container-status-detail">
										<?
										if ($arResult['CANCELED'] !== 'Y')
										{
											echo GetMessage('STATUS_'.$arResult["STATUS"]["ID"]);
										}
										else
										{
											echo Loc::getMessage('SPOD_ORDER_CANCELED');
										}
										?>
									</div>
								</div>

								<!--<div class="col-md-2 col-sm-6 sale-order-detail-about-order-inner-container-price">
									<div class="sale-order-detail-about-order-inner-container-price-title">
										<?//= Loc::getMessage('SPOD_ORDER_PRICE')?>:
									</div>
									<div class="sale-order-detail-about-order-inner-container-price-detail">
										<?//= $arResult["PRICE_FORMATED"]?>
									</div>
								</div>-->
								<div class="sale-order-detail-about-order-inner-container-repeat">
									<a href="<?=$arResult["URL_TO_COPY"]?>" class="sale-order-detail-about-order-inner-container-repeat-button">
										<?= Loc::getMessage('SPOD_ORDER_REPEAT') ?>
									</a>
									<?
									if ($arResult["CAN_CANCEL"] === "Y")
									{
										?>
										<a href="javascript:void(0)" class="sale-order-detail-about-order-inner-container-repeat-cancel">
											<?= Loc::getMessage('SPOD_ORDER_CANCEL') ?>
										</a>
										<?
									}
									?>
								</div>
																	<a class="sale-order-detail-about-order-inner-container-name-read-less" style="display:none">
										<?= Loc::getMessage('SPOD_LIST_LESS') ?>
									</a>
									<a class="sale-order-detail-about-order-inner-container-name-read-more">
										<?= Loc::getMessage('SPOD_LIST_MORE') ?>
									</a>
							</div>
							<div class="sale-order-detail-about-order-inner-container-details" style="display:none">
								<h4 class="sale-order-detail-about-order-inner-container-details-title">
									<?= Loc::getMessage('SPOD_USER_INFORMATION') ?>
								</h4>
								<ul class="sale-order-detail-about-order-inner-container-details-list">
									<?
									if (strlen($arResult["USER"]["EMAIL"]))
									{
										?>
										<li class="sale-order-detail-about-order-inner-container-list-item">
											<?= Loc::getMessage('SPOD_LOGIN')?>:
											<div class="sale-order-detail-about-order-inner-container-list-item-element">
												<?= htmlspecialcharsbx($arResult["USER"]["LOGIN"]) ?>
											</div>
										</li>
										<?
									}
									if (strlen($arResult["USER"]["EMAIL"]))
									{
										?>
										<li class="sale-order-detail-about-order-inner-container-list-item">
											<?= Loc::getMessage('SPOD_EMAIL')?>:
											<a class="sale-order-detail-about-order-inner-container-list-item-link"
											   href="mailto:<?= htmlspecialcharsbx($arResult["USER"]["EMAIL"]) ?>"><?= htmlspecialcharsbx($arResult["USER"]["EMAIL"]) ?></a>
										</li>
										<?
									}
									/*if (strlen($arResult["USER"]["EMAIL"]))
									{
										?>
										<li class="sale-order-detail-about-order-inner-container-list-item">
											<?= Loc::getMessage('SPOD_ORDER_PERS_TYPE') ?>:
											<div class="sale-order-detail-about-order-inner-container-list-item-element">
												<?= htmlspecialcharsbx($arResult["PERSON_TYPE"]["NAME"]) ?>
											</div>
										</li>
										<?
									}*/
									if (isset($arResult["ORDER_PROPS"]))
									{
										foreach ($arResult["ORDER_PROPS"] as $property)
										{
											?>
											<li class="sale-order-detail-about-order-inner-container-list-item">
												<?= htmlspecialcharsbx($property['NAME']) ?>:
												<div class="sale-order-detail-about-order-inner-container-list-item-element">
													<?
													if ($property["TYPE"] == "Y/N")
													{
														echo Loc::getMessage('SPOD_' . ($property["VALUE"] == "Y" ? 'YES' : 'NO'));
													}
													else
													{
														if ($property['MULTIPLE'] == 'Y' && $property['TYPE'] !== 'FILE')
														{
															$propertyList = unserialize($property["VALUE"]);
															foreach ($propertyList as $propertyElement)
															{
																echo $propertyElement . '</br>';
															}
														}
														else
														{
															echo $property["VALUE"];
														}
													}
													?>
												</div>
											</li>
											<?
										}
									}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row sale-order-detail-payment-options">

				<div class="sale-order-detail-payment-options-container no-indent">
					<div class="row">
						<div class="sale-order-detail-payment-options-title no-indent">
							<h3 class="sale-order-detail-payment-options-title-element">
								<?= Loc::getMessage('SPOD_ORDER_PAYMENT') ?>
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="sale-order-detail-payment-options-inner-container">
							<div class="row">
								<div class="sale-order-detail-payment-options-info no-indent">
									<div class="row">
										<!--<div class="col-md-1 col-sm-2 col-xs-2 sale-order-detail-payment-options-info-image"></div>-->
										<div class="sale-order-detail-payment-options-info-container no-indent">
											<div class="sale-order-detail-payment-options-info-order-number">
												<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
													"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
													"#DATE_ORDER_CREATE#"=> $arResult["DATE_INSERT_FORMATED"]
												))?>
												<?
												if ($arResult['CANCELED'] !== 'Y')
												{
													echo strtolower(GetMessage('STATUS_'.$arResult["STATUS"]["ID"]));
												}
												else
												{
													echo strtolower(Loc::getMessage('SPOD_ORDER_CANCELED'));
												}
												?>
											</div>
											<!--<div class="sale-order-detail-payment-options-info-total-price">
												<?//=Loc::getMessage('SPOD_ORDER_PRICE_FULL')?>:
												<span><?//=$arResult["PRICE_FORMATED"]?></span>
											</div>-->
										</div>
									</div>
								</div><!--sale-order-detail-payment-options-info-->
							</div>
							<div class="row">
								<div class="sale-order-detail-payment-options-methods-container">
									<?
									foreach ($arResult['PAYMENT'] as $payment)
									{
										?>
										<div class="row payment-options-methods-row">
											<div class="sale-order-detail-payment-options-methods">
												<div class="row sale-order-detail-payment-options-methods-information-block">
													<div class="sale-order-detail-payment-options-methods-image-container">
													<span class="sale-order-detail-payment-options-methods-image-element"
														  style="background-image: url('<?=htmlspecialcharsbx($payment['PAY_SYSTEM']["SRC_LOGOTIP"])?>');"></span>
													</div>
													<div class="sale-order-detail-payment-options-methods-info">
														<div class="sale-order-detail-payment-options-methods-info-title">
															<div class="sale-order-detail-methods-title">
																<?
																$paymentData[$payment['ACCOUNT_NUMBER']] = array(
																	"payment" => $payment['ACCOUNT_NUMBER'],
																	"order" => $arResult['ACCOUNT_NUMBER']
																);
																$paymentSubTitle = Loc::getMessage('SPOD_TPL_BILL')." ".Loc::getMessage('SPOD_NUM_SIGN').$payment['ACCOUNT_NUMBER'];
																if(isset($payment['DATE_BILL']))
																{
																	$paymentSubTitle .= " ".Loc::getMessage('SPOD_FROM')." ".$payment['DATE_BILL']->format($arParams['ACTIVE_DATE_FORMAT']);
																}
																$paymentSubTitle .=",";
																echo htmlspecialcharsbx($paymentSubTitle);
																?>
																<span class="sale-order-list-payment-title-element"><?=$payment['PAY_SYSTEM_NAME']?></span>
																<?
																if ($payment['PAID'] === 'Y')
																{
																	?>
																	<span class="sale-order-detail-payment-options-methods-info-title-status-success">
																	<?=Loc::getMessage('SPOD_PAYMENT_PAID')?></span>
																	<?
																}
																else
																{
																	?>
																	<span class="sale-order-detail-payment-options-methods-info-title-status-alert">
																	<?=Loc::getMessage('SPOD_PAYMENT_UNPAID')?></span>
																	<?
																}
																?>
															</div>
														</div>
														<div class="sale-order-detail-payment-options-methods-info-total-price">
															<span class="sale-order-detail-sum-name"><?= Loc::getMessage('SPOD_ORDER_PRICE_BILL')?>:</span>
															<span class="sale-order-detail-sum-number"><?=$payment['PRICE_FORMATED']?></span>
														</div>
														<?
														if ($payment['PAID'] !== 'Y' && $arResult['CANCELED'] !== 'Y')
														{
															?>
															<a href="#" id="<?=$payment['ACCOUNT_NUMBER']?>" class="sale-order-detail-payment-options-methods-info-change-link"><?=Loc::getMessage('SPOD_CHANGE_PAYMENT_TYPE')?></a>
															<?
														}
														?>
													</div>
													<?
													if ($payment['PAY_SYSTEM']["IS_CASH"] !== "Y")
													{
														?>
														<div class="sale-order-detail-payment-options-methods-button-container">
															<?
															if ($payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] === 'Y')
															{
																?>
																<a class="btn-theme sale-order-detail-payment-options-methods-button-element-new-window"
																   target="_blank"
																   href="<?=htmlspecialcharsbx($payment['PAY_SYSTEM']['PSA_ACTION_FILE'])?>">
																	<?= Loc::getMessage('SPOD_ORDER_PAY') ?>
																</a>
																<?
															}
															else
															{
																if ($payment["PAID"] === "Y" || $arResult["CANCELED"] === "Y")
																{
																	?>
																	<a class="btn-theme sale-order-detail-payment-options-methods-button-element inactive-button"><?= Loc::getMessage('SPOD_ORDER_PAY') ?></a>
																	<?
																}
																else
																{
																	?>
																	<a class="btn-theme sale-order-detail-payment-options-methods-button-element active-button"><?= Loc::getMessage('SPOD_ORDER_PAY') ?></a>
																	<?
																}
															}
															?>
														</div>
														<?
													}
													?>
													<div class="sale-order-detail-payment-inner-row-template">
														<a class="sale-order-list-cancel-payment">
															<i class="fa fa-long-arrow-left"></i> <?=Loc::getMessage('SPOD_CANCEL_PAYMENT')?>
														</a>
													</div>
												</div>
												<?
												if ($payment["PAID"] !== "Y"
													&& $payment['PAY_SYSTEM']["IS_CASH"] !== "Y"
													&& $payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] !== 'Y'
													&& $arResult['CANCELED'] !== 'Y')
												{
													?>
													<div class="row sale-order-detail-payment-options-methods-template">
														<span class="sale-paysystem-close active-button">
															<span class="sale-paysystem-close-item sale-order-payment-cancel"></span><!--sale-paysystem-close-item-->
														</span><!--sale-paysystem-close-->
														<?=$payment['BUFFERED_OUTPUT']?>
															<!--<a class="sale-order-payment-cancel">--><?//= Loc::getMessage('SPOD_CANCEL_PAY') ?><!--</a>-->
													</div>
													<?
												}
												?>
											</div>
										</div>
										<?
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row sale-order-detail-payment-options-order-content">

				<div class="sale-order-detail-payment-options-order-content-container no-indent">
					<div class="row">
						<div class="sale-order-detail-payment-options-order-content-title no-indent">
							<h3 class="sale-order-detail-payment-options-order-content-title-element">
								<?= Loc::getMessage('SPOD_ORDER_BASKET')?>
							</h3>
						</div>
						<div class="sale-order-detail-order-section bx-active">
								<div class="sale-order-detail-order-table-fade sale-order-detail-order-table-fade-right">
									<div style="width: 100%; overflow-x: auto; overflow-y: hidden;">
										<div class="sale-order-detail-order-item-table">
											<?
											foreach ($arResult['BASKET'] as $basketItem)
											{
												?>
												<div class="sale-order-detail-order-item-tr sale-order-detail-order-basket-info sale-order-detail-order-item-tr-first row">
													<div class="sale-order-detail-order-item-td">
														<div class="sale-order-detail-order-item-block">
															<div class="sale-order-detail-order-item-img-block">
																<span>
																	<?
																	if (strlen($basketItem['PICTURE']['SRC']))
																	{
																		$imageSrc = $basketItem['PICTURE']['SRC'];
																	}
																	else
																	{
																		$imageSrc = $this->GetFolder().'/images/no_photo.png';
																	}
																	?>
																	<div class="sale-order-detail-order-item-imgcontainer"
																		 style="background-image: url(<?=$imageSrc?>);
																			 background-image: -webkit-image-set(url(<?=$imageSrc?>) 1x,
																			 url(<?=$imageSrc?>) 2x)">
																	</div>
																</span>
															</div>
															<div class="sale-order-detail-order-item-content">
																<div class="sale-order-detail-order-item-title">
																	<span>
																		<?=htmlspecialcharsbx($basketItem['NAME'])?>
																	</span>
																</div>
																<?
																if (isset($basketItem['PROPS']) && is_array($basketItem['PROPS']))
																{
																	foreach ($basketItem['PROPS'] as $itemProps)
																	{
																		?>
																		<div class="sale-order-detail-order-item-color">
																		<span class="sale-order-detail-order-item-color-name">
																			<?=htmlspecialcharsbx($itemProps['NAME'])?>:</span>
																			<span class="sale-order-detail-order-item-color-type">
																			<?=htmlspecialcharsbx($itemProps['VALUE'])?></span>
																		</div>
																		<?
																	}
																}
																?>
															</div>
														</div>
													</div>
													<!--<div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right col-md-2 col-sm-2 col-xs-2">
														<div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm">
															<?//= Loc::getMessage('SPOD_PRICE')?>
														</div>
														<div class="sale-order-detail-order-item-td-text">
															<strong class="bx-price"><?//=$basketItem['BASE_PRICE_FORMATED']?></strong>
														</div>
													</div>-->
													<?
													if (strlen($basketItem["DISCOUNT_PRICE_PERCENT_FORMATED"]))
													{
														?>
														<div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties">
															<div class="sale-order-detail-order-item-td-title visible-sm">
																<?= Loc::getMessage('SPOD_DISCOUNT') ?>
															</div>
															<div class="sale-order-detail-order-item-td-text">
																<strong class="bx-price"><?= $basketItem['DISCOUNT_PRICE_PERCENT_FORMATED'] ?></strong>
															</div>
														</div>
														<?
													}
													elseif (strlen($arResult["SHOW_DISCOUNT_TAB"]))
													{
														?>
														<div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties">
															<div class="sale-order-detail-order-item-td-title">
																<?= Loc::getMessage('SPOD_DISCOUNT') ?>
															</div>
															<div class="sale-order-detail-order-item-td-text">
																<strong class="bx-price"></strong>
															</div>
														</div>
														<?
													}
													?>
													<div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties">
														<div class="sale-order-detail-order-item-td-title">
															<?= Loc::getMessage('SPOD_QUANTITY')?>
														</div>
														<div class="sale-order-detail-order-item-td-text">
														<span><?=$basketItem['QUANTITY']?>&nbsp;
															<?
															if (strlen($basketItem['MEASURE_NAME']))
															{
																echo htmlspecialcharsbx($basketItem['MEASURE_NAME']);
															}
															else
															{
																echo Loc::getMessage('SPOD_DEFAULT_MEASURE');
															}
															?></span>
														</div>
													</div>
													<!--<div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right col-md-2 col-sm-2 col-xs-2">
														<div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm"><?//= Loc::getMessage('SPOD_ORDER_PRICE')?></div>
														<div class="sale-order-detail-order-item-td-text">
															<strong class="bx-price all"><?//=$basketItem['FORMATED_SUM']?></strong>
														</div>
													</div>-->
												</div>
												<?
											}
											?>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
			<!--<div class="row sale-order-detail-total-payment">
				<div class="col-md-7 col-md-offset-5 col-sm-12 col-xs-12 sale-order-detail-total-payment-container">
					<div class="row">
						<ul class="col-md-8 col-sm-6 col-xs-6 sale-order-detail-total-payment-list-left">
							<?
							if (floatval($arResult["ORDER_WEIGHT"]))
							{
								?>
								<li class="sale-order-detail-total-payment-list-left-item">
									<?= Loc::getMessage('SPOD_TOTAL_WEIGHT')?>:
								</li>
								<?
							}

							if ($arResult['PRODUCT_SUM_FORMATED'] != $arResult['PRICE_FORMATED'] && !empty($arResult['PRODUCT_SUM_FORMATED']))
							{
								?>
								<li class="sale-order-detail-total-payment-list-left-item">
									<?= Loc::getMessage('SPOD_COMMON_SUM')?>:
								</li>
								<?
							}

							if (strlen($arResult["PRICE_DELIVERY_FORMATED"]))
							{
								?>
								<li class="sale-order-detail-total-payment-list-left-item">
									<?= Loc::getMessage('SPOD_DELIVERY')?>:
								</li>
								<?
							}

							foreach ($arResult["TAX_LIST"] as $tax)
							{
								?>
								<li class="sale-order-detail-total-payment-list-left-item">
									<?= Loc::getMessage('SPOD_TAX') ?>:
								</li>
								<?
							}
							?>
							<li class="sale-order-detail-total-payment-list-left-item"><?= Loc::getMessage('SPOD_SUMMARY')?>:</li>
						</ul>
						<ul class="col-md-4 col-sm-6 col-xs-6 sale-order-detail-total-payment-list-right">
							<?
							if (floatval($arResult["ORDER_WEIGHT"]))
							{
								?>
								<li class="sale-order-detail-total-payment-list-right-item"><?= $arResult['ORDER_WEIGHT_FORMATED'] ?></li>
								<?
							}

							if ($arResult['PRODUCT_SUM_FORMATED'] != $arResult['PRICE_FORMATED'] && !empty($arResult['PRODUCT_SUM_FORMATED']))
							{
								?>
								<li class="sale-order-detail-total-payment-list-right-item"><?=$arResult['PRODUCT_SUM_FORMATED']?></li>
								<?
							}

							if (strlen($arResult["PRICE_DELIVERY_FORMATED"]))
							{
								?>
								<li class="sale-order-detail-total-payment-list-right-item"><?= $arResult["PRICE_DELIVERY_FORMATED"] ?></li>
								<?
							}

							foreach ($arResult["TAX_LIST"] as $tax)
							{
								?>
								<li class="sale-order-detail-total-payment-list-right-item"><?= $tax["VALUE_MONEY_FORMATED"] ?></li>
								<?
							}
							?>
							<li class="sale-order-detail-total-payment-list-right-item"><?=$arResult['PRICE_FORMATED']?></li>
						</ul>
					</div>
				</div>
			</div>-->
		</div><!--sale-order-detail-general-->
	</div>
	<script>
		function cancelOrder(id){
			BX.showWait();
			
			var postData = {};
			postData.action = 'cancel_order';
			postData.id = id;
			$.ajax({
			   // url: '/<?=urlencode($templateFolder.'/ajax.php');?>',
			   url:'<?=$templateFolder.'/ajax.php';?>',
			   data: postData,
			   method: 'POST',
			   dataType: 'json',
			   success: function(data){
					BX.closeWait();
					$('#order_item_'+id+' .sale-order-list-cancel-container').remove();
					$('#order_item_'+id).appendTo("#cancel_order");
			   },

		   });
		}
	</script>
	<?
	$javascriptParams = array(
	"id" => $arResult["ID"],
	"url" => CUtil::JSEscape($templateFolder.'/ajax.php'),
	"templateFolder" => CUtil::JSEscape($templateFolder),
	"paymentList" => $paymentData,
	"url_to_cancel" => $arResult["URL_TO_CANCEL"],
	"url_to_list_orders" => $arResult["URL_TO_LIST"]
	);
	$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
	?>
	<script>
		BX.Sale.PersonalOrderComponent.PersonalOrderDetail.init(<?=$javascriptParams?>);
	</script>
<?
}
?>

