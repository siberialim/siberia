<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
use \Bitrix\Catalog\CatalogViewedProductTable   as   CatalogViewedProductTable ;
global $APPLICATION;
$APPLICATION->SetAdditionalCSS($templateFolder.'/style.css');
$APPLICATION->AddHeadScript($templateFolder.'/script.js');

CatalogViewedProductTable::refresh($arResult['ELEMENT'][ 'ID' ], CSaleBasket::GetBasketUserID()); 