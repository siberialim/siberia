<?
$MESS["CATALOG_QUANTITY"] = "수량";
$MESS["PRICE"] = "가격:";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "from #FROM# to #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# and more";
$MESS["CATALOG_QUANTITY_TO"] = "up to #TO#";
$MESS["CATALOG_PRICE_VAT"] = "tax included";
$MESS["CATALOG_PRICE_NOVAT"] = "tax excluded";
$MESS["CATALOG_VAT"] = "Tax rate";
$MESS["CATALOG_NO_VAT"] = "not taxed";
$MESS["CATALOG_VAT_INCLUDED"] = "Tax rate included in price";
$MESS["CATALOG_VAT_NOT_INCLUDED"] = "Tax rate not included in price";
$MESS["CT_BCE_QUANTITY"] = "수량";
$MESS["CT_BCE_CATALOG_BUY"] = "바로 구매하기";
$MESS["CT_BCE_CATALOG_ADD"] = "바구니에 넣기";
$MESS["CT_BCS_TPL_MESS_BTN_FAV"] = "즐겨찾기 추가";
$MESS["CT_BCE_CATALOG_COMPARE"] = "Compare";
$MESS["CT_BCE_CATALOG_NOT_AVAILABLE"] = "not available from stock";
$MESS["OSTATOK"] = "Quantity in stock";
$MESS["COMMENTARY"] = "Comments";
$MESS["CT_BCE_CATALOG_ECONOMY_INFO"] = "(Savings - #ECONOMY#)";
$MESS["FULL_DESCRIPTION"] = "Full description";
$MESS["CT_BCE_CATALOG_TITLE_ERROR"] = "Error";
$MESS["CT_BCE_CATALOG_TITLE_BASKET_PROPS"] = "Item properties to pass to shopping cart";
$MESS["CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR"] = "Unknown error adding an item to shopping cart";
$MESS["CT_BCE_CATALOG_BTN_SEND_PROPS"] = "Select";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_CLOSE"] = "Close";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "View shopping cart";
$MESS["CT_BCE_CATALOG_ADD_TO_BASKET_OK"] = "Added to your shopping cart";
$MESS["CT_BCE_CATALOG_MESS_BASIS_PRICE"] = "#PRICE# for 1 #MEASURE#";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP"] = "Continue shopping";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_OK"] = "Product has been added to comparison chart";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_TITLE"] = "Product comparison";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR"] = "Error adding the product to comparison chart";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT"] = "Compare products";
$MESS["CT_BCE_CATALOG_PRODUCT_GIFT_LABEL"] = "Gift";
$MESS["COMMENT_PRICE"] = "가격은 부산항까지의 운송료와 관세를 포함하고 있다";
$MESS["PROP_ING"] = "성분";
$MESS["PROP_PACK"] = "포장";
$MESS["PROP_HP"] = "유통 기한";
$MESS["PROP_WEIGHT"] = "무게";
$MESS["PROP_NUTR"] = "제품 100그램당";

$MESS["ADD_TO_BASKET_OK"] = "Item added to cart";
$MESS["ADD_TO_FAV_OK"] = "Item added to favorites";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "Go to shopping cart";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_FAV_REDIRECT"] = "Go to favorites";
?>