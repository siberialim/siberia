<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//use from D7
use \Bitrix\Catalog\CatalogViewedProductTable;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $APPLICATION;
global $USER;

$user_id = CSaleBasket::GetBasketUserID();
if($user_id  > 0)
{
  $product_id = $arResult['ELEMENT']['ID'];
  CatalogViewedProductTable::refresh($product_id, $user_id);
}

$this->setFrameMode(true);
$strMainID = $this->GetEditAreaId($arResult['ELEMENT']['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'FAVOURITE_LINK' => $strMainID.'_favourite_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
?>
<?$APPLICATION->IncludeComponent("it-invest:breadcrumb","siberia",Array(
        "START_FROM" => "0", 
        "PATH" => "", 
        "SITE_ID" => SITE_ID 
    )
);?>
<?
// Выведем актуальную корзину для текущего пользователя
$in_basket = false;
$in_basket_D = false;

$arBasketItems = array();

$dbBasketItems = CSaleBasket::GetList(
    array(
		"NAME" => "ASC",
		"ID" => "ASC"
    ),
    array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL"
    ),
    false,
    false,
    array(
		"ID",
		"CALLBACK_FUNC", 
		"MODULE", 
		"PRODUCT_ID", 
		"QUANTITY", 
		"DELAY", 
		"CAN_BUY", 
		"PRICE", 
		"WEIGHT"
	)
);

while ($arItems = $dbBasketItems->Fetch())
{
    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
    {
        CSaleBasket::UpdatePrice($arItems["ID"], 
                                 $arItems["CALLBACK_FUNC"], 
                                 $arItems["MODULE"], 
                                 $arItems["PRODUCT_ID"], 
                                 $arItems["QUANTITY"]);
        $arItems = CSaleBasket::GetByID($arItems["ID"]);
    }

    $arBasketItems[] = $arItems;
}

foreach($arBasketItems as $itemInCart)
{
	if($itemInCart['PRODUCT_ID'] == $arResult['ELEMENT']['ID']  && $itemInCart['DELAY'] == 'Y')
	{
		$in_basket_D = true;
	}
		
	if($itemInCart['PRODUCT_ID'] == $arResult['ELEMENT']['ID'] && $itemInCart['DELAY'] == 'N')
	{
		$in_basket = true;
	}
}

$this->AddEditAction($arResult['ELEMENT']['ID'], $arResult['ELEMENT']['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arResult['ELEMENT']['ID'], $arResult['ELEMENT']['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
?>
<div class="bx_item_detail" id="<? echo $arItemIDs['ID']; ?>">
	<div class="bx_item_container product-container">
		<div class="row">
			<div class="bx_lt col-xs-12 col-sm-4 col-md-4 col-lg-4 no-indent">
				<div class="bx_item_slider" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
					<div class="bx_bigimages" id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
						<a href="javascript:void(0)" data-add2="favourite" id="<?=$arItemIDs['FAVOURITE_LINK']; ?>" class="add-to-favourite">
							<i class="fa <? echo (!$in_basket_D) ? 'fa-star-o' : 'fa-star active';?>" aria-hidden="true" title="<?=Loc::getMessage('CT_BCS_TPL_MESS_BTN_FAV'); ?>"></i>
						</a>
						<a href="<? echo $arResult['ELEMENT']['RESIZE_DETAIL_PICTURE_LARGE']['src'];  ?>" class="gallery" rel="group"><img style="width:<?=$arResult['ELEMENT']['RESIZE_DETAIL_PICTURE']['width']/10?>rem;height:<?=$arResult['ELEMENT']['RESIZE_DETAIL_PICTURE']['height']/10?>rem" id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arResult['ELEMENT']['RESIZE_DETAIL_PICTURE']['src']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>"></a>
					</div>
					<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_ID']; ?>">
						<div class="bx_slider_scroller_container product-album" id="product_album">
							<div class="bx_slide product-album-pic">
								<div id="<? echo $arItemIDs['SLIDER_LIST']; ?>" class="responsive">
									<?foreach ($arResult['ELEMENT']['RESIZE_MORE_PHOTO_LARGE_VALUE'] as $k => &$arOnePhoto){?>
										<div class="bx_slide_item" data-value="<? echo $k; ?>"><span class="cnt"><a href="<? echo $arOnePhoto['src']; ?>" class="gallery" rel="group"><span class="cnt_item" style="background-image:url('<?=$arResult['ELEMENT']['RESIZE_MORE_PHOTO_VALUE'][$k]['src'];?>');"></span></a></span></div>
									<?}
									unset($arOnePhoto);
									?>
								</div>
							</div>
							<div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
							<div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
						</div>
					</div>
				</div>
				<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
				<script src="//yastatic.net/share2/share.js"></script>
				<div class="ya-share2" data-services="facebook,gplus,twitter,blogger,linkedin,lj,pocket,qzone"></div>
			</div>
			<div id="product_information" class="bx_rt product-information col-xs-12 col-sm-8 col-md-8 col-lg-8">
				<div class="product-titles"><h1><?=$arResult['ELEMENT']['NAME']?></h1></div>
				<div class="price_element">
					<ul>
					<?if ((isset($arResult['ELEMENT']['MIN_PRICE']) || isset($arResult['ELEMENT']['RATIO_PRICE'])) && ($arResult['ELEMENT']['MIN_PRICE']['VALUE'] > 0 || $arResult['ELEMENT']['RATIO_PRICE']['VALUE'] > 0) && $arParams['SHOW_PRICES'] == 'Y') {?>
						<li class="item">
							<div class="wrap_price">
								<span class="name_prop"><?=Loc::getMessage("PRICE")?></span>
								<span class="item_price price">
									<?
									$minPrice = (isset($arResult['ELEMENT']['RATIO_PRICE']) ? $arResult['ELEMENT']['RATIO_PRICE'] : $arResult['ELEMENT']['MIN_PRICE']);
									$boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
									?>
										<span class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>">&nbsp;<b><ins class="action-price"><? echo $minPrice['PRINT_DISCOUNT_VALUE']; ?></ins></b></span>
									<?
									if ($arParams['SHOW_OLD_PRICE'] == 'Y') {
									?>
										<span class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>" style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?></span>
									<?
									}
									if ($arParams['SHOW_OLD_PRICE'] == 'Y') {
										?>
										<span class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>" style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><?// echo($boolDiscountShow ? Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO', array('#ECONOMY#' => $minPrice['PRINT_DISCOUNT_DIFF'])) : ''); ?></span>
									<?
									}
									?>
								</span>
								<!--<p style="font-size:16px"><?//=Loc::getMessage("COMMENT_PRICE")?></p>-->
							</div>
						</li>
					<?}?>
					</ul>
				</div>	
				<div class="detail_text">
					<?=$arResult['ELEMENT']['DETAIL_TEXT']?>
				</div>
				<?unset($minPrice);?>
				<div class="item_info_section">
					<ul class="props">
						<?if (isset($arResult['ELEMENT']['PROPERTY_COMPOSITION_VALUE']) && !empty($arResult['ELEMENT']['PROPERTY_COMPOSITION_VALUE'])) {?>
							<li class="prop"><span class="name_prop"><?=Loc::getMessage("PROP_ING")?>:</span>
								<span><?=$arResult['ELEMENT']['PROPERTY_COMPOSITION_VALUE']?></span>
							</li>
						<?}?>
						<?if (isset($arResult['ELEMENT']['PROPERTY_PACK_VALUE']) && !empty($arResult['ELEMENT']['PROPERTY_PACK_VALUE'])) {?>
							<li class="prop"><span class="name_prop"><?=Loc::getMessage("PROP_PACK")?>:</span>
								<span><?=$arResult['ELEMENT']['PROPERTY_PACK_VALUE']?></span>
							</li>
						<?}?>
						<?if (isset($arResult['ELEMENT']['PROPERTY_SHELF_LIFE_VALUE']) && !empty($arResult['ELEMENT']['PROPERTY_SHELF_LIFE_VALUE'])) {?>
							<li class="prop"><span class="name_prop"><?=Loc::getMessage("PROP_HP")?>:</span>
								<span><?=$arResult['ELEMENT']['PROPERTY_SHELF_LIFE_VALUE']?></span>
							</li>
						<?}?>
						<?if (isset($arResult['ELEMENT']['PROPERTY_WEIGHT_VALUE']) && !empty($arResult['ELEMENT']['PROPERTY_WEIGHT_VALUE'])) {?>
							<li class="prop"><span class="name_prop"><?=Loc::getMessage("PROP_WEIGHT")?>:</span>
								<span><?=$arResult['ELEMENT']['PROPERTY_WEIGHT_VALUE']?></span>
							</li>
						<?}?>
						<?if (isset($arResult['ELEMENT']['PROPERTY_NUTRIENTS_VALUE']) && !empty($arResult['ELEMENT']['PROPERTY_NUTRIENTS_VALUE'])) {?>
							<li class="prop"><span class="name_prop"><?=Loc::getMessage("PROP_NUTR")?>:</span>
								<span><?=$arResult['ELEMENT']['PROPERTY_NUTRIENTS_VALUE']?></span>
							</li>
						<?}?>
					</ul>
				</div>
				<div class="item_info_section">
					<?
						$basisPriceInfo = array(
							'#PRICE#' => $arResult['MIN_BASIS_PRICE']['PRINT_DISCOUNT_VALUE'],
							'#MEASURE#' => (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : '')
						);
					?>
						<span class="prop_count"><?=Loc::getMessage('CATALOG_QUANTITY'); ?>:</span>
						<span class="item_buttons_counter_block counter">
							<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a><!--
							--><input id="<?=$arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<?=$arResult['ELEMENT']['CATALOG_MEASURE_RATIO']?>"><!--
							--><a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
							<span class="bx_cnt_desc" id="<?=$arItemIDs['QUANTITY_MEASURE']; ?>"><?// echo (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
						</span>
				</div>
				<div class="item_buttons vam" id="<?=$arItemIDs['BASKET_ACTIONS']; ?>">
					<div class="row">
							<a href="javascript:void(0);" data-add2="buy" data-click="true" class="bx_big bx_bt_button bx_bue" id="<?=$arItemIDs['BUY_LINK']; ?>"><span><?=($USER->IsAuthorized()) ? Loc::getMessage('CT_BCE_CATALOG_BUY') : Loc::getMessage('CT_BCE_CATALOG_FAST');?></span></a><!--
							--><a href="javascript:void(0);" data-add2="basket" class="bx_big bx_bt_button bx_cart" id="<?=$arItemIDs['ADD_BASKET_LINK']; ?>"><span><?=Loc::getMessage('CT_BCE_CATALOG_ADD'); ?></span></a>		
					</div>
				</div>
				<div class="clb"></div>
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>
	<div class="clb"></div>
</div><?
	if ($arResult['ELEMENT']['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['ELEMENT']['MIN_PRICE']['VALUE']) {
		$arResult['ELEMENT']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['ELEMENT']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
		$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
	}
	$arJSParams = array(
		'USER' => array(
			'AUTH' => $USER->IsAuthorized(),
		),
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => true,
			'SHOW_PRICE' => (isset($arResult['ELEMENT']['MIN_PRICE']) && !empty($arResult['ELEMENT']['MIN_PRICE']) && is_array($arResult['ELEMENT']['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
			'USE_STICKERS' => true,
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
			'PRICE_ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['ELEMENT']['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ELEMENT']['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['ELEMENT']['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['ELEMENT']['MIN_PRICE'],
			'BASIS_PRICE' => $arResult['ELEMENT']['MIN_BASIS_PRICE'],
			'SLIDER_COUNT' => $arResult['ELEMENT']['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['ELEMENT']['MORE_PHOTO'],
			'CAN_BUY' => $arResult['ELEMENT']['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['ELEMENT']['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['ELEMENT']['CATALOG_MEASURE_RATIO'],
			'ADD_URL' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL' => $arResult['~BUY_URL_TEMPLATE'],
		),
		'BASKET' => array(
			'ADD_PROPS' => true,
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => true,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
			'FAVOURITE_URL_TEMPLATE' => $arResult['~FAVOURITE_URL_TEMPLATE'],
		)
	);
?>
<div class="mod">
	<div class="tac ovh">
	</div>
	<div class="tab-section-container">
		<div id="disqus_thread"></div>
		<script>
			/**
			*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
			*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
			/*
			var disqus_config = function () {
			this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
			this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
			};
			*/
			var disqus_config = function () { 
				<?if(SLANGCODE == 'kn'){?>
					this.language = "ko";
				<?}else if(SLANGCODE == 'ch'){?>
					this.language = "zh";
				<?}else if(SLANGCODE == 'ru'){?>
					this.language = "ru";
				<?}else{?>
					this.language = "en";
				<?}?>
			};
			(function() { // DON'T EDIT BELOW THIS LINE
			var d = document, s = d.createElement('script');
			s.src = 'https://siberia-products.disqus.com/embed.js';
			s.setAttribute('data-timestamp', +new Date());
			(d.head || d.body).appendChild(s);
			})();
		</script>
		<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	</div>
</div>

<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
	ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
	BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	PRODUCT_GIFT_LABEL: '<? echo GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL') ?>',
	SITE_ID: '<? echo SITE_ID; ?>',

	CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	CT_BCS_CATALOG_BTN_MESSAGE_FAV_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_FAV_REDIRECT');?>',
	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	ADD_TO_FAV_OK: '<? echo GetMessageJS('ADD_TO_FAV_OK'); ?>',
	
	FAST_BUY_TITLE: '<? echo GetMessageJS('FAST_BUY_TITLE'); ?>',
	FAST_BUY_PROP_NAME: '<? echo GetMessageJS('FAST_BUY_PROP_NAME'); ?>',
	FAST_BUY_PROP_EMAIL: '<? echo GetMessageJS('FAST_BUY_PROP_EMAIL'); ?>',
	FAST_BUY_PROP_PHONE: '<? echo GetMessageJS('FAST_BUY_PROP_PHONE'); ?>',
	FAST_BUY_PROP_MESS: '<? echo GetMessageJS('FAST_BUY_PROP_MESS'); ?>',
	FAST_BUY_BTN: '<? echo GetMessageJS('FAST_BUY_BTN'); ?>',
	
	THANK_YOU_1: '<? echo GetMessageJS('THANK_YOU_1'); ?>',
	THANK_YOU_2: '<? echo GetMessageJS('THANK_YOU_2'); ?>',
});
</script>
<script type="text/javascript">
var viewedCounter = {
    path: '/bitrix/components/bitrix/catalog.element/ajax.php',
    params: {
        AJAX: 'Y',
        SITE_ID: "<?= SITE_ID ?>",
        PRODUCT_ID: "<?= $arResult['ELEMENT']['ID'] ?>",
        PARENT_ID: "<?= $arResult['ELEMENT']['ID'] ?>"
    }
};
BX.ready(
    BX.defer(function(){
        BX.ajax.post(
            viewedCounter.path,
            viewedCounter.params
        );
    })
);
</script>
<script>
$('#product_album .responsive').slick({
  autoplay: false,
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {	  
        slidesToShow: 2,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 5,
		infinite: true,
		dots: false
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
		infinite: true,
		dots: false
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>