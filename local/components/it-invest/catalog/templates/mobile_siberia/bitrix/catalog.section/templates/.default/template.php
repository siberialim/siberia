<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="item_list_component">
	<div id="main_catalog">
<?
if ($_REQUEST["ajax_get_page"] == "Y")
{
	$APPLICATION->RestartBuffer();
}
?>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>


<?

		if(is_array($arElement))
	{
		
		if(is_array($arElement["DETAIL_PICTURE"]))
		{
			$arFilter = '';
			if($arParams["SHARPEN"] != 0)
			{
				$arFilter = array("name" => "sharpen", "precision" => $arParams["SHARPEN"]);
			}
			
			$arFileTmp = CFile::ResizeImageGet(
				$arElement['DETAIL_PICTURE'],
				array("width" => 147, "height" => 147),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true, $arFilter
			);
			
			$arElement["PREVIEW_IMG"] = array(
				"SRC" => $arFileTmp["src"],
				'WIDTH' => $arFileTmp["width"],
				'HEIGHT' => $arFileTmp["height"],
			);
		}
		
		$bPicture = is_array($arElement["PREVIEW_IMG"]);
		?>
	<div class="main_catalog_item">
		<?/*if($arParams["DISPLAY_COMPARE"]):?>
		<noindex>
			<?if(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"])):?>
				<span class="checkbox">
					<a href="javascript:void(0)" onclick="return showOfferPopup(this, 'list', '<?=GetMessage("CATALOG_IN_CART")?>', <?=CUtil::PhpToJsObject($arItem["SKU_ELEMENTS"])?>, <?=CUtil::PhpToJsObject($arItem["SKU_PROPERTIES"])?>, <?=CUtil::PhpToJsObject($arResult["POPUP_MESS"])?>, 'compare');">
						<input type="checkbox" class="addtoCompareCheckbox"/><span class="checkbox_text"><?=GetMessage("CATALOG_COMPARE")?></span>
					</a>
				</span>
			<?else:?>
				<span class="checkbox">
					<a href="<?echo $arItem["COMPARE_URL"]?>" rel="nofollow" onclick="return addToCompare(this, 'list', '<?=GetMessage("CATALOG_IN_COMPARE")?>', '<?=$arItem["DELETE_COMPARE_URL"]?>');" id="catalog_add2compare_link_<?=$arItem['ID']?>">
						<input type="checkbox" class="addtoCompareCheckbox"/><span class="checkbox_text"><?=GetMessage("CATALOG_COMPARE")?></span>
					</a>
				</span>
			<?endif?>
		</noindex>
		<?endif*/?>
		<?if ($bPicture):?>
			<div class="detail_item_img_container">
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="main_catalog_item_img"><img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>"  alt="<?=$arElement["NAME"]?>" /></a>
			</div>
		<?endif?>
		<div class="data_item">
			<h2><a href="<?=$arElement["DETAIL_PAGE_URL"]?>" title="<?=$arElement["NAME"]?>"><?=$arElement["NAME"]?></a></h2>

			<div class="main_catalog_item_price">
			<?if(!is_array($arElement["OFFERS"]) || empty($arElement["OFFERS"])):?>
				<?
					$numPrices = count($arParams["PRICE_CODE"]);
					foreach($arElement["PRICES"] as $code=>$arPrice):
						if($arPrice["CAN_ACCESS"]):?>
							<?if ($numPrices>1):?><p style="padding: 0; margin-bottom: 5px;"><?=$arResult["PRICES"][$code]["TITLE"];?>:</p><?endif?>
							<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
								<div class="price">
									<div class="main_price_container oldprice"><?=GetMessage("MB_PRICE")?>:
										<span class="item_price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
										<span class="item_price_old"><?=$arPrice["PRINT_VALUE"]?></span>
									</div>
								</div>
							<?else:?>
								<div class="main_price_container"><?=GetMessage("MB_PRICE")?>:
									<span class="item_price"><?=$arPrice["PRINT_VALUE"]?></span>
								</div>
							<?endif;
						endif;
					endforeach;
				?>
			<?endif?>
			</div>
			<div class="detail_item_count"><?=GetMessage("CATALOG_QUANTITY")?>:
				<a href="javascript:void(0)" class="count_minus" id="count_minus" ontouchstart="if (BX('item_quantity').value > 1) BX('item_quantity').value--;"><span>-</span></a><!--
				--><input type="number" id="item_quantity" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1"><!--
				--><a href="javascript:void(0)" class="count_plus" id="count_plus" ontouchstart="BX('item_quantity').value++;"><span>+</span></a>
			</div>
		</div>
		<?if ($arElement["CAN_BUY"]):?>
			<div class="wrap_control_btn">
				<noindex>
					<a href="<?=$arElement["ADD_URL"]?>"
						class="main_item_buy"
						rel="nofollow"
						onclick="
							BX.addClass(BX.findParent(this, {class : 'main_catalog_item'}, false), 'add2cart');//	setTimeout('BX.removeClass(obj, \'add2cart\')', 3000);
							return addItemToCart(this);"
						id="catalog_add2cart_link_<?=$arElement['ID']?>">
						<?=GetMessage("CATALOG_ADD")?>
					</a>
					<!--<a href="<?//=$arParams["BASKET_URL"]?>" class="main_catalog_item_cartlink" ontouchstart="BX.toggleClass(this, 'active');" ontouchend="BX.toggleClass(this, 'active');"><?//=GetMessage("CATALOG_IN_CART")?></a>-->
				</noindex>
			</div>
			<?/*elseif ($arNotify[SITE_ID]['use'] == 'Y'):?>
				<?if ($USER->IsAuthorized()):?>
					<noindex><a href="<?echo $arItem["SUBSCRIBE_URL"]?>" rel="nofollow" class="subscribe_link" onclick="return addToSubscribe(this, '<?=GetMessage("CATALOG_IN_SUBSCRIBE")?>');" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?echo GetMessage("CATALOG_SUBSCRIBE")?></a></noindex>
				<?else:?>
					<noindex><a href="javascript:void(0)" rel="nofollow" class="subscribe_link" onclick="showAuthForSubscribe(this, <?=$arItem['ID']?>, '<?echo $arItem["SUBSCRIBE_URL"]?>')" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?echo GetMessage("CATALOG_SUBSCRIBE")?></a></noindex>
				<?endif;?>
			<?*/?>
		<?endif?>
		<div class="clb"></div>
	</div>
	<?}?>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
<?
if ($_REQUEST["ajax_get_page"] == "Y")
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}
?>
	</div>
</div>

<script type="text/javascript">
	app.setPageTitle({"title" : "<?=CUtil::JSEscape(htmlspecialcharsback($arResult["NAME"]))?>"});

	window.pagenNum = 1;
	window.onscroll = function ()
	{
		var preloadCoefficient = 2;

		var clientHeight = document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight;
		var documentHeight = document.documentElement.scrollHeight ? document.documentElement.scrollHeight : document.body.scrollHeight;
		var scrollTop = window.pageYOffset ? window.pageYOffset : (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

		if((documentHeight - clientHeight*(1+preloadCoefficient)) <= scrollTop)
		{
			getBottomItems();
		}
	}

	function getBottomItems()
	{
		if (!(<?=$arResult["NAV_STRING"]?> > <?=$arParams["PAGE_ELEMENT_COUNT"]?>*window.pagenNum))
			return;

		window.pagenNum++;

		BX.ajax({
			timeout:   30,
			method:   'POST',
			url: "<?=CUtil::JSEscape(POST_FORM_ACTION_URI)?>&ajax_get_page=Y&PAGEN_1="+window.pagenNum,
			processData: false,
			onsuccess: function(sectionHTML){
				var sectionDomObjCont = BX("new_items_container");

				if(!sectionDomObjCont)
				{
					sectionDomObjCont= document.createElement("DIV");
					sectionDomObjCont.id = "new_items_container";
					sectionDomObjCont.style.display = "none";
				}
				sectionDomObjCont.innerHTML = sectionHTML;

				var sectionsObj = BX.findChildren(sectionDomObjCont, {tagName : "li"}, false);

				for (var i in sectionsObj)
				{
					BX("section_items").appendChild(sectionsObj[i]);
				}
			},
			onfailure: function(){
			}
		});
	};

</script>

