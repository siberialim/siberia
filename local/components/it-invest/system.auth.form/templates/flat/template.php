<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>

<div class="bx-system-auth-form">
<?if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']){?>
	<div class="alert alert-danger"><?=ShowMessage($arResult['ERROR_MESSAGE']);?></div>
<?}
if($_REQUEST['register'] == 'yes'){
	$APPLICATION->IncludeComponent(
		"it-invest:system.auth.registration",
		"flat",
		Array(
			"REGISTER_URL" => "/login/index.php?register=yes",
			"PROFILE_URL" => "/personal/private",
			"SHOW_ERRORS" => "Y"
		),
		false
	);
}
else if($_REQUEST['forgot_password'] == 'yes'){
	$APPLICATION->IncludeComponent(
		"it-invest:system.auth.forgotpasswd",
		"flat",
		Array(
			"REGISTER_URL" => "/login/index.php?register=yes",
			"PROFILE_URL" => "/personal/private",
			"SHOW_ERRORS" => "Y"
		),
		false
	);
}
else if($_REQUEST['change_password'] == 'yes'){
	$APPLICATION->IncludeComponent(
		"it-invest:system.auth.changepasswd",
		"flat",
		Array(
			"REGISTER_URL" => "/login/index.php?register=yes",
			"PROFILE_URL" => "/personal/private",
			"SHOW_ERRORS" => "Y"
		),
		false
	);
}
else{
	$APPLICATION->IncludeComponent(
		"it-invest:system.auth.authorize",
		"flat",
		Array(
			"REGISTER_URL" => "/login/index.php?register=yes",
			"PROFILE_URL" => "/personal/private",
			"SHOW_ERRORS" => "Y",
		),
		false
	);
}
?>
</div>
