<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div id="carousel-B0PPm" class="carousel slide hidden-xs" data-interval="false" data-wrap="true" data-pause="true" data-keyboard="true" data-ride="carousel">
	<style>
		.carousel .carousel-control{z-index:4}
		.carousel-control .icon-prev:before{content: '';}
		.carousel-control .icon-next:before{content: '';}
		.carousel-control .icon-prev{margin-top: -30px;}
		.carousel-control .icon-next{margin-top: -30px;}
		.carousel-control.right{background-image:none}
		.carousel-control.left{background-image:none}
	</style>
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<?$i = 0;
		for($i; $i < count($arResult["ITEMS"]); $i++):?>
		<li data-target="#carousel-B0PPm" data-slide-to="<?=$i?>" class="<?if($i == 0){echo 'active';}?>"></li>
		<?endfor;?>
	</ol>
	
	<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
		<?$i = 0;
		for($i; $i < count($arResult["ITEMS"]); $i++):
		
			if ($arResult["ITEMS"][$i]["PREVIEW_PICTURE"])
			{
				$file = CFile::ResizeImageGet($arResult["ITEMS"][$i]["PREVIEW_PICTURE"]['ID'], array('width'=>1024, 'height'=>488), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$arResult["ITEMS"][$i]['SLIDER_PHOTO'] = $file;
			}
		
		?>
			<div class="item <?if($i == 0){echo 'active';}?>">
				<?if(isset($arResult["ITEMS"][$i]['PROPERTIES']['LINK']) && !empty($arResult["ITEMS"][$i]['PROPERTIES']['LINK']['VALUE'])){?>
					<a href="<?=$arResult["ITEMS"][$i]['PROPERTIES']['LINK']['VALUE']?>" target="_blank">
						<img alt="" title="" src="<?=$arResult["ITEMS"][$i]['SLIDER_PHOTO']["src"]?>" style="width:1024px !important; height:488px !important" style="border:0;">
					</a>
				<?}else{?>
					<img alt="" title="" src="<?=$arResult["ITEMS"][$i]['SLIDER_PHOTO']["src"]?>" style="width:1024px !important; height:488px !important" style="border:0;">
				<?}?>
			</div>
		<?endfor;?>
		</div>

			<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<img src="/local/templates/siberia_limited/images/static/slider-left.png" alt="img">
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<img src="/local/templates/siberia_limited/images/static/slider-right.png" alt="img">
			<span class="sr-only">Next</span>
		</a>
</div>

	
<script>
	BX("carousel-B0PPm").addEventListener("slid.bs.carousel", function (e) {
		var item = e.detail.curSlide.querySelector('.play-caption');
		if (!!item)
		{
			item.style.display = 'none';
			item.style.left = '-100%';
			item.style.opacity = 0;
		}
	}, false);
	BX("carousel-B0PPm").addEventListener("slide.bs.carousel", function (e) {
		var item = e.detail.curSlide.querySelector('.play-caption');
		if (!!item)
		{
			var duration = item.getAttribute('data-duration') || 500,
				delay = item.getAttribute('data-delay') || 0;

			setTimeout(function() {
				item.style.display = '';
				var easing = new BX.easing({
					duration : duration,
					start : {left: -100, opacity : 0},
					finish : {left: 0, opacity: 100},
					transition : BX.easing.transitions.quart,
					step : function(state){
						item.style.opacity = state.opacity/100;
						item.style.left = state.left + '%';
					},
					complete : function() {
					}
				});
				easing.animate();
			}, delay);
		}
	}, false);
	BX.ready(function(){
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	});
	function mutePlayer(e) {
		e.target.mute();
	}
	function loopPlayer(e) {
		if (e.data === YT.PlayerState.ENDED)
			e.target.playVideo();
	}
	function onYouTubePlayerAPIReady() {
		if (typeof yt_player !== 'undefined')
		{
			for (var i in yt_player)
			{
				window[yt_player[i].id] = new YT.Player(
						yt_player[i].id, {
							events: {
								'onStateChange': loopPlayer
							}
						}
				);
				if (yt_player[i].mute == true)
					window[yt_player[i].id].addEventListener('onReady', mutePlayer);
			}
			delete yt_player;
		}
	}
</script>


