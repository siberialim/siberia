<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

?>
<div class="b-fl-slider s-main-slider-mob">
	<?$i = 0;
	foreach($arResult as $arBanner):		
	?>
		<div class="b-fl-slider__slide">
			<?if(isset($arBanner['PROPERTY_LINK_VALUE']) && !empty($arBanner['PROPERTY_LINK_VALUE'])){?>
				<a href="<?=$arBanner['PROPERTY_LINK_VALUE']?>" target="_blank">
					<img alt="" title="" src="<?=$arBanner['MOBILE_SLIDER_PHOTO']["src"]?>" width="<?=$arBanner['MOBILE_SLIDER_PHOTO']["width"]?>" >
				</a>
			<?}else{?>
				<a><img src="<?=$arBanner['MOBILE_SLIDER_PHOTO']["src"]?>" alt="" title="" width="<?=$arBanner['MOBILE_SLIDER_PHOTO']["width"]?>" ></a>
			<?}?>
		</div>
	<?
	$i+=1;
	endforeach;?>
</div>
<script>
$('.b-fl-slider').slick({
	autoplay: true,
	dots: false,
	infinite: true,
	speed: 300,
	slidesToShow: 1,
	adaptiveHeight: true,
	centerMode:true
});
</script>