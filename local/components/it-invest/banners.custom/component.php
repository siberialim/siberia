<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Iblock;
	
$cache = new CPHPCache();	
	
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'custom.banners.'.IBLOCK_CATALOG_ID;
$cache_path = 'custom.banners/'.IBLOCK_CATALOG_ID;

//isset cache?
if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)){
   $res = $cache->GetVars();
   if (is_array($res[$cache_id]) && (count($res[$cache_id]) > 0))
	  $arResult = $res[$cache_id];
}

//!cache
if (!is_array($res[$cache_id]))
{
	$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => '', 'ACTIVE' => 'Y'); 
	$arSort = array('SORT' => 'ASC');
	$arSelect = array('ID', 'NAME', 'CODE', 'PREVIEW_PICTURE', 'PROPERTY_MOBILE_PICTURE', 'PROPERTY_LINK');
	$arItems = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
	
	$arResult = array();
	while ($arBanner = $arItems->GetNext())
	{
		if ($arBanner['PREVIEW_PICTURE'])
		{
			$file = CFile::ResizeImageGet($arBanner['PREVIEW_PICTURE'], array('width'=>890, 'height'=>488), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
			$arBanner['SLIDER_PHOTO'] = $file;
		}
		
		if ($arBanner['PROPERTY_MOBILE_PICTURE_VALUE'])
		{
			$file = CFile::ResizeImageGet($arBanner['PROPERTY_MOBILE_PICTURE_VALUE'], array('width'=>2500, 'height'=>488), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
			$arBanner['MOBILE_SLIDER_PHOTO'] = $file;
		}
		array_push($arResult, $arBanner);
		unset($arBanner);
	}
	


	//////////// end cache /////////
	if ($cache_time > 0)
	{
		// начинаем буферизирование вывода
		$cache->StartDataCache($cache_time, $cache_id, $cache_path);
		// записываем предварительно буферизированный вывод в файл кеша
		$cache->EndDataCache(array($cache_id=>$arResult));
	}
}

$this->includeComponentTemplate();

