<?
//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="wrap-center option">
	<h1 class="pageTitle"><?=Loc::getMessage("TITLE_PAGE")?></h1>
	<div class="block-out-background">
		<div class="b-head"><?=Loc::getMessage("PERSONAL_INFORMATION")?></div>
		<div class="b-cont">
			<table class="user-data">
				<thead></thead>
				<tbody>
					<tr><td><?=Loc::getMessage("P_NAME")?></td><td><?=$arResult["arUser"]["NAME"].' '.$arResult["arUser"]["LAST_NAME"]?></td></tr>
					<tr><td>E-mail</td><td><?=$arResult["arUser"]["EMAIL"] ? $arResult["arUser"]["EMAIL"] : Loc::getMessage("EMPTY");?></td></tr>
					<tr><td><?=Loc::getMessage("DATE_OF_BIRTH")?></td><td><?=$arResult["arUser"]["PERSONAL_BIRTHDAY"] ? $arResult["arUser"]["PERSONAL_BIRTHDAY"] : Loc::getMessage("EMPTY");?></td></tr>
					<tr><td><?=Loc::getMessage("SEX")?></td><td><?=$arResult["arUser"]["PERSONAL_GENDER"] == "F" ?  Loc::getMessage("USER_FEMALE") : Loc::getMessage("USER_MALE")?></td></tr>
				</tbody>
			</table>
			<a class="tuningLink" href="<?=SITE_DIR?>personal/profile/settings/"><?=Loc::getMessage("CHANGE_PERSONAL_INFORMATION")?></a>
		</div>
	</div>
</div>
<div class="right-sidebar">
	<div class="block-out-background">
		<div class="b-head"><?=Loc::getMessage("MOBILE_PHONE_NO")?></div>
		<div class="b-cont">
			<?=$arResult["arUser"]["PERSONAL_PHONE"] ? $arResult["arUser"]["PERSONAL_PHONE"] : Loc::getMessage("EMPTY");?>
		</div>
		<div class="b-foot">
			<a class="tuningLink" href="<?=SITE_DIR?>personal/profile/settings/"><?=Loc::getMessage("CHANGE_PHONE_NO")?></a>
		</div>
	</div>
	<div class="block-out-background">
		<div class="b-head"><?=Loc::getMessage("PASSWORD")?></div>
		<div class="b-cont"><?=Loc::getMessage("HERE_YOU_CAN_CHANGE_YOUR_PASSWORD")?></div>
		<div class="b-foot">
			<a class="tuningLink" href="<?=SITE_DIR?>personal/profile/settings/"><?=Loc::getMessage("CHANGE_PASSWORD")?></a>
		</div>
	</div>
	<!--<div class="block-out-background">
		<div class="b-head"><?//=Loc::getMessage("DELETE_YOUR_ACCOUNT")?></div>
		<div class="b-cont"><?//=Loc::getMessage("DELETE_YOUR_ACCOUNT_MESS")?></div>
		<div class="b-foot">
			<a class="tuningLink" href="<?//=SITE_DIR?>personal/profile/settings/"><?//=Loc::getMessage("DELETE_YOUR_ACCOUNT")?></a>
		</div>
	</div>-->
</div>