<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<script>
	$( function() {
	$( "input.right-choice" ).checkboxradio({
		icon: false
	});
	$( "#datepicker" ).datepicker({
		dateFormat: "yy.mm.dd"
	});
	} );
</script>
<div class="profile">
	<div class="profile-b-cont">
		<div class="wrap-profile option">
			<div class="message_result">
				<?ShowError($arResult["strProfileError"]);?>
				<?
				if ($arResult['DATA_SAVED'] == 'Y')
				ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
				?>
			</div>
			<div class="block-out-background">
				<h1 class="pageTitle"><?=Loc::getMessage("TITLE_PAGE")?></h1>
				<form class="profile-form-option" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
					<?=$arResult["BX_SESSION_CHECK"]?>
					<input type="hidden" name="lang" value="<?=LANG?>" />
					<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
					<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>">
					<fieldset>
						<div class="row_input">
							<div class="wrap_input">
								<input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" placeholder="<?=Loc::getMessage("NAME")?>"/>
								<span class="error"></span>
							</div>
							<div class="wrap_input">
								<input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" placeholder="<?=Loc::getMessage("LAST_NAME")?>"/>
								<span class="error"></span>
							</div>
						</div>
						<div class="row_input">	
							<div class="wrap_input">
								<?=$arResult["COUNTRY_SELECT"]?>
							</div>
						</div>
						<div class="row_input">	
							<div class="wrap_input">
								<input type="text" name="WORK_COMPANY" maxlength="255" value="<?=$arResult["arUser"]["WORK_COMPANY"]?>" placeholder="<?=Loc::getMessage('USER_COMPANY')?>"/><br/>
								<span class="error"></span>
							</div>
						</div>
						<div class="row_input">	
							<div class="wrap_input">
								<input type="text" name="PERSONAL_PROFESSION" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_PROFESSION"]?>" placeholder="<?=Loc::getMessage('USER_PROFESSION')?>"/><br/>
								<span class="error"></span>
							</div>
						</div>
						<div class="row_input">	
							<div class="wrap_input">
								<input type="text" name="PERSONAL_PHONE" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" placeholder="<?=Loc::getMessage('USER_PHONE')?>"/><br/>
								<span class="error"></span>
							</div>
						</div>
						<div class="row_input">	
							<div class="wrap_input">
								<input type="text" name="EMAIL" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" placeholder="<?=Loc::getMessage('EMAIL')?>"/><br/>
								<span class="error"></span>
							</div>
						</div>
						<div class="row_input">	
							<div class="wrap_input">
								<input type="text" name="LOGIN" maxlength="50" value="<?=$arResult["arUser"]["LOGIN"]?>" placeholder="<?=Loc::getMessage('LOGIN')?>"/><br/>
								<span class="error"></span>
							</div>
						</div>
						<div class="row_input">	
							<div class="wrap_input">
								<input type="password" name="NEW_PASSWORD" maxlength="50" value="" placeholder="<?=Loc::getMessage('NEW_PASSWORD_REQ')?>"/><br/>
								<span class="error"></span>
							</div>
						</div>
						<div class="row_input">	
							<div class="wrap_input">
								<input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" placeholder="<?=Loc::getMessage('NEW_PASSWORD_CONFIRM')?>"/><br/>
								<span class="error"></span>
							</div>
						</div>
						<!--<div class="row_input">
							<div class="wrap_input">
								<label><?//=Loc::getMessage("USER_BIRTHDAY_DT")?> </label>&nbsp;&nbsp;&nbsp;<input type="text" id="datepicker" name="PERSONAL_BIRTHDAY" value="<?//=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>"><br/>
								<span class="error"></span>
							</div> 
						</div>-->
						<!--<div class="row_input">
							<label><?//=Loc::getMessage("USER_GENDER")?></label>&nbsp;&nbsp;&nbsp;
							<label for="radio-2"><?//=Loc::getMessage("USER_MALE")?></label>
							<input type="radio" name="PERSONAL_GENDER" id="radio-2" class="right-choice" value="M"<?//=$arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " checked" : ""?>>
							<label for="radio-3"><?//=Loc::getMessage("USER_FEMALE")?></label>
							<input type="radio" name="PERSONAL_GENDER" id="radio-3" class="right-choice" value="F"<?//=$arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " checked" : ""?>>
							<span class="error"></span>						
						</div>-->
						<p class="simple_personal_text"><span class="title_data"><?=Loc::getMessage('LAST_LOGIN')?></span><br/><?=$arResult["arUser"]["LAST_LOGIN"]?></p>
					</fieldset>
					<div class="wrap-btn">
						<input type="submit" name="save" value="<?=Loc::getMessage("MAIN_SAVE")?>"/>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

