<?
$MESS["ACCOUNT_UPDATE"] = "";
$MESS["PROFILE_DEFAULT_TITLE"] = "이용자 정보";
$MESS["USER_DONT_KNOW"] = "";
$MESS["main_profile_sess_expired"] = "";
$MESS["main_profile_decode_err"] = "";
$MESS["TITLE_PAGE"] = "내 정보";
$MESS["EMPTY"] = "정보가 없다";
$MESS["P_NAME"] = "이름";
$MESS["DATE_OF_BIRTH"] = "생년월일";
$MESS["PERSONAL_INFORMATION"] = "개인 정보";
$MESS["SEX"] = "성별";
$MESS["CHANGE_PERSONAL_INFORMATION"] = "개인 정보 수정하기";
$MESS["MOBILE_PHONE_NO"] = "휴대폰 번호";
$MESS["CHANGE_PHONE_NO"] = "전화 번호 수정하기";
$MESS["HERE_YOU_CAN_CHANGE_YOUR_PASSWORD"] = "여기에서 비밀번호를 수정할 수 있다";
$MESS["PASSWORD"] = "비밀번호";
$MESS["CHANGE_PASSWORD"] = "비밀번호 변경";
$MESS["DELETE_YOUR_ACCOUNT"] = "개인 계정 삭제";
$MESS["DELETE_YOUR_ACCOUNT_MESS"] = "계정이 삭제되자마자 시스템에서 자동으로 로그아웃이 되고 다시 그 계정을 못 사용하게 된다";
$MESS["DELETE_YOUR_ACCOUNT"] = "개인 계정 삭제";
?>