<?
$MESS["ACCOUNT_UPDATE"] = "The administrator has updated your personal information.";
$MESS["PROFILE_DEFAULT_TITLE"] = "个人信息";
$MESS["USER_DONT_KNOW"] = "(unknown)";
$MESS["main_profile_sess_expired"] = "Your session has expired. Please try again.";
$MESS["main_profile_decode_err"] = "Password decryption error (#ERRCODE#).";
$MESS["TITLE_PAGE"] = "我的帐户";
$MESS["EMPTY"] = "Empty";
$MESS["P_NAME"] = "收货人的名字";
$MESS["DATE_OF_BIRTH"] = "出生年月日";
$MESS["PERSONAL_INFORMATION"] = "个人信息";
$MESS["SEX"] = "性别";
$MESS["CHANGE_PERSONAL_INFORMATION"] = "修改个人资料";
$MESS["MOBILE_PHONE_NO"] = "电话号码";
$MESS["CHANGE_PHONE_NO"] = "修改手机号";
$MESS["HERE_YOU_CAN_CHANGE_YOUR_PASSWORD"] = "此栏为重置密码";
$MESS["PASSWORD"] = "密码";
$MESS["CHANGE_PASSWORD"] = "重置密码";
$MESS["DELETE_YOUR_ACCOUNT"] = "注销帐户";
$MESS["DELETE_YOUR_ACCOUNT_MESS"] = "一旦注销帐户，您将自动退出系统，并且此帐户不再继续登陆";
?>