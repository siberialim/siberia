<?
$MESS["ACCOUNT_UPDATE"] = "Администратор сайта изменил ваши регистрационные данные.";
$MESS["PROFILE_DEFAULT_TITLE"] = "Профиль пользователя";
$MESS["USER_DONT_KNOW"] = "(неизвестно)";
$MESS["main_profile_sess_expired"] = "Ваша сессия истекла, повторите попытку.";
$MESS["main_profile_decode_err"] = "Ошибка при дешифровании пароля (#ERRCODE#).";
$MESS["TITLE_PAGE"] = "Мои данные";
$MESS["EMPTY"] = "Не указано";
$MESS["P_NAME"] = "Имя";
$MESS["DATE_OF_BIRTH"] = "Дата рождения";
$MESS["PERSONAL_INFORMATION"] = "Персональные данные";
$MESS["SEX"] = "Пол";
$MESS["CHANGE_PERSONAL_INFORMATION"] = "Изменить персональные данные";
$MESS["MOBILE_PHONE_NO"] = "Номер мобильного телефона";
$MESS["CHANGE_PHONE_NO"] = "Изменить номер телефона";
$MESS["HERE_YOU_CAN_CHANGE_YOUR_PASSWORD"] = "Здесь вы можете изменить свой пароль";
$MESS["PASSWORD"] = "Пароль";
$MESS["CHANGE_PASSWORD"] = "Смена пароля";
$MESS["DELETE_YOUR_ACCOUNT"] = "Удаление личного кабинета";
$MESS["DELETE_YOUR_ACCOUNT_MESS"] = "Как только ваш личный кабинет будет удален, вы автоматически выйдите из системы и больше не сможете войти в этот аккаунт";
$MESS["DELETE_YOUR_ACCOUNT"] = "Удалить личный кабинет";
?>