<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Currency,
	Bitrix\Catalog,
	Bitrix\Iblock;

//else{die();}
$cache = new CPHPCache();
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'catalog.custom.special.elements.new.'.IBLOCK_CATALOG_ID;
$cache_path = 'catalog.custom.special.elements.new/'.IBLOCK_CATALOG_ID;
if (CModule::includeModule('currency'))
{
	$currency = Currency\CurrencyTable::getList(array(
		'select' => array('CURRENCY'),
		'filter' => array('=CURRENCY' => $arParams['CURRENCY_ID'])
	))->fetch();
}
else{die();}

$arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];

//isset cache?
if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
{	
   $res = $cache->GetVars();
   if (is_array($res[$cache_id]) && (count($res[$cache_id]) > 0))
	  $arResult = $res[$cache_id];
		
}

//!cache
if (!is_array($res[$cache_id]))
{	
	$arSelect = array(
					'ID', 'NAME', 'CODE', 
					'DETAIL_PAGE_URL', 
					'PREVIEW_PICTURE', 
					'PROPERTY_NEWPRODUCT', 'PROPERTY_SALELEADER', 'PROPERTY_SPECIALOFFER', 
					'CATALOG_GROUP_1', 
					'CATALOG_QUANTITY'
				);
	
	$arFilter = array(
		'IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ACTIVE' => 'Y'
	);
	
	$arFilter[] = array("PROPERTY_NEWPRODUCT_VALUE" => "Y");
		
	$arSort = array('SORT' => 'ASC');

	$res = CIBlockElement::GetList($arSort, $arFilter, false,  array("nTopCount" => 4), $arSelect);

	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);

	$currentPath = CHTTP::urlDeleteParams(
		$arParams['CUSTOM_CURRENT_PAGE']?: $APPLICATION->GetCurPageParam(),
		array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
		array('delete_system_params' => true)
	);

	$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');
	if ($arParams['COMPARE_PATH'] == '')
	{
		$comparePath = $currentPath;
	}
	else
	{
		$comparePath = CHTTP::urlDeleteParams(
		$arParams['COMPARE_PATH'],
		array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
		array('delete_system_params' => true)
		);
		$comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');
	}
	/*===============================================================================================================
		ajax уходит в component.php компонента catalog.custom.element
	===================================================================================================================*/
	$ajaxPath = '/catalog/section/element/index.php?';
	/*======================================*/
	$arUrlTemplates['~BUY_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~BUY_URL_TEMPLATE']);
	$arUrlTemplates['~ADD_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~ADD_URL_TEMPLATE']);
	$arUrlTemplates['~FAVOURITE_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2FAVOURITE&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['FAVOURITE_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~FAVOURITE_URL_TEMPLATE']);

	$arResult = array_merge($arResult, $arUrlTemplates);
	
	while($ob = $res->GetNextElement()) 
	{	
		if ($ob->fields['PREVIEW_PICTURE'])
		{
			$file = CFile::ResizeImageGet($ob->fields['PREVIEW_PICTURE'], array('width'=>258, 'height'=>263), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);                
			$ob->fields['PREVIEW_PHOTO'] = $file;
		}
		if (!isset($ob->fields["CATALOG_MEASURE_RATIO"])){
			$ob->fields["CATALOG_MEASURE_RATIO"] = 1;
		}			
		$arResult['ITEMS'][] = $ob->fields;
	} 

	$arResult['RESULT'] = $res;
	
	CModule::IncludeModule("iblock");

	//////////// end cache /////////
	if ($cache_time > 0)
	{
		// начинаем буферизирование вывода
		$cache->StartDataCache($cache_time, $cache_id, $cache_path);
		// записываем предварительно буферизированный вывод в файл кеша
		$cache->EndDataCache(array($cache_id=>$arResult));
	}
}

foreach($arResult['ITEMS'] as &$item)
{
	$item["PRICES"] = array();
	$item['MIN_PRICE'] = false;
	$item["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $item, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
	
	if (!empty($item['PRICES']))
		$item['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($item['PRICES']);

	$item["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $item);
	
	$arButtons = CIBlock::GetPanelButtons(
		$item["IBLOCK_ID"],
		$item["ID"],
		$arResult["ID"],
		array("SECTION_BUTTONS"=>false, "SESSID"=>false, "CATALOG"=>true)
	);
	$item["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
	$item["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
}

if (isset($ob))
	unset($ob);

$this->includeComponentTemplate();
