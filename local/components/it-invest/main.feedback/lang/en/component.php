<?
$MESS ['MF_OK_MESSAGE'] = "Thank you. Your message is now being processed.";
$MESS ['MF_REQ_NAME'] = "Enter your name";
$MESS ['MF_REQ_EMAIL'] = "Enter your E-mail";
$MESS ['MF_REQ_MESSAGE'] = "You have not written a message";
$MESS ['MF_EMAIL_NOT_VALID'] = "The e-mail address specified is invalid.";
$MESS ['MF_CAPTCHA_WRONG'] = "The CAPTCHA code you have typed is incorrect.";
$MESS ['MF_CAPTHCA_EMPTY'] = "The CAPTCHA code is required.";
$MESS ['MF_SESS_EXP'] = "Your session has expired. Please send your message again.";
$MESS ['MF_ANTIBOT'] = "Error";
?>