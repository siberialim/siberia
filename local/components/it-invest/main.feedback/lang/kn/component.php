<?
$MESS ['MF_OK_MESSAGE'] = "보내주신 글은 접수되었습니다. 감사합니다.";
$MESS ['MF_REQ_NAME'] = "이름 기입";
$MESS ['MF_REQ_EMAIL'] = "답변을 받고 싶어하시는 e-mail를 기입해 주세요.";
$MESS ['MF_REQ_MESSAGE'] = "본문을 쓰지 않으셨습니다.";
$MESS ['MF_EMAIL_NOT_VALID'] = "The e-mail address specified is invalid.";
$MESS ['MF_CAPTCHA_WRONG'] = "The CAPTCHA code you have typed is incorrect.";
$MESS ['MF_CAPTHCA_EMPTY'] = "The CAPTCHA code is required.";
$MESS ['MF_SESS_EXP'] = "Your session has expired. Please send your message again.";
$MESS ['MF_ANTIBOT'] = "Error";
?>