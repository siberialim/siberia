<?
$MESS ['FEEDBACK'] = "Feedback";
$MESS ['MFT_NAME'] = "고객님의 이름*";
$MESS ['MFT_MESSAGE'] = "문자*";
$MESS ['TOP_MESSAGE'] = "연락처를 남겨 주시면 연락하겠습니다";
$MESS ['BOTTOM_MESSAGE'] = "연락처를 남겨 주시면 연락하겠습니다";
$MESS ['MFT_EMAIL'] = "고객님의 E-mail*";
$MESS ['MFT_CAPTCHA'] = "CAPTCHA";
$MESS ['MFT_CAPTCHA_CODE'] = "Type the letters you see on the picture";
$MESS ['MFT_SUBMIT'] = "보내기";
?>