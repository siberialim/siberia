<?
$MESS ['FEEDBACK'] = "Feedback";
$MESS ['MFT_NAME'] = "您的姓名";
$MESS ['TOP_MESSAGE'] = "请留下您的联系方式，我们将尽快会与您联系";
$MESS ['BOTTOM_MESSAGE'] = "请留下您的联系方式，我们将尽快会与您联系";
$MESS ['MFT_EMAIL'] = "您的邮箱地址";
$MESS ['MFT_MESSAGE'] = "请在此填写信息";
$MESS ['MFT_CAPTCHA'] = "CAPTCHA";
$MESS ['MFT_CAPTCHA_CODE'] = "Type the letters you see on the picture";
$MESS ['MFT_SUBMIT'] = "发送";
?>