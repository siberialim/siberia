<?
$MESS ['FEEDBACK'] = "Обратная связь";
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['TOP_MESSAGE'] = "Оставьте Ваши данные и мы свяжемся с Вами";
$MESS ['BOTTOM_MESSAGE'] = "Оставьте Ваши данные и мы свяжемся с Вами";
$MESS ['MFT_EMAIL'] = "Ваш e-mail";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Отправить";
?>