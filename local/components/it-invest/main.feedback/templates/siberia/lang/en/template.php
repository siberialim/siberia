<?
$MESS ['FEEDBACK'] = "Feedback";
$MESS ['MFT_NAME'] = "Your name";
$MESS ['TOP_MESSAGE'] = "Leave your details and we will contact you";
$MESS ['BOTTOM_MESSAGE'] = "Leave your details and we will contact you";
$MESS ['MFT_EMAIL'] = "Your e-mail";
$MESS ['MFT_MESSAGE'] = "Message";
$MESS ['MFT_CAPTCHA'] = "CAPTCHA";
$MESS ['MFT_CAPTCHA_CODE'] = "Type the letters you see on the picture";
$MESS ['MFT_SUBMIT'] = "Send";
?>