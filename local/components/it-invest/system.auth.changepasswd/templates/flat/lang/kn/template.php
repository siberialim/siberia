<?
$MESS["AUTH_CHANGE_PASSWORD"] = "비밀번호 변경";
$MESS["AUTH_LOGIN"] = "로그인";
$MESS["AUTH_CHECKWORD"] = "Check string";
$MESS["AUTH_NEW_PASSWORD_CONFIRM"] = "비밀번호 확인";
$MESS["AUTH_CHANGE"] = "비밀번호 변경";
$MESS["AUTH_AUTH"] = "로그인";
$MESS["AUTH_NEW_PASSWORD_REQ"] = "New Password";
$MESS["AUTH_SECURE_NOTE"] = "The password will be encrypted before it is sent. This will prevent the password from appearing in open form over data transmission channels.";
$MESS["system_auth_captcha"] = "Enter the characters you see on the picture";
?>