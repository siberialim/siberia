<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?
if(count($arResult["ITEMS"]["AnDelCanBuy"]) > 0 && !isset($_REQUEST["DELAY"]) && $_REQUEST["DELAY"] != 'Y')
{?>
	<div id="backet_controll">
		<a class="cart_item_all_remove" href="<?=$arUrlTempl["deleteAll"]?>?action=deleteAll" title="<?=Loc::getMessage("SALE_DELETE_PRD")?>"><?echo Loc::getMessage("SALE_CLEAR_CART")?></a>
	</div>
<?
}
else if(count($arResult["ITEMS"]["DelDelCanBuy"]) > 0)
{
?>
	<div id="backet_controll">
		<a class="cart_item_all_remove" href="<?=$arUrlTempl["deleteAll"]?>?action=deleteAll&DELAY=Y" title="<?=Loc::getMessage("SALE_DELETE_PRD")?>"><?echo Loc::getMessage("SALE_CLEAR_FAVOURITE")?></a>
	</div>
<?
}
?>
<div id="wrap_order">
	<?
		$arUrlTempl = Array(
			"delete" => $APPLICATION->GetCurPage()."?action=delete&id=#ID#",
			"deleteAll" => $APPLICATION->GetCurPage()."?action=deleteAll",
			"shelve" => $APPLICATION->GetCurPage()."?action=shelve&id=#ID#",
			"add" => $APPLICATION->GetCurPage()."?action=add&id=#ID#",
		);
	?>
	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">
		<?
			if (!isset($_REQUEST["DELAY"]) && $_REQUEST["DELAY"] != 'Y')
			{ 
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");
			}
			else
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_delay.php");
			}
		/*	include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_subscribe.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_notavail.php"); */
		?>
	</form>
</div>