<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?$APPLICATION->SetPageProperty("BodyClass", "cart");
//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if($USER->IsAuthorized()) 
{
	$userAuth = true;
	$urlCreateOrder = 'javascript:void(0);';
}
else
{
	$userAuth = false;
	$urlCreateOrder = SITE_DIR.'createorder/';
}
?>
<div id="id-cart-list">
	<div class="cart_item_list">
		<ul id="id-cart-ul">
	<?if(count($arResult["ITEMS"]["AnDelCanBuy"]) > 0):?>
		<h1><?=Loc::getMessage("SALE_BASKET")?></h1>
		<?if (!$USER->IsAuthorized()):?>
			<p class="simple_cart_text"><?=Loc::getMessage('NOT_AUTORIZ', Array ("#BTN_LOG_IN#" => ' <a href="'.SITE_DIR.'login/?login=yes&backurl='.$APPLICATION->GetCurPage().'">'.Loc::getMessage('BTN_LOG_IN').'</a>'));?></p>
		<?endif?>
		<?
		$i=0;
		foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems)
		{
			if($i == count($arResult["ITEMS"]["AnDelCanBuy"]) - 1){
				$class="last";
			}
			?>
			<li id="<?=$arBasketItems["ID"]?>" <?=(isset($class)) ? "class=".$class : ""?>>
				<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
						<?if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):?>
							<a class="cart_item_list_img" href="<?=$arBasketItems["DETAIL_PAGE_URL"]?>">
						<?endif;?>
						<?if (!empty($arResult["ITEMS_IMG"][$arBasketItems["ID"]]["SRC"])) :?>
							<img style="width:<?=$arResult["ITEMS_IMG"][$arBasketItems["ID"]]["WIDTH"]/10?>rem;height:<?=$arResult["ITEMS_IMG"][$arBasketItems["ID"]]["HEIGHT"]/10?>rem" src="<?=SITE_TEMPLATE_PATH?>/images/static/px.jpg" realsrc="<?=$arResult["ITEMS_IMG"][$arBasketItems["ID"]]["SRC"]?>" alt="<?=$arBasketItems["NAME"] ?>"/>
						<?endif?>
						<?if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):?>
							</a>
						<?endif;?>
					<div class="b-cart-product__info">
						<div class="cart_item_list_title">
							<a href="<?=$arBasketItems["DETAIL_PAGE_URL"]?>"><span><?=$arBasketItems["NAME"] ?></span></a>
						</div>
						
						<?//if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
					<?
					$ratio = isset($arBasketItems["MEASURE_RATIO"]) ? $arBasketItems["MEASURE_RATIO"] : 0;
					$max = isset($arBasketItems["AVAILABLE_QUANTITY"]) ? "max=\"".$arBasketItems["AVAILABLE_QUANTITY"]."\"" : "";
					$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
					$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
					
					if (!isset($arBasketItems["MEASURE_RATIO"]))
					{
						$arBasketItems["MEASURE_RATIO"] = 1;
					}

					?>
					<div class="cart_item_count">
						<span><?=Loc::getMessage("SALE_QUANTITY")?>:&nbsp;</span>
						<a href="javascript:void(0);" class="minus" onclick="setQuantity(<?=$arBasketItems["ID"]?>, <?=$arBasketItems["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);">-</a><!--
						--><input
							class="quantity_input"
							type="text"
							size="3"
							id="QUANTITY_INPUT_<?=$arBasketItems["ID"]?>"
							name="QUANTITY_INPUT_<?=$arBasketItems["ID"]?>"
							size="2"
							maxlength="18"
							min="1"
							<?=$max?>
							step="<?=$ratio?>"
							style="max-width: 50px"
							value="<?=$arBasketItems["QUANTITY"]?>"
							onchange="updateQuantity('QUANTITY_INPUT_<?=$arBasketItems["ID"]?>', '<?=$arBasketItems["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"/><!--
						--><a href="javascript:void(0);" class="plus" onclick="setQuantity(<?=$arBasketItems["ID"]?>, <?=$arBasketItems["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);">+</a>
					</div><!--
				<?//endif;?>
				-->
						<?if (in_array("PROPS", $arParams["COLUMNS_LIST"]))
						{
						?>
							<div class="cart_item_list_description_text">
								<ul>
						<?
								foreach($arBasketItems["PROPS"] as $val)
								{
									echo "<li>".$val["NAME"].": ".$val["VALUE"]."</li>";
								}
						?>
								</ul>
							</div>
						<?}?>
				<?endif;?>
				<?/*if (in_array("VAT", $arParams["COLUMNS_LIST"])):?>
					<td><?=$arBasketItems["VAT_RATE_FORMATED"]?></td>
				<?endif;?>
				<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
					<td><?=$arBasketItems["NOTES"]?></td>
				<?endif;?>
				<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
					<td><?=$arBasketItems["DISCOUNT_PRICE_PERCENT_FORMATED"]?></td>
				<?endif;?>
				<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
					<td><?=$arBasketItems["WEIGHT_FORMATED"]?></td>
				<?endif;*/?>

				<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
					<?if(doubleval($arBasketItems["FULL_PRICE"]) > 0):?>
						<div class="cart_price_conteiner oldprice whsnw"><?=Loc::getMessage("SALE_PRICE")?>:
							<span class="item_price"><?=$arBasketItems["PRICE_FORMATED"]?></span>
							<span class="item_price_old"><?=$arBasketItems["FULL_PRICE_FORMATED"]?></span>
						</div>
					<?else:?>
						<div class="cart_price_conteiner whsnw">
							<span class="item_price"><?=$arBasketItems["PRICE_FORMATED"]?></span>
						</div>
					<?endif?>
				<?endif;?>
				<div class="clear"></div>
				<div class="cart_item_control">
				<?//if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
					<a class="cart_item_remove" href="<?=str_replace("#ID#", $arBasketItems["ID"], $arUrlTempl["delete"])?>" title="<?=Loc::getMessage("SALE_DELETE_PRD")?>"><i class="fa fa-times" aria-hidden="true"></i></a>
				<?//endif;?>
				<?//if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
					<a class="cart_item_delayed" href="<?=str_replace("#ID#", $arBasketItems["ID"], $arUrlTempl["shelve"])?>" title="<?=Loc::getMessage("SALE_ADD_FAVOURITE")?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
				<?//endif;?>
				</div>
				<div class="clb"></div>
				<input type="hidden" id="QUANTITY_<?=$arBasketItems['ID']?>" name="QUANTITY_<?=$arBasketItems['ID']?>" value="<?=$arBasketItems["QUANTITY"]?>" />
				</div>
			</li>
			<?
			$i++;
		}
		?>
		</ul>
	<?else:?>
		<h1 class="b-cart-empty__title"><?=Loc::getMessage("SALE_BASKET")?></h1>
	<?endif?>
	</div>
	<div class="cart_item_bottom" id="cart_item_bottom" <?if(!count($arResult["ITEMS"]["AnDelCanBuy"]) > 0):?>style="display:none"<?endif;?>>
		<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
			<div class="cart_item_total_price" >
				<?=Loc::getMessage("SALE_ALL_WEIGHT")?>:
				<span id="weight"><?=$arResult["allWeight_FORMATED"]?></span>
			</div>
		<?endif;?>
		<div id="all_discount">
		<?if (doubleval($arResult["DISCOUNT_PRICE"]) > 0):?>
			<div class="cart_item_total_price" >
			<?=Loc::getMessage("SALE_CONTENT_DISCOUNT")?><?
					if (strLen($arResult["DISCOUNT_PERCENT_FORMATED"])>0)
						echo " (".$arResult["DISCOUNT_PERCENT_FORMATED"].")";?>:
				<?=$arResult["DISCOUNT_PRICE_FORMATED"]?>
			</div>
		<?endif;?>
		</div>
		<?if ($arParams['PRICE_VAT_SHOW_VALUE'] == 'Y'):?>
		<div class="cart_item_total_price" >
			<?=Loc::getMessage('SALE_VAT_EXCLUDED')?>
			<span id="vat_excluded"><?=$arResult["allNOVATSum_FORMATED"]?></span>
		</div>
		<div class="cart_item_total_price" >
			<?=Loc::getMessage('SALE_VAT_INCLUDED')?>
			<span id="vat_included"><?=$arResult["allVATSum_FORMATED"]?></span>
		</div>
		<?endif;?>
		<!--<div class="cart_item_total_price">
			<?//= Loc::getMessage("SALE_ITOGO")?>: <span class="price"><?//=$arResult["allSum_FORMATED"]?></span>
		</div>-->
		<?if ($arParams["HIDE_COUPON"] != "Y"):?>
		<div class="cart_item_coupon">
			<span><?=Loc::getMessage("SALE_COUPON")?>:</span>
			<div class="cart_item_list_search_input_container">
				<input value="<?if(!empty($arResult["COUPON"])):?><?=$arResult["COUPON"]?><?endif;?>" name="COUPON" type="text">
			</div>
			<div class="clb"></div>
		</div>
		<?endif;?>
		<input type="hidden" value="<?=Loc::getMessage("SALE_UPDATE")?>" name="BasketRefresh" >
		<!--<a href="javascript:void(0)" class="cart_item_refresh button_gray_medium" ontouchstart="BX.toggleClass(this, 'active');" ontouchend="BX.toggleClass(this, 'active');" onclick="
				var data_form = {}, form = BX('basket_form');
				for(var i = 0; i< form.elements.length; i++)
				{
				if (form[i].name != 'BasketOrder')
				data_form[form[i].name] = form[i].value;
				}
				ajaxInCart('<?=CUtil::JSEscape(POST_FORM_ACTION_URI)?>', data_form);
				return BX.PreventDefault(event);"><?=Loc::getMessage("SALE_UPDATE")?></a>-->
		<?
		if($USER->IsAuthorized()) 
		{
		?>
			<div id="wrap_basketOrderButton2"><a href="<?=$urlCreateOrder?>" id="basketOrderButton2" class="cart_item_checkout" onclick="createOrder(<?=$userAuth?>);"><?echo Loc::getMessage("SALE_ORDER")?></a></div>
		<?
		}
		?></div>
	<input type="hidden" id="column_headers" value="NAME,WEIGHT,DELETE,DELAY,QUANTITY" />
	<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
	<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
	<div style="display:none">
		<div id="fail"><div class="mfeedback"><div class="comment"><?=Loc::getMessage("ERR")?></div></div></div>
		<div id="success"><div class="mfeedback"><div class="comment"><?=Loc::getMessage("ORDER_SENT")?></div><p><?=Loc::getMessage("RETURN_TO_CATALOG", Array("#CATALOG#" => '<a href="/catalog/">'.Loc::getMessage("CATALOG").'</a>'))?></p></div></div>
	</div>
	<div class="cart-notetext" id="empty_cart_text" <?if(count($arResult["ITEMS"]["AnDelCanBuy"]) > 0):?>style="display:none"<?endif;?>>
		<div class="detail_item tac">
			<span class="empty_cart_text">
				<p style="margin-top:1.5rem"><span><?=Loc::getMessage("SALE_NO_ACTIVE_PRD");?></span></p>
				<p style="margin-top:1.5rem"><img src="<?=$templateFolder?>/images/icon_basket.png"/></p>
				<p style="margin-top:1.5rem"><a class="btn btn-primary" href="<?=SITE_DIR?>catalog" style="padding: 2rem 8rem;"><?=Loc::getMessage("RETURN_TO_CATALOG", Array ("#CATALOG#" => Loc::getMessage("CATALOG")))?></a></p>
				<p style="margin-top:1.5rem"><?=Loc::getMessage('NOT_AUTORIZ', Array ("#BTN_LOG_IN#" => ' <a href="'.SITE_DIR.'login/?login=yes&backurl='.$APPLICATION->GetCurPage().'">'.Loc::getMessage('BTN_LOG_IN').'</a>'));?></p>
			</span>
		</div>
	</div>
</div>