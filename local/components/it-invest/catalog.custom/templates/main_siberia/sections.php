<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<script>
	/*$(function() {		
		function activeAccordion(){
			$( "#main_catalog" ).accordion({
				'active': 999,
				'header': ".catalog-preview",
				'classes': {
				}
			});
		}
		
		if($(window).width() < 768){
			activeAccordion();
		}
	});*/
		
	/*$(window).scroll(function() {
		if($(window).width() < 768) {
			$( "#main_catalog" ).accordion({
				'active': 999,
				'classes': {
				}
			});
		});
	}); */
</script>
<div id="main_catalog" class="list-sections">
	<?$APPLICATION->IncludeComponent(
		"it-invest:catalog.custom.section.list",
		"main_siberia",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"AR_SELECT" => array('ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'CATALOG_GROUP_1', 'CATALOG_QUANTITY', 'PROPERTY_RECOMMEND'),
			"AR_SORT" => array('SORT' => 'ASC'),
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
			"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
			"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
			"SECTION_FIELDS" => Array("PICTURE","DESCRIPTION","UF_COLOR"),
			"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
			"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
			"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
			"CONVERT_CURRENCY" => "Y",
			"CURRENCY_ID" => CURRENCY_ID,
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"SHOW_PRICES" => $arParams["SHOW_PRICES"],
			"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams['PRODUCT_QUANTITY_VARIABLE'],		
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);
?>
</div>
