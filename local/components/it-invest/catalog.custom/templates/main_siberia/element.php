<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$storage = new GarbageStorage();

$this->setFrameMode(true);
?>
<div id="detail_item">
	<?$ElementID = $APPLICATION->IncludeComponent(
		"it-invest:catalog.custom.element",
		"siberia_responsive",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"SHOW_PRICES" => $arParams["SHOW_PRICES"],
			"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
			"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"SECTION_NAME" => $arResult["VARIABLES"]["NAME"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
			"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
			"CURRENCY_ID" => $arParams["CURRENCY_ID"],
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
			"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
			"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],	
			"SET_VIEWED_IN_COMPONENT" => "Y",			
		),
		$component
	);
	
	$recommendet_elements = GarbageStorage::get('ids');
	
	/*if ($ElementID > 0 && count($recommendet_elements) > 0){
		$APPLICATION->IncludeComponent("it-invest:catalog.custom.recommended.products", "siberia_skyline_responsive", array(
			"LINE_ELEMENT_COUNT" => 5,
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			'CONVERT_CURRENCY' => 'Y',
			'CURRENCY_ID' => $_SESSION['CURRENCY_ID'],
			"IDS" => $recommendet_elements,
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"SHOW_PRICES" => $arParams["SHOW_PRICES"],
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
			"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"RESIZE_WIDTH" => 180,
			"RESIZE_HEIGHT" => 243,
			"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		),
		$component,
		array("HIDE_ICONS" => "Y")
		);
	}*/
	?>
</div>
<?
$APPLICATION->IncludeComponent(
"bitrix:catalog.viewed.products", 
"custom_template",
array(
	"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	"IBLOCK_ID" => $arParams['IBLOCK_ID'],
	"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
	"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
	"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
	"PRODUCT_PROPS_VARIABLE" => 'prop',
	"SHOW_PRODUCTS_".$arParams['IBLOCK_ID'] => "Y",
	"SET_VIEWED_IN_COMPONENT" => "Y",
	"USE_PRODUCT_QUANTITY" => "Y",
	"PRICE_CODE" => $arParams["PRICE_CODE"],
	"SHOW_PRICES" => $arParams["SHOW_PRICES"],
	"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
	"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
	"CURRENCY_ID" => $arParams["CURRENCY_ID"],
	"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],	
	"PAGE_ELEMENT_COUNT" => "8",
	"LINE_ELEMENT_COUNT" => "8",
),
false
);
?>