<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

$flow = $APPLICATION->get_cookie("flow");
?>
<div class="bx-sort-section">
	<form method="POST" action="">
		<div class="wrap_data_form" style="margin:0">
			<label for=""><font class=""><?=GetMessage("TXT_SORT")?>:</font></label>
			<select name="orderBy" id="orderby_catalog" class="action-orderby">
				<option value="" selected=""><font><font><?=GetMessage("VAL_DEFAULT")?></font></font></option>
				<option value="?sort=catalog_PRICE_1&type=desc" <?if($_REQUEST['sort'] == 'catalog_PRICE_1' && $_REQUEST['type'] == 'desc'){echo 'selected';}?>><font><font><?=GetMessage("VAL_HIGH_TO_LOW")?></font></font></option>
				<option value="?sort=catalog_PRICE_1&type=asc" <?if($_REQUEST['sort'] == 'catalog_PRICE_1' && $_REQUEST['type'] == 'asc'){echo 'selected';}?>><font><font><?=GetMessage("VAL_LOW_TO_HIGH")?></font></font></option>
			</select>
		</div><!--
			--><!--<p class="sort">Сортировка:
				<a <?if ($_GET["sort"] == "name"):?> class="active" <?endif;?>
				href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=name&method=asc">название</a> 
				<a <?if ($_GET["sort"] == "catalog_PRICE_3"):?> class="active" <?endif;?>
				href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=catalog_PRICE_3&method=asc">цена</a> 
				<a <?if ($_GET["sort"] == "property_PRODUCT_TYPE"):?> class="active" <?endif;?>
				href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=property_PRODUCT_TYPE&method=desc">лидер продаж</a> 
				<a <?if ($_GET["sort"] == "timestamp_x"):?> class="active" <?endif;?>
				href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=timestamp_x&method=desc">дата поступления</a>
			</p>--><!--
		--><!--<div class="wrap_data_form">
			<a href="?flow=1" class="grid-show <?//if(!isset($flow ) || ($_REQUEST['flow'] == 1 || $flow == 1) && ($_REQUEST['flow'] != 2) || !$_REQUEST['flow']){echo 'active';}?>" data-show-type="grid"><span class="icon"></span><span><?//=GetMessage("VAL_VIEW_1")?></span></a>
			<a href="?flow=2" class="list-show <?//if(($_REQUEST['flow'] == 2 || $flow == 2) && ($_REQUEST['flow'] != 1)){echo 'active';}?>" data-show-type="list"><span class="icon"></span><span><?//=GetMessage("VAL_VIEW_2")?></span></a>
		</div>-->
	</form>
</div>
<script>
	$( function() {
		$( "#orderby_catalog" ).selectmenu({
			change: function( event, ui ) {
				var val = ui.item.value;
				window.location.href = val;
			}
		});
	});
</script>