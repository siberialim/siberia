<?
$MESS["CT_BCSF_FILTER_TITLE"] = "Select by:";
$MESS["CT_BCSF_FILTER_FROM"] = "From";
$MESS["CT_BCSF_FILTER_TO"] = "To";
$MESS["CT_BCSF_SET_FILTER"] = "Show";
$MESS["CT_BCSF_DEL_FILTER"] = "Reset";
$MESS["CT_BCSF_FILTER_COUNT"] = "Selected: #ELEMENT_COUNT#";
$MESS["CT_BCSF_FILTER_SHOW"] = "Show";
$MESS["CT_BCSF_FILTER_ALL"] = "All";

$MESS ['TXT_SORT'] = "Sort";
$MESS ['VAL_VIEW_1'] = "Grid";
$MESS ['VAL_VIEW_2'] = "List";
$MESS ['VAL_DEFAULT'] = "Default";
$MESS ['VAL_LOW_TO_HIGH'] = "Low to hight";
$MESS ['VAL_HIGH_TO_LOW'] = "Hight to low";
?>