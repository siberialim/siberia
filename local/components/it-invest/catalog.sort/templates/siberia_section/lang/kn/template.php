<?
$MESS["CT_BCSF_FILTER_TITLE"] = "Select by:";
$MESS["CT_BCSF_FILTER_FROM"] = "From";
$MESS["CT_BCSF_FILTER_TO"] = "To";
$MESS["CT_BCSF_SET_FILTER"] = "Show";
$MESS["CT_BCSF_DEL_FILTER"] = "필터 지우기";
$MESS["CT_BCSF_FILTER_COUNT"] = "Selected: #ELEMENT_COUNT#";
$MESS["CT_BCSF_FILTER_SHOW"] = "Show";
$MESS["CT_BCSF_FILTER_ALL"] = "All";

$MESS ['TXT_SORT'] = "분류";
$MESS ['VAL_VIEW_1'] = "쇼핑카트";
$MESS ['VAL_VIEW_2'] = "리스트 (품목)";
$MESS ['VAL_DEFAULT'] = "기본적으로";
$MESS ['VAL_LOW_TO_HIGH'] = "높은 가격에서  낮은 가격까지";
$MESS ['VAL_HIGH_TO_LOW'] = "낮은 가격에서 높은 가격까지";
?>