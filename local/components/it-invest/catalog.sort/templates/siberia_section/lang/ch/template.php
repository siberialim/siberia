<?
$MESS["CT_BCSF_FILTER_TITLE"] = "Select by:";
$MESS["CT_BCSF_FILTER_FROM"] = "From";
$MESS["CT_BCSF_FILTER_TO"] = "To";
$MESS["CT_BCSF_SET_FILTER"] = "Show";
$MESS["CT_BCSF_DEL_FILTER"] = "Reset";
$MESS["CT_BCSF_FILTER_COUNT"] = "Selected: #ELEMENT_COUNT#";
$MESS["CT_BCSF_FILTER_SHOW"] = "Show";
$MESS["CT_BCSF_FILTER_ALL"] = "All";

$MESS ['TXT_SORT'] = "排序";
$MESS ['VAL_VIEW_1'] = "表格";
$MESS ['VAL_VIEW_2'] = "清单";
$MESS ['VAL_DEFAULT'] = "默认";
$MESS ['VAL_LOW_TO_HIGH'] = "价格从低到高";
$MESS ['VAL_HIGH_TO_LOW'] = "价格从高到低";
?>