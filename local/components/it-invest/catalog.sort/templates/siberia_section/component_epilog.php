<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;

CJSCore::Init(array('fx', 'popup'));

$APPLICATION->SetAdditionalCSS($templateFolder.'/style.min.css');
$APPLICATION->AddHeadScript($templateFolder.'/script.min.js');
?>