<?
use Bitrix\Main\Loader;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if($_REQUEST['flow']){
	$flow = (int) $_REQUEST['flow'];
	$APPLICATION->set_cookie("flow", $flow, time()+60*60*24*30*12*2, "/");
}
$this->IncludeComponentTemplate();
