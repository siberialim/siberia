<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$this->setFrameMode(true);
?>	
	<?if($arParams["SHOW_TOP_PRODUCT"] == 'Y'){?>
		<div class="catalog-b-header" style="border-bottom-color:<?=$arParams["SECTION_COLOR"]?>">
			<strong style="color: <?=$arParams["SECTION_COLOR"]?>"></strong>
			<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arParams['AR_SECTION']["SECTION_PAGE_URL"]?>"><?=$arParams['AR_SECTION']['NAME']?></a>
		</div>
	<?}?>
<?
if (!empty($arResult['ITEMS'])) {
?>
	<div class="bx_catalog_list_home grid">
		<?$counter = 1;
		foreach ($arResult['ITEMS'] as $key => $arItem) {
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
			$strMainID = $this->GetEditAreaId($arItem['ID']);

			$arItemIDs = array(
				'ID' => $strMainID,
				'PICT' => $strMainID.'_pict',
				'SECOND_PICT' => $strMainID.'_secondpict',
				'STICKER_ID' => $strMainID.'_sticker',
				'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
				'QUANTITY' => $strMainID.'_quantity',
				'QUANTITY_DOWN' => $strMainID.'_quant_down',
				'QUANTITY_UP' => $strMainID.'_quant_up',
				'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
				'BUY_LINK' => $strMainID.'_buy_link',
				'FAVOURITE_LINK' => $strMainID.'_favourite_link',
				'BASKET_ACTIONS' => $strMainID.'_basket_actions',
				'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
				'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
				'COMPARE_LINK' => $strMainID.'_compare_link',

				'PRICE' => $strMainID.'_price',
				'DSC_PERC' => $strMainID.'_dsc_perc',
				'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
				'PROP_DIV' => $strMainID.'_sku_tree',
				'PROP' => $strMainID.'_prop_',
				'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
				'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
			);

			$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

			$productTitle = (
				isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
				? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
				: $arItem['NAME']
			);
			
			$imgTitle = (
				isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
				? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
				: $arItem['NAME']
			);

			$minPrice = false;
			if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
				$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
			

			?>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 <?if($counter == 3){echo 'last';}?>">
				<div class="bx_catalog_item">
					<div class="bx_catalog_item_container" id="<? echo $strMainID; ?>">
						<div class="goods-pic">
							<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="bx_catalog_item_images"><img style="width:<?=$arItem['PREVIEW_PHOTO']['width']/10?>rem;height:<?=$arItem['PREVIEW_PHOTO']['height']/10?>rem" src="<?=SITE_TEMPLATE_PATH?>/images/static/px.jpg" realsrc="<?=$arItem['PREVIEW_PHOTO']['src']; ?>" alt="<?=$productTitle; ?>" title="<?=$imgTitle;?>"></a>
						</div>
						<div class="bx_catalog_item_price top">
							<div id="<? echo $arItemIDs['PRICE']; ?>" class="goods-price bx_price">
							<?
								$minPrice = (isset($arResult['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
								$boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
							?>
								<span class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>">&nbsp;<b><ins class="action-price"><? echo $minPrice['PRINT_DISCOUNT_VALUE']; ?></ins> / <?=Loc::getMessage("CATALOG_PCS")?></b></span>
							<?
								if ($arParams['SHOW_OLD_PRICE'] == 'Y') {
							?>
									<span class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>" style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?></span>
							<?
								}
								if ($arParams['SHOW_OLD_PRICE'] == 'Y') 
								{
							?>
									<span class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>" style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><?// echo($boolDiscountShow ? Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO', array('#ECONOMY#' => $minPrice['PRINT_DISCOUNT_DIFF'])) : ''); ?></span>
							<?
								}
							?>
							</div>
						</div>
						<div class="goods-info">
							<h3 class="goods-name"><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><font><font><? echo $productTitle; ?></font></font></a></h3>
						</div>
						<?	$minPrice = false;
							if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
							$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
						?>
						<div class="bx_catalog_item_price bottom">
							<div id="<? echo $arItemIDs['PRICE']; ?>" class="goods-price bx_price">
							<?
								$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
								$boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);					
							?>
								<span class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>">&nbsp;<b><ins class="action-price"><? echo $minPrice['PRINT_DISCOUNT_VALUE']; ?></ins> / <?=Loc::getMessage("CATALOG_PCS")?></b></span>
							<?
								if ($arParams['SHOW_OLD_PRICE'] == 'Y') {
							?>
									<span class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>" style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?></span>
							<?
								}
								if ($arParams['SHOW_OLD_PRICE'] == 'Y') 
								{
							?>
									<span class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>" style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><?// echo($boolDiscountShow ? Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO', array('#ECONOMY#' => $minPrice['PRINT_DISCOUNT_DIFF'])) : ''); ?></span>
							<?
								}
							?>
							</div>
						</div>
						<div class="goods-action">
							<div class="goods-buy">
								<span class="p-quantity">
									<input type="text" id="<? echo $arItemIDs['QUANTITY']; ?>" name="goods[num]" class="action-quantity-input" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>"><a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0);" class="btn-decrease"><font><font>-</font></font></a><a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0);" class="btn-increase"><font><font>+</font></font></a>
								</span>
								<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo">
									<a id="<? echo $arItemIDs['BUY_LINK']; ?>" data-add2="basket" class="btn btn-major action-addtocart" href="javascript:void(0)">
									<span><?
									if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
										echo Loc::getMessage('CT_BCE_CATALOG_BUY');
									} else {
										echo Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
									}
									?></span></a><!--
									--><a id="<? echo $arItemIDs['FAVOURITE_LINK']; ?>" data-add2="favourite" class="btn btn-major action-addtofavourite" href="javascript:void(0)"><span><i class="fa fa-heart-o" aria-hidden="true"></i> <?=Loc::getMessage('CT_BCS_TPL_MESS_BTN_FAV')?></span></a>							
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?
			if ($counter >= 4) 
			{
				$counter = 1;
			} 
			else {
				$counter+=1;
			}
			
			$arJSParams = array(
					'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
					'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
					'SHOW_ADD_BASKET_BTN' => false,
					'SHOW_BUY_BTN' => true,
					'SHOW_ABSENT' => true,
					'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
					'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
					'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
					'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
					'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
					'PRODUCT' => array(
						'ID' => $arItem['ID'],
						'NAME' => $productTitle,
						'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
						'CAN_BUY' => $arItem["CAN_BUY"],
						'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
						'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
						'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
						'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
						'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
						'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
						'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
					),
					'BASKET' => array(
						'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
						'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
						'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
						'EMPTY_PROPS' => $emptyProductProperties,
						'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
						'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
						'FAVOURITE_URL_TEMPLATE' => $arResult['~FAVOURITE_URL_TEMPLATE'],
					),
					'VISUAL' => array(
						'ID' => $arItemIDs['ID'],
						'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
						'QUANTITY_ID' => $arItemIDs['QUANTITY'],
						'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
						'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
						'PRICE_ID' => $arItemIDs['PRICE'],
						'BUY_ID' => $arItemIDs['BUY_LINK'],
						'FAVOURITE_ID' => $arItemIDs['FAVOURITE_LINK'],
						'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
						'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
						'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
						'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
						'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
					),
					'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
				);

				unset($emptyProductProperties);
				?>
				<script type="text/javascript">
						var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
				</script>
				<?		
		}
		?><div style="clear: both;"></div>
		<div class="wrap_pagenavi">
		<?=$arResult["NAV_STRING"];?>
		</div>
	</div>
<? } ?>
<script type="text/javascript">
BX.message({
	CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
	CT_BCS_CATALOG_BTN_MESSAGE_FAV_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_FAV_REDIRECT');?>',
	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	ADD_TO_FAV_OK: '<? echo GetMessageJS('ADD_TO_FAV_OK'); ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
<?