<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');

if(!$compositeStub):
foreach ($arResult['SECTIONS'] as $arSection)
{	
?>
	<div class="catalog-b" data-cat="<?=$arSection["ID"]?>" style="border-top:5px solid <?=$arSection["UF_COLOR"]?>">
		<div class="catalog-preview" style="background-color:<?=$arSection["UF_COLOR"]?>">
			<a class="catalog-b-pic" <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arSection['SECTION_PAGE_URL']?>"><img style="width:<?=$arSection['PICTURE_RESIZE']['width']/10?>rem;height:<?=$arSection['PICTURE_RESIZE']['height']/10?>rem" src="<?=SITE_TEMPLATE_PATH?>/images/static/px.jpg" realsrc="<?=$arSection['PICTURE_RESIZE']['src']?>" alt="<?=$arSection["NAME"]?>"><span class="go-to-rubric"><?=Loc::getMessage('REDIRECT_SECTION')?> <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span></a>
			<a class="catalog-b-title" href="<?=$arSection['SECTION_PAGE_URL']?>">
				<span class="catalog-b-title-pic"><img style="width:<?=$arSection['LITTLE_PICTURE_RESIZE']['width']/10?>rem;height:<?=$arSection['LITTLE_PICTURE_RESIZE']['height']/10?>rem" src="<?=SITE_TEMPLATE_PATH?>/images/static/px.jpg" realsrc="<?=$arSection['LITTLE_PICTURE_RESIZE']['src']?>" alt="<?=$arSection["NAME"]?>"/></span>
				<span class="catalog-b-title-txt"><?=$arSection["NAME"]?></a></span>
		</div>	
		<div class="bx_catalog_list_home">
		<?
		foreach ($arSection['ITEMS'] as $key => $arItem)
		{
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
			$strMainID = $this->GetEditAreaId($arItem['ID']);

			$arItemIDs = array(
				'ID' => $strMainID,
				'PICT' => $strMainID.'_pict',
				'SECOND_PICT' => $strMainID.'_secondpict',
				'STICKER_ID' => $strMainID.'_sticker',
				'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
				'QUANTITY' => $strMainID.'_quantity',
				'QUANTITY_DOWN' => $strMainID.'_quant_down',
				'QUANTITY_UP' => $strMainID.'_quant_up',
				'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
				'BUY_LINK' => $strMainID.'_buy_link',
				'FAVOURITE_LINK' => $strMainID.'_favourite_link',
				'BASKET_ACTIONS' => $strMainID.'_basket_actions',
				'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
				'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
				'COMPARE_LINK' => $strMainID.'_compare_link',

				'PRICE' => $strMainID.'_price',
				'DSC_PERC' => $strMainID.'_dsc_perc',
				'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
				'PROP_DIV' => $strMainID.'_sku_tree',
				'PROP' => $strMainID.'_prop_',
				'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
				'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
			);

			$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

			$productTitle = (
				isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
				? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
				: $arItem['NAME']
			);
			$imgTitle = (
				isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
				? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
				: $arItem['NAME']
			);

			$minPrice = false;
			if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
				$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);

			?><div class="bx_catalog_item col-sm-3 col-md-3 col-lg-3 col-xs-3 no-indent"><div class="bx_catalog_item_container" id="<? echo $strMainID; ?>">
				<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> id="<? echo $arItemIDs['PICT']; ?>" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="bx_catalog_item_images" title="<? echo $imgTitle; ?>">
					<img style="width:<?=$arItem['PREVIEW_PHOTO']['width']/10?>rem;height:<?=$arItem['PREVIEW_PHOTO']['height']/10?>rem" src="<?=SITE_TEMPLATE_PATH?>/images/static/px.jpg" alt="<? echo $imgTitle; ?>" realsrc="<?=$arItem['PREVIEW_PHOTO']['src']?>"/>
				</a><div class="bx_catalog_item_title"><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>" style="color:<?=$arSection["UF_COLOR"]?>"><? echo $productTitle; ?></a></div>
			<div class="bx_catalog_item_price"><div id="<? echo $arItemIDs['PRICE']; ?>" class="bx_price"><?
			if (!empty($minPrice) && $arParams["SHOW_PRICES"] == 'Y')
			{
				if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
				{
					echo Loc::getMessage(
						'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
						array(
							'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
							'#MEASURE#' => Loc::getMessage(
								'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
								array(
									'#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
									'#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
								)
							)
						)
					);
				}
				else
				{
					?><span class="price"><?echo $minPrice['PRINT_DISCOUNT_VALUE'];?></span><?
				}
				if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
				{
					?> <span class="old_price"><? echo $minPrice['PRINT_VALUE']; ?></span><?
				}
			}
			unset($minPrice);
			?></div></div><?
				?><div class="bx_catalog_item_controls">
						<div class="bx_catalog_item_controls_blockone">
							<div style="display: inline-block;position: relative;">
								<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)" class="bx_bt_button_type_2 bx_small" rel="nofollow">-</a><!--
								--><input type="text" class="bx_col_input" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>"><!--
								--><a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)" class="bx_bt_button_type_2 bx_small" rel="nofollow">+</a>
							</div>
						</div>
					<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo">
						<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> id="<? echo $arItemIDs['BUY_LINK']; ?>" data-add2="basket" class="bx_bt_button bx_medium" href="javascript:void(0)" rel="nofollow"><span><?
						if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY')
						{
							echo Loc::getMessage('CT_BCS_TPL_MESS_BTN_BUY');
						}
						else
						{
							echo Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
						}
						?></span></a><!--				
						--><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> id="<? echo $arItemIDs['FAVOURITE_LINK']; ?>" data-add2="favourite" class="bx_bt_button bx_medium favourite" href="javascript:void(0)" rel="nofollow"><span><?=Loc::getMessage('CT_BCS_TPL_MESS_BTN_FAV');?></span></a>	
					</div>
					<div style="clear: both;"></div>
				</div><?
				$arJSParams = array(
					'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
					'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
					'SHOW_ADD_BASKET_BTN' => false,
					'SHOW_BUY_BTN' => true,
					'SHOW_ABSENT' => true,
					'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
					'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
					'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
					'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
					'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
					'PRODUCT' => array(
						'ID' => $arItem['ID'],
						'NAME' => $productTitle,
						'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
						'CAN_BUY' => $arItem["CAN_BUY"],
						'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
						'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
						'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
						'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
						'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
						'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
						'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
					),
					'BASKET' => array(
						'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
						'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
						'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
						'EMPTY_PROPS' => $emptyProductProperties,
						'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
						'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
						'FAVOURITE_URL_TEMPLATE' => $arResult['~FAVOURITE_URL_TEMPLATE'],
					),
					'VISUAL' => array(
						'ID' => $arItemIDs['ID'],
						'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
						'QUANTITY_ID' => $arItemIDs['QUANTITY'],
						'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
						'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
						'PRICE_ID' => $arItemIDs['PRICE'],
						'BUY_ID' => $arItemIDs['BUY_LINK'],
						'FAVOURITE_ID' => $arItemIDs['FAVOURITE_LINK'],
						'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
						'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
						'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
						'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
						'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
					),
					'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
				);
				unset($emptyProductProperties);
			?><script type="text/javascript">
					var <? echo $strObName; ?> = new JCCatalogSection2(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
			</script><?
			?></div></div><?
		}
		?><div style="clear: both;"></div>
		</div>
		
		
	</div>
<?
}
endif;
?>