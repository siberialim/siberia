<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<div class="widget-links-top">
	<a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="basket-widget-link"><i class="ico-card-red"></i></a>
	<a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="goods-count"><?echo $arResult['NUM_PRODUCTS'];?></a>
</div>
