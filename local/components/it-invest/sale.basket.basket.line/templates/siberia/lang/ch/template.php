<?
$MESS["TSB1_PERSONAL"] = "Personal Section";
$MESS["TSB1_EXPAND"] = "Expand";
$MESS["TSB1_COLLAPSE"] = "Hide";
$MESS["TSB1_CART"] = "购物车";
$MESS["TSB1_TOTAL_PRICE"] = "Total price";
$MESS["TSB1_YOUR_CART"] = "Your cart";
$MESS["TSB1_READY"] = "In stock";
$MESS["TSB1_DELAY"] = "On wish list";
$MESS["TSB1_NOTAVAIL"] = "Unavailable";
$MESS["TSB1_SUBSCRIBE"] = "Watch list";
$MESS["TSB1_SUM"] = "Total";
$MESS["TSB1_DELETE"] = "Delete";
$MESS["TSB1_2ORDER"] = "Check out";
$MESS["TSB1_LOGIN"] = "进入本站";
$MESS["TSB1_LOGOUT"] = "退出";
$MESS["TSB1_REGISTER"] = "注册";

$MESS["LANGUAGE"] = "语言";

$MESS["LANGUAGE_RU"] = "Russian";
$MESS["LANGUAGE_KN"] = "韩语";
$MESS["LANGUAGE_EN"] = "英语";
$MESS["LANGUAGE_CH"] = "汉语";
$MESS["LANGUAGE_VI"] = "越南与";