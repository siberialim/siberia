<?
$MESS["TSB1_PERSONAL"] = "Personal Section";
$MESS["TSB1_EXPAND"] = "Expand";
$MESS["TSB1_COLLAPSE"] = "Hide";
$MESS["TSB1_CART"] = "Cart";
$MESS['TSB1_FAVORITES'] = "Favorites";
$MESS["TSB1_TOTAL_PRICE"] = "Total price";
$MESS["TSB1_YOUR_CART"] = "Your cart";
$MESS["TSB1_READY"] = "In stock";
$MESS["TSB1_DELAY"] = "On wish list";
$MESS["TSB1_NOTAVAIL"] = "Unavailable";
$MESS["TSB1_SUBSCRIBE"] = "Watch list";
$MESS["TSB1_SUM"] = "Total";
$MESS["TSB1_DELETE"] = "Delete";
$MESS["TSB1_2ORDER"] = "Check out";
$MESS["TSB1_LOGIN"] = "Log In";
$MESS["TSB1_LOGOUT"] = "Log Out";
$MESS["TSB1_REGISTER"] = "Register";

$MESS["CURRENCY"] = "Currency";
$MESS["LANGUAGE"] = "Language";

$MESS["LANGUAGE_RU"] = "Russian";
$MESS["LANGUAGE_KN"] = "Korean";
$MESS["LANGUAGE_EN"] = "English";
$MESS["LANGUAGE_CH"] = "Chinese";
$MESS["LANGUAGE_VI"] = "Viet Nam";