<?
$MESS['TSB1_YOUR_CART'] = "Ваша корзина";
$MESS['TSB1_CART'] = "Корзина";
$MESS['TSB1_FAVORITES'] = "Избранное";
$MESS['TSB1_TOTAL_PRICE'] = "на сумму";
$MESS['TSB1_PERSONAL'] = "Персональный раздел";
$MESS['TSB1_LOGIN'] = "Авторизация";
$MESS['TSB1_LOGOUT'] = "Выйти";
$MESS['TSB1_REGISTER'] = "Регистрация";
$MESS['TSB1_READY'] = "Готовые к покупке товары";
$MESS['TSB1_DELAY'] = "Отложенные товары";
$MESS['TSB1_NOTAVAIL'] = "Недоступные товары";
$MESS['TSB1_SUBSCRIBE'] = "Подписанные товары";
$MESS['TSB1_SUM'] = "на сумму";
$MESS['TSB1_DELETE'] = "Удалить";
$MESS['TSB1_2ORDER'] = "Оформить заказ";
$MESS['TSB1_EXPAND'] = "Раскрыть";
$MESS['TSB1_COLLAPSE'] = "Скрыть";

$MESS["CURRENCY"] = "Валюта";
$MESS["LANGUAGE"] = "Язык";

$MESS["LANGUAGE_RU"] = "Русский";
$MESS["LANGUAGE_KN"] = "Корейский";
$MESS["LANGUAGE_EN"] = "Английский";
$MESS["LANGUAGE_CH"] = "Китайский";
$MESS["LANGUAGE_VI"] = "Вьетнамский";
