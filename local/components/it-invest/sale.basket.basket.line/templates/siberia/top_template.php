<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<div class="row">
	<div class="col-xs-6"> 
	<?//if (!$compositeStub && $arParams['SHOW_AUTHOR'] == 'Y'):?>
		<div class="user-b">
			<?if ($USER->IsAuthorized()):
				$name = trim($USER->GetFullName());
				if (! $name)
					$name = trim($USER->GetLogin());
				if (strlen($name) > 15)
					$name = substr($name, 0, 12).'...';
				?>
				<a href="<?=$arParams['PATH_TO_PROFILE']?>"><?=$USER->GetLogin()?></a>
				&nbsp;
				<a href="?logout=yes"><?=GetMessage('TSB1_LOGOUT')?></a>
			<?else:?>
				<a href="<?=$arParams['PATH_TO_REGISTER']?>"><?=GetMessage('TSB1_LOGIN')?></a>
				&nbsp;
				<!--<a href="<?//=$arParams['PATH_TO_REGISTER']?>?register=yes"><?//=GetMessage('TSB1_REGISTER')?></a>-->
			<?endif?>
		</div>
		<div class="lang btn-group">
			<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
				<span class="data-drop"><img src="<?=SITE_TEMPLATE_PATH?>/images/flags/<?=SLANGCODE?>.jpg" alt="flag"></span>
				<span class="caret"></span>
			</a>	
			<ul class="dropdown-menu">
				<li><a href="/location.php?code_location=ru"><img src="/local/templates/siberia_limited/images/flags/ru.jpg" alt="flag"><?=Loc::getMessage("LANGUAGE_RU")?></a></li>
				<li><a href="/location.php?code_location=en"><img src="/local/templates/siberia_limited/images/flags/en.jpg" alt="flag"><?=Loc::getMessage("LANGUAGE_EN")?></a></li>
				<!--<li><a href="/location.php?code_location=kn"><img src="/local/templates/siberia_limited/images/flags/kn.jpg" alt="flag"><?//=Loc::getMessage("LANGUAGE_KN")?></a></li>
				<li><a href="/location.php?code_location=ch"><img src="/local/templates/siberia_limited/images/flags/ch.jpg" alt="flag"><?//=Loc::getMessage("LANGUAGE_CH")?></a></li>-->
			<?
			/*$ar_result = CLanguage::GetList();
			while($language = $ar_result->GetNext())
			{
				?>
				<li><a href="<?if($language['LID']){echo '/location.php?code_location='.$language['LID'];};?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/flags/<?=$language['LID']?>.jpg" alt="flag"><?=$language['NAME']?></a></li>
				<?
			}*/
			?>
			</ul>
		</div>
	<?//endif?>
	<?
		if(CModule::IncludeModule("currency")) 

		// Выведем все курсы USD, отсортированные по дате
		$arFilter = array(
		);
		
		$by = "date";
		$order = "desc";
		//здесь можно использовать функции и классы модуля
		
		$db_rate = CCurrency::GetList();
		$arrCurrences = array();
		while($ar_rate = $db_rate->Fetch())
		{
		   array_push($arrCurrences, $ar_rate);
		}	
	?>
	<?
	if (INCLUDE_PRICES == "Y")
	{
	?>
		<?
		if (!CURRENCY_ID)
		{
			$currency_id = 'USD';
		}
		else
		{
			$currency_id = CURRENCY_ID;
		}
		?>
		<div class="top-bar-links btn-group currency">
			<a class="btn dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><span class="data-drop"><?=$currency_id?></span><span class="caret"></span></a>
			<ul class="dropdown-menu">
				<?
				foreach ($arrCurrences as $currency)
				{
				?>
					<li><a href="/currency.php?id=<?=$currency["CURRENCY"]?>"><?=$currency["CURRENCY"]?></a></li>
				<?}?>
			</ul>
		</div>
	<?}?>
	</div>
	<div class="col-xs-6">
		<div class="top-bar-links basket-line pull-right">
			<?
				if (!$arResult["DISABLE_USE_BASKET"])
				{
			?>
					<a class="basket-link favorites" href="<?= $arParams['PATH_TO_BASKET'] ?>?DELAY=Y"><i class="shopping-ico"></i><?= GetMessage('TSB1_FAVORITES') ?><?
					if (!$compositeStub)
					{
						if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_DELAY'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'))
						{
							?>&nbsp;<span class="num_products">(<?echo $arResult['NUM_DELAY'];?>)</span><?
						}
					}
					?>
					</a>
					<a class="basket-link" href="<?= $arParams['PATH_TO_BASKET'] ?>"><i class="shopping-ico"></i><?= GetMessage('TSB1_CART') ?><?
					if (!$compositeStub)
					{
						if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'))
						{
							?>&nbsp;<span class="num_products">(<?echo $arResult['NUM_PRODUCTS'];?>)</span><?
						}
						if ($arParams['SHOW_TOTAL_PRICE'] == 'Y' && INCLUDE_PRICES == "Y"):?>
						<span>
							<?= GetMessage('TSB1_TOTAL_PRICE') ?>
							<? if ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'):?>
								<strong><?= $arResult['TOTAL_PRICE'] ?></strong>
							<?endif ?>
						</span>
						<?endif;?>
					<?
					}
					?>
					</a>
			<?
				}
			?>
		</div>
	</div>
</div>