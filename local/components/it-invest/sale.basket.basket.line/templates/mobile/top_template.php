<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<div class="left_part"> 
	<div class="btn-sandwich js-click-openMenu" id="js-click-openMenu">
		<span class="ico-sandwich"><i class="fa fa-list-ul" aria-hidden="true"></i></span>
	</div>
	<a id="mobile_title" href="<?=SITE_DIR?>"></a>
</div>
<div class="right_part">
	<div class="top-bar-links mobile-basket-line pull-right">
	<? if (!$arResult["DISABLE_USE_BASKET"]) { ?>
		<a class="basket-btn btn-m" href="<?= $arParams['PATH_TO_BASKET'] ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i><?
			if (!$compositeStub) {
				if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')) {
					?>&nbsp;<span class="mobile_num_products"><?echo $arResult['NUM_PRODUCTS'];?></span><?
				}
			}
		?></a><?
	} ?>
	</div>
	<div class="top-bar-links btn-search mobile-search-line pull-right js-click-openSearch">
		<span class="search-btn btn-m">
			<i class="fa fa-search" aria-hidden="true"></i>
		</span>
	</div>
	<div class="lang pull-right">
		<a href="javascript:void(0)" class="js-click-openSelectLanguage language-btn btn-m">
			<img src="<?=SITE_TEMPLATE_PATH?>/images/flags/<?=SLANGCODE?>.jpg" alt="flag">
			<span class="caret"></span>
		</a>
	</div>
</div>
