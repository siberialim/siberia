<?
$MESS["TSB1_PERSONAL"] = "Personal Section";
$MESS["TSB1_EXPAND"] = "Expand";
$MESS["TSB1_COLLAPSE"] = "Hide";
$MESS["TSB1_CART"] = "바구니";
$MESS["TSB1_TOTAL_PRICE"] = "총 금액";
$MESS["TSB1_YOUR_CART"] = "Your cart";
$MESS["TSB1_READY"] = "In stock";
$MESS["TSB1_DELAY"] = "On wish list";
$MESS["TSB1_NOTAVAIL"] = "Unavailable";
$MESS["TSB1_SUBSCRIBE"] = "Watch list";
$MESS["TSB1_SUM"] = "총 금액";
$MESS["TSB1_DELETE"] = "Delete";
$MESS["TSB1_2ORDER"] = "Check out";
$MESS["TSB1_LOGIN"] = "들어가기";
$MESS["TSB1_LOGOUT"] = "로그 아웃";
$MESS["TSB1_REGISTER"] = "Register";

$MESS["CURRENCY"] = "Currency";
$MESS["LANGUAGE"] = "언어";

$MESS["LANGUAGE_RU"] = "Russian";
$MESS["LANGUAGE_KN"] = "한국어";
$MESS["LANGUAGE_EN"] = "영어";
$MESS["LANGUAGE_CH"] = "중국어";
$MESS["LANGUAGE_VI"] = "베트남어";
