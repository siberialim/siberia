<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');

if(count($arResult['CATEGORIES']['DELAY']) > 0){
	$number = '3';
}
else{
	$number = '2';
}
?>
<div class="b-header-favourite__icon">
	<div class="b-header-cart__counter">
		<?
		if (!$compositeStub)
		{
			if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_DELAY'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'))
			{
				?><span class="num_products"><?echo $arResult['NUM_DELAY'];?></span><?
			}
		}
		?>
	</div>
	<a href="<?= $arParams['PATH_TO_BASKET'] ?>?DELAY=Y" style="position:absolute;
			left:0;
			right:0;
			top:0;
			bottom:0;
		"></a>
</div>
<div class="b-header-cart__icon">
	<div class="b-header-cart__counter">
		<?
		if (!$compositeStub)
		{
			if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'))
			{
				?><span class="num_products"><?echo $arResult['NUM_PRODUCTS'];?></span><?
			}
		}
		?>
		<a href="<?= $arParams['PATH_TO_BASKET'] ?>" style="position:absolute;
			left:0;
			right:0;
			top:0;
			bottom:0;
		"></a>
	</div>
</div>
<div class="top-bar-links basket-line pull-right">
</div>
