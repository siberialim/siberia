<?
$MESS["SEARCH_GOODS"] = "Например: печенье, конфеты";
$MESS["SEARCH_OTHER"] = "Прочее";
$MESS["FOOTER_COMPANY_ABOUT"] = "Подробнее о компании";
$MESS["FOOTER_COMPANY_PHONE"] = "Свяжитесь с нами";
$MESS["FOOTER_UP_BUTTON"] = "Вверх";
$MESS["HEADER_MENU"] = "Каталог";
$MESS["CONTACT_US"] = "Свяжитесь с нами";
$MESS["CATALOG_BREADCRUMBS"] = "Каталог";
$MESS["HOME_BREADCRUMBS"] = "Главная";

$MESS["CURRENCY"] = "Валюта";
$MESS["LANGUAGE"] = "Язык";

$MESS["LANGUAGE_KN"] = "Корейский";
$MESS["LANGUAGE_EN"] = "Английский";
$MESS["LANGUAGE_CH"] = "Китайский";
$MESS["LANGUAGE_VI"] = "Вьетнамский";

$MESS["MORE"] = "Ещё...";
?>