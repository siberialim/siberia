<?
$MESS["SEARCH_GOODS"] = "For example: biscuits, candies";
$MESS["SEARCH_OTHER"] = "Other";
$MESS["FOOTER_COMPANY_ABOUT"] = "About Us";
$MESS["FOOTER_COMPANY_PHONE"] = "Contact Us";
$MESS["FOOTER_UP_BUTTON"] = "TOP";
$MESS["HEADER_MENU"] = "Menu";
$MESS["CONTACT_US"] = "Contact us";
$MESS["CATALOG_BREADCRUMBS"] = "Catalog";
$MESS["HOME_BREADCRUMBS"] = "Home";

$MESS["CURRENCY"] = "Currency";
$MESS["LANGUAGE"] = "Language";

$MESS["LANGUAGE_KN"] = "Korean";
$MESS["LANGUAGE_EN"] = "English";
$MESS["LANGUAGE_CH"] = "Chinese";
$MESS["LANGUAGE_VI"] = "Viet Nam";

$MESS["MORE"] = "Etc.";
?>