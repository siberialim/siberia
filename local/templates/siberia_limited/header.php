<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//use from D7
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);

IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
//путь до текущей страницы относительно корня
$curPage = $APPLICATION->GetCurPage(true);

CModule::IncludeModule('iblock');

use \Itinvest\Catalog\Iblock;

// Подключим языковой файл
// Предполагаем, что языковой файл расположен стандартным образом
IncludeTemplateLangFile(__FILE__, SLANGCODE);

?>
<!DOCTYPE html>
<html xml:lang="<?=SLANGCODE?>" lang="<?=SLANGCODE?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?=$APPLICATION->ShowMeta("keywords")?>
		<?=$APPLICATION->ShowMeta("description")?>
		<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/images/static/favicon.ico" />
		<!--<link type="text/css" rel="stylesheet" href="<?//=SITE_TEMPLATE_PATH?>/fonts/stylesheet.css" />-->
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/WOW-master/css/libs/animate.css" />
		<link type="text/css" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/slick-1.8.0/slick/slick.css" />
		<link type="text/css" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/bootstrap.min.css" />
		<?
			Asset::getInstance()->addJs("https://use.fontawesome.com/2c231ed3f0.js");
		?>
		<?
			$APPLICATION->ShowHead();
			// для js-файлов
			CJSCore::Init(array('jquery'));
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.min.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-contained-sticky-scroll.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap.min.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/WOW-master/dist/wow.min.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slick-1.8.0/slick/slick.min.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js");	
		?>
		<title><?=$APPLICATION->ShowTitle()?></title>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" >
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter41390384 = new Ya.Metrika({
							id:41390384,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true,
							webvisor:true
						});
					} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/41390384" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->	
	</head>
	<script type="text/javascript">
		(function($){
		  $(document).ready(
			  function() { 
				$("a.gallery").fancybox({});
						
				$('#popupbutton, #popupbutton_right').fancybox({
					'padding': 37,
					'overlayOpacity': 0.87,
					'overlayColor': '#fff',
					'transitionIn': 'none',
					'transitionOut': 'none',
					'titlePosition': 'inside',
					'centerOnScroll': true,
					'width': 400,
					'minHeight': 310
				});
		  });
		})(jQuery);
	</script>
	<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="top-bar navbar-fixed-top">
		<?$APPLICATION->IncludeComponent(
			"it-invest:sale.basket.basket.line", 
			"siberia", 
			array(
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
				"PATH_TO_PERSONAL" => SITE_DIR."personal/private/",
				"SHOW_PERSONAL_LINK" => "N",
				"SHOW_NUM_PRODUCTS" => "Y",
				"SHOW_TOTAL_PRICE" => "Y",
				"SHOW_PRODUCTS" => "N",
				"POSITION_FIXED" => "N",
				"SHOW_AUTHOR" => "Y",
				"PATH_TO_REGISTER" => SITE_DIR."login",
				"PATH_TO_PROFILE" => SITE_DIR."personal/private/",
				"COMPONENT_TEMPLATE" => "siberia",
				"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
				"SHOW_EMPTY_VALUES" => "Y",
				"HIDE_ON_BASKET_PAGES" => "Y"
			),
			false
		);?>
	</div>
	<div class="mobile-top-bar navbar-fixed-top">
		<?$APPLICATION->IncludeComponent(
			"it-invest:sale.basket.basket.line", 
			"mobile", 
			array(
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
				"PATH_TO_PERSONAL" => SITE_DIR."personal/private/",
				"SHOW_PERSONAL_LINK" => "N",
				"SHOW_NUM_PRODUCTS" => "Y",
				"SHOW_TOTAL_PRICE" => "Y",
				"SHOW_PRODUCTS" => "N",
				"POSITION_FIXED" => "N",
				"SHOW_AUTHOR" => "Y",
				"PATH_TO_REGISTER" => SITE_DIR."login",
				"PATH_TO_PROFILE" => SITE_DIR."personal/private/",
				"COMPONENT_TEMPLATE" => "siberia",
				"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
				"SHOW_EMPTY_VALUES" => "Y",
				"HIDE_ON_BASKET_PAGES" => "Y"
			),
			false
		);?>
	</div>
	<div id="main_loader" class="box_loader" style="display:none"><div class="loader"></div></div>
    <header>
		<div id="p_prldr"><div class="contpre"></div></div>
		<div class="header">
			<div class="container">
				<div class="row" id="top-bn-wrp">
					<div class="col-xs-12 col-sm-3 col-md-3 col-ls-3">
						<div class="logo-b">
							<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=SITE_DIR?>" class="logo-link"><img src="<?=SITE_TEMPLATE_PATH?>/images/static/logo.png" alt="logo"></a>					
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-ls-6 no-indent">
						<div class="search-form-wrp">					
							<?$APPLICATION->IncludeComponent(
								"it-invest:search.title", 
								"siberia", 
								array(
									"NUM_CATEGORIES" => "5",
									"TOP_COUNT" => "5",
									"CHECK_DATES" => "N",
									"SHOW_OTHERS" => "N",
									"PAGE" => SITE_DIR."catalog/",
									"CATEGORY_0_TITLE" => Loc::getMessage("SEARCH_GOODS"),
									"CATEGORY_0" => array(
										0 => "iblock_catalog",
									),
									"CATEGORY_0_iblock_catalog" => array(
										0 => IBLOCK_CATALOG_ID,
									),
									"CATEGORY_OTHERS_TITLE" => Loc::getMessage("SEARCH_OTHER"),
									"SHOW_INPUT" => "Y",
									"INPUT_ID" => "title-search-input",
									"CONTAINER_ID" => "search",
									"PRICE_CODE" => array(
										0 => "BASE",
									),
									"SHOW_PRICES" => "N",
									"SHOW_PREVIEW" => "Y",
									"PREVIEW_WIDTH" => "75",
									"PREVIEW_HEIGHT" => "75",
									"CONVERT_CURRENCY" => "Y",
									"COMPONENT_TEMPLATE" => "siberia",
									"ORDER" => "date",
									"USE_LANGUAGE_GUESS" => "Y",
									"PRICE_VAT_INCLUDE" => "Y",
									"PREVIEW_TRUNCATE_LEN" => "",
									"CURRENCY_ID" => CURRENCY_ID
								),
								false
							);?>				
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-ls-3">
						<!--<div class="contacts-b">
							<ul>
								<li class="contact-links-link">
									<!--<a href="tel:8(3842)75-78-55" class="contact-links-info">8 (3842) 75 78 55</a>-->
									<!--<a href="tel:8(3842)65-02-30" class="contact-links-info">+7 3842 650 230</a>
								</li>
							</ul>
						</div>-->
						<div class="basket-b_head">
							<?$APPLICATION->IncludeComponent(
								"it-invest:sale.basket.basket.line", 
								"basket", 
								array(
									"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
									"PATH_TO_PERSONAL" => SITE_DIR."personal/private/",
									"SHOW_PERSONAL_LINK" => "N",
									"SHOW_NUM_PRODUCTS" => "Y",
									"SHOW_TOTAL_PRICE" => "Y",
									"SHOW_PRODUCTS" => "N",
									"POSITION_FIXED" => "N",
									"SHOW_AUTHOR" => "Y",
									"PATH_TO_REGISTER" => SITE_DIR."login",
									"PATH_TO_PROFILE" => SITE_DIR."personal/private/",
									"COMPONENT_TEMPLATE" => "siberia",
									"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
									"SHOW_EMPTY_VALUES" => "Y",
									"HIDE_ON_BASKET_PAGES" => "Y",
									"CONVERT_CURRENCY" => "Y",
									'CURRENCY_ID' => CURRENCY_ID,
								),
								false
							);?>
						</div>
					</div>
				</div>
				<div class="row" id="main_menu">
					<div class="col-sm-3 col-md-3">
					</div>
					<div class="col-sm-9 col-md-9">
						<div class="menu-wrp">
							<div class="main_top_menu">
								<?$APPLICATION->IncludeComponent("it-invest:menu", "siberia_top_horizontal_menu", array(
										"ROOT_MENU_TYPE" => "top".SLANGCODE,
										"MAX_LEVEL" => "1",
										"MENU_CACHE_TYPE" => "A",
										"CACHE_SELECTED_ITEMS" => "N",
										"MENU_CACHE_TIME" => "36000000",
										"MENU_CACHE_USE_GROUPS" => "Y",
										"MENU_CACHE_GET_VARS" => array(
										),
									),
									false
								);?>
							</div>
						</div>
					</div>
				</div>					
				<?// if ($APPLICATION->GetCurPage(false) != '/'): ?>
					<div id="main_catalog_list" class="col-sm-3 col-md-3 wrapper-primary-menu">
						<div class="primary-menu <?if ($curPage != '/index.php'){ echo ' contracted'; }?>">
							<?$APPLICATION->IncludeComponent("it-invest:custom.section.list", "siberia_hide", array(
								),
								false
							);?>
						</div>
					</div>
				<?// endif; ?> 
			</div>
		</div>
    </header>
	<div id="mobile_left_menu">
		<?$APPLICATION->IncludeComponent("it-invest:menu", "mobile", array(
				"ROOT_MENU_TYPE" => "top".SLANGCODE,
				"MAX_LEVEL" => "1",
				"MENU_CACHE_TYPE" => "A",
				"CACHE_SELECTED_ITEMS" => "N",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
			),
			false
		);?>
	</div>
	<div id="mobile_search_box">
		<?$APPLICATION->IncludeComponent(
			"it-invest:search.title", 
			"siberia_mobile", 
			array(
				"NUM_CATEGORIES" => "3",
				"TOP_COUNT" => "15",
				"CHECK_DATES" => "N",
				"SHOW_OTHERS" => "N",
				"PAGE" => SITE_DIR."catalog/",
				"CATEGORY_0_TITLE" => Loc::getMessage("SEARCH_GOODS"),
				"CATEGORY_0" => array(
					0 => "iblock_catalog",
				),
				"CATEGORY_0_iblock_catalog" => array(
					0 => IBLOCK_CATALOG_ID,
				),
				"CATEGORY_OTHERS_TITLE" => Loc::getMessage("SEARCH_OTHER"),
				"SHOW_INPUT" => "Y",
				"INPUT_ID" => "title-search-input-mobile",
				"CONTAINER_ID" => "title-search-mobile",
				"PRICE_CODE" => array(
					0 => "BASE",
				),
				"SHOW_PRICES" => "N",
				"SHOW_PREVIEW" => "Y",
				"PREVIEW_WIDTH" => "110",
				"PREVIEW_HEIGHT" => "110",
				"CONVERT_CURRENCY" => "Y",
				"COMPONENT_TEMPLATE" => "siberia_mobile",
				"ORDER" => "date",
				"USE_LANGUAGE_GUESS" => "Y",
				"PRICE_VAT_INCLUDE" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"CURRENCY_ID" => CURRENCY_ID
			),
			false
		);?>
	</div>
	<div id="mobile_language_select">
		<ul class="mobile_list_language">
			<li><button class="cross js-click-openSelectLanguage">&#215;</button></li>
			<li><a href="/location.php?code_location=ru" <?if(SLANGCODE == 'ru'){echo 'class="active"';}?>><img src="/local/templates/siberia_limited/images/flags/ru.jpg" alt="flag"><?=Loc::getMessage("LANGUAGE_RU")?></a></li>
			<li><a href="/location.php?code_location=en" <?if(SLANGCODE == 'en'){echo 'class="active"';}?>><img src="/local/templates/siberia_limited/images/flags/en.jpg" alt="flag"><?=Loc::getMessage("LANGUAGE_EN")?></a></li>
			<!--<li><a href="/location.php?code_location=kn" <?//if(SLANGCODE == 'kn'){echo 'class="active"';}?>><img src="/local/templates/siberia_limited/images/flags/kn.jpg" alt="flag"><?//=Loc::getMessage("LANGUAGE_KN")?></a></li>
			<li><a href="/location.php?code_location=ch" <?//if(SLANGCODE == 'ch'){echo 'class="active"';}?>><img src="/local/templates/siberia_limited/images/flags/ch.jpg" alt="flag"><?//=Loc::getMessage("LANGUAGE_CH")?></a></li>
			-->
		</ul>
	</div>
	<div id="mobile_currency_select">
	</div>
    <section class="main">
        <div class="container output">