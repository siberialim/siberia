$(document).ready(function() {
    
    $('.widget-links-bottom, .top-btn').on('click', returnUp);
	
	$(window).on('scroll', onOrOffMobileUp);
	
	$('.js-click-openMenu').bind('click', {
			flip: 0
		},
		openMobileMenu
	);
		
	$('.js-click-openSearch').bind('click', {
			flip: 0
		},
		openMobileSearch
	);
	
	$('.js-click-openSelectLanguage').bind('click', {
			flip: 0
		},
		openSelectLanguage
	);
	
	window.onscroll = showVisible;
    showVisible();
	
	if (document.body.clientWidth < 768) {
		onMobileV();
	};
	
	// перекрывает стандартный битриксовый лоадер
	BX.showWait = function(node, msg) {
		$('#main_loader').show();
	};
	BX.closeWait = function(node, obMsg) {
		$('#main_loader').hide();
	};
	
		
	$(window).resize(function() {
		if (window.screen.width < 768) {
			onMobileV();
		} else {
			offMobileV();
		};
	});
	
	// скрывает/показывает кнопку "вверх" для мобильной версии сайта
	if (window.pageYOffset > document.documentElement.clientHeight || 
			document.documentElement.scrollTop > document.documentElement.clientHeight) {
		$('.top-btn.mobile').show();
	} else {
		$('.top-btn.mobile').hide();
	};
	
	// скрывает/показывает кнопку "вверх" для мобильной версии сайта при прокрутке
	function onOrOffMobileUp() {	
		if (window.pageYOffset > document.documentElement.clientHeight || 
				document.documentElement.scrollTop > document.documentElement.clientHeight) {
			$('.top-btn.mobile').show();
		} else {
			$('.top-btn.mobile').hide();
		};
	};

	//$('#phone').mask('+7 (000) 000-00-00');
    /*$("#form-feedback").submit(function(event) {
        if ($('#name').val() == "") {
			$('#bthrow_error_name').fadeIn(1000).html('Представьтесь, пожалуйста.');
        } else if ($('#phone').val() == "") {
			$('#bthrow_error_name').empty();
			$('#bthrow_error_phone').fadeIn(1000).html('Как с Вами связаться?');
        } else {
			var postForm = {
				'name'  : $('#name').val(),
				'phone'  : $('#phone').val()
			};

			$.ajax({
				type        : 'POST',
				url         : 'feedback.php',
				data        : postForm,
				dataType    : 'json',
				success     : function(data) {
					if (!data.success) {
						if (data.errors.name) {
							$('.throw_error').fadeIn(1000).html(data.errors.name);
						}
					} else {
						$('#form-feedback').fadeIn(1000).html('<p>' + data.posted + '</p>');
					}
				}
			});	
		}
 
		event.preventDefault();
 
    });*/

		/* Placeholder for IE */
	/*if ($.browser.msie) { // Условие для вызова только в IE
		$("form").find("input[type='text'], textarea").each(function() {
			var tp = $(this).attr("placeholder");
			$(this).attr('value',tp).css('color','#777');
		});

		/* Protected send form */
	/*	$("form").submit(function() {
			$(this).find("input[type='text']").each(function() {
				var val = $(this).attr('placeholder');
				if ($(this).val() == val) {
					$(this).attr('value','');
				}
			})
		});
	};*/
	
	//возвращает наверх
	function returnUp() {
        $('body,html').animate({scrollTop:0},800);
	};
	
	//скрывает/показывает мобильное меню
	function openMobileMenu( event ) {
		event.data.flip++;
		$('body').toggleClass('noscroll');
		if (event.data.flip % 2 == 0) {
			$('#mobile_left_menu').animate({left: -32+'rem'}, 250);
		} else {
			$('#mobile_left_menu').show();
			$('#mobile_left_menu').animate({left: 0}, 250);
		}
	};
	
	//скрывает/показывает экран поиска
	function openMobileSearch( event ) {
		event.data.flip++;
		$('body').toggleClass('noscroll');
		if (event.data.flip % 2 == 0) {
			$('#mobile_search_box').animate({top: -100+'%'}, 250);
		} else {
			$('#mobile_search_box').css({'height':document.documentElement.clientHeight})
			$('#mobile_search_box').show();
			$('#mobile_search_box').animate({top: 0}, 250);
		}
	};
	
	//скрывает/показывает экран выбора языка
	function openSelectLanguage( event ) {
		event.data.flip++;
		$('body').toggleClass('noscroll');
		if (event.data.flip % 2 == 0) {
			$('#mobile_language_select').animate({top: -100+'%'}, 250);
		} else {
			$('#mobile_language_select').css({'height':document.documentElement.clientHeight})
			$('#mobile_language_select').show();
			$('#mobile_language_select').animate({top: 0}, 250);
		}
	};
	
	$('.primary-menu.contracted').click(function(e) {
		$('.primary-menu__cont').not('.secondary').show();
	});
	
	$('.primary-menu__cont').click(function(event) {
		if ($(event.target).closest("#message").length) return;
		//$('.primary-menu__cont').hide();
		event.stopPropagation();
	});
	
	$('.primary-menu__cont .css-show-hidden-section').hover(function(event) {
		$('.primary-menu__cont.parent.more').show();
	});
	
	$('.primary-menu__cont .css-show-hidden-section').on('mouseleave', function(event) {
		$('.primary-menu__cont.parent.more').hide();
	});
	
	$('.primary-menu__cont.more').hover(function(event) {
		$('.primary-menu__cont.parent.more').show();
	});
	
	$('.primary-menu__cont.more').on('mouseleave', function(event) {
		$('.primary-menu__cont.parent.more').hide();
	});
		
	$(document).mouseup(function (e) { // событие клика по веб-документу
		var el = $(".primary-menu.contracted .primary-menu__cont"); // 
		if (!el.is(e.target) // если клик был не по нашему блоку
		    && el.has(e.target).length === 0) { // и не по его дочерним элементам
			el.hide(); // скрываем его
		}
	});
	
	function onMobileV() {
		$('.top-btn').addClass('mobile');
	};
	
	function offMobileV() {
		$('.top-btn').removeClass('mobile');
	};
	
	/**
     * Проверяет элемент на попадание в видимую часть экрана.
     * Для попадания достаточно, чтобы верхняя или нижняя границы элемента были видны.
    */
	
   function isVisible(elem) {
		var coords = elem.getBoundingClientRect();

		var windowHeight = document.documentElement.clientHeight;

		// верхняя граница elem в пределах видимости ИЛИ нижняя граница видима
		var topVisible = coords.top > 0 && coords.top < windowHeight;
		var bottomVisible = coords.bottom < windowHeight && coords.bottom > 0;

		return topVisible || bottomVisible;
    };

    /**
    Вариант проверки, считающий элемент видимым,
    если он не более чем -1 страница назад или +1 страница вперед

    function isVisible(elem) {

      var coords = elem.getBoundingClientRect();

      var windowHeight = document.documentElement.clientHeight;

      var extendedTop = -windowHeight;
      var extendedBottom = 2 * windowHeight;

      // top visible || bottom visible
      var topVisible = coords.top > extendedTop && coords.top < extendedBottom;
      var bottomVisible = coords.bottom < extendedBottom && coords.bottom > extendedTop;

      return topVisible || bottomVisible;
    }
    */

	function showVisible() {
		var imgs = document.getElementsByTagName('img');
		for (var i = 0; i < imgs.length; i++) {

			var img = imgs[i];

			var realsrc = img.getAttribute('realsrc');
			if (!realsrc) continue;

			if (isVisible(img)) {
				img.src = realsrc;
				img.setAttribute('realsrc', '');
			}
		}
	};
		
});