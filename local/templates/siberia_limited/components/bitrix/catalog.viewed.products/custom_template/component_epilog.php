<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use \Bitrix\Catalog\CatalogViewedProductTable as CatalogViewedProductTable;

global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}

CatalogViewedProductTable::refresh($arResult['ID'], CSaleBasket::GetBasketUserID());

CJSCore::Init(array("popup"));
?>