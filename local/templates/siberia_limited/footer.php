<?
//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
		<!-- Add mousewheel plugin (this is optional) -->
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
		
		<!-- Add jquery-ui -->
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui/jquery-ui.min.css" type="text/css" media="screen" />
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui/jquery-ui.min.js"></script>
		
		<!-- Optionally add helpers - button, thumbnail and/or media -->
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		
		<script type="text/javascript">
			(function($){
			  $(document).ready(
				  function() { 
					$("a.gallery").fancybox();							  
			  });
			})(jQuery);
		</script>
	</div>    
</section>
<footer>
	<div class="wrapPopupWindow-addCart" style="display:none">
		<div class="popup-window-titlebar"><span class="popup-window-titlebar-text">#TITLE_POPUP#</span></div>
		<div class="popup-window-content">#CONTENT#</div>
		<div class="popup-window-buttons"><a class="bx_medium bx_bt_button siberia_btn cart" id="" href="#LINK_BASKET#">#TEXT_BASKET#</a></div>
	</div>
	<div class="footer-main">
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-8">
						<?/*$APPLICATION->IncludeComponent("it-invest:menu", "siberia_bottom_vertical_section_menu", array(
								"ROOT_MENU_TYPE" => "bottom".SLANGCODE,
								"MAX_LEVEL" => "1",
								"MENU_CACHE_TYPE" => "A",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_TIME" => "36000000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(
								),
							),
							false
						);*/?>
						<div class="our_app">
							<a target="_blank" href="https://itunes.apple.com/ru/app/siberia-products/id1239805943?mt=8"><img src="<?=SITE_TEMPLATE_PATH?>/images/static/app_store.png"/></a>
							<a target="_blank" href="https://play.google.com/store/apps/details?id=com.siberiaproducts.app&hl=ru"><img src="<?=SITE_TEMPLATE_PATH?>/images/static/google_play.png"/></a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-ls-4 footer_contact">
						<!--<div class="qr-f pull-right">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/static/qr.png" alt="qr">
							<span>Android</span>
						</div>-->
						<div style="margin: 2.6rem 0 0 0;font-size:1.6em">
							<?
							$path = (SLANGCODE != "ru") ? "/include/footer_contact.php" : "/include/footer_contact_ru.php";
							$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => $path,
								"EDIT_TEMPLATE" => ""
							),
							false
							);
							unset($path);
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>   
	<div class="footer-line">
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-ls-4">
						<div id="bx-composite-banner"></div>
						<div class="footer-copyright"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => "/include/copyright.php",
						"EDIT_TEMPLATE" => ""
						),
						false
						);?></div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 col-ls-8">					
						<?/*$APPLICATION->IncludeComponent("it-invest:menu", "siberia_bottom_horizontal_menu", array(
								"ROOT_MENU_TYPE" => "bottom".SLANGCODE,
								"MAX_LEVEL" => "1",
								"MENU_CACHE_TYPE" => "A",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_TIME" => "36000000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(
								),
							),
							false
						);*/?>
						<?
						if (SLANGCODE == "ru"):
						?>
						<span class="site_created" style="display:block; text-align:right;"><a href="http://it-invest42.ru" target="_blank">Разработка веб-сайта ООО "Айти Инвест"</a></span>
						<?endif;?>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<!-- BITRIX24 -->
	<script data-skip-moving="true">
		(function(w,d,u,b){
				s=d.createElement('script');r=(Date.now()/60000|0);s.async=1;s.src=u+'?'+r;
				h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
		})(window,document,'https://cdn.bitrix24.ru/b1851199/crm/site_button/<?=ID_WIDGET_BX24?>.js');
	</script>
	<script type="text/javascript">
		$(window).on('load', function () {
			var $preloader = $('#p_prldr'),
			$svg_anm   = $preloader.find('.load-logo');
			$svg_anm.fadeOut();
			$preloader.delay(250).fadeOut('slow');
		});
	</script>
</footer>		
	</body>
</html>
