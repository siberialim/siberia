<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
session_start();
if (!CModule::IncludeModule("mobileapp")) die();
if (!CModule::IncludeModule("sale")) die;
CMobile::Init();

//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

// Подключим языковой файл
// Предполагаем, что языковой файл расположен стандартным образом

IncludeTemplateLangFile(__FILE__, SLANGCODE);

$itemsInBasket = CSaleBasket::GetList(
	array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
	array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"ORDER_ID" => "NULL",
			"DELAY" => "N"
		),
	false,
	false,
	array("ID")
);
$countItemsInBasket = 0;
while($arItemInBasket = $itemsInBasket->Fetch()){
	$countItemsInBasket+=1;
}
?>

<!DOCTYPE html>
<html <?=$APPLICATION->ShowProperty("Manifest");?> class="<?=CMobile::$platform;?>">
	<head>
		<?$APPLICATION->ShowHead();?>
		<meta http-equiv="Content-Type" content="text/html;charset=<?=SITE_CHARSET?>"/>
		<meta name="format-detection" content="telephone=no">
		<!--<link href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/template_styles.css")?>" type="text/css" rel="stylesheet" />-->
		<?//$APPLICATION->ShowHeadStrings();?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/script.js");?>
		<?CJSCore::Init('ajax');?>
		<title><?$APPLICATION->ShowTitle()?></title>

		<?CJSCore::Init(array("jquery"));?>
	</head>
	<body id="body" class="<?=$APPLICATION->ShowProperty("BodyClass");?>">
	<script>
		/*window.onerror = function (msg, url, line) {
			alert(msg + "\n" + url + "\n" + "\n" + line);
			return true;
		};*/
	</script>
	<?if (!CMobile::getInstance()->getDevice()) $APPLICATION->ShowPanel();?>
	<script type="text/javascript">

		app.pullDown({
			enable:true,
			callback:function(){document.location.reload();},
			downtext:"<?=Loc::getMessage("MB_PULLDOWN_DOWN")?>",
			pulltext:"<?=Loc::getMessage("MB_PULLDOWN_PULL")?>",
			loadtext:"<?=Loc::getMessage("MB_PULLDOWN_LOADING")?>"
		});
		//Деактивация контрола
		BXMobileApp.UI.Page.Refresh.setEnabled(false);
	</script>
	<?
	if (!$USER->IsAuthorized())
	{
	?>
		<script type="text/javascript">
			//Cкрываем навигационную панель
			BXMobileApp.UI.Page.TopBar.hide(); 
			//Блокируем открытие левой части слайдера
			BXMobileApp.UI.Slider.setStateEnabled(BXMobileApp.UI.Slider.state.LEFT, false);
		</script>
	<?
	}
	if ($APPLICATION->GetCurPage(true) != SITE_DIR."personal/cart/index.php")
	{
	?>
		<script type="text/javascript">
			//добавим кнопку для корзины в верхней панели
			app.addButtons({menuButton: {
				type:    'basket',
				name:    'basket',
				badgeCode: 'basket',
				style:   'custom',
				callback: function()
				{
					app.openNewPage("<?=SITE_DIR?>personal/cart/");
				}
			}});
			//добавим счетчик для корзины
			BXMobileApp.UI.Badge.setButtonBadge('basket', <?=$countItemsInBasket?>);
			
			//событие по которому будет обновляться счетчик
			BX.addCustomEvent("recalcart", function(data) {
				BXMobileApp.UI.Badge.setButtonBadge('basket', data.count);
			});
			
			//добавим кнопку для выбра языка
			/*app.addButtons({menuButton: {
				type:    'basket1',
				name:    'basket1',
				badgeCode: 'basket1',
				style:   'custom1',
				callback: function()
				{
					//Создаем контекстное меню, состоящее из трех пунктов. В данном примере “test” является идентификатором данного контекстного меню. 

					var menu = new BXMobileApp.UI.Menu(
					{
						items: 

						[
							{
								name:"<?=Loc::getMessage("LANGUAGE_RU")?>",
								image: "/local/templates/siberia_limited/images/flags/ru.jpg",
								url:"/location.php?code_location=ru",
							},
							{
								name:"<?=Loc::getMessage("LANGUAGE_RU")?>",
								image: "/local/templates/siberia_limited/images/flags/ru.jpg",
								url:"/location.php?code_location=ru",
							}
							/*{
								name:"Корпоративный портал",
								action:function() 

								{

								app.alert(
								{    
									title: "Корпоративный портал",
									button: "OK",
									text: "Система управления внутренним информационным ресурсом компании для коллективной работы над задачами, проектами и документами, для эффективных внутренних коммуникаций. "
								});
								},

								icon: 'check'

							},
							{
								name:"Мобильное приложение",
								url:"/my_site/index2.php",
								arrowFlag:"true"
							}*/         
						/*]

					}, "test");

//Отобразим данное контекстное меню путем нажатия на заголовок в навигационной панели, используя методы навигационной панели. Задаем основной текст заголовка, используя BXMobileApp.UI.Page.TopBar.title.setText();. Используя обработчик события нажатия на заголовок BXMobileApp.UI.Page.TopBar.title.setCallback();, отобразим созданное контекстное меню. Используем BXMobileApp.UI.Page.TopBar.title.show();, чтобы отобразить активный заголовок на навигационной панели.

   /*menu.show();//отображаем созданное контекстное меню        


				}
			}});*/
			
		</script>
	<?
	}
	?>
	<div class="wrap">

	<?