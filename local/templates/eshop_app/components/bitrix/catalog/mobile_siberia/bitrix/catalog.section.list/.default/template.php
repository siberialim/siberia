<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="maincontent_component">
	<div class="main_button_component">
		<span><?=$arResult["SECTION"]["NAME"]?></span><!--
		--><!--
		--><a href="/eshop_app/" class = "current" class="main_button_catalog"><?=GetMessage("CATALOG")?></a><!--
		--><div class="clb"></div>
	</div>
<div class="item_listcategory">
	<ul>
<?foreach($arResult["SECTIONS"] as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));?>
	<li id="<?=$this->GetEditAreaId($arSection['ID']);?>">
		<a href="<?=$arSection["SECTION_PAGE_URL"]?>">
			<?=$arSection["NAME"]?><?if($arParams["COUNT_ELEMENTS"]):?>&nbsp;(<?=$arSection["ELEMENT_CNT"]?>)<?endif;?>
		</a>
	</li>
<?endforeach;?>
	</ul>
</div>

<?if (is_array($arResult["SECTIONS"]) && count($arResult["SECTIONS"]) > 0):?>
<div class="bgline"><br></div>
<?endif?>
</div>
