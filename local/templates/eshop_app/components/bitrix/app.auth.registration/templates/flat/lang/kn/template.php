<?
$MESS["AUTH_REGISTER"] = "회원가입";
$MESS["AUTH_NAME"] = "이름";
$MESS["AUTH_LAST_NAME"] = "성";
$MESS["AUTH_LOGIN_MIN"] = "로그인";
$MESS["AUTH_CONFIRM"] = "비밀번호 확인";
$MESS["CAPTCHA_REGF_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_REQ"] = "";
$MESS["AUTH_AUTH"] = "로그인";
$MESS["AUTH_PASSWORD_REQ"] = "비밀번호";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "На указанный в форме e-mail придет запрос на подтверждение регистрации.";
$MESS["AUTH_EMAIL_SENT"] = "На указанный в форме e-mail было выслано письмо с информацией о подтверждении регистрации.";
$MESS["AUTH_EMAIL"] = "E-Mail";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
?>