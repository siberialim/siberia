<?
//use from D7
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
	<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH ."/bootstrap.css");?>
	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	
	<!-- Add jquery-ui -->
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui/jquery-ui.min.css" type="text/css" media="screen" />
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui/jquery-ui.min.js"></script>
	
		<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	
	<script type="text/javascript">
		(function($){
		  $(document).ready(
			  function() { 
				$("a.gallery").fancybox({});
				
				//$("#currences").selectmenu();
				
				$('#popupbutton').fancybox({
				'padding': 37,
				'overlayOpacity': 0.87,
				'overlayColor': '#fff',
				'transitionIn': 'none',
				'transitionOut': 'none',
				'titlePosition': 'inside',
				'centerOnScroll': true,

				});
				
				$('#popAddCommentButton').fancybox({
				'padding': 37,
				'overlayOpacity': 0.87,
				'overlayColor': '#fff',
				'transitionIn': 'none',
				'transitionOut': 'none',
				'titlePosition': 'inside',
				'centerOnScroll': true,
				'maxWidth': 650,
				'minHeight': 310

				});
			  
		  });
		})(jQuery);
	</script>
	 </div>    
		</section>
		<footer>
			<div class="footer-main">
				<div class="footer">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-8">
								<?/*$APPLICATION->IncludeComponent("it-invest:menu", "siberia_bottom_vertical_section_menu", array(
										"ROOT_MENU_TYPE" => "bottom".SLANGCODE,
										"MAX_LEVEL" => "1",
										"MENU_CACHE_TYPE" => "A",
										"CACHE_SELECTED_ITEMS" => "N",
										"MENU_CACHE_TIME" => "36000000",
										"MENU_CACHE_USE_GROUPS" => "Y",
										"MENU_CACHE_GET_VARS" => array(
										),
									),
									false
								);*/?>
								<div class="our_app">
									<a target="_blank" href="https://itunes.apple.com/ru/app/siberia-products/id1239805943?mt=8"><img src="<?=SITE_TEMPLATE_PATH?>/images/static/app_store.png"/></a>
									<a target="_blank" href="https://play.google.com/store/apps/details?id=com.siberiaproducts.app&hl=ru"><img src="<?=SITE_TEMPLATE_PATH?>/images/static/google_play.png"/></a>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-ls-4 footer_contact">
								<!--<div class="qr-f pull-right">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/static/qr.png" alt="qr">
									<span>Android</span>
								</div>-->
									<div style="margin: 2.6rem 0 0 0;font-size:1.6em">
										<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => "/include/footer_contact.php",
											"EDIT_TEMPLATE" => ""
										),
										false
										);?>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>   
			<div class="footer-line">
				<div class="footer">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-ls-4">
								<span class="footer-copyright"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => "/include/copyright.php",
								"EDIT_TEMPLATE" => ""
								),
								false
								);?></span>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-ls-8">					
								<?$APPLICATION->IncludeComponent("it-invest:menu", "siberia_bottom_horizontal_menu", array(
										"ROOT_MENU_TYPE" => "bottom".SLANGCODE,
										"MAX_LEVEL" => "1",
										"MENU_CACHE_TYPE" => "A",
										"CACHE_SELECTED_ITEMS" => "N",
										"MENU_CACHE_TIME" => "36000000",
										"MENU_CACHE_USE_GROUPS" => "Y",
										"MENU_CACHE_GET_VARS" => array(
										),
									),
									false
								);?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="popupform">
				<?$APPLICATION->IncludeComponent(
					"it-invest:main.feedback", 
					"siberia", 
					array(
						"AJAX_MODE" => "Y",
						"USE_CAPTCHA" => "N",
						"OK_TEXT" => Loc::getMessage('CSST_TEMPLATE_FEEDBACK_TEXT'),
						"EMAIL_TO" => "info@siberia-limited.com",
						"REQUIRED_FIELDS" => array(
							0 => "NAME",
							1 => "EMAIL",
							2 => "MESSAGE",
						),
						"EVENT_MESSAGE_ID" => array(
						),
						"COMPONENT_TEMPLATE" => "siberia"
					),
					false
				);?>
			</div>
			<div id="window">	
			</div>
			<!-- Yandex.Metrika counter -->
			<script type="text/javascript">
				(function (d, w, c) {
					(w[c] = w[c] || []).push(function() {
						try {
							w.yaCounter41390384 = new Ya.Metrika({
								id:41390384,
								clickmap:true,
								trackLinks:true,
								accurateTrackBounce:true
							});
						} catch(e) { }
					});

					var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
					s.type = "text/javascript";
					s.async = true;
					s.src = "https://mc.yandex.ru/metrika/watch.js";

					if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
					} else { f(); }
				})(document, window, "yandex_metrika_callbacks");
			</script>
			<noscript><div><img src="https://mc.yandex.ru/watch/41390384" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
			<!-- /Yandex.Metrika counter -->			
		</footer>		
		<div class="right-bar">
				<?$APPLICATION->IncludeComponent("it-invest:sale.basket.basket.line", "right_bn", array(
							"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
							"PATH_TO_PERSONAL" => SITE_DIR."personal/",
							"SHOW_PERSONAL_LINK" => "N",
							"SHOW_NUM_PRODUCTS" => "Y",
							"SHOW_TOTAL_PRICE" => "Y",
							"SHOW_PRODUCTS" => "N",
							"POSITION_FIXED" =>"N",
							"SHOW_AUTHOR" => "Y",
							"PATH_TO_REGISTER" => SITE_DIR."login/",
							"PATH_TO_PROFILE" => SITE_DIR."personal/"
						),
						false,
						array()
					);?>
		
			<div class="widget-links-middle">
				<div class="table-f">
					<div class="cell-f">
						<ul>
							<!--<li><a href="/"><img src="<?//=SITE_TEMPLATE_PATH?>/images/static/call.png" alt="img"></a></li>-->
							<li>
							<?$APPLICATION->IncludeComponent("it-invest:sale.basket.basket.line", "right_delay_y", array(
							"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
							"PATH_TO_PERSONAL" => SITE_DIR."personal/",
							"SHOW_PERSONAL_LINK" => "N",
							"SHOW_NUM_PRODUCTS" => "Y",
							"SHOW_TOTAL_PRICE" => "Y",
							"SHOW_PRODUCTS" => "Y",
							"POSITION_FIXED" =>"N",
							"SHOW_AUTHOR" => "Y",
							"SHOW_DELAY" => "Y",
							"PATH_TO_REGISTER" => SITE_DIR."login/",
							"PATH_TO_PROFILE" => SITE_DIR."personal/"
						),
						false,
						array()
					);?>
							</li>
							<li><a href="#popupform" id="popupbutton_right"><img style="right: -4px;" src="<?=SITE_TEMPLATE_PATH?>/images/static/chat.png" alt="img"></a></li>
							<!--<li><a href="/"><img src="<?//=SITE_TEMPLATE_PATH?>/images/static/something.png" alt="img"></a></li>-->
						</ul>
					</div>
				</div>
				<div class="qr-right">
					<!--<div class="qr-right-ins">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/static/right-qr.png" alt="right-qr">
						<span>Android</span>
					</div>-->
				</div>
			</div>
			<div class="widget-links-bottom">
				<span class="up-arrow"></span><br/>
				<span><?=Loc::getMessage("FOOTER_UP_BUTTON")?></span>
			</div>
		</div>	
		<div class="top-btn"><div class="top-btn-inner"></div></div>
	</body>
	<script data-skip-moving="true">
        (function(w,d,u,b){
                s=d.createElement('script');r=(Date.now()/1000|0);s.async=1;s.src=u+'?'+r;
                h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b1851199/crm/site_button/loader_2_xwjya9.js');
</script>
</html>
