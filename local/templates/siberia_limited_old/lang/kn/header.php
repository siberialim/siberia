<?
$MESS["SEARCH_GOODS"] = "예 : 비스킷, 사탕";
$MESS["SEARCH_OTHER"] = "Other";
$MESS["FOOTER_COMPANY_ABOUT"] = "About Us";
$MESS["FOOTER_COMPANY_PHONE"] = "Contact Us";
$MESS["FOOTER_UP_BUTTON"] = "위";
$MESS["HEADER_MENU"] = "메뉴";
$MESS["CONTACT_US"] = "이메일을 보내기";
$MESS["CATALOG_BREADCRUMBS"] = "카탈로그";
$MESS["HOME_BREADCRUMBS"] = "메인 페이즈";

$MESS["LANGUAGE"] = "언어";

$MESS["LANGUAGE_KN"] = "한국어";
$MESS["LANGUAGE_EN"] = "영어";
$MESS["LANGUAGE_CH"] = "중국어";
$MESS["LANGUAGE_VI"] = "베트남어";
?>