<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
//путь до текущей страницы относительно корня
$curPage = $APPLICATION->GetCurPage(true);

CModule::IncludeModule('iblock');
//фильтер
$arFilter = array(		
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => IBLOCK_CATALOG_ID,
	'GLOBAL_ACTIVE'=>'Y',
);

$cache = new CPHPCache();

if(!isset($arParams["CACHE_TIME"])){
	$arParams["CACHE_TIME"] = 36000000;
}
$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'catalog.custom.section.list.'.IBLOCK_CATALOG_ID;
$cache_path = 'catalog.custom.section.list/'.IBLOCK_CATALOG_ID;

	//isset cache?
	if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
	{
	   $res = $cache->GetVars();
	   if (is_array($res["catalog_custom_section_list"]) && (count($res["catalog_custom_section_list"]) > 0))
		  $arResult['ROOT'] = $res["catalog_custom_section_list"];
	}
	//!cache
	if (!is_array($res["catalog_custom_section_list"]))
	{	
		//селект
		$arSelect = array('IBLOCK_ID','ID','NAME','DESCRIPTION','PICTURE','IBLOCK_SECTION_ID', 'SECTION_PAGE_URL', "UF_COLOR","UF_ICON", "UF_BACKGROUND_IMAGE");
		//сорт
		$arOrder = array('SORT'=>'ASC', 'ID'=>'ASC');
		//получили разделы
		$rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
		$sectionLinc = array();
		//строим древовидный массив разделов
		$arResult['ROOT'] = array();
		$sectionLinc[0] = &$arResult['ROOT'];
		while($arSection = $rsSections->GetNext()) {
			if ($arSection['UF_ICON'][0])
			{
				$file = CFile::ResizeImageGet($arSection['UF_ICON'][0], array('width'=>17, 'height'=>22), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$arSection['UF_ICON'] = $file;
			}	
			$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']] = $arSection;
			$sectionLinc[$arSection['ID']] = &$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']];
		}
		unset($sectionLinc);
		
		/////////// end cache /////////
		if ($cache_time > 0)
		{
			$cache->StartDataCache($cache_time, $cache_id, $cache_path);
			$cache->EndDataCache(array("catalog_custom_section_list"));
		}
	}
// Подключим языковой файл
// Предполагаем, что языковой файл расположен стандартным образом
IncludeTemplateLangFile(__FILE__, SLANGCODE);

?>
<!DOCTYPE html>
<html xml:lang="<?=SLANGCODE?>" lang="<?=SLANGCODE?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">

		<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/images/static/favicon.ico" />
		
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.js"></script>

		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH ."/js/jquery-contained-sticky-scroll.js"?>"></script>

		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH ."/js/bootstrap.js"?>"></script>
		<link type="text/css" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/bootstrap.css" />

		
		<?
		// для js-файлов
		CJSCore::Init(array('jquery'));
		$APPLICATION->ShowHead();
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH ."/js/jquery-contained-sticky-scroll.js");	
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH ."/js/main.js");	
		?>
		<title><?=$APPLICATION->ShowTitle()?></title>
	</head>
	<script type="text/javascript">
		(function($){
		  $(document).ready(
			  function() { 
				$("a.gallery").fancybox({});
				
				//$("#currences").selectmenu();
				
				$('#popupbutton').fancybox({
					'padding': 37,
					'overlayOpacity': 0.87,
					'overlayColor': '#fff',
					'transitionIn': 'none',
					'transitionOut': 'none',
					'titlePosition': 'inside',
					'centerOnScroll': true,
					'width': 400,
					'minHeight': 310
				});
				
				$('#popupbutton_right').fancybox({
					'padding': 37,
					'overlayOpacity': 0.87,
					'overlayColor': '#fff',
					'transitionIn': 'none',
					'transitionOut': 'none',
					'titlePosition': 'inside',
					'centerOnScroll': true,
					'width': 400,
					'minHeight': 310
				});
			  
		  });
		})(jQuery);
	</script>
	<body class="bx-background-image bx-theme-<?=$theme?>">
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="top-bar navbar-fixed-top">
		<?$APPLICATION->IncludeComponent(
			"it-invest:sale.basket.basket.line", 
			"siberia", 
			array(
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
				"PATH_TO_PERSONAL" => SITE_DIR."personal/",
				"SHOW_PERSONAL_LINK" => "N",
				"SHOW_NUM_PRODUCTS" => "Y",
				"SHOW_TOTAL_PRICE" => "Y",
				"SHOW_PRODUCTS" => "N",
				"POSITION_FIXED" => "N",
				"SHOW_AUTHOR" => "Y",
				"PATH_TO_REGISTER" => SITE_DIR."login",
				"PATH_TO_PROFILE" => SITE_DIR."personal/",
				"COMPONENT_TEMPLATE" => "siberia",
				"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
				"SHOW_EMPTY_VALUES" => "Y",
				"HIDE_ON_BASKET_PAGES" => "Y"
			),
			false
		);?>
	</div>
	<div id="main_loader" class="box_loader" style="display:none"><div class="loader">Loading...</div></div>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <div class="logo-b">
                        <a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=SITE_DIR?>" class="logo-link"><img src="<?=SITE_TEMPLATE_PATH?>/images/static/logo.png" alt="logo"></a>
						<span><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => "/include/head_disc.php",
						"EDIT_TEMPLATE" => ""
						),
						false
						);?></span>						
                    </div>
                </div>
                <div class="col-xs-6 no-indent">
					<div class="search-form-wrp">					
						<?$APPLICATION->IncludeComponent(
							"it-invest:search.title", 
							"siberia", 
							array(
								"NUM_CATEGORIES" => "5",
								"TOP_COUNT" => "5",
								"CHECK_DATES" => "N",
								"SHOW_OTHERS" => "N",
								"PAGE" => SITE_DIR."catalog/",
								"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
								"CATEGORY_0" => array(
									0 => "iblock_catalog",
								),
								"CATEGORY_0_iblock_catalog" => array(
									0 => "all",
								),
								"CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
								"SHOW_INPUT" => "Y",
								"INPUT_ID" => "title-search-input",
								"CONTAINER_ID" => "search",
								"PRICE_CODE" => array(
									0 => "BASE",
								),
								"SHOW_PRICES" => "N",
								"SHOW_PREVIEW" => "Y",
								"PREVIEW_WIDTH" => "75",
								"PREVIEW_HEIGHT" => "75",
								"CONVERT_CURRENCY" => "Y",
								"COMPONENT_TEMPLATE" => "siberia",
								"ORDER" => "date",
								"USE_LANGUAGE_GUESS" => "Y",
								"PRICE_VAT_INCLUDE" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"CURRENCY_ID" => CURRENCY_ID
							),
							false
						);?>				
						<span class="search-form-sub-text"><?=GetMessage("SEARCH_GOODS")?></span>
					</div>
                </div>
			</div>
            <div class="menu-wrp">
				<div class="primary-menu <?if($curPage != '/index.php' && $curPage != '/knr/index.php'){echo 'contracted';}?>">
					<div class="primary-menu__trigger">
						<i class="menu-ico"></i>
						<span><?=GetMessage("HEADER_MENU")?></span>
					</div>
					<div class="primary-menu__cont">
						<ul>
							<?foreach($arResult['ROOT']['CHILD'] as $arSection){?>
							<li class="dropdown-submenu <?if(isset($arSection['CHILD']) && count($arSection['CHILD']) > 0){echo ' isset_child';};?>">
								<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> tabindex="-1" href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
								<?if(isset($arSection['CHILD']) && count($arSection['CHILD']) > 0){?>
								<ul class="dropdown-menu">
									<?foreach($arSection['CHILD'] as $arSectionChild){?>
									<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arSectionChild['SECTION_PAGE_URL']?>"><?=$arSectionChild['NAME']?></a></li>
									<?}?>
								</ul>
								<?}?>
							</li>
							<?}?>
						</ul>
					</div>
				</div>
				<?$APPLICATION->IncludeComponent("it-invest:menu", "siberia_top_horizontal_menu", array(
						"ROOT_MENU_TYPE" => "bottom".SLANGCODE,
						"MAX_LEVEL" => "1",
						"MENU_CACHE_TYPE" => "A",
						"CACHE_SELECTED_ITEMS" => "N",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => array(
						),
					),
					false
				);?>
				<a href="#popupform" id="popupbutton"><?=GetMessage('CONTACT_US')?></a>
            </div>
        </div>
    </header>
    <section class="main">
        <div class="container output">