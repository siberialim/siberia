<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//use from D7
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);

IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
//путь до текущей страницы относительно корня
$curPage = $APPLICATION->GetCurPage(true);

CModule::IncludeModule('iblock');

CModule::IncludeModule('itinvest.catalog');
use \Itinvest\Catalog\Iblock;

//фильтер
$arFilter = array(		
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => IBLOCK_CATALOG_ID,
	'GLOBAL_ACTIVE'=>'Y',
);

$cache = new CPHPCache();

if (!isset($arParams["CACHE_TIME"])){
	$arParams["CACHE_TIME"] = 36000000;
}

$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'catalog.custom.section.list.'.IBLOCK_CATALOG_ID;
$cache_path = 'catalog.custom.section.list/'.IBLOCK_CATALOG_ID;

//isset cache?
if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)) {
   $res = $cache->GetVars();
   if (is_array($res["catalog_custom_section_list"]) && (count($res["catalog_custom_section_list"]) > 0))
	  $arResult['ROOT'] = $res["catalog_custom_section_list"];
}
//!cache
if (!is_array($res["catalog_custom_section_list"])) {	
	//селект
	$arSelect = array('IBLOCK_ID','ID','NAME','DESCRIPTION','PICTURE','IBLOCK_SECTION_ID', 'SECTION_PAGE_URL', "UF_COLOR","UF_ICON", "UF_BACKGROUND_IMAGE");
	//сорт
	$arOrder = array('SORT'=>'ASC', 'ID'=>'ASC');
	//получили разделы
	$rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
	$sectionLinc = array();
	//строим древовидный массив разделов
	$arResult['ROOT'] = array();
	$sectionLinc[0] = &$arResult['ROOT'];
	while ($arSection = $rsSections->GetNext()) {
		if ($arSection['UF_ICON'][0]){
			$file = CFile::ResizeImageGet($arSection['UF_ICON'][0], array('width'=>17, 'height'=>22), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
			$arSection['UF_ICON'] = $file;
		}	
		$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']] = $arSection;
		$sectionLinc[$arSection['ID']] = &$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']];
	}
	unset($sectionLinc);
	
	/////////// end cache /////////
	if ($cache_time > 0) {
		$cache->StartDataCache($cache_time, $cache_id, $cache_path);
		$cache->EndDataCache(array("catalog_custom_section_list"));
	}
}
// Подключим языковой файл
// Предполагаем, что языковой файл расположен стандартным образом
IncludeTemplateLangFile(__FILE__, SLANGCODE);

?>
<!DOCTYPE html>
<html xml:lang="<?=SLANGCODE?>" lang="<?=SLANGCODE?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?=$APPLICATION->ShowMeta("keywords")?>
		<?=$APPLICATION->ShowMeta("description")?>

		<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/images/static/favicon.ico" />
		
		<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.js"></script>

		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.js"></script>
		<link type="text/css" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/bootstrap.css" />

		<?
		// для js-файлов
		CJSCore::Init(array('jquery'));
		$APPLICATION->ShowHead();
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH ."/js/jquery-contained-sticky-scroll.js");	
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH ."/js/main.js");	
		?>
		<title><?=$APPLICATION->ShowTitle()?></title>
	</head>
	<script type="text/javascript">
		(function($){
		  $(document).ready(
			  function() { 
				$("a.gallery").fancybox({});
				
				//$("#currences").selectmenu();
				
				$('#popupbutton').fancybox({
					'padding': 37,
					'overlayOpacity': 0.87,
					'overlayColor': '#fff',
					'transitionIn': 'none',
					'transitionOut': 'none',
					'titlePosition': 'inside',
					'centerOnScroll': true,
					'width': 400,
					'minHeight': 310
				});
				
				$('#popupbutton_right').fancybox({
					'padding': 37,
					'overlayOpacity': 0.87,
					'overlayColor': '#fff',
					'transitionIn': 'none',
					'transitionOut': 'none',
					'titlePosition': 'inside',
					'centerOnScroll': true,
					'width': 400,
					'minHeight': 310
				});
			  
		  });
		})(jQuery);
	</script>
	<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="top-bar navbar-fixed-top">
		<?$APPLICATION->IncludeComponent(
			"it-invest:sale.basket.basket.line", 
			"siberia", 
			array(
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
				"PATH_TO_PERSONAL" => SITE_DIR."personal/private/",
				"SHOW_PERSONAL_LINK" => "N",
				"SHOW_NUM_PRODUCTS" => "Y",
				"SHOW_TOTAL_PRICE" => "Y",
				"SHOW_PRODUCTS" => "N",
				"POSITION_FIXED" => "N",
				"SHOW_AUTHOR" => "Y",
				"PATH_TO_REGISTER" => SITE_DIR."login",
				"PATH_TO_PROFILE" => SITE_DIR."personal/private/",
				"COMPONENT_TEMPLATE" => "siberia",
				"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
				"SHOW_EMPTY_VALUES" => "Y",
				"HIDE_ON_BASKET_PAGES" => "Y"
			),
			false
		);?>
	</div>
	<div class="mobile-top-bar navbar-fixed-top">
		<?$APPLICATION->IncludeComponent(
			"it-invest:sale.basket.basket.line", 
			"mobile", 
			array(
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
				"PATH_TO_PERSONAL" => SITE_DIR."personal/private/",
				"SHOW_PERSONAL_LINK" => "N",
				"SHOW_NUM_PRODUCTS" => "Y",
				"SHOW_TOTAL_PRICE" => "Y",
				"SHOW_PRODUCTS" => "N",
				"POSITION_FIXED" => "N",
				"SHOW_AUTHOR" => "Y",
				"PATH_TO_REGISTER" => SITE_DIR."login",
				"PATH_TO_PROFILE" => SITE_DIR."personal/private/",
				"COMPONENT_TEMPLATE" => "siberia",
				"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
				"SHOW_EMPTY_VALUES" => "Y",
				"HIDE_ON_BASKET_PAGES" => "Y"
			),
			false
		);?>
	</div>
	<div id="main_loader" class="box_loader" style="display:none"><div class="loader"></div></div>
    <header>
		<div class="header">
			<div class="container">
				<div class="row" id="top-bn-wrp">
					<div class="col-xs-12 col-sm-6 col-md-6 col-ls-6">
						<div class="logo-b">
							<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=SITE_DIR?>" class="logo-link"><img src="<?=SITE_TEMPLATE_PATH?>/images/static/logo.png" alt="logo"></a>
							<span id="main_slogan"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include/head_disc.php",
							"EDIT_TEMPLATE" => ""
							),
							false
							);?></span>						
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-ls-6 no-indent">
						<div class="search-form-wrp">					
							<?$APPLICATION->IncludeComponent(
								"it-invest:search.title", 
								"siberia", 
								array(
									"NUM_CATEGORIES" => "5",
									"TOP_COUNT" => "5",
									"CHECK_DATES" => "N",
									"SHOW_OTHERS" => "N",
									"PAGE" => SITE_DIR."catalog/",
									"CATEGORY_0_TITLE" => Loc::getMessage("SEARCH_GOODS"),
									"CATEGORY_0" => array(
										0 => "iblock_catalog",
									),
									"CATEGORY_0_iblock_catalog" => array(
										0 => IBLOCK_CATALOG_ID,
									),
									"CATEGORY_OTHERS_TITLE" => Loc::getMessage("SEARCH_OTHER"),
									"SHOW_INPUT" => "Y",
									"INPUT_ID" => "title-search-input",
									"CONTAINER_ID" => "search",
									"PRICE_CODE" => array(
										0 => "BASE",
									),
									"SHOW_PRICES" => "N",
									"SHOW_PREVIEW" => "Y",
									"PREVIEW_WIDTH" => "75",
									"PREVIEW_HEIGHT" => "75",
									"CONVERT_CURRENCY" => "Y",
									"COMPONENT_TEMPLATE" => "siberia",
									"ORDER" => "date",
									"USE_LANGUAGE_GUESS" => "Y",
									"PRICE_VAT_INCLUDE" => "Y",
									"PREVIEW_TRUNCATE_LEN" => "",
									"CURRENCY_ID" => CURRENCY_ID
								),
								false
							);?>				
							<span class="search-form-sub-text"><?=Loc::getMessage("SEARCH_GOODS")?></span>
						</div>
					</div>
				</div>
				<div class="row" id="main_menu">
					<div class="menu-wrp">
						<div class="primary-menu <?if($curPage != '/index.php'){echo 'contracted';}?>">
							<div class="primary-menu__trigger">
								<i class="menu-ico"></i>
								<span><?=Loc::getMessage("HEADER_MENU")?></span>
							</div>
							<div style="position:relative;
										top: 48.8rem;">
								<div class="primary-menu__cont">
									<ul>
										<?
											$n_section = count($arResult['ROOT']['CHILD']);
											$c_menu = $n_section / 10;
											$c_section = 0;
											
											$arrSections = $arResult['ROOT']['CHILD'];
											
											foreach($arrSections as $key=>$arSection)
											{
												if($c_section > 8) break;
										?>
												<li class="dropdown-submenu <?if(isset($arSection['CHILD']) && count($arSection['CHILD']) > 0){echo ' isset_child';};?>">
													<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> tabindex="-1" href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
													<?
														if(isset($arSection['CHILD']) && count($arSection['CHILD']) > 0)
														{
													?>
														<ul class="dropdown-menu">
															<?
																foreach($arSection['CHILD'] as $arSectionChild)
																{
															?>
																	<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arSectionChild['SECTION_PAGE_URL']?>"><?=$arSectionChild['NAME']?></a></li>
															<?
																}
															?>
														</ul>
													<?
														}
													?>
												</li>
										<?	
												$c_section+=1;
												unset($arrSections[$key]);
												
											}
											
											if($n_section > 9)
											{
										?>
												<li class="dropdown-submenu css-show-hidden-section"><a tabindex="-1" href="javascript:void(0)">Etc.</a></li>
										<?
											}
										?>
										
									</ul>
								</div>
								<?
									$i = 0;
									for($i; $i < $c_menu - 1; $i++)
									{
										$output = 17.6 * ($i + 1);
								?>
										<div class="primary-menu__cont secondary" style="left:<?=$output?>rem">
											<ul>
												<?
													$c_section = 0;
													$c_section_static = 1;
													
													foreach($arrSections as $arSection)
													{
														if($c_section > 10 * $i - 1 && $c_section < 10 * $i +10)
														{
												?>
															<li class="dropdown-submenu <?if(isset($arSection['CHILD']) && count($arSection['CHILD']) > 0){echo ' isset_child';};?>">
																<a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> tabindex="-1" href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
																<?
																	if(isset($arSection['CHILD']) && count($arSection['CHILD']) > 0){?>
																		<ul class="dropdown-menu">
																			<?
																				foreach($arSection['CHILD'] as $arSectionChild)
																				{
																			?>
																					<li><a <?if(SLANGCODE == 'ch'){echo 'target="_blank"';}?> href="<?=$arSectionChild['SECTION_PAGE_URL']?>"><?=$arSectionChild['NAME']?></a></li>
																			<?
																				}
																			?>
																		</ul>
																<?
																	}
																?>
															</li>
												<?		}
														$c_section+=1;
													}
												?>						
											</ul>
										</div>
								<?
									}
								?>
							</div>					
						</div><!--
						--><div class="main_top_menu">
							<?				
								$APPLICATION->IncludeComponent("it-invest:menu", "siberia_top_horizontal_menu", array(
									"ROOT_MENU_TYPE" => "top".SLANGCODE,
									"MAX_LEVEL" => "1",
									"MENU_CACHE_TYPE" => "A",
									"CACHE_SELECTED_ITEMS" => "N",
									"MENU_CACHE_TIME" => "36000000",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(
									),
								),
								false
							);?>
							<a href="#popupform" id="popupbutton"><?=Loc::getMessage('CONTACT_US')?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
    </header>
	<div id="mobile_left_menu">
		<?$APPLICATION->IncludeComponent("it-invest:menu", "mobile", array(
			"ROOT_MENU_TYPE" => "bottom".SLANGCODE,
			"MAX_LEVEL" => "1",
			"MENU_CACHE_TYPE" => "A",
			"CACHE_SELECTED_ITEMS" => "N",
			"MENU_CACHE_TIME" => "36000000",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(
			),
			),
			false
		);?>
	</div>
	<div id="mobile_search_box">
		<?$APPLICATION->IncludeComponent(
			"it-invest:search.title", 
			"siberia_mobile", 
			array(
				"NUM_CATEGORIES" => "3",
				"TOP_COUNT" => "15",
				"CHECK_DATES" => "N",
				"SHOW_OTHERS" => "N",
				"PAGE" => SITE_DIR."catalog/",
				"CATEGORY_0_TITLE" => Loc::getMessage("SEARCH_GOODS"),
				"CATEGORY_0" => array(
					0 => "iblock_catalog",
				),
				"CATEGORY_0_iblock_catalog" => array(
					0 => IBLOCK_CATALOG_ID,
				),
				"CATEGORY_OTHERS_TITLE" => Loc::getMessage("SEARCH_OTHER"),
				"SHOW_INPUT" => "Y",
				"INPUT_ID" => "title-search-input-mobile",
				"CONTAINER_ID" => "title-search-mobile",
				"PRICE_CODE" => array(
					0 => "BASE",
				),
				"SHOW_PRICES" => "N",
				"SHOW_PREVIEW" => "Y",
				"PREVIEW_WIDTH" => "110",
				"PREVIEW_HEIGHT" => "110",
				"CONVERT_CURRENCY" => "Y",
				"COMPONENT_TEMPLATE" => "siberia_mobile",
				"ORDER" => "date",
				"USE_LANGUAGE_GUESS" => "Y",
				"PRICE_VAT_INCLUDE" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"CURRENCY_ID" => CURRENCY_ID
			),
			false
		);?>
	</div>
	<div id="mobile_language_select">
		<ul class="mobile_list_language">
			<li><button class="cross js-click-openSelectLanguage">×</button></li>
			<li><a href="/location.php?code_location=en" <?if(SLANGCODE == 'en'){echo 'class="active"';}?>><img src="/local/templates/siberia_limited/images/flags/en.jpg" alt="flag"><?=Loc::getMessage("LANGUAGE_EN")?></a></li>
			<li><a href="/location.php?code_location=kn" <?if(SLANGCODE == 'kn'){echo 'class="active"';}?>><img src="/local/templates/siberia_limited/images/flags/kn.jpg" alt="flag"><?=Loc::getMessage("LANGUAGE_KN")?></a></li>
			<li><a href="/location.php?code_location=ch" <?if(SLANGCODE == 'ch'){echo 'class="active"';}?>><img src="/local/templates/siberia_limited/images/flags/ch.jpg" alt="flag"><?=Loc::getMessage("LANGUAGE_CH")?></a></li>
		</ul>
	</div>
	<div id="mobile_currency_select">
	</div>
    <section class="main">
        <div class="container output">