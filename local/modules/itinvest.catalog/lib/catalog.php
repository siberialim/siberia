<?
namespace Itinvest\Catalog;

class Iblock
{
    public static function getListSection()
    {
		$arSelect = array('IBLOCK_ID','ID','NAME','DESCRIPTION','PICTURE','IBLOCK_SECTION_ID', 'SECTION_PAGE_URL', "UF_COLOR","UF_ICON", "UF_BACKGROUND_IMAGE");
		//сорт
		$arOrder = array('SORT'=>'ASC', 'ID'=>'ASC');
		//получили разделы
		$rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
		$sectionLinc = array();
		//строим древовидный массив разделов
		$arResult['ROOT'] = array();
		$sectionLinc[0] = &$arResult['ROOT'];
		while ($arSection = $rsSections->GetNext()) {
		if ($arSection['UF_ICON'][0]){
		$file = CFile::ResizeImageGet($arSection['UF_ICON'][0], array('width'=>17, 'height'=>22), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
		$arSection['UF_ICON'] = $file;
		}	
		$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']] = $arSection;
		$sectionLinc[$arSection['ID']] = &$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']];
		}
		return $arSection;
    }
}