<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);
?>
<div class="row">
	<?$APPLICATION->IncludeComponent("it-invest:breadcrumb","siberia",Array(
			"START_FROM" => "0", 
			"PATH" => "", 
			"SITE_ID" => SITE_ID 
		)
	);?>
	<div class="maxwIn">
		<div class="catSec">
			<div id="main" class="clearfix">
				<div class="page-maincontent">
					<div class="bx-sidebar-block">

							<?/*$APPLICATION->IncludeComponent(
								"it-invest:catalog.smart.filter", 
								"siberia_section", 
								array(
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								//"SECTION_ID" => $arCurSection['ID'],
								"FILTER_NAME" => $arParams["FILTER_NAME"],
								"PRICE_CODE" => $arParams["PRICE_CODE"],
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"SAVE_IN_SESSION" => "N",
								"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
								"XML_EXPORT" => "Y",
								"SECTION_TITLE" => "NAME",
								"SECTION_DESCRIPTION" => "DESCRIPTION",
								'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
								//"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
								'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
								'CURRENCY_ID' => $arParams['CURRENCY_ID'],
								"SEF_MODE" => $arParams["SEF_MODE"],
							//	"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
							//"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
								"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
								"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
								),
								$component,
								array('HIDE_ICONS' => 'Y')
							);*/?>
					</div>
					<?
					$APPLICATION->IncludeComponent(
						"it-invest:catalog.sort",
						"siberia_section",
						Array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $arCurSection["ID"],
							"FILTER_NAME" => "arrFilter",
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_NOTES" => "",
							"CACHE_GROUPS" => "Y",
							"SAVE_IN_SESSION" => "N"
						),
						false
					);
					$flow = $APPLICATION->get_cookie("flow");
		
					if(($_REQUEST['flow'] == 2 || $flow == 2) && ($_REQUEST['flow'] != 1)){
						//$template_catalog = 'siberia_category_list';
						$template_catalog = 'siberia_category_grid';
					}
					else{
						$template_catalog = 'siberia_category_grid';
					}
					$APPLICATION->IncludeComponent(
						"it-invest:catalog.custom.section",
						$template_catalog,
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"AR_SELECT" => array('ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'CATALOG_GROUP_1', 'CATALOG_QUANTITY', 'PROPERTY_RECOMMEND'),
							"AR_SORT" => array('SORT' => 'ASC'),
							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"FILTER_NAME" => "arrFilter",
							
							"PAGE_ELEMENT_COUNT" => 100,
							"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
							
							"SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"SECTION_COLOR" => $section['UF_COLOR'],
							"SEF_MODE" => $arParams["SEF_MODE"],
							"AJAX_MODE" => $arParams["AJAX_MODE"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => 0,
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_FILTER" => $arParams["CACHE_FILTER"],
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"SHOW_PRICES" => $arParams["SHOW_PRICES"],
							"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
							"CURRENCY_ID" => $arParams["CURRENCY_ID"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams['USE_PRODUCT_QUANTITY'],
							"ADD_PROPERTIES_TO_BASKET" => "Y",
							"PRODUCT_PROPS_VARIABLE" => "prop",
							"PARTIAL_PRODUCT_PROPERTIES" => "N",
							"PRODUCT_PROPERTIES" => array(
							),
							"ADD_TO_BASKET_ACTION" => "ADD",
							"PAGER_TEMPLATE" => "",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "Y",

							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_BASE_LINK_ENABLE" => "N",
						),
						$component
					);?>
				</div>
			</div>
		</div>
<?
$recommendet_elements = GarbageStorage::get('ids');
?>
		<div class="catRec">   
<?		
			if (count($recommendet_elements) > 0){
				$APPLICATION->IncludeComponent("it-invest:catalog.custom.recommended.products", 
					"siberia_vertical", array(
						"LINE_ELEMENT_COUNT" => 3,
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						'CONVERT_CURRENCY' => 'Y',
						'CURRENCY_ID' => $arParams["CURRENCY_ID"],
						"IDS" => $recommendet_elements,
						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"SHOW_PRICES" => $arParams["SHOW_PRICES"],
						'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
						"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
						"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
						"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"RESIZE_WIDTH" => 149,
						"RESIZE_HEIGHT" => 106,
						"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
						"PRODUCT_QUANTITY_VARIABLE" => $arParams['USE_PRODUCT_QUANTITY'],
					),
					$component,
					array("HIDE_ICONS" => "Y")
				);
			}
?>
		</div>
		<div class="clear"></div>
	</div>
</div>
