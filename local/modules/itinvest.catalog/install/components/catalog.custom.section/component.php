<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Currency,
	Bitrix\Catalog,
	Bitrix\Iblock;

if(isset($arParams['AR_SECTION']['CODE']) && !empty($arParams['AR_SECTION']['CODE'])){
	$section = $arParams['AR_SECTION']['CODE'];
}
//else{die();}
$cache = new CPHPCache();
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'catalog.custom.section.'.IBLOCK_CATALOG_ID.'.'.$section;
$cache_path = 'catalog.custom.section/'.IBLOCK_CATALOG_ID.'/'.$section;
if (CModule::includeModule('currency')){
	$currency = Currency\CurrencyTable::getList(array(
		'select' => array('CURRENCY'),
		'filter' => array('=CURRENCY' => $arParams['CURRENCY_ID'])
	))->fetch();
}
else{die();}

$arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];

	//isset cache?
	if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
	{		
	   $res = $cache->GetVars();
	   if (is_array($res[$cache_id]) && (count($res[$cache_id]) > 0))
		  $arResult = $res[$cache_id];
	  		
	}
	//!cache
	if (!is_array($res[$cache_id]))
	{
	//	$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => $arParams['SECTION_ID'], 'ACTIVE' => 'Y'); 
	
		if($arParams["SECTION_ID"] > 0)
		{
			$arFilter["ID"]=$arParams["SECTION_ID"];
			$arFilter["=IBLOCK_ID"]=$arParams["IBLOCK_ID"];
			$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arResult['SECTION'] = $rsSection->GetNext();
			if($arResult['SECTION'])
				$bSectionFound = true;
		}
		elseif(strlen($arParams["SECTION_CODE"]) > 0)
		{
			$arFilter["=CODE"]=$arParams["SECTION_CODE"];
			$arFilter["=IBLOCK_ID"]=$arParams["IBLOCK_ID"];
			$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arResult['SECTION'] = $rsSection->GetNext();

			if($arResult['SECTION'])
				$bSectionFound = true;
		}
		
		$arSort = $arParams['AR_SORT'];

		if($_REQUEST['sort'] && $_REQUEST['type']){
			 $arSort[$_REQUEST['sort']] = $_REQUEST['type'];
		}
		$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => $arResult['SECTION']['ID'], 'ACTIVE' => 'Y'); 
		$res = CIBlockElement::GetList($arSort, $arFilter, false,  array("nPageSize" => $arParams['PAGE_ELEMENT_COUNT'], "bShowAll" => false), $arParams['AR_SELECT']);
		$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);

		$currentPath = CHTTP::urlDeleteParams(
			$arParams['CUSTOM_CURRENT_PAGE']?: $APPLICATION->GetCurPageParam(),
			array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
			array('delete_system_params' => true)
		);

		$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');
		if ($arParams['COMPARE_PATH'] == '')
		{
			$comparePath = $currentPath;
		}
		else
		{
			$comparePath = CHTTP::urlDeleteParams(
			$arParams['COMPARE_PATH'],
			array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
			array('delete_system_params' => true)
			);
			$comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');
		}
		/*===============================================================================================================
			ajax уходит в component.php компонента catalog.custom.element
		===================================================================================================================*/
		$ajaxPath = '/catalog/section/element/index.php?';
		/*======================================*/
		$arUrlTemplates['~BUY_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arUrlTemplates['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~BUY_URL_TEMPLATE']);
		$arUrlTemplates['~ADD_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arUrlTemplates['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~ADD_URL_TEMPLATE']);
		$arUrlTemplates['~FAVOURITE_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2FAVOURITE&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arUrlTemplates['FAVOURITE_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~FAVOURITE_URL_TEMPLATE']);

		$arResult = array_merge($arResult, $arUrlTemplates);
		while($ob = $res->GetNextElement()) 
		{
			if ($ob->fields['PREVIEW_PICTURE'])
			{
				$file = CFile::ResizeImageGet($ob->fields['PREVIEW_PICTURE'], array('width'=>200, 'height'=>163), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);                
				$ob->fields['PREVIEW_PHOTO'] = $file;
			}
			if (!isset($ob->fields["CATALOG_MEASURE_RATIO"])){
				$ob->fields["CATALOG_MEASURE_RATIO"] = 1;
			}			
			$arResult['ITEMS'][] = $ob->fields;
		} 
		$arResult['RESULT'] = $res;
		
		CModule::IncludeModule("iblock");
		$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues(
			$arParams["IBLOCK_ID"], // ID инфоблока
			$arResult["SECTION"]["ID"] // ID раздела
		);
		$arResult["SECTION"]["META"] = $ipropValues->getValues();
		
		//////////// end cache /////////
		if ($cache_time > 0)
		{
			// начинаем буферизирование вывода
			$cache->StartDataCache($cache_time, $cache_id, $cache_path);
			// записываем предварительно буферизированный вывод в файл кеша
			$cache->EndDataCache(array($cache_id=>$arResult));
		}
	}
	
	$arr_recom = array();
	
	foreach($arResult['ITEMS'] as &$item){
		if(isset($item['PROPERTY_RECOMMEND_VALUE']) && count($item['PROPERTY_RECOMMEND_VALUE']) > 0){
			array_push($arr_recom, $item['PROPERTY_RECOMMEND_VALUE']);
		}
		$item["PRICES"] = array();
		$item['MIN_PRICE'] = false;
		$item["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $item, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
		if (!empty($item['PRICES']))
			$item['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($item['PRICES']);

		$item["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $item);
		
	}
	
	$navComponentParameters = array();
	if ($arParams["PAGER_BASE_LINK_ENABLE"] === "Y")
	{
		$pagerBaseLink = trim($arParams["PAGER_BASE_LINK"]);
		if ($pagerBaseLink === "")
			$pagerBaseLink = $arResult['SECTION']["SECTION_PAGE_URL"];

		if ($pagerParameters && isset($pagerParameters["BASE_LINK"]))
		{
			$pagerBaseLink = $pagerParameters["BASE_LINK"];
			unset($pagerParameters["BASE_LINK"]);
		}

		$navComponentParameters["BASE_LINK"] = CHTTP::urlAddParams($pagerBaseLink, $pagerParameters, array("encode"=>true));
	}
		
	$arResult["NAV_STRING"] = $arResult['RESULT']->GetPageNavStringEx(
		$navComponentObject,
		$arParams["PAGER_TITLE"],
		$arParams["PAGER_TEMPLATE"],
		$arParams["PAGER_SHOW_ALWAYS"],
		$this,
		$navComponentParameters
	);

	if (isset($ob))
		unset($ob);
	
	//сохраним id элемента для рекомендаций
	GarbageStorage::set('ids', $arr_recom); #установить значение

	$APPLICATION->SetTitle($arResult['SECTION']["NAME"]);
	$APPLICATION->SetPageProperty("keywords", $arResult["SECTION"]["META"]["ELEMENT_META_KEYWORDS"]);
	$APPLICATION->SetPageProperty("description", $arResult["SECTION"]["META"]["ELEMENT_META_DESCRIPTION"]);

	$this->includeComponentTemplate();
//}