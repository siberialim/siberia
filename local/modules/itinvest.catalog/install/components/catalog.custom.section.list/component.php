<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Currency,
	Bitrix\Catalog,
	Bitrix\Iblock;

$cache = new CPHPCache();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'catalog.custom.section.list.catalog.'.IBLOCK_CATALOG_ID;
$cache_path = 'catalog.custom.section.list.catalog/'.IBLOCK_CATALOG_ID;

if (CModule::includeModule('currency')){
	$currency = Currency\CurrencyTable::getList(array(
		'select' => array('CURRENCY'),
		'filter' => array('=CURRENCY' => $arParams['CURRENCY_ID'])
	))->fetch();
}
else{die();}

$arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];

	//isset cache?
	if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
	{
	   $res = $cache->GetVars();
	   if (is_array($res[$cache_id]) && (count($res[$cache_id]) > 0))
		  $arResult = $res[$cache_id];
	}
	
	//!cache
	if (!is_array($res[$cache_id]))
	{
		
		$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
		/*===============================================================================================================
		ajax уходит в component.php компонента catalog.custom.element
		===================================================================================================================*/
		$ajaxPath = '/catalog/section/element/index.php?';
		/*======================================*/
		$arResult['~BUY_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arResult['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arParams['~BUY_URL_TEMPLATE']);
		$arResult['~ADD_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arResult['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arParams['~ADD_URL_TEMPLATE']);
		$arResult['~FAVOURITE_URL_TEMPLATE'] = $ajaxPath.$arParams["ACTION_VARIABLE"]."=ADD2FAVOURITE&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
		$arResult['FAVOURITE_URL_TEMPLATE'] = htmlspecialcharsbx($arParams['~FAVOURITE_URL_TEMPLATE']);
		
		$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"],'SECTION_ID' =>  SECTION_ID, 'ACTIVE' => 'Y'); 
		$arSort = array('SORT' => 'ASC');
		$arSelect = array('ID', 'NAME', 'CODE', 'SECTION_PAGE_URL', 'UF_BACKGROUND_IMAGE', 'UF_MBACKGROUND_IMAGE', 'UF_ICON', 'UF_COLOR');
		$rsSections = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
		
		while ($arSection = $rsSections->GetNext())
		{
			/*if ($arSection['UF_ICON'][0])
			{
				$file = CFile::ResizeImageGet($arSection['UF_ICON'][0], array('width'=>42, 'height'=>42), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$arSection['UF_ICON'] = $file;
			}*/
			
			if ($arSection['UF_BACKGROUND_IMAGE'])
			{
				$file = CFile::ResizeImageGet($arSection['UF_BACKGROUND_IMAGE'], array('width'=>248, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$arSection['PICTURE_RESIZE'] = $file;
			}
			if ($arSection['UF_MBACKGROUND_IMAGE'])
			{
				$file = CFile::ResizeImageGet($arSection['UF_MBACKGROUND_IMAGE'], array('width'=>110, 'height'=>110), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$arSection['LITTLE_PICTURE_RESIZE'] = $file;
			}
					
			//найдем элементы раздела
			$arFilter = array();
			$arItems = array();

			$arSort = $arParams['AR_SORT'];

			if($_REQUEST['sort'] && $_REQUEST['type']){
				 $arSort[$_REQUEST['sort']] = $_REQUEST['type'];
			}
			$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => $arSection['ID'], 'ACTIVE' => 'Y'); 
			$res = CIBlockElement::GetList($arSort, $arFilter, false,  array("nPageSize" => $arParams['PAGE_ELEMENT_COUNT'], "bShowAll" => false), $arParams['AR_SELECT']);
			
			$currentPath = CHTTP::urlDeleteParams(
				$arParams['CUSTOM_CURRENT_PAGE']?: $APPLICATION->GetCurPageParam(),
				array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
				array('delete_system_params' => true)
			);

			$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');
			if ($arParams['COMPARE_PATH'] == '')
			{
				$comparePath = $currentPath;
			}
			else
			{
				$comparePath = CHTTP::urlDeleteParams(
				$arParams['COMPARE_PATH'],
				array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
				array('delete_system_params' => true)
				);
				$comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');
			}

			while($ob = $res->GetNextElement()) 
			{
				if ($ob->fields['PREVIEW_PICTURE'])
				{
					$file = CFile::ResizeImageGet($ob->fields['PREVIEW_PICTURE'], array('width'=>200, 'height'=>163), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);                
					$ob->fields['PREVIEW_PHOTO'] = $file;
				}
				if (!isset($ob->fields["CATALOG_MEASURE_RATIO"])){
					$ob->fields["CATALOG_MEASURE_RATIO"] = 1;
				}
				$ob->fields['CATALOG_TYPE'] = CCatalogProduct::TYPE_PRODUCT;
				$arItems[] = $ob->fields;
			} 
			$arSection['ITEMS'] = $arItems;
			$arResult['SECTIONS'][] = $arSection;
			$arIBlockListID[] = $arSection['ID'];
			
			
				
		}
	
		//////////// end cache /////////
		if ($cache_time > 0)
		{
			// начинаем буферизирование вывода
			$cache->StartDataCache($cache_time, $cache_id, $cache_path);
			// записываем предварительно буферизированный вывод в файл кеша
			$cache->EndDataCache(array($cache_id=>$arResult));
		}
	}
	
	foreach($arResult['SECTIONS'] as &$section){
		foreach($section['ITEMS'] as &$item){

			$item["PRICES"] = array();
			$item['MIN_PRICE'] = false;
	
			$item["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $item, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
			
			if (!empty($item['PRICES']))
				$item['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($item['PRICES']);

			$item["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $item);		
		}
	}
	
	$this->includeComponentTemplate();
