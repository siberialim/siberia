<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Currency,
	Bitrix\Catalog,
	Bitrix\Iblock;

/*========================================================================
			ajax add2cart/add2favourite/buy
=======================================================================*/
$strError = '';
$successfulAdd = true;

if (isset($_REQUEST[$arParams["ACTION_VARIABLE"]]) && isset($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]))
{
	if(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."BUY"]))
		$action = "BUY";
	elseif(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."ADD2BASKET"]))
		$action = "ADD2BASKET";
	elseif(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."ADD2FAVOURITE"]))
		$action = "ADD2FAVOURITE";
	else
		$action = strtoupper($_REQUEST[$arParams["ACTION_VARIABLE"]]);

	$productID = (int)$_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]];
	if (($action == "ADD2BASKET" || $action == "BUY" || $action == "ADD2FAVOURITE" || $action == "SUBSCRIBE_PRODUCT") && $productID > 0)
	{
		if (Loader::includeModule("sale") && Loader::includeModule("catalog"))
		{
			$addByAjax = isset($_REQUEST['ajax_basket']) && $_REQUEST['ajax_basket'] === 'Y';
			if ($addByAjax)
				CUtil::JSPostUnescape();
			$QUANTITY = 0;
			$product_properties = array();

			if ($action == "SUBSCRIBE_PRODUCT")
				$arParams['ADD_PROPERTIES_TO_BASKET'] = "N";

			$intProductIBlockID = (int)CIBlockElement::GetIBlockByID($productID);
			if (0 < $intProductIBlockID)
			{
				if ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y')
				{
					if ($intProductIBlockID == $arParams["IBLOCK_ID"])
					{
						if (!empty($arParams["PRODUCT_PROPERTIES"]))
						{
							if (
								isset($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
								&& is_array($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
							)
							{
								$product_properties = CIBlockPriceTools::CheckProductProperties(
									$arParams["IBLOCK_ID"],
									$productID,
									$arParams["PRODUCT_PROPERTIES"],
									$_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]],
									$arParams['PARTIAL_PRODUCT_PROPERTIES'] == 'Y'
								);
								if (!is_array($product_properties))
								{
									$strError = GetMessage("CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR");
									$successfulAdd = false;
								}
							}
							else
							{
								$strError = GetMessage("CATALOG_EMPTY_BASKET_PROPERTIES_ERROR");
								$successfulAdd  = false;
							}
						}
					}
					else
					{
						$skuAddProps = (isset($_REQUEST['basket_props']) && !empty($_REQUEST['basket_props']) ? $_REQUEST['basket_props'] : '');
						if (!empty($arParams["OFFERS_CART_PROPERTIES"]) || !empty($skuAddProps))
						{
							$product_properties = CIBlockPriceTools::GetOfferProperties(
								$productID,
								$arParams["IBLOCK_ID"],
								$arParams["OFFERS_CART_PROPERTIES"],
								$skuAddProps
							);
						}
					}
				}
				if ($arParams["USE_PRODUCT_QUANTITY"])
				{
					if (isset($_REQUEST[$arParams["PRODUCT_QUANTITY_VARIABLE"]]))
					{
						$QUANTITY = doubleval($_REQUEST[$arParams["PRODUCT_QUANTITY_VARIABLE"]]);
					}
				}
				if (0 >= $QUANTITY)
				{
					
					$rsRatios = CCatalogMeasureRatio::getList(
						array(),
						array('PRODUCT_ID' => $productID),
						false,
						false,
						array('PRODUCT_ID', 'RATIO')
					);
					if ($arRatio = $rsRatios->Fetch())
					{
						$intRatio = (int)$arRatio['RATIO'];
						$dblRatio = doubleval($arRatio['RATIO']);
						$QUANTITY = ($dblRatio > $intRatio ? $dblRatio : $intRatio);
					}
				}
				if (0 >= $QUANTITY)
					$QUANTITY = 1;
			}
			else
			{
				$strError = GetMessage('CATALOG_ELEMENT_NOT_FOUND');
				$successfulAdd = false;
			}

			if ($successfulAdd)
			{
				$notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
				$arNotify = unserialize($notifyOption);
				$arRewriteFields = array();
				if ($action == "SUBSCRIBE_PRODUCT" && $arNotify[SITE_ID]['use'] == 'Y')
				{
					$arRewriteFields["SUBSCRIBE"] = "Y";
					$arRewriteFields["CAN_BUY"] = "N";
				}
				if($action == "ADD2FAVOURITE"){
					$arRewriteFields["DELAY"] = "Y";
				}
				if(!Add2BasketByProductID($productID, $QUANTITY, $arRewriteFields, $product_properties))
				{
					if ($ex = $APPLICATION->GetException())
						$strError = $ex->GetString();
					else
						$strError = GetMessage("CATALOG_ERROR2BASKET");
					$successfulAdd = false;
				}
			}

			if ($addByAjax)
			{
				if ($successfulAdd)
				{
					$addResult = array('STATUS' => 'OK', 'MESSAGE' => GetMessage('CATALOG_SUCCESSFUL_ADD_TO_BASKET'));
				}
				else
				{
					$addResult = array('STATUS' => 'ERROR', 'MESSAGE' => $strError);
				}
				$APPLICATION->RestartBuffer();
				echo CUtil::PhpToJSObject($addResult);
				die();
			}
			else
			{
				if ($successfulAdd)
				{
					$pathRedirect = (
						$action == "BUY"
						? $arParams["BASKET_URL"]
						: $APPLICATION->GetCurPageParam("", array(
							$arParams["PRODUCT_ID_VARIABLE"],
							$arParams["ACTION_VARIABLE"],
							$arParams['PRODUCT_QUANTITY_VARIABLE'],
							$arParams['PRODUCT_PROPS_VARIABLE']
						))
					);
					LocalRedirect($pathRedirect);
				}
			}
		}
	}
}
if (!$successfulAdd)
{
	ShowError($strError);
	return 0;
}
/*=========================================================================
	Cache
=========================================================================*/
$cache = new CPHPCache();

if(isset($arParams['~ELEMENT_CODE']) && !empty($arParams['~ELEMENT_CODE'])){
	$elem = $arParams['~ELEMENT_CODE'];
}
else if(isset($arParams['~ELEMENT_ID']) && !empty($arParams['~ELEMENT_ID'])){
	$elem = $arParams['~ELEMENT_ID'];
}
else{
	die();
}
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$cache_time = $arParams["CACHE_TIME"];
$cache_id = 'catalog.custom.section.element.'.IBLOCK_CATALOG_ID.'.'.$elem;
$cache_path = 'catalog.custom.section.element/'.IBLOCK_CATALOG_ID.'/'.$elem;

if (CModule::includeModule('currency')){
	$currency = Currency\CurrencyTable::getList(array(
		'select' => array('CURRENCY'),
		'filter' => array('=CURRENCY' => $arParams['CURRENCY_ID'])
	))->fetch();
}
else{die();}

$arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];

//isset cache?
if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
{
   $res = $cache->GetVars();
   if (is_array($res[$cache_id]) && (count($res[$cache_id]) > 0))
	  $arResult = $res[$cache_id];
}
//!cache
if (!is_array($res[$cache_id]))
{	
	
	if($arParams["SECTION_ID"] > 0)
	{
		$arFilter["ID"]=$arParams["SECTION_ID"];
		$arFilter["IBLOCK_ID"]=$arParams["IBLOCK_ID"];
		$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
		$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult['SECTION'] = $rsSection->GetNext();
		if($arResult['SECTION'])
			$bSectionFound = true;
	}
	elseif(strlen($arParams["SECTION_CODE"]) > 0)
	{
		$arFilter["=CODE"]=$arParams["SECTION_CODE"];
		$arFilter["IBLOCK_ID"]=$arParams["IBLOCK_ID"];
		$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
		$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult['SECTION'] = $rsSection->GetNext();
		if($arResult['SECTION'])
			$bSectionFound = true;
	}
	
	$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"]); 
	if(!(int) $arParams['ELEMENT_ID']){
		$arFilter['CODE'] = $arParams['ELEMENT_CODE'];
	}
	else{
		$arFilter['ID'] = $arParams['ELEMENT_ID'];
	};
	$arFilter['ACTIVE'] = 'Y';
	$arSelect = array('ID', 'NAME', 'CODE', 'DETAIL_PICTURE', 'DETAIL_TEXT', 'CATALOG_GROUP_1', 'CATALOG_QUANTITY', 'UF_ICON', 'UF_COLOR', 'PROPERTY_PACK', 'PROPERTY_NUTRIENTS', 'PROPERTY_WEIGHT', 'PROPERTY_COMPOSITION', 'PROPERTY_MORE_PHOTO', 'PROPERTY_SHELF_LIFE', 'PROPERTY_RECOMMEND');
	$rsElement = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
	$currentPath = CHTTP::urlDeleteParams(
		$arParams['CUSTOM_CURRENT_PAGE']?: $APPLICATION->GetCurPageParam(),
		array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
		array('delete_system_params' => true)
	);
	$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');
	if ($arParams['COMPARE_PATH'] == '')
	{
		$comparePath = $currentPath;
	}
	else
	{
		$comparePath = CHTTP::urlDeleteParams(
		$arParams['COMPARE_PATH'],
		array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
		array('delete_system_params' => true)
		);
		$comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');
	}
	
	$arUrlTemplates['~BUY_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~BUY_URL_TEMPLATE']);
	$arUrlTemplates['~ADD_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~ADD_URL_TEMPLATE']);
	$arUrlTemplates['~FAVOURITE_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=ADD2FAVOURITE&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arUrlTemplates['FAVOURITE_URL_TEMPLATE'] = htmlspecialcharsbx($arUrlTemplates['~FAVOURITE_URL_TEMPLATE']);
	$arResult = array_merge($arResult, $arUrlTemplates);
	while ($arElement = $rsElement->GetNext())
	{
		$arElement["PRICES"] = array();
		if ($arElement['DETAIL_PICTURE'])
		{
			$File = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
			$file = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array('width'=>320, 'height'=>420), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$file2 = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array('width'=>1024, 'height'=>768), BX_RESIZE_IMAGE_PROPORTIONAL, true);    			
			$arElement['RESIZE_DETAIL_PICTURE'] = $file;
			$arElement['RESIZE_DETAIL_PICTURE_LARGE'] = $file2;
		}
		unset($file);
		unset($file2);
		unset($File);
		$arElement['RESIZE_MORE_PHOTO_VALUE'] = array();
		if(isset($arElement['PROPERTY_MORE_PHOTO_VALUE']) && count($arElement['PROPERTY_MORE_PHOTO_VALUE']) > 0){
			foreach($arElement['PROPERTY_MORE_PHOTO_VALUE'] as $key=>$photo){
				if ($photo)
				{
					$File = CFile::GetFileArray($arElement["PROPERTY_MORE_PHOTO_VALUE"][$key]);
					$file = CFile::ResizeImageGet($photo, array('width'=>76, 'height'=>76), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$file2 = CFile::ResizeImageGet($photo, array('width'=>1024, 'height'=>768), BX_RESIZE_IMAGE_PROPORTIONAL, true);    					
					$arElement['RESIZE_MORE_PHOTO_VALUE'][] = $file;
					$arElement['RESIZE_MORE_PHOTO_LARGE_VALUE'][] = $file2;
					$arElement['PROPERTY_MORE_PHOTO_VALUE'][$key] = $File;
				}
			}
		}
		if (!isset($arElement["CATALOG_MEASURE_RATIO"])){
			$arElement["CATALOG_MEASURE_RATIO"] = 1;
		}

		$arElement["CATALOG_MEASURE_RATIO"] = 1;
		$arResult['ELEMENT'] = $arElement;	
	}
	
	if($arParams["SECTION_ID"] > 0)
	{
		$arSectionFilter["ID"] = $arParams["SECTION_ID"];
	}
	else
	{
		//$arSectionFilter["HAS_ELEMENT"] = $arParams["ELEMENT_ID"];
		$arSectionFilter["=CODE"] = $arParams["SECTION_CODE"];
	}
	
	CModule::IncludeModule("iblock");
	$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(
		$arParams["IBLOCK_ID"], // ID инфоблока
		$arResult["ELEMENT"]["ID"] // ID элемента
	);
	$arResult["ELEMENT"]["META"] = $ipropValues->getValues();
	
	//////////// end cache /////////
	if ($cache_time > 0)
	{
		// начинаем буферизирование вывода
		$cache->StartDataCache($cache_time, $cache_id, $cache_path);
		// записываем предварительно буферизированный вывод в файл кеша
		$cache->EndDataCache(array($cache_id=>$arResult));
	}
}



		$arResult['ELEMENT']["PRICES"] = array();
		$arResult['ELEMENT']['MIN_PRICE'] = false;
		$arResult['ELEMENT']["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $arResult['ELEMENT'], $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
		if (!empty($arResult['ELEMENT']['PRICES']))
			$arResult['ELEMENT']['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($arResult['ELEMENT']['PRICES']);

		$arResult['ELEMENT']["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $arResult['ELEMENT']);


//сохраним id элемента для рекомендаций
GarbageStorage::set('ids', $arResult['ELEMENT']['PROPERTY_RECOMMEND_VALUE']); #установить значение

$APPLICATION->SetTitle($arResult['ELEMENT']["NAME"]);
$APPLICATION->SetPageProperty("keywords", $arResult["ELEMENT"]["META"]["ELEMENT_META_KEYWORDS"]);
$APPLICATION->SetPageProperty("description", $arResult["ELEMENT"]["META"]["ELEMENT_META_DESCRIPTION"]);
		
$this->includeComponentTemplate();
