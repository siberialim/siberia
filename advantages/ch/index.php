<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "我们的优势");
$APPLICATION->SetTitle("我们的优势");
?><div id="main_pic"><img src="<?=SITE_DIR?>images/top_work.jpg"></div>
<div id="working">
	<div class="row row_line_1">
		<div class="wrap_item col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
 <img src="images/p1.jpg">
			</div>
			<div class="item">
				<ol>
					<p>
						 我们的产品通过了俄罗斯联邦消费者权益及公民平安保护监督局的工艺流程和卫生标准的严格控制，您得到的是绝对的最优质产品。<br>
					</p>
				</ol>
			</div>
		</div>
		<div class="wrap_item col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
 <img src="<?=SITE_DIR?>images/p2.jpg">
			</div>
			<div class="item">
				<ol>
					<p>
						 西伯利亚区域的产品会很快补充食品缺乏。
					</p>
				</ol>
			</div>
		</div>
		<div class="wrap_item end col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
 <img src="<?=SITE_DIR?>images/p3.jpg">
			</div>
			<div class="item">
				<ol>
					<p>
						 我们的产品和物流服务的成本低，因此会给您带来丰厚的经济效益。
					</p>
				</ol>
			</div>
		</div>
	</div>
	<div class="row row_line_2">
		<div class="wrap_item col-sm-6 col-md-6 col-lg-6 col-xs-12">
			<div class="pic">
 <img src="<?=SITE_DIR?>images/p4.jpg">
			</div>
			<div class="item">
				<p>
					 在很短时间内您将会给老百姓提供大量的优质绿色食品。
				</p>
			</div>
		</div><!--
	--><div class="wrap_item end col-sm-6 col-md-6 col-lg-6 col-xs-12">
			<div class="pic">
 <img src="<?=SITE_DIR?>images/p5.jpg">
			</div>
			<div class="item">
				<ol>
					<p align="left">
						 我们完全有能力按照客户提供的配方来生产产品的同时，也会遵守最严格的标准要求，利用最纯净的原料，因此您完全可以通过与我们合作而满足当地市场对当地特产食品的需求，并且提供优质绿色食品。<br>
					</p>
				</ol>
			</div>
		</div>
	</div>
 <br>
	<div class="row">
		<div class="wrap_item end die">
			<div class="item">
				<p style="text-align: center;">
					 欢迎各位贵客建立互利合作关系！
				</p>
			</div>
		</div>
	</div>
</div>
<style>
	p{
		font-size:1.8em;
	}.row_line_1{
		margin: 0 auto;
		width: 74.58%;
	}.row_line_2{
		margin: 0 auto;
		width: 49.16%;
	}.grinch{
		color:#71a53b;
	}#main_pic img{
		width:100%;
	}#title_about{
		text-align:center;
		font-size:2.4em;
	}#siberia{
		display:block;
		margin:0 auto;
	}#wrap_disc_siberia{
		margin:5rem 0;
	}#working{
		margin:5rem 0;
	}#working .wrap_item{
		display:inline-block;
		vertical-align:top;
		position:relative;
		margin-bottom: 2rem;
	}#working .wrap_item.end{
		margin-right: 0;
	}#working .wrap_item .item{
		position:relative;
		padding:0 0 0 3.8rem;
	}#working .wrap_item .item ol{
		padding:0;
	}#working .wrap_item .pic img{
		width:100%;
		margin-bottom: 1.5rem;
	}#working .wrap_item .item:before{
		content:" ";
		background:url(<?=SITE_DIR?>images/ok.png);
		background-repeat:no-repeat;
		background-size:100%;
		width:2.1rem;
		height:1.8rem;
		position:absolute;
		left:0;
		top:0;
	}#working .end.wrap_item.die{
		display: block;
		text-align: center;
		width: 100%;
	}#working .end.wrap_item.die .item:before{
		content:" ";
		background:none;
	}
	@media (max-width: 767px){
		.row_line_2{
			width: 74.58%;
		}
	}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>