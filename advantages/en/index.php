<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "Our advantages");
$APPLICATION->SetTitle("Our advantages");
?><div id="main_pic"><img src="<?=SITE_DIR?>images/top_work.jpg"></div>
<div id="working">
	<div class="row row_line_1">
		<div class="wrap_item col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p1.jpg">
			</div>
			<div class="item">
				<ol>
					<p>
						 You will get the goods of high quality due to Federal Service for Surveillance on Consumer Rights and Human Wellbeing strictly controls the production technology and compliance with health standards
					</p>
				</ol>
			</div>
		</div>
		<div class="wrap_item col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p2.jpg">
			</div>
			<div class="item">
				<ol>
					<p>
						 You will easily make up the food shortages due to the Siberian products
					</p>
				</ol>
			</div>
		</div>
		<div class="wrap_item end col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p3.jpg">
			</div>
			<div class="item">
				<ol>
					<p>
						 You will save money on the low cost of our products and logistics
					</p>
				</ol>
			</div>
		</div>
	</div>
	<div class="row row_line_2" style="margin: 0 auto;">
		<div class="wrap_item col-sm-6 col-md-6 col-lg-6 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p4.jpg">
			</div>
			<div class="item">
				<p>
					 You will provide the nation with the environmentally friendly&nbsp;
				</p>
			</div>
		</div>
		<div class="wrap_item end col-sm-6 col-md-6 col-lg-6 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p5.jpg">
			</div>
			<div class="item">
				<ol>
					<p align="left">
						 You will provide the market demand for traditional national food made of the purest raw materials with the most stringent standards since we are ready to manufacture products on your recipes.
					</p>
				</ol>
			</div>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="wrap_item end die">
			<div class="item">
				<p align="center">
					 We are ready to cooperate!
				</p>
			</div>
		</div>
	</div>
</div>
<style>
	p{
		font-size:1.8em;
	}.row_line_1{
		margin: 0 auto;
		width: 74.58%;
	}.row_line_2{
		margin: 0 auto;
		width: 49.16%;
	}.grinch{
		color:#71a53b;
	}#main_pic img{
		width:100%;
	}#title_about{
		text-align:center;
		font-size:2.4em;
	}#siberia{
		display:block;
		margin:0 auto;
	}#wrap_disc_siberia{
		margin:5rem 0;
	}#working{
		margin:5rem 0;
	}#working .wrap_item{
		display:inline-block;
		vertical-align:top;
		position:relative;
		margin-bottom: 2rem;
	}#working .wrap_item.end{
		margin-right: 0;
	}#working .wrap_item .item{
		position:relative;
		padding:0 0 0 3.8rem;
	}#working .wrap_item .item ol{
		padding:0;
	}#working .wrap_item .pic img{
		width:100%;
		margin-bottom: 1.5rem;
	}#working .wrap_item .item:before{
		content:" ";
		background:url(<?=SITE_DIR?>images/ok.png);
		background-repeat:no-repeat;
		background-size:100%;
		width:2.1rem;
		height:1.8rem;
		position:absolute;
		left:0;
		top:0;
	}#working .end.wrap_item.die{
		display: block;
		text-align: center;
		width: 100%;
	}#working .end.wrap_item.die .item:before{
		content:" ";
		background:none;
	}
	@media (max-width: 767px){
		.row_line_2{
			width: 74.58%;
		}
	}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>