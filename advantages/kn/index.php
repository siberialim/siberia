<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
}
$APPLICATION->SetPageProperty("title", "우리 강점");
$APPLICATION->SetTitle("우리 강점");
?><div id="main_pic"><img src="<?=SITE_DIR?>images/top_work.jpg"></div>
<div id="wrap_disc_siberia">
	<h1 id="title_about" class="grinch">
	<p>
		 왜 'Siberia Products'사와 협력해 보셔야 되는지 5개의 이유 :
	</p>
 </h1>
</div>
<div id="working">
	<div class="row row_line_1">
		<div class="wrap_item col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p1.jpg">
			</div>
			<div class="item">고품질의 제품을 받으실 수 있겠습니다. 러시아에서 Rospotrebnadzor (소비자권리보호감독국)은 국내의 업체들이 생산공정과 위생규칙에 준수하게끔 엄격하게 감시하고 있습니다.</div>
		</div><div class="wrap_item col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p2.jpg">
			</div>
			<div class="item">귀사는 부족한 식품들을 러시아 시베리아의 식품으로 쉽게 보충할 수 있겠습니다.</div>
		</div>
		<div class="wrap_item end col-sm-4 col-md-4 col-lg-4 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p3.jpg">
			</div>
			<div class="item">귀사는 우리가 제공하는 식품의 저렴한 가격과 편리한 운송방법 덕분에 돈을 아끼게 되시겠습니다.</div>
		</div>
	</div>
	<div class="row row_line_2">
		<div class="wrap_item col-sm-6 col-md-6 col-lg-6 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p4.jpg">
			</div>
			<div class="item">귀사는 자기 국민에게 친환경적이고 품질이 높은 식품을 대량으로 단기간 이내 공급하실 수 있겠습니다.</div>
		</div>
		<div class="wrap_item end col-sm-6 col-md-6 col-lg-6 col-xs-12">
			<div class="pic">
				<img src="<?=SITE_DIR?>images/p5.jpg">
			</div>
			<div class="item">우리는 당신의 요리법에 따라 좋은 재료만 사용하고 품질과 위생의 엄격한 기준을 준수하여 식품도 생산할 준비가 되어 있기 때문에 귀사는 자기 시장에서 부족한 전통 식품까지 공급하실 수 있겠습니다.</div>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="wrap_item end die">
			<div class="item">
				<p align="center">
					상호간에 유익한 협력을 할 준비가 되어 있습니다!
				</p>
			</div>
		</div>
	</div>
</div>
<style>
	p{
		font-size:1.8em;
	}.row_line_1{
		margin: 0 auto;
		width: 74.58%;
	}.row_line_2{
		margin: 0 auto;
		width: 49.16%;
	}.grinch{
		color:#71a53b;
	}#main_pic img{
		width:100%;
	}#title_about{
		text-align:center;
		font-size:2.4em;
	}#siberia{
		display:block;
		margin:0 auto;
	}#wrap_disc_siberia{
		margin:5rem 0;
	}#working{
		margin:5rem 0;
	}#working .wrap_item{
		display:inline-block;
		vertical-align:top;
		position:relative;
		margin-bottom: 2rem;
	}#working .wrap_item.end{
		margin-right: 0;
	}#working .wrap_item .item{
		position:relative;
		padding:0 0 0 3.8rem;
	}#working .wrap_item .item ol{
		padding:0;
	}#working .wrap_item .pic img{
		width:100%;
		margin-bottom: 1.5rem;
	}#working .wrap_item .item:before{
		content:" ";
		background:url(<?=SITE_DIR?>images/ok.png);
		background-repeat:no-repeat;
		background-size:100%;
		width:2.1rem;
		height:1.8rem;
		position:absolute;
		left:0;
		top:0;
	}#working .end.wrap_item.die{
		display: block;
		text-align: center;
		width: 100%;
	}#working .end.wrap_item.die .item:before{
		content:" ";
		background:none;
	}
	@media (max-width: 767px){
		.row_line_2{
			width: 74.58%;
		}
	}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>