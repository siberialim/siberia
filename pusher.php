<?

define('BX_SECURITY_SHOW_MESSAGE', 1);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_FILE_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


if(!$USER->isAdmin())
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

CUtil::InitJSCore(Array('ajax', 'window', 'popup', 'fx'));
$APPLICATION->ShowHead();
?>
<script type="text/javascript">
	var clear = true;
	var users = [];
	function log(text)
	{
		if (clear == true)
			BX('text').innerHTML = "";
		BX('text').innerHTML += text + "</br>";
		clear = false;
	}

	function Recalc(e)
	{
		var target = e.target;
		var id = target.id;
		if (target.checked)
			users[users.length] = id;
		else
		{
			var tempUsers = [];
			for (i = 0; i < users.length; i++)
			{
				if (users[i] == id)
				{
					users.splice(i, 1);
					break;
				}

			}
		}

		console.log(users);
	}

	function ToggleDeviceList(e)
	{

		var target = e.target;
		var listid = target.getAttribute("data-list");
		if (listid)
		{
			list = document.getElementById(listid);
			if (list.style.display == "none")
			{
				list.style.display = "block";
				target.innerHTML = "Hide devices";
			}
			else
			{
				target.innerHTML = "Show devices";
				list.style.display = "none";
			}


		}
		console.log(listid);
	}

	function deleteDevice(id)
	{
		BX.showWait();
		log("Deleting message: " + id);
		BX.ajax.post(
			document.location.href,
			{
				id: id,
				delete: "Y"
			},
			function (result)
			{
				if (result == "ok")
				{
					BX("device_" + id + "_token").style.display = "none";
					BX("device_" + id).style.display = "none";

				}
				log("Result: " + result + "</br>-----------------------");
				BX.closeWait();
			});
	}

	function Send()
	{
		BX.showWait();
		log("Sending message: <br>Text: " + BX("message").value + "<br>Title: " + BX("title").value);
		var params = "";
		try
		{
			params = JSON.parse(BX("params").value);
		}
		catch (e)
		{
			params = BX("params").value;
		}
		BX.ajax.post(
			document.location.href,
			{

				text: BX("message").value,
				title: BX("title").value,
				app_id: BX("APP_ID").value,
				params: params,
				to: users,
				push: "Y"
			},
			function (data)
			{
				log("Result: " + data + "</br>-----------------------");
				BX.closeWait();
			});

	}

	function Clear()
	{
		clear = true;
		BX('text').innerHTML = "Send your message and you will get response here...";
	}
</script>
<style type="text/css">
	#log {
		padding: 10px;
		border: 1px dotted #f0f0f0;
		position: fixed;
		background: #F0F0F0;
		right: 0px;
		overflow: hidden;
		width: 45%;
		font-size: 20px;
		height: 90%;
		top:0px
	}

	#clear {
		margin-top: -10px;
		margin-left: -10px;
		color: #999;
		border: 1px dotted #aaa;
		border-top: none;
		border-left: none;
		cursor: pointer;
	}

	#clear:hover {
		margin-top: -10px;
		margin-left: -10px;
		color: #999;
		border-top: none;
		border-left: none;
		cursor: pointer;
		color: #ffffff;
		outline: none;
	}

	#data {
		padding: 10px;
		top: 150px;
		position: absolute;
		border: 1px solid #f0f0f0;
		border-radius: 3px;
		float: left;
		width: 48%;
		overflow: hidden;
	}

	body {
		padding: 20px;
		background: #FFF;
		font-size: 20px;
		font-family: Consolas, Arial;
	}

	.table_device {
		border: 1px solid #f0f0f0;
		border-collapse: collapse;
		-webkit-border-radius: 2px;
	}

	.td_device {
		border: 1px solid #f0f0f0;
		padding: 5px;
		font-size: 12px;
	}

	.td_delete {
		padding: 0px;
	}

	.send_button {

		width: 100px;
		height: 55px;
		background: none;
		border: 1px;
		cursor: pointer;
		font-size: 20px;
		padding-top:30px;
		text-align: center;
		border-radius: 5px;
		/*border:1px dotted #f0f0f0;*/
	}

	.push_button
	{

		background: #E4FFE4;
		color:#000000;
	}

	.push_button:hover {

		background:#1E9100;
		color: #FFFFFF;
	}




	.name {

		padding: 5px 5px 5px 5px;
		font-size: 14px;
		display: inline-block;

	}

	.input_mess {

		resize: none;
		padding: 10px;
		width:100%;
		font-size: 15px;
		width: 100%;
		background: #E4FFE4;
		outline: none;
		border: 1px solid #f0f0f0;


	}

	.app_select
	{
		outline: none;
		overflow: hidden;
		padding: 5px;
		font-size: 16px;
		line-height: 1;
		border: 0;
		border-radius: 0;
		height: 34px;
		text-align: center;
		-webkit-appearance: none;
		cursor: pointer;
		background: #f0f0f0;
	}


	.table_text {
		font-size: 11px;
		text-overflow: ellipsis;
		color: #999;
		max-width: 600px;
		word-wrap: break-word;
	}

	.device_delete {
		background: indianred;
		width: 70px;
		height: 50px;
		cursor: pointer;
		text-align: center;
		font-weight: bold;
		font-size: 16px;
	}

	.device_delete:hover {
		background: #FF7070;
		width: 70px;
		height: 50px;
		cursor: pointer;
		text-align: center;
		font-weight: bold;
		font-size: 16px;
		color: #FFFFFF;
	}
	.toggle_button
	{
		border:1px solid #f0f0f0;
		padding:10px;
		cursor:pointer;
		background:#FFFFFF;
	}
</style>
<?
CModule::IncludeModule("pull");

if ($_REQUEST["push"] == "Y")
{
	$APPLICATION->RestartBuffer();
	$a = new CPushManager();
	$text = $_REQUEST["text"];
	$title = $_REQUEST["title"];
	$app_id = $_REQUEST["app_id"];
	if (toUpper(SITE_CHARSET) != "UTF-8")
	{
		$text = $APPLICATION->ConvertCharset($text, "UTF-8", SITE_CHARSET);
		$title = $APPLICATION->ConvertCharset($title, "UTF-8", SITE_CHARSET);
	}

	for ($i = 0; $i < count($_REQUEST["to"]); $i++)
	{
		$arMessages[] = Array(
			"USER_ID" => intval($_REQUEST["to"][$i]),
			"TITLE" => $title,
			"APP_ID"=>$app_id,
			"MESSAGE" => $text,
			"EXPIRY"=> 0,
//			"SOUND" => "call.aif",
			"BADGE" => 0,
			"PARAMS" => $_REQUEST['params']
		);
	}




	if(CModule::IncludeModule("pull"))
	{
			 $arMessages[] = Array(
			 	"USER_ID" => 1,
			 	"TITLE" => "Siberia",
			 	"APP_ID" => "BitrixMobile",
			 	"MESSAGE" => "��� push-�����������",
			 	"BADGE" => 1,
			 	"PARAMS" => Array("somekey" => "somevalue")
			);

		$manager = new CPushManager();
		$manager->SendMessage($arMessages);
	}







	echo "ok";
	die();
}



//adds test users
//if($_REQUEST["generate"]=="Y")
//{
//	for ($i = 1; $i <= 1; $i++)
//	{
//		CPullPush::Add(
//			Array(
//				"USER_ID" => 1,
//				"DEVICE_TYPE" => "APPLE",
//				"DEVICE_NAME" => "DEVICE_" . $i,
////				"DEVICE_TOKEN" => "DA0B4721D79AD5D5649A625C48C062DD31EF418BBE4AB39BB151AA7059D81A6A",
//				"DEVICE_ID" => md5($i . rand(1, 100)),
//				"APP_ID" => "idc",
//				"BADGE"=>""
//			)
//		);
//	}
//	echo "OK";
//	die();
//}


if ($_REQUEST["delete"] == "Y")
{
	$APPLICATION->RestartBuffer();
	$ID = $_REQUEST["id"];

	if (CPullPush::Delete($ID))
		echo "ok";
	else
		echo "fail";
	die();
}

CModule::IncludeModule("pull");

$apps = Array(
	"Bitrix24",
	"BitrixMobile",
	"BitrixAdmin",
//	"idc"
);
$dbtokens = CPullPush::GetList();
$arTokens = Array();

while ($token = $dbtokens->Fetch())
{
	if(!in_array($token["APP_ID"], $apps))
		$apps[] = $token["APP_ID"];
	if (!$arTokens["USER_" . $token["USER_ID"]])
		$arTokens["USER_" . $token["USER_ID"]] = Array(
			"USER_ID" => $token["USER_ID"],
			"DEVICES" => Array()
		);
	$arTokens["USER_" . $token["USER_ID"]]["DEVICES"][] = $token;
}

?>
<div style="width:50%;background:#FFF;position:fixed;top:0px;z-index: 1000; height: 150px">
	<table style="width:100%;border-collapse: collapse" border="0" style="width:100%">

		<tr>
			<td width="85%"><input class="input_mess" id="message" type="text" value="Hello"
			              placeholder="The message"></td>
			</td>
			<td  rowspan="2">
				<div class="send_button push_button" onclick="Send();">Send</div>
		</tr>
		<tr>
			<td><input class="input_mess" id="title" type="text" value="Title"
			           placeholder="The title"></td>
			<td>
		</tr>
		<tr>
			<td >
				<input class="input_mess" type="text" id="params" placeholder='IM_MESS_' value='IM_MESS_'>
			</td>
			<td >
				<select class="app_select" name="APP_ID" id="APP_ID">
					<?for ($i=0; $i < count($apps); $i++) { ?>
						<option class="app_select" value="<?=$apps[$i];?>"><?=$apps[$i];?></option>
					<?}?>
				</select>
			</td>
		</tr>
	</table>


</div>
<div id="data">

	<?
	foreach($arTokens as $k=>$v):
	$rsUser = CUser::GetByID($v["USER_ID"]);
	$arUser = $rsUser->Fetch();
	?>
	<div>
		<span><button class="toggle_button" onclick="ToggleDeviceList(event)" data-list="tb_device_<?= $v["USER_ID"]; ?>" type="button">show
				devices
			</button></span>
		<span><input onchange="Recalc(event);" id="<?= $v["USER_ID"]; ?>" type="checkbox"></span>
		<span class="name"><label
				for="<?= $v["USER_ID"]; ?>"><?=$arUser["NAME"] . " " . $arUser["LAST_NAME"]."(User ". $v["USER_ID"].")"?></label></span>

		<table class="table_device" id="tb_device_<?= $v["USER_ID"]; ?>" style="display: none">

			<?for ($i = 0; $i < count($v["DEVICES"]); $i++)
			{
				$dv = $v["DEVICES"][$i];
				?>

				<tr id="device_<?= $dv["ID"]; ?>">
					<td class="td_device"><?=$dv["DEVICE_TYPE"];?>/<?=$dv["DEVICE_NAME"];?><br><?=$dv["DEVICE_ID"];?></td>
					<td class="td_device"><?=$dv["APP_ID"];?></td>

					<td rowspan="2" class="td_device device_delete" onclick="deleteDevice(<?= $dv["ID"] ?>)">
						delete
					</td>
				</tr>
				<tr id="device_<?= $dv["ID"]; ?>_token">
					<td colspan="2" class="td_device table_text">

							<?=$dv["DEVICE_TOKEN"];?>
					</td>
				</tr>


			<? }?>
		</table>

		<?endforeach;?>
	</div>
</div>
<div id="log">
	<button id="clear" onclick="Clear();">Clear Log</button>
	<div id="text" style="overflow:auto;height: 100%;font-size: 15px; padding: 10px;word-wrap:break-word;">
		Send your message and you will get response here...
	</div>

</div>





